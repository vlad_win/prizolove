<?php
defined('_JEXEC') or die;
?>
<?php
	$class = '';
	if (is_numeric($question->answer)) $class = ' class="numeric" ';
?>
<?php //print_r($campaign); ?>
<?php //print_r($question); ?>
<?php echo $question->text_before; ?>
<?php if ($question->type == 'letter') : ?>
<?php for($i=0; $i < mb_strlen($question->answer); $i++) : ?>
<div class="form">
   <input class="letter<?=$i;?>" name="questions[<?=$campaign->id;?>][<?=$question->id;?>][<?=$i;?>]" type="text" value="" maxlength="1">
 </div>
<?php endfor; ?>
<?php elseif ($question->type == 'pattern') : ?>
<?php $chrArray = preg_split('//u', $question->pattern, -1, PREG_SPLIT_NO_EMPTY); ?>
<div class="input1">
<?php 
	foreach($chrArray as $i=>$letter) {
		if ($letter != '_' && $letter != ' ') {
			echo '<div class="bukvi">'.$letter.'</div>';
		} else {
			if ($letter == ' ') {
				echo '&nbsp;';
			} else {
				echo '<div class="form"><input class="letter'.$i.'" type="text" name="questions['.$campaign->id.']['.$question->id.']['.$i.']" maxlength="1"></div>';
			}
		}
	}
?>
</div>

<?php else: ?>
<input name="questions[<?=$campaign->id;?>][<?=$question->id;?>]" data-question="<?=$question->id;?>" type="text" value="" maxlength="<?=mb_strlen($question->answer);?>"<?=$class;?>>
<?php endif; ?>
<?php echo $question->text_after; ?>
