<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('ModQuestionsHelper', __DIR__ . '/helper.php');


$campaign = ModQuestionshelper::getCampaign($params);
$question = ModQuestionshelper::getQuestion($params);

$layout = 'default';

ModQuestionshelper::setSession($campaign, $question);


if ($question->type == 'complex_steps') $layout = 'steps';

$doc = JFactory::getDocument();
$doc->addStylesheet( JURI::base(true) . '/modules/mod_questions/assets/css/style.css' );

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_questions', $layout);
