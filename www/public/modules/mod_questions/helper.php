<?php


defined('_JEXEC') or die;



class ModQuestionsHelper
{
	/**
	 * Retrieve list of archived articles
	 *
	 * @param   \Joomla\Registry\Registry  &$params  module parameters
	 *
	 * @return  array
	 *
	 * @since   1.5
	 */
	public static function getCampaign(&$params)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('c.*','ce.event_id'))
			->from($db->quoteName('#__pl_campaigns','c'))
            ->join('LEFT', $db->quoteName('#__pl_campaigns_events', 'ce') . ' ON (' . $db->quoteName('ce.puzzle_id') . ' = ' . $db->quoteName('c.id') . ')')
			->where($db->quoteName('c.id').' = '.$params->get('campaign'))
			->where('c.state = 1');
			
		$db->setQuery($query);	
		
		$campaign = $db->loadObject();

		return $campaign;
		
	}
	
	public static function getQuestion(&$params)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__pl_questions')
			->where('id = '.$params->get('question'))
			->where('state = 1');
			
		$db->setQuery($query);	
		
		$question = $db->loadObject();
		//$question->settings = json_decode($question->settings);

		return $question;		
	}
	
	public static function setSession($campaign, $question)
	{
		$session = JFactory::getSession();
		$questions = json_decode($session->get('questions', json_encode(array())), true);
				
		if ($question->pattern) 
			$questions[$campaign->id][$question->id]['pattern'] = $question->pattern;
		
		$questions[$campaign->id][$question->id]['type'] = $question->type;
		$questions[$campaign->id][$question->id]['event_id'] = $campaign->event_id;
		$questions[$campaign->id][$question->id]['answer'] = $question->answer;
		$questions[$campaign->id][$question->id]['rightLink'] = $campaign->right_link;		
		$questions[$campaign->id][$question->id]['wrongLink'] = $campaign->wrong_link;		
		$questions[$campaign->id][$question->id]['settings'] = json_decode($question->settings);


		$session->set('questions', json_encode($questions));			
		
	}
}
