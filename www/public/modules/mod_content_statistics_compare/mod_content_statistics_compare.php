<?php

/*------------------------------------------------------------------------
# mod_content_statistics_compare - Content Statistics Comparison Module
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2015 joomlathat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlathat.com
# Technical Support:	Forum - http://www.joomlathat.com/support
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$thelayout = $params->get('layout', 'default');

if($thelayout != "ajax"){

	// Include the syndicate functions only once
	require_once (dirname(__FILE__).DS.'helper.php');

	// Get the items
	$result	= modContentStatisticsCompareHelper::getItems($params);

	$items	= $result['items'] ;
	$items2	= $result['items2'] ;
	$items3 = $result['items3'] ;
	$items4	= $result['items4'] ;
	$items5	= $result['items5'] ;
	$items6	= $result['items6'] ;
	$items7	= $result['items7'] ;
	$items8	= $result['items8'] ;
}

$layout_path = JPATH_BASE.DS.'plugins'.DS.'contentstats'.DS.$params->get('component').DS.'mod_content_statistics_compare'.DS.$params->get('layout', 'new').'php';
if (!file_exists($layout_path)) $layout_path = JModuleHelper::getLayoutPath('mod_content_statistics_compare', $params->get('layout', 'new')) ;

require($layout_path);