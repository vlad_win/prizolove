<?php 

/*------------------------------------------------------------------------
# mod_content_statistics_individual - Content Statistics Evolution Module
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2011 JoomlaContentStatistics.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlacontentstatistics.com
# Technical Support:	Forum - http://www.joomlacontentstatistics.com/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access'); 


$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_content_statistics_compare/tmpl/statistics.css');

$document->addScript('https://www.google.com/jsapi');

$rand = rand();

$js = "";

$backgroundcolor = $params->get('backgroundcolor') ;
if(substr($backgroundcolor,0,1) != "#") $backgroundcolor = "#".$backgroundcolor ;

$chartareacolor = $params->get('chartareacolor') ;
if(substr($chartareacolor,0,1) != "#") $chartareacolor = "#".$chartareacolor ;

if($backgroundcolor == "#000000") $backgroundcolor = "";
if($chartareacolor == "#000000") $chartareacolor = "";

if($backgroundcolor){
	$backcolors[] = 'bg,s,'.$backgroundcolor ; //chart background color
}
if($chartareacolor){
	$backcolors[] = 'c,s,'.$chartareacolor ; //chart area color
}

$backgroundcolor = "'".$backgroundcolor."'";
$chartareacolor = "'".$chartareacolor."'";

$days = $params->get( 'num_units_time', 7 ); 
$current = $days ;
$max = 0; 

$num_labels = $params->get( 'num_labels', 1 ); 
$i = 0 ;

$date_format = $params->get('other_label_format') ? $params->get('other_label_format') : $params->get('label_format', '%a') ;

if($params->get('markers')){
	 $markersize = $params->get('markersize')  ;
}
else{
	$markersize =  0 ;
}

if(!$params->get('backgroundcolor') || $params->get('backgroundcolor') == "#000000"){
	$backgroundcolor =  "{fill:'transparent'}" ;
}
else{
	$backgroundcolor = "'".$backgroundcolor."'";
}

$comment_colors = "";
$curveType = "";
$steps = "";
$stacked = "";

switch($params->get('chart','lc')){
	case 'bvg':
		$chartype = "ColumnChart" ;
	break;
	case 'area':
		$chartype = "AreaChart" ;
	break;
	case 'smooth':
		$chartype = "LineChart" ;
		$curveType = "curveType: 'function'," ;
	break;
	case 'stepped':
		$chartype = "SteppedAreaChart" ;
	break;
	case 'steppedunconnected':
		$chartype = "SteppedAreaChart" ;
		$steps = "connectSteps: false," ;
	break;
	case 'lc':default:
		$chartype = "LineChart" ;
	break;
}

if($params->get('stacked')){
	$stacked = "isStacked: true," ;
}

$colors = array();

$linecolor = $params->get('linecolor') ;
if(substr($linecolor,0,1) != "#") $linecolor = "#".$linecolor ;
$linecolor2 = $params->get('linecolor2') ;
if(substr($linecolor2,0,1) != "#") $linecolor2 = "#".$linecolor2 ;
$linecolor3 = $params->get('linecolor3') ;
if(substr($linecolor3,0,1) != "#") $linecolor3 = "#".$linecolor3 ;
$linecolor4 = $params->get('linecolor4') ;
if(substr($linecolor4,0,1) != "#") $linecolor4 = "#".$linecolor4 ;
$linecolor5 = $params->get('linecolor5') ;
if(substr($linecolor5,0,1) != "#") $linecolor5 = "#".$linecolor5 ;
$linecolor6 = $params->get('linecolor6') ;
if(substr($linecolor6,0,1) != "#") $linecolor6 = "#".$linecolor6 ;
$linecolor7 = $params->get('linecolor7') ;
if(substr($linecolor7,0,1) != "#") $linecolor7 = "#".$linecolor7 ;
$linecolor8 = $params->get('linecolor8') ;
if(substr($linecolor8,0,1) != "#") $linecolor8 = "#".$linecolor8 ;

if($params->get('linecolor')) $colors[] =	"'".$linecolor."'" ;
if($params->get('linecolor2')) $colors[] =	"'".$linecolor2."'" ;
if($params->get('linecolor3')) $colors[] =	"'".$linecolor3."'" ;
if($params->get('linecolor4')) $colors[] =	"'".$linecolor4."'" ;
if($params->get('linecolor5')) $colors[] =	"'".$linecolor5."'" ;
if($params->get('linecolor6')) $colors[] =	"'".$linecolor6."'" ;
if($params->get('linecolor7')) $colors[] =	"'".$linecolor7."'" ;
if($params->get('linecolor8')) $colors[] =	"'".$linecolor8."'" ;

if(!$params->get('linecolor')) $comment_colors = "//" ;

$colorstring = implode(",", $colors) ;

$responsive = $params->get('responsive') ;

if($responsive) $thewidth = "width".$rand ;
else $thewidth = $params->get('width') ;

$labelcolor = $params->get('labelcolor','#999999') ;
if(substr($labelcolor,0,1) != "#") $labelcolor = "#".$labelcolor ;

if($params->get('hAxis', 1)){
	
	$hAxis= "{showTextEvery: ".$params->get('num_labels').",
				textStyle: {color: '".$labelcolor."'},
				title: null,
			}";

}
else{

	$hAxis = "{ textPosition: 'none', gridlines: {count: 0} }";

}

if($params->get('vAxis', 1)){

	$vAxis= "{textStyle: {color: '".$labelcolor."'},
				//format: '".$params->get('symbol_before')."#,###.##".$params->get('symbol_after')."'
			}";

		}
else{

	$vAxis = "{ textPosition: 'none', gridlines: {count: 0}, baseline: 'none' }";

}

$js .= "google.load('visualization', '1', {packages:['corechart']});
      google.setOnLoadCallback(drawChart".$rand.");

      var data".$rand." = null;
	  var options".$rand." = null;

";

$js .= "function drawChart".$rand."(update_data) {

      	var width".$rand." = document.getElementById('chart_div".$rand."').offsetWidth;
		var height".$rand." = document.getElementById('chart_div".$rand."').offsetHeight;

		options".$rand." = {
			width: ".$thewidth.", 
			height: ".$params->get('height').",
			".$comment_colors."colors: [".$colorstring."],
			lineWidth: ".$params->get( 'linewidth', '2').",
			".$curveType."
			".$stacked."
			".$steps."
			chartArea: {left: ".$params->get('space_left',30).", top: ".$params->get('space_top',10).", width: (width".$rand." - ".$params->get('space_right',50)."), height: ".($params->get('height')-$params->get('space_bottom',40))."},
			legend: '".$params->get('legendposition', 'none')."',
			fontSize: 11,
			hAxis: ".$hAxis.",
			vAxis: ".$vAxis.",
			pointSize: ".$markersize.",
			backgroundColor: ".$backgroundcolor.",
			animation:{
					    duration: 500,
					    easing: 'out',
					    startup: true
					  }
			};
      	
      	  // Create our data table out of JSON data loaded from server.
		  chart".$rand." = new google.visualization.".$chartype."(document.getElementById('chart_div".$rand."'));

		  if(update_data) updateChart".$rand."();
		  else chart".$rand.".draw(data".$rand.", options".$rand.");
}

function updateChart".$rand."() {
	jsonData".$rand." = jQuery.ajax({
          url: 'index.php?task=contentstats_update_module&module_id=".$module->id."' + cs_module_append_url,
          dataType: 'json',
          async: true,
          success: function(result)
			{
				data".$rand." = new google.visualization.DataTable(result);
				chart".$rand.".draw(data".$rand.", options".$rand.");
			}
          });

}" ;

$js .= "contentstats_modules_to_update.push('".$rand."');";

if($responsive) $js .= "jQuery( window ).resize(function() { drawChart".$rand."(false); })";

$document->addScriptDeclaration($js);

echo $params->get('introtext');

if($params->get('debug')) echo "<pre>".$js."</pre>" ;
echo "<div id='chart_div".$rand."'></div>" ;
echo $params->get('introtext2');		  
		  
?>