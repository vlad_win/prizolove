<?php

/*------------------------------------------------------------------------
# mod_content_statistics_compare - Content Statistics Comparison Module
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlathat.com
# Technical Support:	Forum - http://www.joomlathat.com/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modContentStatisticsCompareHelper
{

	static function getItems(&$params, $json = false){
		
		$mainframe = JFactory::getApplication();
		$_db	= JFactory::getDBO();
		$user	= JFactory::getUser();

		$numdatasets = 8;

		$dispatcher   = JDispatcher::getInstance();

		$time = $params->get( 'time' ) ;
		$days = $params->get( 'num_units_time', 7 ); 

		$view = JRequest::getVar('view');
		$id = JRequest::getInt('id');
		
		$query = '' ;
		
		$current = $days - 1 ;

		$first_month = "";
		$first_year = "";
		
		//$where clause initialization (time)
		switch($time){
			case 'DAYS':

			$where = ' AND st.date_event BETWEEN SUBDATE(CURDATE(), '.($days).') AND NOW() ' ;

			break;

			case 'MONTHS':

			$current = $days - 1 ;
			
			$year = date('Y') ;
			$month = date('m') ;
			$month = ($month - $current) % 12 ;
			if($month <=0) $month += 12;

			$years = ceil(($days) / 12) ;
			$year -= $years ;

			$year = date('Y') - ceil($days / 12) + 1;
			$year = date ( 'Y' , strtotime ( '-'.($days - 1).' month' , strtotime ( date ('Y-m-15') ) ) );

			$first_month = $month ;
			$first_year = $year ;

			$where = ' AND st.date_event >= "'.$year.'-'.$month.'-01 00:00:00" AND NOW() ' ;

			break;
			case 'YEARS':

			$year = date('Y') - $days + 1;
			$first_year = $year ;

			$where = ' AND st.date_event >= "'.$year.'-01-01 00:00:00" AND NOW() ' ;

			break;
		}

		//multiple component var initialization
		for ($i=1; $i <= $numdatasets; $i++) { 
			$num = $i;
			if($i == 1) $num = "";

			$component_var = "component".$num;
			$criteria_var = "criteria".$num;
			$selector_var = "selector".$num;
			$specific_id_var = "specific_id".$num;
			$viewer_var = "viewer".$num;
			$specific_user_var = "specific_user".$num;
			$unique_var = "unique".$num;

			$more_where_var = "more_where".$num;
			$where_var = "where".$num;

			//example if i = 4: $component4 = $params->get( 'component4' );
			$$component_var = $params->get( 'component'.$num );
			$$criteria_var = $params->get( 'criteria'.$num );
			$$selector_var = $params->get( 'selector'.$num );
			$$specific_id_var = $params->get( 'specific_id'.$num );
			$$viewer_var = $params->get( 'filter_user'.$num  );
			$$specific_user_var = $params->get( 'filter_specific_user'.$num  );
			$$unique_var = $params->get( 'unique'.$num  );

			$$more_where_var = "";

			if($$viewer_var){
				if($user->id) $$more_where_var .= ' AND st.user_id = ' . $user->id ;
				else $$more_where_var .= ' AND 0 ' ; // return NOTHING
			}
			elseif($$specific_user_var){
				$$more_where_var .= ' AND st.user_id = ' . $$specific_user_var ;
			}

			//load the plugins
			if($$component_var) $plugin_ok = JPluginHelper::importPlugin('contentstats', $$component_var);

			if($i == 1) {
				//the case for i=1 we do it below.. to not override the $where var...
			}
			else {
				$$where_var = ' AND st.component = "'.$$component_var.'" ' . $where . $$more_where_var ;
				if($$unique_var == 2){ $$where_var .= " AND st.user_id != 0 "; }
			}

		}
		
		$where = ' AND st.component = "'.$component.'" ' . $where . $more_where ;

		if($unique == 2){ $where .= " AND st.user_id != 0 "; }

		//here is the fun. we built the queries for each data set
		for ($k=1; $k <= $numdatasets; $k++) { 
			$num = $k;
			if($k == 1) $num = "";

			$component_var = "component".$num;
			$criteria_var = "criteria".$num;
			$selector_var = "selector".$num;
			$specific_id_var = "specific_id".$num;
			$viewer_var = "viewer".$num;
			$specific_user_var = "specific_user".$num;
			$unique_var = "unique".$num;

			$where_var = "where".$num;

			$items_var = "items".$num;
			$results_var = "results".$num;

			if($$component_var){
				//K-th component
				$params->set('extra_filters', $params->get('extra_filters'.$num));
				$$results_var = $dispatcher->trigger('getQueryEvolution_'.$$component_var, array($$criteria_var, $$selector_var, $$specific_id_var, $$where_var, $params));
				
				foreach($$results_var as $result){
					if($result->component == $$component_var){
						$query = $result->query ;
					}
				}
				
				$month = $first_month ;
				$year = $first_year ;
				
				$temp2 = array();
				$$items_var = array();
				
				switch($$unique_var){
					case 1: // IP
					$query = str_replace("COUNT(st.id)", "COUNT(DISTINCT st.reference_id, st.ip)", $query);
					break;
					case 2: // user
					$query = str_replace("COUNT(st.id)", "COUNT(DISTINCT st.reference_id, st.user_id)", $query);
					break;
				}
				
				switch($time){
					case 'DAYS':

					$query = $query . ' GROUP BY DAY(date_event), MONTH(date_event), YEAR(date_event) ORDER BY date_event ' ;
					$_db->setQuery( $query );
					$temp_result = $_db->loadObjectList();

					foreach($temp_result as $res){
						$temp2[substr($res->date_event, 0, 10)] = $res ;
					}

					for($i = $days - 1; $i >= 0 ; $i--){
						$time1 = strtotime('-'.$i.' DAY');
						$date_day = date('Y-m-d', $time1) ;
						
						if(array_key_exists($date_day, $temp2)){
							array_push($$items_var, $temp2[$date_day]);
						}
						else{
							$tempobj = new stdClass();
							$tempobj->howmuch = 0;
							$tempobj->date_event = $date_day;

							array_push($$items_var, $tempobj);
						}

					}
					
					break;
					case 'MONTHS':

					$query = $query . ' GROUP BY MONTH(date_event), YEAR(date_event) ORDER BY date_event ' ;
					$_db->setQuery( $query );
					$temp_result = $_db->loadObjectList();

					foreach($temp_result as $res){
						$temp2[substr($res->date_event, 0, 7).'-01'] = $res ;
					}

					for($i = $days - 1; $i >= 0 ; $i--){
						if($month < 10){
							$disp_month = '0' . (int)$month ;
						}
						else $disp_month = $month ;

						$date_day = $year . '-' . $disp_month . '-01' ;

						if(array_key_exists($date_day, $temp2)){
							array_push($$items_var, $temp2[$date_day]);
						}
						else{
							$tempobj = new stdClass();
							$tempobj->howmuch = 0;
							$tempobj->date_event = $date_day;

							array_push($$items_var, $tempobj);
						}

						$month++;
						if($month == 13){
							$month = 1 ;
							$year++;
						}
					}
					
					break;
					case 'YEARS':

					$query = $query . ' GROUP BY YEAR(date_event) ORDER BY date_event ' ;
					$_db->setQuery( $query );
					$temp_result = $_db->loadObjectList();

					foreach($temp_result as $res){
						$temp2[substr($res->date_event, 0, 4).'-01-01'] = $res ;
					}

					for($i = $days - 1; $i >= 0 ; $i--){

						$date_day = $year . '-01-01' ;

						if(array_key_exists($date_day, $temp2)){
							array_push($$items_var, $temp2[$date_day]);
						}
						else{
							$tempobj = new stdClass();
							$tempobj->howmuch = 0;
							$tempobj->date_event = $date_day;

							array_push($$items_var, $tempobj);
						}

						$year++;

					}
					
					break;
				}
				
			}

		}

		//print_r(str_replace("#_","jos",$query));echo "\n";

		$current-- ;

		for ($k=1; $k <= $numdatasets; $k++) { 
			$num = $k;
			if($k == 1) $num = "";

			$items_var = "items".$num;

			if(!isset($$items_var)) $$items_var = array();
			$return[$items_var] = $$items_var ;

		}

		if($json){
			
			$date_format = $params->get('other_label_format') ? $params->get('other_label_format') : $params->get('label_format', '%a') ;

			$i = 0;
			$items_json = array();

			foreach($items as $item){

				$label = JHTML::_('date', $item->date_event, JText::_($date_format)) ;
				
				if(!$item->howmuch) $item->howmuch = 0 ;
				
				$items_json[$i] = new stdClass();
				$items_json[$i]->c = array();

				$items_json[$i]->c[0] = new stdClass();
				$items_json[$i]->c[0]->v = $label ;
				$items_json[$i]->c[0]->f = null ;

				//iterate through all datasets
				for ($k=1; $k <= $numdatasets; $k++) { 
					$num = $k;
					if($k == 1) $num = "";

					$items_var = "items".$num;
					$component_var = "component".$num;

					if(!isset($$items_var[$i]->howmuch) && $$component_var) $$items_var[$i]->howmuch = 0 ;

					if($$component_var){
						$items_json[$i]->c[$k] = new stdClass();
						$items_json[$i]->c[$k]->v = $$items_var[$i]->howmuch ;
						$items_json[$i]->c[$k]->f = null ;
					}

				}
				
				$i++ ;
			}

			$cols = array();

			$cols[0] = new stdClass();
			$cols[0]->id = ""; 
			$cols[0]->label = "Time"; 
			$cols[0]->pattern = ""; 
			$cols[0]->type = "string"; 

			//iterate through all datasets for the columns...
			for ($k=1; $k <= $numdatasets; $k++) { 
				$num = $k;
				if($k == 1) $num = "";

				$component_var = "component".$num;

				if($$component_var){
					$cols[$k] = new stdClass();
					$cols[$k]->id = ""; 
					$cols[$k]->label = $params->get('dataname'.$num); 
					$cols[$k]->pattern = ""; 
					$cols[$k]->type = "number";
				} 
			}

			$return_json = new stdClass();
			$return_json->cols = $cols;
			$return_json->rows = $items_json;

			return $return_json ;

			//print_r("<pre>".json_encode($items_json)."</pre>");
		}
		
		return $return;

	}

}