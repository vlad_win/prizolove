<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$currentSession = JFactory::getSession();

$redirect = $currentSession->get('plUserCreated', '');
$redirectUrl = $params->get('redirect_url', '');

$currentSession->clear('plUserCreated');

if ($redirect && $redirectUrl) {
    $app = JFactory::getApplication();
    $app->redirect($redirectUrl);
}


$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_social_buttons', $params->get('layout', 'default'));
