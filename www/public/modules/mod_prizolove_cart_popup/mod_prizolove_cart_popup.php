<?php

defined('_JEXEC') or die;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

$app		= JFactory::getApplication();
$user		= JFactory::getUser();
$groups		= implode(',', $user->getAuthorisedViewLevels());
$lang 		= JFactory::getLanguage()->getTag();
$clientId 	= (int) $app->getClientId();

$session = JFactory::getSession();
$cart = json_decode($session->get('plCart', json_encode(array())), true);

$db	= JFactory::getDbo();
$query = $db->getQuery(true);
$query->select('m.id, m.title, m.module, m.position, m.ordering, m.content, m.showtitle, m.params');
$query->from('#__modules AS m');
$query->where('m.published = 1');

$query->where('m.id = 456');

$date = JFactory::getDate();

$now = $date->toSql();

$nullDate = $db->getNullDate();

$query->where('(m.publish_up = '.$db->Quote($nullDate).' OR m.publish_up <= '.$db->Quote($now).')');
$query->where('(m.publish_down = '.$db->Quote($nullDate).' OR m.publish_down >= '.$db->Quote($now).')');

$query->where('m.access IN ('.$groups.')');
$query->where('m.client_id = '. $clientId);

// Filter by language
if ($app->isSite() && $app->getLanguageFilter()) {
    $query->where('m.language IN (' . $db->Quote($lang) . ',' . $db->Quote('*') . ')');
}

// Set the query
$db->setQuery($query);
$module = $db->loadObject();

$renderModule = JModuleHelper::renderModule($module, array('style' => 'none'));

require JModuleHelper::getLayoutPath('mod_prizolove_cart_popup', $params->get('layout', 'default'));