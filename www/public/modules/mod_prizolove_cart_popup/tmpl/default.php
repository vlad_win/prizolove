<?php

defined('_JEXEC') or die;

    function plural_form($number) {
        $after = array('товар','товара','товаров');
        $cases = array(2,0,1,1,1,2);
        return $number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
    }

    $cost = 0;
    $num = 0;
?>


    <h3>Корзина</h3>
    <table class="table cart">


<?php if (!empty($cart['items'])) { ?>

        <tr>
            <th>Фото</th>
            <th>Название</th>
            <th>Количество</th>
            <th>Цена</th>
            <th>Итого</th>
            <th>Удалить</th>
            </tr>
                <?php
                    foreach($cart['items'] as $id=>$item) :
                        $num += $item['quantity'];
                        $cost += $item['quantity'] * $item['price'];

                ?>
                    <tr class="cart-row" data-id="<?=$id;?>">
                        <td><img src="<?=$item['image'];?>" alt="<?=$item['name'];?>"></td>
                        <td class="cart__product__title"><span><?=$item['name'];?></span></td>
                        <td><a href="#" class="cart__plus" data-act="plus">+</a><input class="cart__count" value="<?=$item['quantity'];?>" maxlength="3"><a href="#" class="cart__minus" data-act="minus">-</a></div></td>
                        <td class="cart__col__price"><?=$item['price'];?></td>
                        <td class="cart__col__quantity"><?=($item['quantity'] * $item['price']);?></td>
                        <td><a data-act="delete" class="cart__delete" href="javascript:void(0)"><img src="/images/cart1.png" alt=""></a></td>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td>В корзине:</td>
                        <td colspan="5" class="cart__total-num"><?=plural_form($num);?></td>
                        </tr>
                    <tr>
                        <td colspan="5">Итого к оплате</td>
                        <td class="cart__total-cost"><?=$cost;?></td>
                    </tr>

<?php } ?>
    </table>
    <div class="cart__buttons<?=((!$num)?' hidden':'');?>">
        <a id="continue-ordering" class="sppb-btn  sppb-btn-custom sppb-btn-round">Продолжить покупки</a>
        <a href="/orderpage" id="process-order" class="sppb-btn sppb-btn-custom sppb-btn-round">Оформить заказ</a>
    </div>
    <div class="cart__order-form">
        <?=$renderModule;?>
    </div>

