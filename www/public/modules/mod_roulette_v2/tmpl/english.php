<?php

defined('_JEXEC') or die;
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$lang_code = $menu->language;
if ($lang_code == 'en-GB') {
$newLang = JLanguage::getInstance($lang_code);
$app->loadLanguage($newLang);
$app->set('menuLanguage', $app->getLanguage());
$lang = $app->get('menuLanguage');
}
?>
<style>
    #spend a{
        background-color: #30a01a;
        color: #FFFFFF;
        padding: 20px 30px 20px 30px;
        font-size: 20px;
    }
    #spin {
        background-color: #d5313a;
        color: #FFFFFF;
        padding: 20px 30px 20px 30px;
        font-size: 20px;
	    position: relative;
	    z-index: 5;
        display: block !important;
    }
    .roulette__arrow_area {
	    position: absolute;
	    top:0;
	    left:0;
	    right: 0;
	    height: 100px;
    }
    .roulette__arrow {
	    width: 84px;
	    height: 54px;
	    margin: 0 auto;
    }
    .roulette__arrow img {
        display: block;
        margin: auto;
    }
    .wheel_bg {
	    width: 580px; height: 600px;
    }

    .wheel_bg {
	    margin: 0 auto;
	    background: url(https://prizolove.com/images/background-roullet.png);
	    background-repeat: no-repeat;
	    background-position: center center;
	    background-size: cover;
    }
    #wheelcanvas {
	    margin: 0 auto;
    }
    @media screen and (max-width:500px){   	
	.wheel_bg {
		width: 205px;
		height: 238px;
	}
	.roulette__arrow {
		width: 18px;
		height: 30px;
	}
    }	
</style>
<div class="roulette" style="text-align: center;position: relative;">
    <div class="row">
        <div class="col col-6">
	        <div class="roulette__arrow_area">
		        <div class="roulette__arrow">
			        <img src="/images/arrow2-sm.png" alt="" />
		        </div>
	        </div>
	        <div class="wheel_bg">
		        <canvas id="wheelcanvas" width="500" height="500" class="center"></canvas>
	        </div>
        </div>
        <div class="col col-6">
            <div id="bonusText" class="text <?=$showBonus?'':'hidden'?>"><div class="bonus-text"></div><div class="bonus-desc"></div></div>
            <div><?php if ($showSpin) :?><button id="spin" type="button" class="sppb-btn button-red sppb-btn-custom sppb-btn-rounded"><?=JText::_('SPIN_WHEEL_OF_FORTUNE_EN');?></button>
            <?php endif;?></div>


            <div id="spend" <?=$showSpend?'':'class="hidden"'?>>
            <?php /* <a class="sppb-btn button-red sppb-btn-custom sppb-btn-rounded" href="<?=$prizeUrl;?>">Потратить Ваш бонус</a> */ ?>
            </div>
        </div>
    </div>
</div>


<script type="application/javascript">
  var resizeState = false;
  var h = 500;
  var w = 500;
  var cw, ch;
  var prize;
  var prizeId, endAngle, prizeAngle;



  var fields = [

    <?php foreach($prizeArray as $key=>$value) : ?>
      <?php if ($key % 2) : ?>
        {color:'#fefdf1', number:'<?=$value['name'];?>', desc:'<?=$value['desc'];?>', fontColor: '#000'},
      <?php else: ?>
        {color:'#c1252c', number:'<?=$value['name'];?>', desc:'<?=$value['desc'];?>', fontColor: '#fff'},
      <?php endif; ?>
    <?php endforeach; ?>
  ];


  jQuery(function($) {
      $(document).ready(function () {
          $('.button-see').on('click', function(e){
              var userConfirm = confirm('<?=$lang->_('CONFIRM_GAME_ARE_YOU_SURE');?>');
              if (!userConfirm) return false;
          });
          /* $('.button').on('click', function() {
              $('div.button').hide();
              $('#div1').css('display','block');
              $('.sppb-section').show();

          }); */
          $('#spin').on('click', function() {
              $('#bonusText').hide();
              var spinButton = $(this);
              spinButton.hide();
              $(this).hide();

              $.ajax({
                  method: "POST",
                  url: "/index.php?option=com_ajax&plugin=roulette&format=json",
              })
                  .done(function(data) {
                      rlData = data;
                      if (data.data[0].prize != undefined) {
                          prize = data.data[0].prize;
                          spin();
                      }
                  })
              ;


          });
          windowSize();
          defineCanvas();
          draw();
      });


      function windowSize() {
          ch = $(window).height(), cw = $(window).width();
      }

      function defineCanvas() {
          if (cw >= 500) {
              w = 500;
              h = 500;
          }
          if (cw < 500) {
              w = 320;
              h = 320;
	      $('#wheelcanvas').height(h);		      	
          }
      }


  });   
</script>
	
	
