<?php

defined('_JEXEC') or die;

class ModRouletteV2Helper
{

    public static function getPrizeArray(&$params)
    {
        $db = JFactory::getDbo();

        $query = "SELECT `*` FROM #__prizolove_promo_items WHERE `cat_id` = ".$params->get('promo');

        $db->setQuery($query);

        $items = $db->loadObjectList();
        $prizeArray = array();
        foreach($items as $item) {
            $item->params = json_decode($item->params, true);
            $prize = array();
            $prize['name'] = $item->name;
            $prize['desc'] = $item->custom_prize;
            if (!empty($item->params['roulette_number'])) {
                for($i=0; $i<intval($item->params['roulette_number']); $i++) {
                    $prizeArray[] =  $prize;
                }
            } else {
                $prizeArray[] =  $prize;
            }
        }

        return $prizeArray;

    }

    public static function getPrizeText($prizeId)
    {
        $db = JFactory::getDbo();

        $query = "SELECT name FROM #__prizolove_promo_items WHERE `id` = ".$prizeId;

        $db->setQuery($query);

        $result =  $db->loadObject();

        return $result->name;
    }

}