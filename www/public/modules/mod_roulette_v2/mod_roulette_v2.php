<?php
defined('_JEXEC') or die;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

JLoader::register('ModRouletteV2Helper', __DIR__ . '/helper.php');

$prizeArray = ModRouletteV2Helper::getPrizeArray($params);

$currentSession = JFactory::getSession();

$url =$_SERVER['REQUEST_URI'];
$pageId = substr(parse_url($url, PHP_URL_PATH), 1);

$currentSession = JFactory::getSession();

$promos = $currentSession->get('plPromos');

$promos[$pageId] = $params->get('promo');

$currentSession->set('plPromos', $promos);

shuffle($prizeArray);

$prizeUrl = $params->get('prize_url', '');

$showSpin = true;
$showBonus = false;
$bonusText = '';
$showSpend = false;

$app = JFactory::getApplication();
$input = $app->input;

$plPrm = $input->cookie->get($name = '_plprm'.intval($params->get('promo')), $defaultValue = 0);
$plPrmP = $input->cookie->get($name = '_plprmp'.intval($params->get('promo')), $defaultValue = 0);

/* if ($plPrm >= 3) {
    $showSpin = false;
} */
if ($plPrm > 0) {
    $showSpend = true;
}

/* if ($plPrmP) {
    $bonusText = ModRouletteV2Helper::getPrizeText($plPrmP);
    $showBonus = true;
} */


$doc = JFactory::getDocument();
$doc->addScript( JURI::base(true) . '/modules/mod_roulette_v2/assets/js/script.js' );

require JModuleHelper::getLayoutPath('mod_roulette_v2', $params->get('layout', 'default'));
