<?php
defined('_JEXEC') or die;

?>

<?php
    if ($user->id) :
?>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-4 col-md-6 col-sm-6 logo-block">
		<a href="/accounts"><img src="https://prizolove.com/images/logo-vertical-white.png"></a>
            </div>
            <div class="col-7 col-md-6 col-sm-6 user-block">
                <div class="inline"><span class="name-user">Привет, <?=$user->username;?>!</span>
                    <br/><?php /* <a href="#"><i class="fas fa-cog"></i>  <span>Настройки</span></a> */?><a href="/logout"><i class="fas fa-times"></i> <span>Выйти</span></a>
                </div>
                <div class="inline avatar-block">
		    <img src="https://prizolove.com/images/avatar-user.png">
		</div>
            </div>        
	</div>
    </div>
</div>
<?php endif; ?>

