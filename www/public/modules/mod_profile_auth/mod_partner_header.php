<?php

defined('_JEXEC') or die;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

$user = JFactory::getUser();

require JModuleHelper::getLayoutPath('mod_partner_header', $params->get('layout', 'default'));