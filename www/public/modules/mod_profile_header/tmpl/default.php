<?php
defined('_JEXEC') or die;
?>
<style>
    #sp-component {
        font-family: Roboto;
    }
    #sp-main-body {
        background-image: url(/images/background-sign.jpg);
        background-repeat: no-repeat;
        background-size: cover;
        background-attachment: scroll;
        background-position: 50% 50%;
    }
    *{font-weight:400;}
    .prev{width:215px;border-radius:10px;box-shadow:0px 0px 3px rgba(0,0,0,0.25);background:white;border:none !important;}
    .sort{font-family:"Roboto",sans-serif;font-size:14px;text-align:right;width:240px;border-radius:10px;box-shadow:0px 0px 3px rgba(0,0,0,0.25);background:white;border:none !important;}
    .count{font-family:"Roboto",sans-serif;font-size:14px;text-align:right;width:80px;border-radius:10px;box-shadow:0px 0px 3px rgba(0,0,0,0.25);background:white;border:none !important;}
    .table-length{border-bottom:1px solid rgba(0,0,0,0.10);}
    .header-table{box-shadow:0px 0px 3px rgba(0,0,0,0.25);}
    .header{box-shadow:0px 3px 13px rgba(0,0,0,0.1);}
    .logo img{width:50%;}
    .button-settings{border:1px solid #959595;}
    .button-settings:hover{border-color:#ff5550;}
    .button-inline{border-radius:4px !important;width:150px;}

    .sppb-divider{margin:0px !important;width:205px;}

    #order-table {
        padding: 15px 0;
        background-color: #ffffff;
    }

    #orders { border: 1px solid #ccc;}
    #orders thead td{
        background: #ccc;
        text-align: left;
    }
    #orders td{
        padding: 5px;
    }

    .header-table {
        font-size: 16px;
        line-height: 16px;
        color: #424242;
    }
    .table-length {
        padding: 25px 0;
        font-family: "Roboto", sans-serif;
        font-size: 16px;
        line-height: 20px;
    }
    #pagination-block {
        width: 100%;
    }
    .center {text-align: center;}
</style>
<section class="sppb-section header">
    <div class="sppb-row-container">
        <div class="sppb-row sppb-no-gutter">
            <div class="sppb-col-md-7">
                <div class="sppb-column sppb-align-items-center">
                    <div class="sppb-column-addons">
                        <div class="clearfix">
                            <div class="sppb-addon sppb-addon-single-image sppb-text-left logo">
                                <div class="sppb-addon-content">
                                    <div class="sppb-addon-single-image-container">
                                        <img class="sppb-img-responsive" src="/images/logotype.png" alt="Image" title="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sppb-col-md-2">
                <div class="sppb-column">
                    <div class="sppb-column-addons">
                        <div class="clearfix">
                            <div class="sppb-text-right">
                                <a href="/profile-orders" class="sppb-btn  button-inline sppb-btn-custom sppb-btn-rounded"><i class="fa fa-shopping-basket"></i> Мои заказы</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sppb-col-md-2">
                <div class="sppb-column">
                    <div class="sppb-column-addons">
                        <div class="clearfix">
                            <div class="sppb-text-right">
                                <a href="/profile/edit-profile" class="sppb-btn  button-inline button-settings sppb-btn-custom sppb-btn-rounded"><i class="fa fa-gear"></i> Настройки</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sppb-col-md-1">
                <div class="sppb-column">
                    <div class="sppb-column-addons">
                        <div class="clearfix">
                            <div class="sppb-text-right">
                                <a href="/profile/profile-logout" class="sppb-btn  sppb-btn-custom sppb-btn-rounded"><i class="fa fa-power-off"></i> Выйти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>