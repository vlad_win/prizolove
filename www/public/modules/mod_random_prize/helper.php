<?php


defined('_JEXEC') or die;



class ModRandomPrizeHelper
{
	public static function getPrize(&$params)
	{
		
		$session = JFactory::getSession();
		
		$images = array();
		$prizes = array();
		$labels = array();
	
		if (!empty($params->get('image1')) && !empty($params->get('prizetext1'))) {
			$images[] = $params->get('image1');
			$labels[] = $params->get('prizetext1');
			for($i = 0; $i < 80; $i++) {
				$prizes[] = 0;
			}
		}
		
		if (!empty($params->get('image2')) && !empty($params->get('prizetext2'))) {
			$images[] = $params->get('image2');
			$labels[] = $params->get('prizetext2');
			for($i = 0; $i < 20; $i++) {
				$prizes[] = 1;
			}
		}
		
		$inputCookie  = JFactory::getApplication()->input->cookie;
		$cookiePrize = $inputCookie->get($name = '_pl', $defaultValue = false);
		
		
		
		if ($cookiePrize && !empty($labels[$cookiePrize])) {
			$welcomeBonus = array(
				'image' => $images[$cookiePrize],
				'text' => $labels[$cookiePrize]
			);
			
			$session->set('welcomeBonus', json_encode($welcomeBonus));			
			return $welcomeBonus;
		}
		
		shuffle($prizes);
		
		$inputCookie->set($name = '_pl', $value = $prizes[0], $expire = (time() + (60 * 15)));
		
		$welcomeBonus = array(
				'image' => $images[$prizes[0]],
				'text' => $labels[$prizes[0]]
		);
		
		$session->set('welcomeBonus', json_encode($welcomeBonus));
		return $welcomeBonus;
	}
	
	public static function getPrize2(&$params)
	{
		
		$session = JFactory::getSession();
		
		$images = array();
		$prizes = array();
		$labels = array();
		$shortText = array();
	
		if (!empty($params->get('image1')) && !empty($params->get('prizetext1'))) {
			$images[0] = $params->get('image1');
			$labels[0] = $params->get('prizetext1');			
			$shortText[0] = $params->get('prizeshorttext1');			
			$prizes[] = 0;			
		}
		
		if (!empty($params->get('image2')) && !empty($params->get('prizetext2'))) {
			$images[1] = $params->get('image2');
			$labels[1] = $params->get('prizetext2');			
			$shortText[1] = $params->get('prizeshorttext2');			
			$prizes[] = 1;			
		}
		
		if (!empty($params->get('image3')) && !empty($params->get('prizetext3'))) {
			$images[2] = $params->get('image3');
			$labels[2] = $params->get('prizetext3');
			$shortText[2] = $params->get('prizeshorttext3');						
			$prizes[] = 2;			
		}               
		
		if (!empty($params->get('image4')) && !empty($params->get('prizetext4'))) {
			$images[3] = $params->get('image4');
			$labels[3] = $params->get('prizetext4');			
			$prizes[] = 3;			
		}               
		
		if (!empty($params->get('image5')) && !empty($params->get('prizetext5'))) {
			$images[4] = $params->get('image5');
			$labels[4] = $params->get('prizetext5');			
			$shortText[4] = $params->get('prizeshorttext5');						
			$prizes[] = 4;			
		}
		
		$inputCookie  = JFactory::getApplication()->input->cookie;
		$cookiePrize = $inputCookie->get($name = '_pl', $defaultValue = false);
		$cookieCounter = $inputCookie->get($name = '_plc', $defaultValue = false);
		
		$cookieCounter = (intval($cookieCounter) > 2)?2:(intval($cookieCounter));		
		
		$cookieCounter++;
	
		if ($cookieCounter < 3 || $cookiePrize === false) {
			shuffle($prizes);
			$prizeId = $prizes[1];
		} else {
			$prizeId = $cookiePrize;
		}
		
		if ($cookiePrize && !empty($labels[$cookiePrize])) {
                        
			$welcomeBonus = array(
				'image' => $images[$prizeId],
				'text' => (!empty($shortText[$prizeId]))?$shortText[$prizeId]:$labels[$prizeId],
				'counter' => $cookieCounter 
			);

			$inputCookie->set($name = '_pl', $value = $prizeId, $expire = (time() + (60 * 15)));
			$inputCookie->set($name = '_plc', $value = $cookieCounter, $expire = (time() + (60 * 15)));
			$session->set('welcomeBonus2', json_encode($welcomeBonus));			
			
			return $welcomeBonus;
		}
		
		shuffle($prizes);
		
		$inputCookie->set($name = '_pl', $value = $prizeId, $expire = (time() + (60 * 15)));
		$inputCookie->set($name = '_plc', $value = $cookieCounter, $expire = (time() + (60 * 15)));
		
		$welcomeBonus = array(
			'counter' => $cookieCounter,
			'image' => $images[$prizeId],
			'text' => (!empty($shortText[$prizeId]))?$shortText[$prizeId]:$labels[$prizeId],
		);
		
		$session->set('welcomeBonus2', json_encode($welcomeBonus));
		return $welcomeBonus;
	}	
	
}