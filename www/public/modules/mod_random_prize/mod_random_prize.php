<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('ModRandomPrizeHelper', __DIR__ . '/helper.php');

$layout = $params->get('layout', 'default');

if (strpos($layout, 'prize') !== false) {
	$prize = ModRandomPrizehelper::getPrize2($params);
} else {
	$prize = ModRandomPrizehelper::getPrize($params);
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_random_prize', $layout);