<?php

defined('_JEXEC') or die;

class ModRouletteHelper
{

    public static function getPrizeArray(&$params)
    {
        $db = JFactory::getDbo();

        $query = "SELECT `*` FROM #__prizolove_promo_items WHERE `cat_id` = ".$params->get('promo');

        $db->setQuery($query);

        $items = $db->loadObjectList();
        $prizeArray = array();
        foreach($items as $item) {
            $item->params = json_decode($item->params, true);
            if (!empty($item->params['roulette_number'])) {
                $prize = array();
                $prize['name'] = $item->name;
                for($i=0; $i<intval($item->params['roulette_number']); $i++) {
                    $prizeArray[] =  $prize['name'];
                }
            } else {
                $prizeArray[] =  $item->name;
            }
        }

        return $prizeArray;

    }

    public static function getPrizeText($prizeId)
    {
        $db = JFactory::getDbo();

        $query = "SELECT name FROM #__prizolove_promo_items WHERE `id` = ".$prizeId;

        $db->setQuery($query);

        $result =  $db->loadObject();

        return $result->name;
    }

    /* public static function getPrize(&$params)
    {

        $session = JFactory::getSession();

        $images = array();
        $prizes = array();
        $labels = array();

        if (!empty($params->get('image1')) && !empty($params->get('prizetext1'))) {
            $images[] = $params->get('image1');
            $labels[] = $params->get('prizetext1');
            for($i = 0; $i < 80; $i++) {
                $prizes[] = 0;
            }
        }

        if (!empty($params->get('image2')) && !empty($params->get('prizetext2'))) {
            $images[] = $params->get('image2');
            $labels[] = $params->get('prizetext2');
            for($i = 0; $i < 20; $i++) {
                $prizes[] = 1;
            }
        }

        $inputCookie  = JFactory::getApplication()->input->cookie;
        $cookiePrize = $inputCookie->get($name = '_pl', $defaultValue = false);



        if ($cookiePrize && !empty($labels[$cookiePrize])) {
            $welcomeBonus = array(
                'image' => $images[$cookiePrize],
                'text' => $labels[$cookiePrize]
            );

            $session->set('welcomeBonus', json_encode($welcomeBonus));
            return $welcomeBonus;
        }

        shuffle($prizes);

        $inputCookie->set($name = '_pl', $value = $prizes[0], $expire = (time() + 31536000));

        $welcomeBonus = array(
            'image' => $images[$prizes[0]],
            'text' => $labels[$prizes[0]]
        );

        $session->set('welcomeBonus', json_encode($welcomeBonus));
        return $welcomeBonus;
    } */
}