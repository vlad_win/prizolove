var startAngle = 0;
var arc = Math.PI / 8;
var spinTimeout = null;
var spinArcStart = 10;
var spinTime = 0;
var spinTimeTotal = 0;
var ctx;
var rlData;

function draw() {
    drawRouletteWheel()
}

function drawRouletteWheel() {
    var canvas = document.getElementById("wheelcanvas");
    if (canvas.getContext) {
        var outsideRadius = h / 2.35;
        var textRadius = h / 3.125;
        var insideRadius = h / 14;
        ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, w, h);
        ctx.strokeStyle = "black";
        ctx.lineWidth = 5;
        ctx.font = '14px sans-serif';
        for (var i = 0; i < 16; i++) {
            var angle = startAngle + i * arc;
            ctx.fillStyle = fields[i].color;
            ctx.beginPath();
            ctx.arc((w / 2), (w / 2), outsideRadius, angle, angle + arc, !1);
            ctx.arc((w / 2), (w / 2), insideRadius, angle + arc, angle, !0);
            ctx.stroke();
            ctx.fill();
            ctx.save();
            ctx.fillStyle = fields[i].fontColor;
            //ctx.translate((w / 2) + Math.cos(angle + arc / 2) * textRadius, (w / 2) + Math.sin(angle + arc / 2) * textRadius);
            //ctx.rotate(angle + arc / 2 + Math.PI / 2);
            ctx.translate((w / 2) + Math.cos(angle + arc / 2.5) * textRadius, (w / 2) + Math.sin(angle + arc / 2) * textRadius);
            ctx.rotate(angle + arc / -3.5 + Math.PI / -1.1);
            var text = fields[i].number;
            //ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
            ctx.fillText(text, -ctx.measureText(text).width / 3, 0);
            ctx.restore()
        }
        ctx.fillStyle = "gold"; /* ctx.beginPath();ctx.moveTo((w/2)-4,(w/2)-(outsideRadius+0));ctx.lineTo((w/2)+4,(w/2)-(outsideRadius+0));ctx.lineTo((w/2)+4,(w/2)-(outsideRadius-5));ctx.lineTo((w/2)+9,(w/2)-(outsideRadius-5));ctx.lineTo((w/2)+0,(w/2)-(outsideRadius-13));ctx.lineTo((w/2)-9,(w/2)-(outsideRadius-5));ctx.lineTo((w/2)-4,(w/2)-(outsideRadius-5));ctx.lineTo((w/2)-4,(w/2)-(outsideRadius+5));ctx.fill()*/
    }
}

function getPrize() {
    prizeId = Math.floor(Math.random() * fields.length);
    while (prize != fields[prizeId].number) {
        prizeId = Math.floor(Math.random() * fields.length)
    }
}

function spin() {
    getPrize();
    startAngle = 0;
    spinAngleStart = (180 / 16 / 2);
    prizeAngle = prizeId * (180 / 8);
    endAngle = ((625 - spinAngleStart - prizeAngle) * Math.PI / 180);
    spinTime = 0;
    spinTimeTotal = Math.random() * 3 + 4 * 1000;
    rotateWheel()
}

function rotateWheel() {
    if (startAngle >= endAngle) {
        stopRotateWheel();
        return
    }
    var spinAngle = spinAngleStart - easeOut(spinTime, 0, spinAngleStart, spinTimeTotal);
    startAngle += ((spinAngle * Math.PI / 180));
    drawRouletteWheel();
    spinTimeout = setTimeout('rotateWheel()', 5)
}

function stopRotateWheel() {
    clearTimeout(spinTimeout);
    var degrees = startAngle * 180 / Math.PI + 90;
    var arcd = arc * 180 / Math.PI;
    var index = Math.floor((360 - degrees % 360) / arcd);
    ctx.save();
    ctx.fillStyle = '#fff';
    ctx.font = 'bold 24px sans-serif';
    jQuery('#bonusText span').text(fields[index].number);
    jQuery('#bonusText').removeClass('hidden');
    jQuery('#bonusText').show();

    //ctx.fillText(text, (w / 2) - ctx.measureText(text).width / 2, (w / 2) + 10);
    ctx.restore();
    checkButtons();
}

function easeOut(t, b, c, d) {
    var ts = (t /= d) * t;
    var tc = ts * t;
    return b + c * (tc + -3 * ts + 3 * t)
}


function checkButtons() {
    if (rlData.data[0].removeSpin != undefined) {
        jQuery('#spin').remove();
        jQuery('#spend').removeClass('hidden');
    } else {
        jQuery('#spin').show();
    }
    jQuery('#spend').removeClass('hidden');
}