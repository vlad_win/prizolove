<?php

defined('_JEXEC') or die;
?>

<?php
if (!empty($cart['items'])) {
    $num = 0;
    foreach ($cart['items'] as $product) {
        $num += $product['quantity'];
    }
?>
    <div id="pl-cart" class="active">
        <div class="pl-cart__image">

        </div>
        <div class="pl-cart__product-num">
            <?=$num;?>
        </div>
    </div>
<?php } else { ?>
    <div id="pl-cart">
        <div class="pl-cart__image">

        </div>
        <div class="pl-cart__product-num">

        </div>
    </div>
<?php
}