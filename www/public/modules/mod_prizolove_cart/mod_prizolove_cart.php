<?php

defined('_JEXEC') or die;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

$session = JFactory::getSession();
$cart = json_decode($session->get('plCart', json_encode(array())), true);

require JModuleHelper::getLayoutPath('mod_prizolove_cart', $params->get('layout', 'default'));