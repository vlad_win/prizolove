<?php
$_SERVER['HTTP_HOST']  = 'prizolove.com`';
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
if (file_exists(dirname(__FILE__) . '/defines.php')) {
 include_once dirname(__FILE__) . '/defines.php';
}
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', dirname(__FILE__));
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');

if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
 echo 'This code can not work without the AcyMailing Component';
 return false;
}

JLoader::import('pl_lib.library');

$db = JFactory::getDBO();

$curDate = new DateTime();
$curDate->modify("-3 day");
$lastHour = new DateTime();
$lastHour->modify("-1 hour");

$sql  = '
			SELECT o.order_id, p.type, s.email, o.date, p.name, p.id product_id
			FROM #__prizolove_orders o 
			INNER JOIN #__prizolove_order_items oi ON oi.order_id = o.order_id
			INNER JOIN #__prizolove_products p ON p.id = oi.product_id
			INNER JOIN #__acymailing_subscriber s ON s.subid = o.customer_id
			LEFT JOIN #__pl_order_mail_reminder omr ON omr.order_id = o.order_id
			WHERE o.sent = 0 AND o.date >= "'.$curDate->format('Y-m-d H:i:s').'" AND o.date <= "'.$lastHour->format('Y-m-d H:i:s').'" AND omr.order_id IS NULL
';
$db->setQuery($sql);

$orders = $db->loadObjectList();
$mailer = acymailing_get('helper.mailer');
$mailer->report = true;
$mailer->trackEmail = true;
$mailer->forceVersion = 1;
$mailer->autoAddUser = false;

foreach($orders as $order) {
	$report = new StdClass();
	switch($order->type) {
		case 'business':
			$report->mailid = 392;
			switch($order->product_id) {
				case 354:
					$mailer->addParam('leads',11);		
					break;
				case 256:
					$mailer->addParam('leads',100);		
					break;	
				case 256:
					$mailer->addParam('leads',110);		
					break;	
				case 355:
					$mailer->addParam('leads',21);		
					break;	
				case 356:
					$mailer->addParam('leads',27);		
					break;						
				default: 
					$mailer->addParam('leads','');		
					break;
			}
			break;
		case 'product':
			$report->mailid = 390;
			break;
	}
	$report->order_id = $order->order_id;
	$report->sent = date('Y-m-d H:i:s');
	
	$mailer->addParam('product_list',$order->name);		
	$mailer->addParam('product_name',$order->name);			
	$mailer->addParam('orderId',$order->order_id);
	$mailer->addParam('order_date', $order->date);
	$mailer->addParam('orderpageId', 'pid='.$order->product_id);

	$mailer->sendOne($report->mailid, $order->email); 
	$db->insertObject('#__pl_order_mail_reminder',$report);
}
print_r($orders);
