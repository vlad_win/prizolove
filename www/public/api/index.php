<?php
die();
$request = json_decode(file_get_contents('php://input'), true);


if (!$request) die(json_encode(array('status'=>'2','error'=>'No input data')));

$logLine = date('Y-m-d H:i:s').' |'.json_encode($request,JSON_UNESCAPED_UNICODE).'|'."\r\n";

file_put_contents('requests.log', $logLine, FILE_APPEND);

$_SERVER['HTTP_HOST']  = 'prizolove.com';
$_SERVER['DOCUMENT_URI']  = '/';
$_SERVER['REQUEST_URI']  = '/';

$_SERVER['SCRIPT_NAME'] = str_replace('/api/','/', $_SERVER['SCRIPT_NAME']);

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', '/var/www/preview');
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
require_once 'lib.php';
$app = JFactory::getApplication('site');

if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
 echo 'This code can not work without the AcyMailing Component';
 return false;
}
$userClass = acymailing_get('class.subscriber');
$subscriberClass = acymailing_get('class.subscriber');

if (empty($request['email']) || empty($request['phone']) || empty($request['name'])) die(json_encode(array('error'=>'Not enough input data')));

$myUser = $userClass->get($request['email']);

file_put_contents('acy.log', print_r($myUser, true), FILE_APPEND);

if (!$myUser) {
	$myUser = new stdClass();
 
	$myUser->email = $request['email'];
 
	$myUser->name = $request['name']; 
	$myUser->phone = $request['phone']; 
 
	$subid = $subscriberClass->save($myUser); 	
	
 
	$newSubscription = array();
		
	$newList = array();
	$newList['status'] = 2;
	$newSubscription[39] = $newList; 
	$newSubscription[46] = $newList; 
	$newSubscription[47] = $newList; 
	$newSubscription[62] = $newList; 
 
	$userClass->saveSubscription($subid, $newSubscription);
}


die();