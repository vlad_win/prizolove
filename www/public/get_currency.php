<?php
//https://api.privatbank.ua/p24api/exchange_rates?date=01.12.2014
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');		
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($ch);
		$currencies = json_decode($resp, true);
		$rates = array();
		foreach($currencies as $currency) {
			print_r($currency);
			$rates[$currency['cc']] = $currency['rate'];
		}		
		
		
		if ($resp) file_put_contents('currencies.txt', json_encode($rates));
