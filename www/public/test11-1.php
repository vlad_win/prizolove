<?php
$_SERVER['HTTP_HOST']  = 'prizolove.com`';
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
if (file_exists(dirname(__FILE__) . '/defines.php')) {
 include_once dirname(__FILE__) . '/defines.php';
}
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', dirname(__FILE__));
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');

JLoader::import('pl_lib.library');
//JPluginHelper::importPlugin( 'system' );

if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
 echo 'This code can not work without the AcyMailing Component';
 return false;
}
$mailer = acymailing_get('helper.mailer');
$mailer->report = true; 
$mailer->trackEmail = true;

$db = JFactory::getDBO();

$sql = 'SELECT ls.subid, s.name, s.email, s.phone, u.id, a.id atid 
		FROM #__acymailing_listsub ls  
		INNER JOIN #__acymailing_subscriber s ON ls.subid = s.subid
		LEFT JOIN #__users u ON u.email = s.email
		LEFT JOIN #__affiliate_tracker_accounts a ON a.user_id = u.id
		WHERE ls.listid = 39 and ls.status = 1 AND a.id IS NULL
		';
	
$db->setQuery($sql);

$users = $db->loadObjectList();
//print_r($users);

foreach($users as $cUser) {
    $userId = PlLibHelperUsers::getInstance()->checkJoomlaUser($cUser, true);
    if ($userId) {
        $userId = PlLibHelperUsers::getInstance()->createPartnerFreeProfile($cUser, $userId);
    }
}