<?php

defined('_JEXEC') or die('Restricted access');


$controller = JControllerLegacy::getInstance('OrderPage');

// Require helper file
JLoader::register('OrderPageHelper', JPATH_COMPONENT . '/helpers/orderpage.php');

$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

$controller->redirect();