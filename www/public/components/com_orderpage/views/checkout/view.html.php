<?php

defined('_JEXEC') or die('Restricted access');

class OrderPageViewCheckout extends JViewLegacy
{
    protected $order_id;

    function display($tpl = null)
	{
        $app = JFactory::getApplication();
        $this->assignRef('order_id', $app->input->get('orderId'));
        //$order_id = $app->input->get('orderId');


        $doc = JFactory::getDocument();
        $doc->addStyleSheet('/components/com_sppagebuilder/assets/css/sppagebuilder.css');
        $doc->addStyleSheet('/components/com_sppagebuilder/assets/css/sppagecontainer.css');

		//$this->msg = 'Order Page';
		
		parent::display($tpl);
	}
}