<?php
defined('_JEXEC') or die('Restricted access');
$userFormData = json_decode($this->orderUserData, true);
$fullname = (!empty($userFormData['fullname']))?$userFormData['fullname']:'';
$city = (!empty($userFormData['city']))?$userFormData['city']:'';
$np_address = (!empty($userFormData['np_address']))?$userFormData['np_address']:'';
$phone = (!empty($userFormData['phone']))?$userFormData['phone']:'';
$fname = (!empty($userFormData['fname']))?$userFormData['fname']:'';
$lname = (!empty($userFormData['lname']))?$userFormData['lname']:'';
$mname = (!empty($userFormData['mname']))?$userFormData['mname']:'';
$email = (!empty($userFormData['email']))?$userFormData['email']:'';

?>
<style type="text/css">
    .cart__product__title span {
        font-family: Montserrat;
        font-weight: 600;
        font-size: 16px;
        line-height: 100%;
    }

    .cart__buttons a:hover{
        background: #f51f1f;
        color:#fff;
    }
    .cart__buttons a{
        cursor: pointer;
        background-color: #fc4545;
        color: #FFFFFF;
        padding: 10px 30px 10px 30px;
        font-size: 18px;
        font-weight: 500;
        font-family: Montserrat;
    }



    .sp-module ul>li{border:none !important;}
    .sp-module ul>li>a {

        line-height: 24px !important;
    }


    .sp-module ul>li>a:before {
        font-family: FontAwesome;
        content: " " !important;
        margin-right: 0px !important;
    }

    .attention {
        text-align: right;
        margin-bottom: 20px;
    }

    .attention img {
        display: inline;
        width: 20px;
        margin-right: 10px;
        vertical-align: middle;
        height: 20px;
    }

    .attention-text {
        font-size: 10px;
        line-height: 13px;
        font-family: Montserrat, sans-serif;
        color: #929191;
    }

    input[type="radio"] {
        display: none;
    }

    input[type="radio"] + label {
        color: #212121;
        font-family: Montserrat, sans-serif;
    }

    input[type="radio"] + label span {
        display: inline-block;
        width: 17px;
        height: 17px;
        margin: -2px 10px 0 0;
        vertical-align: middle;
        background: #fff;
        border: 1px solid #b5b5c3;
        border-radius: 50%;
        box-shadow: inset 3px 4px 0 0 rgba(238, 238, 239, .7);
        cursor: pointer;
    }

    input[type="radio"]:hover + label {
        color: #fb3f4c;
        cursor: pointer;
    }

    input[type="radio"]:hover + label span {
        border: 1px solid #fb3f4c;
    }

    input[type="radio"]:checked + label span {
        border: none;
        background: #4dbf42;
        box-shadow: inset 3px 4px 0 0 #49b53f;
    }

    .custom div {
        text-align: left !important;
        font-family: "Montserrat", sans-serif;
        font-size: 12px;
        font-weight: 400;
    }

    * {
        font-weight: 500;
    }

    .logotype img {
        width: 25% !important;
    }

    .check-step {
        position: relative;
    }

    .check-step-number {
        position: absolute;
        top: -10px;
        left: -50px;
        width: 32px;
        border: 2px solid #56B736;
        border-radius: 50%;
        font-family: "Montserrat", sans-serif;
        font-size: 16px;
        line-height: 27px;
        text-align: center;
        font-weight: 500;
    }

    .check-f-i-title {
        vertical-align: top;
        display: inline-block;
        width: 39%;
        font-size: 15px;
        font-family: "Montserrat", sans-serif;
        color: #212121;
    }

    .check-f-i-field {
        display: inline-block;
        width: 59%;
    }

    .input-text {
        width: 100%;
        font-family: "Montserrat", sans-serif;
        border: 1px solid #c2c2cc;
        border-radius: 3px;
        box-shadow: inset 3px 3px 1px 0 #eeeeef;
        padding-left: 10px;
    }

    .input-text:focus {
        border: 1px solid #56B736;
        /* Рамка при получении фокуса */
    }

    .f-i-sign {
        font-size: 10px;
        line-height: 25px;
        color: #929191;
        font-family: Montserrat, sans-serif;
    }

    .f-i-sign a {
        text-decoration: none;
        border-bottom: 1px dotted;
        color: #3e77aa;
    }

    .f-i-sign a:hover {
        color: #fb3f4c;
    }

    .btn-link-i {
        margin-top: 0px;
        margin-bottom: 2px;
        border-radius: 5px;
        background: -webkit-linear-gradient(top, #79d670, #4bbe3f);
        box-shadow: 3px 3px 7px 0 rgba(105, 206, 95, .5), inset 0 -3px 0 0 #3a9731;
        border: none;
        padding: 7px 20px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .3);
        font-size: 18px;
        color: white;
        font-family: Montserrat, sans-serif;
        font-weight: 500;
    }

    .btn-link-i:hover {
        background: #1fc80d;
        background: -webkit-linear-gradient(top, #69f95b, #1fc80d);
    }

    .btn-link-i:active {
        margin-top: 2px;
        margin-bottom: 0px;
        box-shadow: 3px 3px 7px 0 rgba(105, 206, 95, .5), inset 0 -1px 0 0 #3a9731;
        background: -webkit-linear-gradient(top, #62cf57, #29b11b);
    }

    .btn-link-i-blue {
        margin-top: 0px;
        margin-bottom: 2px;
        border-radius: 5px;
        background: -webkit-linear-gradient(top, #578ebf, #3f78ab);
        box-shadow: 3px 3px 7px 0 rgba(64, 121, 171, .5), inset 0 -3px 0 0 #326089;
        border: none;
        padding: 7px 20px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .3);
        font-size: 18px;
        color: white;
        font-family: Montserrat, sans-serif;
        font-weight: 500;
    }

    .btn-link-i-blue:hover {
        background: #1fc80d;
        background: -webkit-linear-gradient(top, #4faafa, #2083dc);
    }

    .btn-link-i-blue:active {
        margin-top: 2px;
        margin-bottom: 0px;
        box-shadow: 3px 3px 7px 0 rgba(105, 206, 95, .5), inset 0 -1px 0 0 #3a9731;
        background: -webkit-linear-gradient(top, #4faafa, #2083dc);
    }

    .btn-link {
        padding-left: 40%;
    }

    @media screen and (max-width:450px) {
        .check-step-number {
            left: -50px;
            top: 0;
        }
        .logotype img {
            width: 100%;
            text-align: center;
        }
        .attention {
            width: 9%;
        }
        .attention-text {
            width: 89%;
        }
        .btn-link {
            padding-left: 0;
        }
    }
    .cancel{position: absolute; width: 15px; right: 0; z-index: 99999;} .parent-block{position:relative;}

    .sertificate {
        width:200px;
        position:absolute;
        top:-20px;right:0px;
        z-index:-1;
    }
    .f-i-sign-bold {
        font-size:12px;
        line-height:25px;
        color:#FF0F24;
        font-family:Montserrat,sans-serif;
        font-weight:600;
        font-style:italic;
    }
    .pl-logo {
        padding: 10px 0 50px;
        margin: 0;
    }


    .bill {
        background: #eaf6fb;
        border: 1px solid #ececec;
        border-radius: 3px;
        box-shadow: 3px 5px 7px 0 rgba(230, 244, 251, .5), inset 0 -3px 0 0 #bbc5c9;
        width: 320px;
        margin: 0 auto;
    }
    .bill-inner {
        margin: 10px;
        position: relative;
    }
    .order-form-header {
        font-family: Montserrat;
        color: #212121;
        margin: 0 0 30px;
    }
    .order-form-header__title {
        font-size: 30px;
        line-height: 32px;
        margin: 0 0 10px;
    }
    .order-form-header__desc {
        font-size: 18px;
        line-height: 21px;
        background-color: rgba(255, 255, 255, 0.5);
        box-shadow: 0 0 0 0 #ffffff;
        margin: 0 0 30px;
    }
    .order-form-body {
        padding: 0 0 0 50px;
    }
    .check-step-title {
        font-size: 18px;
        line-height: 18px;
        color: #000;
        margin: 0 0 30px;
    }
    .check-step-body {
        margin: 0 0 50px;
    }
    .order-form__field {
        margin: 0 0 30px;
    }

    .check-step-2 {
        padding: 20px 30px 0;
        border: 1px solid #ececec;
        border-radius: 5px;
        box-shadow: 3px 3px 7px 0 rgba(200, 200, 200, .5), inset 0 -5px 0 0 #c1c1c1;
    }
    #order-submit {
        margin: 0 0 30px;
    }

    hr {
        height: 1px;
        color: #d9d9d9;
        background-color: #d9d9d9;
        width: 100%;
        padding: 0;
    }
    .order-block {
        display: flex;
    }
    @media screen and (max-width:992px) {
        .order-block :nth-child(1) {
            order: 2;
        }

        .order-block :nth-child(2) {
            order: 1;
	    margin: 0 0 50px;	
        }
    }
</style>

        <div id="system-message-container"></div>
        <div class="sppb-row-container">
        <div class="sppb-row order-block">
	    <div class="pl-logo logotype"><img class="sppb-img-responsive" src="/images/logo.png" alt="logo.png" title=""></div>
            <div class="sppb-col-md-6">
                <div class="row">
                    <div class="order-form-header">
                        <div class="order-form-header-text">
                            <div class="order-form-header__title"><strong>Оформление заказа</strong></div>
                            <div class="order-form-header__desc">(к каждому заказу&nbsp;прилагается&nbsp;Сертификат Финалиста на розыгрыш 250 000 грн.)</div>
                        </div>
                        <div class="sertificate">
                            <img class="sppb-img-responsive" src="/images/sertifikat123.jpg" alt="Image" title="">
                        </div>
                    </div>
                    <form method="post" action="index.php?option=com_orderpage&task=checkout">
                    <div class="order-form-body clearfix">
                        <div class="check-step">
                            <div class="check-step-header">
                                <div class="check-step-number">1</div>
                                <div class="check-step-title">Контактные данные</div>
                            </div>
                            <div class="check-step-body" data-step="1">
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Имя и фамилия</div>
                                    <div class="check-f-i-field">
                                        <input required autofocus="autofocus" class="input-text step1" type="text" name="fullname" value="<?=$fullname;?>">
                                    </div>
                                </div>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Город</div>
                                    <div class="check-f-i-field">
                                        <input required id="town" autofocus="autofocus" placeholder="Введите свой город" class="input-text step1" name="city" type="text" value="<?=$city;?>">
                                        <div class="f-i-sign">например, <a href="javascript:void(0)" onclick="document.getElementById('town').value = 'Киев'">Киев</a></div>
                                    </div>
                                </div>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Мобильный телефон</div>
                                    <div class="check-f-i-field">
                                        <input required autofocus="autofocus" placeholder="Ваш номер телефона" class="input-text step1" name="phone" type="tel" value="<?=$phone;?>">
                                    </div>
                                </div>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Эл. почта</div>
                                    <div class="check-f-i-field">
                                        <input required autofocus="autofocus" placeholder="Ваша эл. почта" class="input-text step1" name="email" type="email" value="<?=$email;?>">
                                        <div class="f-i-sign">Если вы хотите следить за статусом выполнения заказа</div>
                                    </div>
                                </div>
                                <span class="btn-link">
                                    <button type="button" class="btn-link-i" data-step="1">Далее</button>
                                </span>
                            </div>
                        </div>
                        <div class="check-step">
                            <div class="check-step-header">
                                <div class="check-step-number">2</div>
                                <div class="check-step-title">Выбор способ доставки и оплаты</div>
                            </div>
                            <div class="check-step-body check-step-2 hidden" data-step="2">
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Способ доставки</div>
                                    <div class="check-f-i-field check-f-i-radio">
                                        <input type="radio" id="r1" name="delivery" value="np" checked class="step2"/>
                                        <label for="r1"><span></span>самовывоз из Новой Почты</label>
                                    </div>
                                </div>
                                <hr>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Оплата</div>
                                    <div class="check-f-i-field check-f-i-radio">
                                        <input type="radio" id="r4" name="payment" checked value="card" class="step2"/>
                                        <label for="r4"><span></span>Оплата картой Visa/MasterCard</label>
                                        <div class="f-i-sign-bold green"><img style="margin-top:10px;width:100%;" src="/images/bus.png"></div><br>
                                        <input type="radio" id="r3" name="payment" value="cash" class="step2"/>
                                        <label for="r3"><span></span>Наличными</label>
                                        <div class="f-i-sign-bold"><img style="display:inline-block;width:20px;margin-right:10px;" src="/images/checkmark.png">Доставка взимается Новой Почтой<br>35-50 грн + 20 грн + 2% от суммы заказа за оплату при получении</div><br>
                                    </div>
                                </div>
                                <hr>
                                <div class="order-form__field">
                                    <div class="check-f-i-title attention"><img src="/images/checkmark.png"></div>
                                    <div class="check-f-i-field attention-text"><b>Внимание! Получение заказа по паспорту.</b>
                                        <br/>Введите фамилию, имя и отчество получателя заказа.</div>
                                </div>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Адрес доставки</div>
                                    <div class="check-f-i-field">
                                        <input required autofocus="autofocus" class="input-text step2" name="np_address" type="text" value="<?=$np_address;?>">
                                        <div class="f-i-sign">Город и отделение Новой Почты</div>
                                    </div>
                                </div>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Фамилия</div>
                                    <div class="check-f-i-field">
                                        <input required autofocus="autofocus" class="input-text step2" name="lname" type="text" value="<?=$lname;?>">
                                    </div>
                                </div>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Имя</div>
                                    <div class="check-f-i-field">
                                        <input required autofocus="autofocus" class="input-text step2" name="fname" type="text" value="<?=$fname;?>">
                                    </div>
                                </div>
                                <div class="order-form__field">
                                    <div class="check-f-i-title">Отчество</div>
                                    <div class="check-f-i-field">
                                        <input required autofocus="autofocus" class="input-text step2" name="mname" type="text" value="<?=$mname;?>">
                                    </div>
                                </div>
                                <hr>
                                <span class="btn-link">
                                    <button id="order-submit" type="submit" data-step="2" class="btn-link-i disabled">Заказ подтверждаю</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
            <div class="sppb-col-md-6">

                <div class="bill">
                    <div class="bill-inner">
                <?php $cost = 0; ?>
                <?php foreach($this->cart['items'] as $pid=>$item) : ?>
                    <?php $cost += ($item['quantity']*$item['price']); ?>
                    <div id="sppb-addon-1526894067420" class="clearfix pid-<?=$pid;?>">
                        <div class="parent-check">
                            <a data-product="<?=$pid;?>" href="javascript:void(0)" class="remove"><img src="https://prizolove.com/images/cancel.png" class="cancel"></a>
                            <div class="check-f-i-title">
                                <?php if ($item['image']) :?> <img src="<?=$item['image'];?>" alt="<?=$item['name'];?>"> <?php endif; ?>
                            </div>
                            <div class="check-f-i-field"><?=$item['name'];?>
                                <br/><?=$item['quantity'];?> шт. / <span class="item-cost"><?=number_format(($item['quantity']*$item['price']), 0,'.', ' ');?></span> грн</div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php if ($cost) : ?>
                    <div id="sppb-addon-1526894067482" class="clearfix">
                        <div class="sppb-addon sppb-addon-text-block 0 sppb-text-center ">
                            <div class="sppb-addon-content cart-sum">Итого:&nbsp;&nbsp;<strong><span><?=number_format($cost, 0,'.', ' ');?></span> грн.</strong></div>
                        </div>
                    </div>
                <?php endif; ?>
                </div>
                </div>
            </div>
            </div>
        </div>





<div class="modal" tabindex="-1" role="dialog" id="auth">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><strong>Для оплаты выбранным способом </strong>
                    <br /><strong>требуется ввести адрес эл. почты</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="auth-form" method="post">

                    <div class="sppb-col-md-12" id="column-wrap-id-1526917232427">
                        <div id="column-id-1526917232427" class="sppb-column">
                            <div class="sppb-column-addons">
                                <div id="sppb-addon-1526917232431" class="clearfix">
                                    <div class="sppb-addon sppb-addon-text-block 0 sppb-text-left ">
                                        <div class="sppb-addon-content"></div>
                                    </div>
                                </div>
                                <div id="sppb-addon-1526917232436" class="clearfix">
                                    <div class="sppb-addon sppb-addon-text-block 0 sppb-text-left ">
                                        <div class="sppb-addon-content">Эл. почта необходима для получения актуальной
                                            <br /> информации о статусе и составе заказа</div>
                                    </div>
                                </div>
                                <div id="sppb-addon-1526917232447" class="clearfix">
                                    <div class="sppb-addon sppb-addon-raw-html ">
                                        <div class="sppb-addon-content">
                                            <div>
                                                <div class="check-f-i-title">Эл. почта:</div>
                                                <div class="check-f-i-field">
                                                    <input required id="username" autofocus="autofocus" class="input-text" name="username" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sppb-addon-1526917232452" class="clearfix">
                                    <div class="sppb-addon sppb-addon-raw-html ">
                                        <div class="sppb-addon-content">
                                            <div>
                                                <div class="check-f-i-title">Пароль:</div>
                                                <div class="check-f-i-field">
                                                    <input required id="password" autofocus="autofocus" class="input-text" name="password" type="password">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sppb-addon-1526917232463" class="clearfix">
                                    <div class="sppb-addon sppb-addon-raw-html ">
                                        <div class="sppb-addon-content">
                                            <button id="login" class="btn-link-i-blue">Войти</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>

        <script>
            jQuery(document).ready(function() {
                jQuery('input[name="phone"]').each(function () {
                    jQuery(this).mask('+38(099) 999 99 99', {placeholder: "+38(0--) --- -- --"});
                });
                jQuery('#order-submit').on('click', function () {
                    let fPhone = jQuery('input[name="phone"]');
                    fPhone.val(fPhone.val().replace(/^(\+){1}|([^\+\d])/gm, ''));
                });
                jQuery('.parent-check a.remove').on('click', function() {
                    var removeButton = jQuery(this);
                    jQuery.ajax({
                        method: "POST",
                        url: "/index.php?option=com_ajax&plugin=cart&format=json",
                        dataType: 'json',
                        data: {pid: removeButton.data('product'), act: 'delete'}
                    }).done(function(data) {
                        removeButton.closest('.pid-' + removeButton.data('product')).remove();
                        var cartCost = 0;
                        jQuery('.item-cost span').each(function() {
                            cartCost = cartCost + parseFloat(jQuery(this).text());
                        });
                        jQuery('.cart-sum span').text(cartCost);
                        if (cartCost == 0) location.reload();
                    });
                    return false;
                });
                jQuery('button.btn-link-i[type="button"]').on('click', function() {
                    if (jQuery(this).attr('type') == 'submit') return;
                    var step = parseInt(jQuery(this).data('step'));
                    var filled = true;
                    jQuery('input.step' + step).each(function() {
                        this.setCustomValidity('');
                        if (jQuery(this).val() == '') {
                            filled = false;
                            jQuery(this).focus();
                            this.setCustomValidity('Заполните это поле');
                            this.reportValidity();

                        }
                    });
                    if (filled) jQuery('div[data-step="'+(step + 1)+'"]').removeClass('hidden');
                });

                jQuery('button.btn-link-i[type="submit"]').on('click', function() {
                    var step = parseInt(jQuery(this).data('step'));
                    var filled = true;
                    jQuery('input.step' + step).each(function() {
                        this.setCustomValidity('');
                        if (jQuery(this).val() == '') {
                            filled = false;
                            jQuery(this).focus();
                            this.setCustomValidity('Заполните это поле');
                            this.reportValidity();
                        }
                    });
                    if (!filled) return false;
                });

                <?php /* jQuery('input[name="payment"]').on('change', function() {
            if (jQuery(this).val() == 'card') jQuery('#auth').modal('show');
        }); */ ?>

                <?php /* jQuery('#login').on('click',function(e){
            e.preventDefault();
                $username = jQuery('#username').val();
                $password = jQuery('#password').val();
            jQuery.ajax({
                    type: 'post',
                    url: 'index.php?option=com_ajax&plugin=ajaxlogin&format=json',
                    dataType: 'json',
                    data: {username: $username, password: $password}
            }).done(function(data) {
                    if (data.data[0].user_id != undefined && data.data[0].user_id > 0) jQuery('#auth').modal('hide');
            });


        }); */ ?>

            });
        </script>