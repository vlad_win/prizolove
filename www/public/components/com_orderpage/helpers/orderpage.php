<?php

abstract class OrderPageHelper
{

    public static function validateEmail($email)
    {
        list($user, $domain) = explode('@', $email, 2);
        return checkdnsrr($domain, substr(PHP_OS, 0, 3) == 'WIN' ? 'A' : 'MX');
    }

    public static function checkAcyUser($userData)
    {
        if (!empty($userData['email']) && !empty($userData['phone']) && !empty($userData['fullname'])) {

            if (!include_once(rtrim(JPATH_ADMINISTRATOR, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_acymailing' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'helper.php')) {
                echo 'This code can not work without the AcyMailing Component';
                return false;
            }

            $userClass = acymailing_get('class.subscriber');
            $mailer = acymailing_get('helper.mailer');
            $mailer->report = true;
            $mailer->trackEmail = true;
            $mailer->autoAddUser = false;

            $subid = $userClass->subid($userData['email']);

            if (!$subid) {
                $myUser = new stdClass();

                $myUser->email = $userData['email'];
                $myUser->phone = $userData['phone'];
                $myUser->name = $userData['fullname']; //this information is optional

                $subscriberClass = acymailing_get('class.subscriber');
                $subscriberClass->sendConf = false;

                $subid = $subscriberClass->save($myUser); //this function will return you the ID of the user inserted in the AcyMailing table

                //if ($subid) $response['mail'] = intval($mailer->sendOne(8, $userData['email']));

                $newSubscription = array();
                $newList = array();

                $newList['status'] = 2;
                $newSubscription[2] = $newList;
                $newSubscription[16] = $newList;

                $userClass->saveSubscription($subid, $newSubscription);

            }
            return $subid;
        }
    }
}
