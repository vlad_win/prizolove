<?php defined('_JEXEC') or die;

class FrontEventsController extends JControllerLegacy
{

    public function cron()
    {
        $cron = $this->getModel('Cron', 'FrontEventsModel');
        $cron->runCron();
        exit(0);
    }

    public function manualcron()
    {
        $cron = $this->getModel('Cron', 'FrontEventsModel');
        $cron->runCron();
        $this->setRedirect('/administrator/index.php?option=com_acyevents&view=events');
        return;
    }

}