<?php defined('_JEXEC') or die;

class AcyEventsControllerApi extends JControllerLegacy
{
    public function redirect()
    {
        header('content-type: application/json; charset=utf-8');
        JFactory::getApplication()->close();
    }

    public function certificate()
    {
        try{
            $cert_hash = JFactory::getApplication()->input->get('hash');
            $user = $this->getModel('certificate')->check($cert_hash);
            if(!$user) {
                throw new Exception('Sertificat not found');
            }
            echo new JResponseJson($user);
        }catch (Exception $e){
            echo new JResponseJson($e);
        }

    }

}