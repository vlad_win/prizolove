<?php defined('_JEXEC') or die;
class FrontEventsModelEvents extends JModelLegacy
{
    /**
     * @param string $name
     * @param string $prefix
     * @param array $options
     *
     * @return TableCertificate
     *
     * @since version
     * @throws Exception
     */
    public function getTable($name = 'Certificate', $prefix = 'Table', $options = array())
    {
        return parent::getTable($name, $prefix, $options);
    }

    public function getCertificate()
    {
        $table = $this->getTable();
        $table->certificate(10882);
        $table->get('certificate_id');
    }

}