<?php defined('_JEXEC') or die;

class FrontEventsModelCron extends JModelLegacy
{


	/**
	 *  1. Find the Array of Events
	 *      1.1 Date modification (Start & End data)
	 *      1.2
	 *
	 * @since version
	 * @throws Exception
	 */
	public function runCron()
    {
        $events = $this->_db->getQuery(true);
        $events->select('*')
            ->from('#__acyevents_event')
            ->where('start_at < NOW()')
            ->where('next_at < NOW()')
            ->where('UNIX_TIMESTAMP(next_at) != 0')
            ->where( 'published = 1');
        $this->_db->setQuery($events);
	    $events = $this->_db->loadObjectList();

	    foreach ($events as $event)
	    {
	    	$end_date = new DateTime(strtotime($events->next_at));
	    	$start_date = clone $end_date;
		    $start_date->modify('-1 '. AcyEventsHelper::getFrequency($event->frequency));

		    $find_certificate = $this->_db->getQuery(true);
		    $find_certificate->select(
		    	array(
				    '*',
				    'SUM(0) AS sum_pays',
				    'COUNT(certificate_id) AS the_count'
			    )
		    )
			    ->from('#__acyevents_certificate')
			    ->where('activated_at BETWEEN '.
				    $this->_db->q($start_date->format(DateTime::ATOM)) .
				    ' AND '.
				    $this->_db->q($end_date->format(DateTime::ATOM))
			    )
			    ->where('expired = 0')
			    ->order('RAND()');


		    $find_certificate = $this->_db->setQuery($find_certificate, 0, 1)->loadObject();

		    if($find_certificate->certificate_id !== null){
			    $update_certificate = new stdClass();
			    $update_certificate->expired = 1;
			    $update_certificate->certificate_id = $find_certificate->certificate_id;
//			    $this->_db->updateObject('#__acyevents_certificate', $update_certificate, 'certificate_id');
		    }


		    JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_acyevents/models', 'AcyEventsModel');
		    $find_winner = JModelLegacy::getInstance('Certificate', 'AcyEventsModel')->getCertificate($find_certificate->certificate_id);
			if($find_winner !== null){
				$mailer = AcyEventsHelper::sendWinner($find_winner, $event->acymailing_template_id);
			}


		    $table_event = $this->getTable('event');
		    $table_event->id = $event->id;
		    if($event->frequency == 0){
			    $table_event->next_at = 'NULL';
		    }else{
//			    $table_event->next_at = $end_date->modify('+1 '. AcyEventsHelper::getFrequency($event->frequency))->format(DateTime::ATOM);
		    }
//		    $table_event->store();
		    $table_event->hit($event->id);

		    $table_history = $this->getTable('history');
		    $table_history->save(
		    	[
				    'event_id' => $event->id,
					'user_id' => $find_winner->user_id,
				    'user_fund' => AcyEventsHelper::getFund($find_winner->sum_pays, $event->fund_percent, $event->fund_max),
				    'user_data' => json_encode([
				    	$find_winner->name,
					    $find_winner->phone,
					    $find_winner->email
				    ])
			    ]
		    );

	    }
	    var_dump($find_winner, $events, $mailer);
    }

}