<?php defined('_JEXEC') or die;

class FrontEventsModelCertificate extends JModelLegacy
{

	public function getCertificate()
	{
		$hash = JFactory::getApplication()->input->getString('hash', false);
		if(!$hash) return false;

		return $this->check($hash);

	}


	public function check($hash)
    {
        $table_users = $this->getTable('users');
        if($table_users->load(['user_sertificate' => $hash]) ) {
            return array(
                'user_name' => $table_users->user_name,
                'user_phone' => $table_users->user_phone,
                'user_mail' => $table_users->user_mail,
                'created_at' => $table_users->created_at
            );
        }else{
            return false;
        }
    }

    public function addNew($id)
    {

        $acy_certificate = new stdClass();
	    $acy_certificate->certificate_id = intval($id);
	    $this->_db->insertObject('#__acyevents_certificate', $acy_certificate);

        $letter_id = JComponentHelper::getParams('com_acyevents')->get('acymailing_template_invite');

        AcyEventsHelper::sendCertificate($acy_certificate, $letter_id);
    }


}