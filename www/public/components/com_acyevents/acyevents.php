<?php defined('_JEXEC') or die('Restricted access');
/**
 * @var $controller \FrontEventsController
 */
JLoader::register('AcyEventsHelper',JPATH_ADMINISTRATOR.'/components/com_acyevents/helpers/acyevents.php');
$controller = JControllerLegacy::getInstance('FrontEvents');
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));
$controller->redirect();