<?php defined('_JEXEC') or die;

class FrontEventsViewWinner extends JViewLegacy
{
    public function display($tpl = null)
    {
		$this->winner = $this->get('Winner');
        return parent::display($tpl);
    }
}