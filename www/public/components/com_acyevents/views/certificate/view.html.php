<?php defined('_JEXEC') or die;

class FrontEventsViewCertificate extends JViewLegacy
{
	public $certificate;

    public function display($tpl = null)
    {
		$this->certificate = $this->get('Certificate');
        return parent::display($tpl);
    }
}