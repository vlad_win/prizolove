<?php                                                                                                   
defined('_JEXEC') or die('Restricted access.');
$app = JFactory::getApplication();
$cid = $app->input->get('cid', 1, 'INT');
$categories = PlLibHelperCatalog::getInstance()->getCategories();
$products = PlLibHelperCatalog::getInstance()->getProductsByCategoryId($cid);
?>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">


<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 breadcrumbs-block">
                <a href="https://prizolove.com/">Главная</a> - <span>Каталог товаров</span>
            </div>
            <?php /* <div class="col-12 col-sm-12 col-md-4 col-lg-4 search-block"><input type="search" placeholder="Поиск по товарам"><input type="submit" value=""></div> */ ?>
        </div>
    </div>
</div>
<div class="slider-block">
    <div class="container">
        <div class="row">
            <div class="col-4 category-block">
                <?php foreach($categories as $category) : ?>
                <div class="sub-link">
                    <div class="name">
                        <?=$category->svg;?>
                        <a href="<?=JRoute::_('index.php?option=com_prizolove&view=catalog&cid='.$category->id);?>"><?=$category->name;?></a>
                    </div>
                </div>
                <?php endforeach; ?>







            </div>

        </div>
    </div>
</div>
<div class="catalog-block">
    <div class="container">
        <div class="row product-blocks">
            <?php foreach($products as $product) :?>
            <div class="col-12 col-md-3 col-sm-12 col-lg-3">
                <div class="product">
                    <a href="/orderpage?pid=<?=$product->id;?>" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <?php /* <div class="top-icon">Топ продаж</div> */ ?>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><?php if ($product->image) :?><img src="<?=$product->image;?>"><?php endif;?></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper"><?=$product->name;?></div>
                        <div class="price-wrapper"><?=$product->price;?> <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>

</div>
<?php /*
<div class="sale-block">
    <div class="container">
        <div class="row">
            <div class="col-12 title-block"><h2>Специальные предложения</h2></div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 col-sm-12 col-lg-6"><div class="banner-block"></div></div>
            <div class="col-12 col-md-3 col-sm-12 col-lg-3"><div class="banner-block-2"></div></div>
            <div class="col-12 col-md-3 col-sm-12 col-lg-3"><div class="banner-block-3"></div></div>
        </div>
        <div class="row">
            <div class="col-12"><div class="partner-block"></div></div>
        </div>
    </div>
</div> */ ?>
<?php /*
<div class="foryou-block">
    <div class="container">
        <div class="row">
            <div class="col-12 title-block"><h2>Рекомендуем вам</h2></div>
        </div>
        <div class="row">
            <div class="col-12"><div class="tabs">
                    <a href="#" class="active">Все категории</a> <a href="#">Часы</a> <a href="#">Все для кухни</a>
                    <a href="#">Красота и здоровье</a> <a href="#">Для водителей</a> <a href="#">Аксессуары</a> <a href="#">Гаджеты и техника</a> <a href="#">Зоотовары</a> <a href="#">Для дома и дачи</a> <a href="#">Спортивные товары</a>
                </div></div>
        </div>
    </div>
</div>
<div class="catalog-block-2 catalog-block">
    <div class="container">
        <div class="row product-blocks">
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">4 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">1 строка</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row product-blocks">
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
*/ ?>