<?php                                                                                                   
defined('_JEXEC') or die('Restricted access.');
$app = JFactory::getApplication();
$pid = $app->input->get('pid', 1, 'INT');
$product = PlLibHelperCatalog::getInstance()->getProductById($pid);
if (!$product) {
    JError::raiseError(404, JText::_("Page Not Found"));
}

//$categories = PlLibHelperCatalog::getInstance()->getCategories();
//$products = PlLibHelperCatalog::getInstance()->getProductsByCategoryId($cid);
?>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,900" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">

<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a href="https://prizolove.com/">Главная</a> - <a href="https://prizolove.com/186">Каталог товаров</a> - <span><?=$product->name;?></span>
            </div>
        </div>
    </div>
</div>
<div class="product-block">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1><?=$product->name;?></h1>

            </div>

        </div>
        <div class="row product-wrapper">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 image-wrapper"><?php if ($product->image) :?><img src="<?=$product->image;?>"><?php endif;?></div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 about-product-wrapper">
                <div class="price-block">
                    <div class="price">
                        <span class="valute">$</span>
                        <span class="number"><?=$product->price_usd;?></span>


                    </div>
                    <?php /* <div class="quantity"><a href="#" class="plus"></a><input class="input-text"  value="1" type="text" ><a href="#" class="minus"></a></div> */ ?>

                    <br/>
                    <a class="button" href="/orderpage?pid=<?=$product->id;?>"><img src="https://prizolove.com/images/cart-catalog.png">Купить</a>
                </div>
                <div class="sertificate-block"><img src="https://prizolove.com/images/diamond_red.png"> Дает право на участие в розыгрыше 10 000$<a href="#" class="see-more">Узнать больше</a></div>
                <div class="height"></div>
                <div class="money-block"><b>Оплата</b> Наличными, Visa/MasterCard</div>
                <?php /* <div class="share-block"><b>Поделитесь с друзьями</b>, каждая покупка по этой ссылке даст Вам 3$ <div class="social-buttons"><a href="#" class="social-icon"><i class="fab fa-facebook-f"></i></a> <a href="#" class="social-icon"><i class="fab fa-instagram"></i></a> <a href="#" class="social-icon"><i class="fab fa-odnoklassniki"></i></a><a href="#" class="social-icon"><i class="fab fa-vk"></i></a></div> */ ?>
                </div> </div>
        </div>
    </div>
</div>
<div class="about-block">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title">Описание продукта</div>
                <div class="text">
                    <?=$product->full_description;?>
                </div>
            </div>
        </div>

    </div>

</div>
