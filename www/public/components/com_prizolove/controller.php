<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$my = JFactory::getUser();
/* if(empty($my->id)){
	$usercomp = 'com_users';
	$uri = JFactory::getURI();
	$url = '/login?return='.base64_encode($uri->toString());	
	$app = JFactory::getApplication();
	$app->redirect($url);
	return false;
} */

class PrizoloveController extends JControllerLegacy
{
    public function display($cachable = false, $urlparams = false)
    {
        // Get the document object.
        $document = JFactory::getDocument();
        $app = JFactory::getApplication();
        $pid = $app->input->get('pid', 0, 'INT');
        $user = JFactory::getUser();

        $view = $this->getView( 'catalog', 'html' );
		$vName   = $this->input->getCmd('view', 'catalog');
		if ($pid) {
            $view = $this->getView( 'product', 'html' );
            $vName   = $this->input->getCmd('view', 'product');
        }

		
		$vFormat = $document->getType();
        $lName   = $this->input->getCmd('layout', 'default');


        $view->setLayout($lName);
        $view->display();



        //parent::display($cachable);

        //return $this;


    }
}
