<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$my = JFactory::getUser();
if(empty($my->id)){
	$usercomp = 'com_users';
	$uri = JFactory::getURI();
	$url = '/login?return='.base64_encode($uri->toString());	
	$app = JFactory::getApplication();
	$app->redirect($url);
	return false;
}

class PrizoloveProfileController extends JControllerLegacy
{
    public function display($cachable = false, $urlparams = false)
    {
        // Get the document object.
        $document = JFactory::getDocument();
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
		
		$vName   = $this->input->getCmd('view', 'profile');

		
		$vFormat = $document->getType();
        $lName   = $this->input->getCmd('layout', 'default');        

        if ($view = $this->getView($vName, $vFormat))
        {
			switch ($vName)
				{
					case 'profile':
                        $model = $this->getModel('Orders');
                        $view->setModel($model, true);
					break;
					case 'accounts':
						if (!in_array(15, $user->groups)) {
							$app->redirect('/personal');
						}

				}
		}



        parent::display($cachable);

        return $this;
			
		$view->setLayout($lName);

        // Push document object into the view.
		$view->document = $document;

        $view->display();	


    }
}
