<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument();

unset($doc->_scripts[$this->baseurl . 'media/jui/js/bootstrap.min.js']); // Remove joomla core bootstrap
//unset($doc->_scripts[$this->baseurl . '/media/jui/js/bootstrap.min.js']); // Remove joomla core bootstrap
unset($doc->_styleSheets[$this->baseurl . 'media/jui/css/bootstrap.min.css']);
$this->helix3 = helix3::getInstance();
$this->helix3->addCSS('bootstrap4.min.css')// CSS Files
->addJS('bootstrap4.min.js');// JS Files
JLoader::import('pl_lib.library');

//$events = PlLibHelperEvents::getInstance()->getEvents();

$user = JFactory::getUser();
$parentId = PlLibHelperAffiliate::getParent();
$events = PlLibHelperEvents::getInstance()->getLatestEvents();
$products = PlLibHelperPersonal::getInstance()->getLatestProducts();

//$doc = JFactory::getDocument();
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagebuilder.css');
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagecontainer.css');
?>
<div class="profile-body">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 col-sm-12">
                <div class="block-list-akcii">
                    <div class="title-block">
                        <div class="title">Список доступных Вам акций
                            <?php /* <div class="parent-qa"><i class="fas fa-question-circle question4-icon"></i>
                                <span class="question-4">Количество покупок совершенных покупателями вашей сети.</span></div> */ ?>
                        </div>
                        <?php /* <div class="count">2 акции завершено</div> */ ?>
                    </div>
                    <div class="body text-center">
                        <?php foreach($events as $event) : ?>
                            <a href="<?=$event->url?>" class="akciya">
				            <div class="product_image_wrapper">
                                <?php if ($event->image) : ?>
                                    <img src="/<?=$event->image;?>">
                                <?php else: ?>
                                    <img src="https://prizolove.com/images/second.jpg">
                                <?php endif; ?>
				            </div>
                            <div class="name"><?=$event->name;?></div>
                            <?php if ($event->difficulty) : ?>
                            <div class="slojnost">Сложность: <?=$event->difficulty;?></div>
                            <?php else: ?>
                            <div class="slojnost">Сложность: низкая</div>
                            <?php endif; ?>
                            </a>
                        <?php endforeach?>
                        <a href="/personal/events" class="btn-see-more">Посмотреть все акции</a>

                    </div>
                </div>
                <div class="block-list-vitrina">
                    <div class="title-block">
                        <div class="title">Список доступных Вам товаров
                            <?php /* <div class="parent-qa"><i class="fas fa-question-circle question4-icon"></i>
                            <span class="question-4">Количество покупок совершенных покупателями вашей сети.</span></div> */ ?>
                        </div>
                    </div>
                    <div class="body text-center">
                        <?php foreach($products as $product) : ?>
                            <a href="<?=($product->landing)?($product->landing):'/186';?>" class="product_block_wrapper">
                                <?php /* <div class="free">Только в Prizolove</div> */ ?>
                                <div class="diamond_product"></div>
                                <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                                <div class="product_image_wrapper">
                                <?php if ($product->image) :?>
                                    <img class="product_image" src="<?=$product->image;?>">
                                <?php endif; ?>
                                </div>

                                <div class="title_name"><?=$product->name;?></div>
                                <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                                <div class="price">от <?=$product->price;?> грн.</div>

                            </a>
                        <?php endforeach; ?>
                        <?php /*
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/kitchen1.png">
                            <div class="title_name">Набор для кухни</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 407 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">
                            <div class="free">Только в Prizolove</div>
                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/kitchen1.png">
                            <div class="title_name">Набор для кухни</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 407 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a> */ ?>
                        <a href="/186" class="btn-see-more">Посмотреть все товары</a>

                    </div>
                </div>
                <?php /*
                <div class="block-list-vitrina">
                    <div class="title-block">
                        <div class="title">Список ваших последних покупок   <div class="parent-qa"><i class="fas fa-question-circle question4-icon"></i>
                                <span class="question-4">Количество покупок совершенных покупателями вашей сети.</span></div></div>
                    </div>
                    <div class="body text-center">

                        <a href="#" class="product_block_wrapper">
                            <div class="free">Только в Prizolove</div>
                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/kitchen1.png">
                            <div class="title_name">Набор для кухни</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 407 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a>

                    </div>
                </div>
                <div class="block-banner-partner">
                    <a href="/350" target="_blank"></a>
                    <div class="text">Пассивный доход <br/>от 500$/месяц</div><div class="button">Стать партнером Prizolove</div>
                </div>
                */ ?>
                <br>
                </div>
                <div class="col-12 col-md-3 col-sm-12">
                    <?php /* <div class="block-share">
                         <div class="title text-center">
                            <div class="rating-parent">
                                <img src="https://prizolove.com/images/karat.png">
                                <div class="right-block"><span class="count">485</span><span class="text">Бонусов заработано</span></div>

                            </div>
                            <div class="rating-parent">
                                <img src="https://prizolove.com/images/growth-profile.png">
                                <div class="right-block"><span class="count">1171</span><span class="text">Призов выиграно</span></div>

                            </div>
                                Поделитесь с друзьями получите деньги   <div class="parent-qa"><i class="fas fa-question-circle question4-icon"></i>
                                <span class="question-4">Количество покупок совершенных покупателями вашей сети.</span></div></div>
                        <div class="buttons">
                            <a href="#" class="btn-share share-vk"><i class="fab fa-vk"></i> Вконтакте</a>
                            <a href="#" class="btn-share share-ok"><i class="fab fa-odnoklassniki"></i> Одноклассники</a>
                            <a href="#" class="btn-share share-fb"><i class="fab fa-facebook-f"></i> Facebook</a>
                            <a href="#" class="btn-share share-friend"><i class="fas fa-share-alt"></i> Отправить ссылку</a>
                        </div>
                    </div>
                    <div class="block-facebook-group"></div>
                    */ ?>
                </div>




        </div>

    </div>
</div>








