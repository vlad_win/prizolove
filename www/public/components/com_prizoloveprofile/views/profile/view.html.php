<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class PrizoloveProfileViewProfile extends JViewLegacy
{

	function display($tpl = null)
	{
		$items = $this->get('Items');
        $pagination = $this->get('Pagination');
        $total = $this->get('Total');
        $limit = $this->get('Limit');
        $limitstart = $this->get('Limitstart');

        $this->assignRef('items', $items);
        $this->assignRef('pagination', $pagination);
        $this->assignRef('total', $total);
        $this->assignRef('limit', $limit);
        $this->assignRef('limitstart', $limitstart);


		// Assign data to the view
		$this->msg = 'Profile';

		// Display the view
		parent::display($tpl);
	}
}
