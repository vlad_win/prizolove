<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
//$doc = JFactory::getDocument();
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagebuilder.css');
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagecontainer.css');
?>

<div id="order-table" class="sppb-section header-table">
    <div class="sppb-row sppb-no-gutter">
        <div class="sppb-col-md-1 sppb-text-center"><strong>ID</strong></div>
        <div class="sppb-col-md-2 sppb-text-center"><strong>Дата заказа</strong></div>
        <div class="sppb-col-md-5 sppb-text-center"><strong>Информация о заказе</strong></div>
        <div class="sppb-col-md-2 sppb-text-center"><strong>Статус заказа</strong></div>
        <div class="sppb-col-md-2 sppb-text-center"><strong>Комментарий</strong></div>
    </div>
</div>
<?php
$styles = array(
        'sppb-col-md-1',
        'sppb-col-md-2',
        'sppb-col-md-5',
        'sppb-col-md-2',
        'sppb-col-md-2'
);

$cols = array(
        'order_id',
        'date',
        'product_name',
        'sent',
        'customer_comment'
);


if (!empty($this->items[0])) {
    $numColumns = count($this->items[0]);
}
?>
    <?php foreach($this->items as $item) : ?>
    <div class="sppb-section table-length">
        <div class="sppb-container-inner">
            <div class="sppb-row sppb-no-gutter">
                    <?php for($j=0; $j < 5; $j++) : ?>
                        <div class="<?=$styles[$j];?>">
                            <div class="sppb-column sppb-align-items-center">
                                <div class="sppb-column-addons">
                                    <div class="clearfix">
                                        <div class="sppb-addon sppb-addon-text-block 0 sppb-text-center">
                                            <div class="sppb-addon-content">
                                                <?php
                                                echo $item->{$cols[$j]};
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>

<div id="pagination-block" class="pagination center">
    <p class="counter pull-center"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
    <div class="pull-center"><?php echo $this->pagination->getPagesLinks(); ?></div>
</div>
