<?php                                                                                                   
defined('_JEXEC') or die('Restricted access.');
$doc = JFactory::getDocument();

unset($doc->_scripts[$this->baseurl . 'media/jui/js/bootstrap.min.js']); // Remove joomla core bootstrap
//unset($doc->_scripts[$this->baseurl . '/media/jui/js/bootstrap.min.js']); // Remove joomla core bootstrap
unset($doc->_styleSheets[$this->baseurl . 'media/jui/css/bootstrap.min.css']);
$this->helix3 = helix3::getInstance();
$this->helix3->addCSS('bootstrap4.min.css')// CSS Files
->addJS('bootstrap4.min.js');// JS Files
JLoader::import('pl_lib.library');
require_once(JPATH_ROOT.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');


$user  = JFactory::getUser();
$paid = (in_array(16, $user->groups))?true:false;

$children = PlLibHelperAffiliate::getChildren();

$totals = PlLibHelperAffiliate::getTotal();

$parentId = PlLibHelperAffiliate::getParent();

$orders = PlLibHelperAffiliate::getOrders($parentId);

$totalLeads = PlLibHelperAffiliate::getTotalLeads($parentId);

$levelOneChildren = PlLibHelperAffiliate::getLevelOneChildren($parentId);

$levelTwoChildren = array();
if (count($levelOneChildren)) $levelTwoChildren = PlLibHelperAffiliate::getLevelTwoChildren($levelOneChildren);

$levelTwoSales = PlLibHelperAffiliate::getLevelTwoSales($parentId);

$levelTwoComission = PlLibHelperAffiliate::getLevelTwoComission($parentId);

$expireDays = PlLibHelperAffiliate::getExpireDays();

$totalComission = PlLibHelperAffiliate::getTotalComission($user);

$totalSales = PlLibHelperAffiliate::getTotalSales($user);



?>
  
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<script>
    jQuery(document).ready(function() {
	   jQuery('.ref-links-button').on('click', function() {
		   jQuery('.ref-links-accordion').toggleClass("ref-links-active");
	   });
       jQuery('.copy').on('click', function() {
		   var $this = jQuery(this);
           var $temp = jQuery("<input>");
           jQuery("body").append($temp);
           $temp.val(jQuery(this).closest('div.link-ref-block').text()).select();
           document.execCommand("copy");
           $this.notify('Скопировано', {
               position: "top right",
               autoHideDelay: 250,
               className: 'success',
               showDuration: 100,
               arrowShow: false
           });
           $temp.remove();
       });
    });
</script>
<style>
*{font-family:"Montserrat",sans-serif;transition: .5s !important;text-decoration:none !important;}
body{position:relative;overflow-x:hidden;}

.overlay{position:absolute;top:0;left:0;width:100%;height:110%;background:rgba(0,0,0,0.35);z-index:-1;}


.popup_registration, .popup_enter, .popup_lost, .popup_thankyou{overflow:hidden;position:relative;width:100%;border-radius:15px;padding:20px;max-width:400px;margin:50px auto; background:white;text-align:center;font-family:Montserrat,Arial,sans-serif;box-shadow:0px 0px 30px rgba(0,0,0,0.15);}

.close_popup{position:absolute;top:10px;right:10px;color:white;z-index:99999;font-weight:bold;background:white;color:#212121;width:30px;height:30px;font-size:14px;line-height:30px;border-radius:50%;transition:1s;}
.close_popup:hover{background:#e74c3c;color:white;transform: rotate(360deg);}

.popup_registration .title, .popup_enter .title, .popup_lost .title{color:#212121;font-weight:700;font-size:20px;}
.popup_registration .text{color:#212121;padding:20px 0;font-weight:500;}
.popup_registration .btn_winner,.popup_registration .btn_partner{text-align:left;display:block;width:100%;border:2px solid #dfdfdf;border-radius:10px;padding:10px 0;text-decoration:none;font-size:16px;font-weight:500;color:#212121;margin-bottom:20px;transition:.5s;}
.popup_registration .btn_winner:hover{color:#ff5754;border:2px solid #FF5754;}
.popup_registration .btn_partner:hover{color:#825d75;border:2px solid #825d75;}

.popup_registration, .popup_thankyou{padding:120px 20px 20px 20px;}

.popup_registration::before, .popup_thankyou::before{content:"";position:absolute;top:-25px;left:0;display:block;max-width:400px;width:100%;height:150px;background:url(https://prizolove.com/images/header-popup.png) no-repeat center center;background-size:contain;}

.popup_registration .enter_in, .popup_enter .enter_in, .popup_enter .lost_password, .popup_lost .enter_in{color:#212121;font-size:14px;font-weight:500;}
.popup_registration .enter_in a, .popup_enter .enter_in a,.popup_enter .lost_password a, .popup_lost .enter_in a{text-decoration:none;color:#48B750;font-weight:700;transition:.5s;}
.popup_registration .enter_in a:hover, .popup_enter .enter_in a:hover,.popup_enter .lost_password a:hover, .popup_lost .enter_in a:hover{color:#147334;}
.btn_winner img, .btn_partner img{width:40px;margin:0 20px;vertical-align:middle;}

.popup_enter .lost_password{margin-top:10px;}


.popup_enter .inputs, .popup_lost .inputs, .popup_registration .inputs{text-align:left;margin:30px 0px 15px 0;}
.popup_enter label, .popup_lost label, .popup_registration label{color:#39414b;font-size:16px;font-weight:600;}
.popup_enter input, .popup_lost input, .popup_registration input{width:100%;border-radius:10px;outline:none !important;padding:15px 0px 15px 15px;box-shadow:none;border:2px solid #dfdfdf;transition:.5s;margin:5px auto 20px auto;background:white !important;font-family:Montserrat,Arial,sans-serif;font-size:16px;font-weight:500;color:#212121;}
.popup_enter input:hover, .popup_lost input:hover, .popup_registration input:hover{border:2px solid #48B750;}
.popup_enter input:focus, .popup_lost input:focus, .popup_registration input:focus{border:2px solid #48B750;}

.popup_enter .btn-form, .popup_lost .btn-form, .popup_registration .btn-form{display:block;width:100%;text-align:center;background:#48B750;border-radius:10px;padding:15px 0;color:white;font-size:18px;font-weight:600;text-decoration:none;transition:.5s;}
.popup_enter .btn-form:hover, .popup_lost .btn-form:hover, .popup_registration .btn-form:hover{background:#147334;}


.header{padding:10px 0 !important;background:#34495e;font-family:Montserrat,Arial,sans-serif;}

.name-user{color:white;font-weight:500;font-family:Montserrat,Arial,sans-serif;font-size:18px;}
.inline{display:inline-block;vertical-align:middle;}
.inline img{margin-left:20px;}
.inline a{margin-left:20px;transition:.5s;}
.inline a span{color:white;border-bottom:1px dotted white;font-size:12px;}
.inline i{border-bottom:0px;color:white;font-size:12px;}
.inline a:hover{opacity:0.6;text-decoration:none;}

.enters{text-align:right;}
.enters img{width:20px;margin-right:10px;vertical-align:middle;}
.enters a{color:#ecf0f1;text-decoration:none;font-size:14px;display:inline-block;font-weight:600;padding: 7px 0;transition:.5s;}
.enters a:hover{color:#FF5754;}
.popup_thankyou img{width:100px;
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;    -webkit-animation-name: bounceIn;
    animation-name: bounceIn;position:relative;z-index:2;}
.popup_thankyou .image::before,.popup_thankyou .image::after{
    content:"";
    position:absolute;
    display:block;
    width:100px;
    height:100px;
}
.popup_thankyou .title{font-weight:700;font-size:21px;color:#3FB659;padding:20px 0;}
.popup_thankyou .text{font-size:14px;line-height:18px;font-weight:500;}
.popup_thankyou .image::before{left:0;top:120px;background:url(https://prizolove.com/images/firework-left-popup.png) no-repeat center center;background-size:contain;transform:translateX(100px);-webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;    -webkit-animation-name: leftFadeIn;
    animation-name: leftFadeIn;}
.popup_thankyou .image::after{right:0;top:120px;
    background:url(https://prizolove.com/images/firework-right-popup.png) no-repeat center center;background-size:contain;transform:translateX(-100px);-webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;    -webkit-animation-name: rightFadeIn;
    animation-name: rightFadeIn;}

@-webkit-keyframes bounceIn {
    0% {
        opacity: 0;
        -webkit-transform: scale(.3);
    }
    30% {
        opacity: 1;
        -webkit-transform: scale(1.15);
    }
    60% {
        -webkit-transform: scale(.9);
    }
    85% {
        opacity: 1;
        -webkit-transform: scale(1.15);
    }


    100% {
        -webkit-transform: scale(1);
    }
}

@keyframes bounceIn {
    0% {
        opacity: 0;
        transform: scale(.3);
    }
    30% {
        opacity: 1;
        transform: scale(1.15);
    }
    60% {
        transform: scale(.9);
    }
    85% {
        opacity: 1;
        -webkit-transform: scale(1.15);
    }
    100% {
        transform: scale(1);
    }
}



@-webkit-keyframes leftFadeIn {
    0% {
        opacity: 0;
        transform:translateX(100px);
    }


    100% {
        opacity: 1;
        transform:translateX(25px);
    }
}

@keyframes leftFadeIn {
    0% {
        opacity: 0;
        transform:translateX(100px);
    }


    100% {
        opacity: 1;
        transform:translateX(50px);
    }
}


@-webkit-keyframes rightFadeIn {
    0% {
        opacity: 0;
        transform:translateX(-100px);
    }


    100% {
        opacity: 1;
        transform:translateX(-25px);
    }
}

@keyframes rightFadeIn {
    0% {
        opacity: 0;
        transform:translateX(-100px);
    }


    100% {
        opacity: 1;
        transform:translateX(-50px);
    }
}


.sidebar-cabinet{position:absolute;height:100%;background:#34495e;width:7%;top:0;left:0;box-shadow:0px 2px 6px rgba(0,0,0,0.15);z-index:99999;text-align:center;min-height:100vh;}
.sidebar-cabinet img{width:50px;}
.sidebar-cabinet .menu-link{padding:10px;color:white;font-weight:700;display:flex;justify-content:center;min-height:110px;align-items:center;Flex-wrap:wrap;font-size:13px;}
.sidebar-cabinet .menu-link:hover{background:rgba(255,255,255,0.05);}
.sidebar-cabinet .active-menu{background:rgba(255,255,255,0.05);}
.sidebar-cabinet .active-menu .img-wrapper img{-webkit-filter: none;
  -moz-filter: none;
  -ms-filter: none;
  -o-filter: none;
  filter: none;
  filter: none; /* IE 6-9 */}
.sidebar-cabinet .menu-link:hover img{-webkit-filter: none;
  -moz-filter: none;
  -ms-filter: none;
  -o-filter: none;
  filter: none;
  filter: none; /* IE 6-9 */}
.sidebar-cabinet .img-wrapper img{-webkit-filter: grayscale(100%);
  -moz-filter: grayscale(100%);
  -ms-filter: grayscale(100%);
  -o-filter: grayscale(100%);
  filter: grayscale(100%);
  filter: gray;}

.img-wrapper{width:100%;}

.header{padding:20px 0;background:#34495e;}
.user-block{text-align:right;}
.name-user{color:white;font-weight:500;font-family:"Montserrat",sans-serif;font-size:18px;}
.inline{display:inline-block;vertical-align:middle;}
.inline img{margin-left:20px;}
.inline a{margin-left:20px;}
.inline a span{color:white;border-bottom:1px dotted white;font-size:12px;}
.inline i{border-bottom:0px;color:white;font-size:12px;}
.inline a:hover{opacity:0.6;text-decoration:none;}
.title-block{background-color:#fff;padding:10px 0 0 0;position:relative; color: #000;}

.overlay-title-block{position:absolute;top:0;left:0;width:100%;height:100%;background:url(https://prizolove.com/images/pattern.png) top left;opacity:0.04;}

.title{color:#000;font-weight:700;font-family:"Montserrat",sans-serif; margin:0 !important;}

.affiliate-menu{width:100%;text-align:center;}
.affiliate-menu a{border-top-left-radius:10px;border-top-right-radius:10px;display:inline-block;width:32%;text-align:center;padding:10px 20px;background:rgba(0,0,0,0.2);color:#000;font-weight:500;font-family:"Montserrat",sans-serif;}
.affiliate-menu .active{background:white;color:black; border: 1px solid rgba(0,0,0,0.2); border-bottom: none;}

.affiliate-menu a:hover{background:white;color:black;}



.my-accounts-block{padding:20px 0;}
.stat-block{background:rgba(241, 196, 15,0.2);width:100%;text-align:center;border:1px solid #dbca9d;border-radius:10px;}
.stat-block div{width:33%;vertical-align:top;display:inline-block;padding:20px 0;}
.stat-block .count{font-weight:700;font-size:40px;line-height:50px;color:#212121;}
.stat-block .color-block{margin:5px 0;display:inline-block;padding:10px;text-transform:uppercase;font-weight:bold;border-radius:5px;color:white;font-size:12px;}
.ref{background:rgba(52, 152, 219,0.9);text-shadow:1px 2px 2px #3498db;}
.convers{background:rgba(211, 84, 0,0.9);text-shadow:1px 2px 2px #d35400;}
.com{background:rgba(142, 68, 173,0.9);text-shadow:1px 2px 2px #8e44ad;}

.stat-block .days{font-size:12px;font-weight:500;color:black;opacity:.5;}


.table-affiliate .row{text-align:left;background:white;font-family:"Montserrat",sans-serif;font-weight:bold;color:#2c2b2b;border-radius:7px;padding:15px 0px 15px 0px ;box-shadow:0px 2px 15px rgba(0,0,0,0.25);margin-bottom:10px;}

/*.tabs-block{display:none;}*/
.links-ref-block{text-align:center;margin:0;color:#000;}
.title-ref{position:relative;text-align:center;font-weight:bold;padding:10px;font-size:21px;color:white;}
.copy{display:block;padding:5px 8px 5px 8px;position:absolute;top:50%;right:20px;text-align:center;margin-top:2px;/* color:white !important;background:#7a231d; */}
.copy:hover{ /* background:#a03531; */}
.copy:active{transform:scale(1.5);}
.link-ref{padding:10px;font-weight:500;font-size:21px;}
.link-ref-block{margin-top:10px;padding:10px 0;font-size:18px;}


.spravka{text-align:left;background:#f1c40f;color:#272727 !important;font-size:14px;line-height:16px;font-weight:bold;display:inline-block;padding:20px 10px 20px 60px;border-radius:10px;box-shadow:0px 3px 0px #d6aa26;position:relative;}
.spravka:hover{background:#F2C437;}
.spravka:active{
    box-shadow:none;
    background-color: #F2C437;
    border-color: rgb(129, 33, 19);
    border-top-width: 3px;
    border-bottom-width: 1px;
    box-shadow: inset 0px 2px 2px 0px rgba(0, 0, 0, 0.25);
    transform:translateY(2px);
}
.spravka::before{content:"";display:block;width:40px;height:40px;background:url(https://prizolove.com/images/agenda.png) no-repeat center center;background-size:contain;position:absolute;top:50%;left:10px;transform:translateY(-50%);}


.rows-table-affiliate .container{background:#FDF3D7;font-family:"Montserrat",sans-serif;font-weight:500;color:#2c2b2b;border-radius:7px;padding:0px 15px 0px 15px;box-shadow:0px 2px 15px rgba(0,0,0,0.25);margin-bottom:20px;overflow:hidden;}

.rows-table-affiliate .row{padding:10px 0;border-bottom:1px solid rgba(0,0,0,0.10);}

.prise-upper{color:#08CB76;font-weight:bold;}





.parent-qa{width:20px !important;padding-bottom:0px !important;display:inline-block;position:relative;}
.question1-icon,.question2-icon,.question3-icon,.question4-icon,.question5-icon{opacity:0.5;cursor:pointer;z-index:9999;display:inline-block;}
.question-1, .question-2, .question-3, .question-4, .question-5{position:absolute;width:300px !important;max-width:300px !important;font-size:14px;background:white;border-radius:10px; color:#212121;padding:10px;box-shadow:0px 0px 15px rgba(0,54,255,0.19); transform:translateY(-15px);transition:.5s;font-weight:500; z-index: 999999999; visibility: hidden; opacity: 0;}

.question-1{top:-360%;left:-135px;}
.question-2{top:-500%;left:-202px;width:450px !important;max-width:450px !important;}
.question-3{top:-280%;left:-135px;}
.question-4{top:-120%;left:-135px;}
.question-5{top:-90%;left:-135px;}
.question-1::after, .question-2::after, .question-3::after, .question-4::after , .question-5::after{
    content: ''; 
    position: absolute; /* Абсолютное позиционирование */
    left: 45%; bottom: -20px; /* Положение треугольника */
    border: 10px solid transparent; /* Прозрачные границы */
    border-top: 10px solid white; /* Добавляем треугольник */
   }

@media screen and (min-width:769px){
	.question1-icon:hover ~ .question-1, .question2-icon:hover ~ .question-2, .question3-icon:hover ~ .question-3, .question4-icon:hover ~ .question-4, .question5-icon:hover ~ .question-5{opacity:1 !important;transform:translateY(0px) !important;visibility:visible;} 
}
.question1-icon:hover ~ .question-1, .question2-icon:hover ~ .question-2, .question3-icon:hover ~ .question-3, .question4-icon:hover ~ .question-4, .question5-icon:hover ~ .question-5{opacity:1 !important;transform:translateY(0px) !important;visibility:visible;} 

.money-btn{background:#e74c3c;padding:10px 45px;display:inline-block;font-size:14px;color:white !important;text-transform:uppercase;font-weight:bold;border-radius:10px;margin:0px 0px 5px 0px;opacity:0.6;box-shadow:0px 3px 0px #c0392b;transition:.5s;}
.money-btn-a:hover{background:#c0392b;}
.money-btn-a:active{box-shadow:none;transform:translateY(2px);}





.alert-min0{position:relative;width:auto !important;padding-bottom:0px !important;display:inline-block;position:relative;padding-top:0px !important;}
.alert-block{position:absolute;top:50px;left:-70px;position:absolute;width:330px !important;font-size:14px;background:white;border-radius:10px;color:#212121;padding:20px;box-shadow:0px 0px 15px rgba(0,54,255,0.19);  /* transform:translateY(-15px);transition:.5s;*/z-index:9999;font-weight:500;}

.alert-block a{color:#3498db;border-bottom:1px solid #3498db;}
.alert-block a:hover{color:#2980b9;border-bottom:1px solid #2980b9;}

.alert-block::after{
    content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */
    left: 40%; top: -30px;
    border: 20px solid transparent;	border-bottom: 20px solid white;
  }

@media screen and (min-width:768px){.ref-links-accordion {
    margin:5px 0;
}}
                     
@media screen and (max-width:767px){
  .link-ref,.link-ref-block{font-size:16px !important;}
  .avatar-block{display:none !important;}
  .inline{width:100%;}
  .title-block{text-align:center;}
  .title-block .title{font-size:25px;}
  .links-ref-block{margin:10px auto;}
  .title-block .spravka{display:none;}
  .affiliate-menu a{width:100% !important;border-radius:10px;margin:5px 0;}
  .stat-block div{width:100%;}
  .link-ref-block .copy{position:relative;top:0;left:0;}
  .table-affiliate{font-size:10px;margin-top:20px;}
  .rows-table-affiliate{font-size:10px;}
  .question-1{left:-200px;}
  .question-2{width:300px !important;left:-180px;top:-550%;}
  .question-1, .question-2, .question-3, .question-4, .question-5, .alert-block{font-size:12px;height:auto;}
  .sidebar-cabinet{display:none;}
  .question-0 {top: 50px !important; left: -350px !important; }
  .question-5 { left: -250px !important; }
}
@media screen and (max-width:768px){
    .money-btn{padding:10px 20px;font-size:10px;}
    .stat-block .color-block{font-size:10px;}
    .stat-block div{width:32%;}
    .stat-block .count{font-size:30px;}
    .stat-block .days{font-size:8px;}
    .ref-links-accordion label { font-size: 16px;}
    .my-accounts-block {padding: 10px 0;}
    .ref-links-active article { height: 600px !important;}
    .ref-links-accordion label{font-size:16px !important;}
    .title{font-size:16px !important;}
    .name-user{font-size:16px !important}
    .title-block {padding:0px !important;}
    .ref-links-accordion {margin: 0px 0 !important;}
}
                     
@media screen and max-width(768px) and min-width(445px){
	.ref-links-active article { height: 280px; height: 280px; }
    .ref-links-accordion input:checked ~ article {height: 280px; height: 280px;}
}


.question-2 {
    top: -180px;
    z-index:9999;
}
.question-1 {
    top: -135px;
    z-index:9999;}



@media screen and (max-width:445px){
	.ref-links-active article{height: 950px !important;}
    .ref-links-accordion input:checked ~ article { height: 305px;}
}
.alert-min0{position:relative;width:auto !important;padding-bottom:0px !important;display:inline-block;position:relative;padding-top:0px !important; border}
.alert-block{position:absolute;top:140%;left:-50%;position:absolute;width:330px !important;font-size:14px;background:white;border-radius:10px;color:#212121;padding:20px;box-shadow:0px 0px 15px rgba(0,54,255,0.19); /* transform:translateY(15px);transition:.5s */; z-index:99999;font-weight:500; visibility: hidden;opacity:0;}
.alert-block:hover {opacity: 1; visibility: visible;}
.alert-block a{color:#3498db;border-bottom:1px solid #3498db;}
.alert-block a:hover{color:#2980b9;border-bottom:1px solid #2980b9;}

.alert-block::after{
    content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */
    left: -35px; bottom: 40px;
    border: 20px solid transparent;	border-bottom: 20px solid white;
   }
.alert-block.left::before{    	
    left: 140px; top: -40px;    
	content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */    
    border: 20px solid transparent;	border-bottom: 20px solid white;
}   
.alert-block.left::after{        
	content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */
    left: 0; bottom: 0;
    border: 0;
}   
a.money-btn:hover ~ .alert-block {opacity:1 !important;transform:translateY(0px) !important;visibility:visible;}

.links-ref-block{text-align:center;border-bottom-left-radius:10px;border-bottom-right-radius:10px;color:#000;}
.title-ref{position:relative;text-align:center;font-weight:bold;padding:10px;font-size:21px;color:white;}
.link-ref{padding:10px;font-weight:500;font-size:21px;}
.link-ref-block{margin-top:10px;padding:10px 0;font-size:18px;}

.parent-qa{width:5px !important;padding-bottom:0px !important;display:inline-block;position:relative;}


/* СТИЛИ ДЛЯ АККОРДЕОНА */

.ref-links-accordion{
    width: 100%;
}


.ref-links-accordion label{
    text-align:center;
    margin-bottom:0 !important;
    position:relative;
    z-index: 10;
    display: block;
    cursor: pointer;
    color: #000;
    font-size: 21px;
    padding:15px;
    font-weight:600;
    border-top-left-radius:10px;
    border-top-right-radius:10px;
    border-bottom-left-radius:10px;
    border-bottom-right-radius:10px;
    text-decoration: underline !important;	
}
.ref-links-accordion label:hover{
    text-decoration: none !important;	
}
/* .ref-links-accordion label:hover{background:#EB5449;} */



.ref-links-accordion input:checked + label,
.ref-links-accordion input:checked + label:hover{
    color: #000;
    border-top-left-radius:10px;
    border-top-right-radius:10px;
    border-bottom-left-radius:0px;
    border-bottom-right-radius:0px;

}


.ref-links-accordion input{
    display: none;
}

.ref-links-accordion article{
	background: #fff;
    overflow: hidden;
    height: 0;
    position: relative;
    z-index: 10;
    -webkit-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -moz-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -o-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -ms-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
}
.ref-links-accordion input:checked ~ article {
    -webkit-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -moz-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -o-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -ms-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    height: 250px;
    overflow: initial !important;
}

.ref-links-button{
    text-align:center;
	margin-bottom:0 !important;
	position:relative;
    
    display: block;
    cursor: pointer;
    color: #000;
    font-size: 16px;
	font-weight:500;
	text-decoration:underline !important;
	border-radius:10px;
}
.ref-links-button:hover{background:white;color:black;}
.ref-links-active article{
	background: #fff;
	-webkit-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -moz-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -o-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -ms-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    height: 500px;
    overflow: initial !important;
}
                 
                 
                .parent-qa-0{width:5px !important;padding-bottom:0px !important;display:inline-block;position:relative;}
.question0-icon{opacity:0.5;cursor:pointer;z-index:9999999 !important;display:inline-block;}
.question-0{position:absolute;width:500px !important;max-width:500px !important;font-size:14px;background:white;border-radius:10px;color:#212121;padding:10px;box-shadow:0px 0px 15px rgba(0,54,255,0.19);z-index:999999 !important; transform:translateX(15px);transition:.5s;opacity:0;visibility:hidden;font-weight:500;}

.question-0{top:-20px;left:40px;}

.question-0::after{
    content: ''; 
    position: absolute; /* Абсолютное позиционирование */
    left: -20px; top: 20px; /* Положение треугольника */
    border: 10px solid transparent; /* Прозрачные границы */
    border-right: 10px solid white; /* Добавляем треугольник */
   }


.parent-qa-0:hover .question-0{opacity:1 !important;transform:translateX(0px) !important;visibility:visible;}

.question-0 a{text-decoration:underline !important;}
            .question-0 .line{width:40px;height:2px;background:black;margin:10px 0;opacity:0.2;}

.title-block .title{display:inline-block;}
.align-vertical{vertical-align:middle !important;}

/* Новые стили */
.to-continue-alert{margin-top:20px;margin-bottom:-10px;}
.rows-table-affiliate .title{text-align:center;font-size:16px;}
.rows-table-affiliate .generation-2{background:#94cc9a !important;}
.rows-table-affiliate .generation-3{background:#ffa62e !important;}
.generation-parent-2 .container, .generation-parent-3 .container{ padding-left:0;padding-right:0;color:black;}
.generation-parent-2 .table-inner, .generation-parent-3 .table-inner{padding-left:15px;padding-right:15px;}

.rows-table-affiliate .stats{text-align:center;padding:20px 0;}
.generation-parent-2 .stats{background:#addbad;border-bottom:1px solid #94cc9a;}
.generation-parent-2 .table-inner{background:#addbad;}
.generation-parent-3 .stats{background:#ffb566;border-bottom:1px solid #d88329;}
.generation-parent-3 .table-inner{background:#ffb566;}
.table-inner{text-align:center !important;}
.rows-table-affiliate .stats div{display:inline-block;width:32%;text-align:center;font-size:16px;font-weight:bold;}
.alert-date{
    font-weight:500;
    border-left:10px solid #e16a6a;
    border-color: #e16a6a;
    color: #ce2d2d;
    background: #e7c3c3;
}
                                  
     .money-btn{
opacity:0.9;
     }                             
                                  
                                  
                                  
/* Popup container - can be anything you want */
.popup_hap2 {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* The actual popup */
.popup_hap2 .popuptext {
    visibility: hidden;
    width: 250px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
 
    margin-left: -200px;
}

/* Popup arrow */
.popup_hap2 .popuptext::after {
    content: "";
    position: absolute;
    top: 100%;

    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup_hap2 .show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
</style>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">



<div class="title-block">
    <div class="overlay-title-block"></div>
    <div class="container">
        <div class="row">

            <div class="col-sm-12 col-md-8 col-12 align-vertical">
                <h1 class="title">  Партнерская программа</h1> <div class="parent-qa-0"><i class="fas fa-question-circle question0-icon"></i>
     <span class="question-0">Суть проекта<br/>
- Мы проводим конкурсы и товарные акции.<br/>
- Мы продаем классные товары и разыгрываем призы среди тысяч участников.<br/>
- Наши Клиенты лояльны и количество их растет!<br/>
  <div class="line"></div>
- Для роста бизнеса нужно больше покупателей.<br/>
- А для роста количества клиентов нужна реклама.<br/>
- Для рекламы нужны деньги.<br/>
- Деньги на рекламные кампании мы привлекаем через краудфандинг. Это когда много людей скидываются по чуть-чуть. И получают долю от продаж.
  <div class="line"></div>
- Мы проводим всю работу по привлечению новых покупателей, продаже товаров и генерации дохода.<br/>
- Доход же разделяем между владельцами партнерских пакетов. То есть между вами.<br/>
<br/>
       <a href="https://prizolove.com/350" target="_blank">Подробнее</a></span></div>
            </div>

            <div class="col-sm-12 col-md-4 col-12 text-right">
				<a href="#" class="ref-links-button">Ваши реферальные ссылки</a>
            </div>
            
        </div>

        <section class="ref-links-accordion">
            <article>
                <div class="row-fluid links-ref-block">
			        <div class="col-12 link-ref">
                        Ссылка на регистрацию:
                        <div class="parent-qa"><?php /* <i class="fas fa-question-circle question1-icon"></i>
                                <div class="question-1">Отправьте эту ссылку друзьям. После их регистрации и первой покупки вы получите 4$. А каждая повторная покупка принесет 3$.</div> */ ?>
                        </div>
                        <div class="link-ref-block"><?php echo AffiliateHelper::get_account_link( $parentId );   ?><a href="#" class="copy"><i class="fas fa-copy"></i></a></div>
                        </div>
                        <div class="col-12 link-ref">
                            Ссылка на регистрацию партнера:
                            <div class="parent-qa"><i class="fas fa-question-circle question1-icon"></i>
                                <div class="question-1">Отправьте эту ссылку друзьям. После их регистрации и первой покупки вы получите 4$. А каждая повторная покупка принесет 3$.</div>
                            </div>
                            <div class="link-ref-block"><?php echo str_replace('?atid=','350?atid=',AffiliateHelper::get_account_link( $parentId )); ?><a href="#" class="copy"><i class="fas fa-copy"></i></a></div>
                        </div>
                        <?php /* <div class="col-12 link-ref">
                            Ссылка на покупку товара:
                            <div class="parent-qa"><i class="fas fa-question-circle question2-icon"></i>
                                <div class="question-2">Рекламируйте, рекомендуйте товар со своей меткой. Каждая покупка любого товара с вашей меткой принесет вам 3$. А если покупку совершит не зарегистрированный до этого покупатель, то к первой его покупке добавится 1 $ за регистрацию и последующие покупки принесут 3$ за каждую.</div>
                            </div>
                            <div class="link-ref-block"><?php echo str_replace('?atid=','186?atid=',AffiliateHelper::get_account_link( $parentId )); ?><a href="#" class="copy"><i class="fas fa-copy"></i></a></div>

                        </div>    */ ?>

                    </div>
                               <strong>Что такое реферальная сеть.</strong><br>
                                   Партнерские пакеты начиная с Business открывают перед Вами возможность получать доход от линий генерации.<br/>
                                   Представьте себе, что зарегистрированные Вами друзья получили точно такие же предложения и некоторые из них купили партнерские пакеты. Это вторая линия генерации.<br>
                                   То же самое сделали люди, привлеченные на суммы их партнерских пакетов. Это третья линия генерации. И т.д.<br>
                                   Различные партнерские пакеты дают возможность получать комиссию от покупок совершенных покупателями, привлеченными на партнерские пакеты до 5 линии генерации. Это увеличивает Ваш плановый доход в 10 - ки раз.<br>
                                   От покупок 1 - й линии Вы получаете 3$<br>
                                   От покупок 2 - й линии Вы получаете 5%<br>
                                   От покупок 3 - й линии Вы получаете 2%<br>
                                   От покупок 4 - й линии Вы получаете 1,5%<br>
                                   От покупок 5 - й линии Вы получаете 1%<br>
                                   Все это Вы видите в своем кабинете.
                </article>    
                             
        </section>
        <div class="alert alert-success">
            <i class="fas fa-check" style="padding-right:10px;"></i> <b>Отлично!</b> Вы в кабинете партнера Prizolove. Здесь вы видите доход который приносят ваши партнерские пакеты.<br/>
            Пакет активирован. И теперь он начал работать на вас. Об изменениях в вашем доходе будут приходить уведомления на емейл. Посмотрите на кабинет, на подсказки и если появятся вопросы, мы ждем их в чате (правый нижний угол экрана)
            <a href="javascript::void(0)" data-dismiss="alert" class="close-btn"><i class="fas fa-times-circle"></i></a>
        </div>
	<?php if ($expireDays <= 2) : ?>
    <div class="to-continue-alert">
        <div class="container">
            <div class="alert alert-date">
                <i class="fas fa-exclamation-triangle" style="padding-right:10px;"></i>
                <strong>Внимание!</strong> До окончания действия пакета Proba - <?=max($expireDays,0);?> дня. <a target="_blank" href="/362">Перейдите на платный партнерский пакет, чтобы продолжить сотрудничество</a>.
            </div>
        </div>
    </div>
	<?php endif; ?>
	<div class="row tabs-block">
            <div class="col-12">
                <div class="affiliate-menu">
                   <a href="/accounts" class="active">Мой доход</a>
                   <a href="/366" target="_blank">Витрина</a>		
                    <?php /* <a href="/calculator.html">Калькулятор успеха</a> */ ?>
                    <a href="/362" target="_blank">Партнерские пакеты</a>
                </div>
            </div>
        </div> 

        <?php /* <div class="row tabs-block">
            <div class="col-12">
                <div class="affiliate-menu">
                    <a href="#">Конверсии</a>
                    <a href="#">Логи</a>
                    <a href="#" class="active">Мои аккаунты</a>
                    <a href="#">Платежи</a>
                    <a href="#">Маркетинг</a>
                </div>
            </div>
        </div> */?>

    </div>
</div>
</div>

<div class="my-accounts-block">
    <div class="container">
        <div class="row">

            <div class="stat-block">
                <div>
                    <span class="count"><?php if ($parentId == 3) :?>157<?php else :?><?php echo $totalLeads; ?><?php endif;?></span>
					<div class="parent-qa"><i class="fas fa-question-circle question3-icon"></i>
					<div class="question-3">Количество покупателей привлеченных системой в Вашу сеть. А так же покупатели зарегистрированные по вашим реферальным ссылкам.</div></div>
					<br/>
                    <span class="ref color-block">Человек</span><br/>
                    <?php /* <span class="days">За последние 30 дней</span> */ ?>
                </div>
                <div>
                    <span class="count"><?php if ($parentId == 3) :?>67<?php else :?><?php echo $totalSales; ?><?php endif; ?></span>
					<div class="parent-qa"><i class="fas fa-question-circle question4-icon"></i>
					<span class="question-4">Количество покупок совершенных покупателями вашей сети.</span></div><br/>					
                    <span class="convers color-block">Покупок</span><br/>
                    <?php /* <span class="days">За последние 30 дней</span> */ ?>
                </div>
                <div>
                    <span class="count"><?php if ($parentId == 3) :?>212,50<?php else :?><?php echo $totalComission; ?><?php endif;?> $</span>
					<div class="parent-qa"><i class="fas fa-question-circle question5-icon"></i>
					<span class="question-5">Ваш доход к выводу.</span></div><br/>
                    <?php if (intval($comission)) : ?>
                        <div class="alert-min0">
		            <span class="popup_hap2 money-btn" btn-see-more onclick="myFunction()">Вывести
                       <?php if (!$paid) : ?>
                            <span class="popuptext bg_e74c3c" id="myPopup">
Для вывода средств, напишите в чат (справа внизу)<br/>
Прошу вевести сумму: <br/>
Из моего личного аккаунта: емейл, телефон<br/>
На карту номер:<br/>
                        </span>
                        <?php elseif ($paid && $totalComission < 10):?>
                            <span class="popuptext bg_e74c3c" id="myPopup">
                                Чтобы вывести средства Ваш баланс должен быть больше $10
                            </span>
                       <?php elseif ($paid && $totalComission > 10):?>
                           <span class="popuptext bg_e74c3c" id="myPopup">
                                Хорошо! Напишите заявку в чат. Зелёный значок в правом нижнем углу экрана.
                            </span>
                       <?php endif;?>

                          <script>
                          // When the user clicks on div, open the popup
                          function myFunction() {
                              var popup = document.getElementById("myPopup");
                              popup.classList.toggle("show");
                          }
                          </script>
                      
                      
                      
                      </div><br/>
                    <?php else: ?>
                      
                      
                        <div class="alert-min0">
                
                          <span class="popup_hap2 money-btn" btn-see-more onclick="myFunction()">Вывести
                        <?php if (!$paid) : ?>
                            <span class="popuptext bg_e74c3c" id="myPopup">
                            Что бы вывести средства вам нужно быть владельцем платного аккаунта. Посмотрите партнерские пакеты.
                            (<a target="_blank" href="https://prizolove.com/362">Выберите наиболее подходящий</a>.)
                            Минимальный следующий <a target="_blank" href="https://prizolove.com/orderpage?pid=256">шаг</a>
                            </span>
                        <?php elseif ($paid && $totalComission < 10):?>
                            <span class="popuptext bg_e74c3c" id="myPopup">
                                Чтобы вывести средства Ваш баланс должен быть больше $10
                            </span>
                        <?php elseif ($paid && $totalComission > 10):?>
                           <span class="popuptext bg_e74c3c" id="myPopup">
                                Хорошо! Напишите заявку в чат. Зелёный значок в правом нижнем углу экрана.
                            </span>
	                <?php endif;?>
                          </span>

                          <script>
                          // When the user clicks on div, open the popup
                          function myFunction() {
                              var popup = document.getElementById("myPopup");
                              popup.classList.toggle("show");
                          }
                          </script>
                      
                      </div>
                      
                      
                      
                      <br/>
                    <?php endif;?>
                    <?php /* <span class="days">За последние 30 дней</span> */ ?>
                </div>
            </div>

        </div>


    </div>


</div>


<div class="table-affiliate">
    <div class="container">
        <div class="row">
            <div class="col-4">
                Имя
            </div>
            <div class="col-4">
                Покупка
            </div>
            <div class="col-3">
                Ваш доход
            </div>



        </div>
    </div>
</div>

<div class="rows-table-affiliate">
    <div class="container">
        <?php if($parentId == 3) :?>
            <div class="row">
                <div class="col-4">Иван</div>
                <div class="col-4">2018-08-20 03:47:26</div>
                <div class="col-3 prise-upper">
                    + 9,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Ольга</div>
                <div class="col-4">2018-08-16 07:20:58</div>
                <div class="col-3 prise-upper">
                    + 6,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Михаил</div>
                <div class="col-4">2018-08-22 15:41:57</div>
                <div class="col-3 prise-upper">
                    + 6,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Марина</div>
                <div class="col-4">2018-08-19 05:18:46</div>
                <div class="col-3 prise-upper">
                    + 14,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Алена</div>
                <div class="col-4">2018-08-20 01:24:21</div>
                <div class="col-3 prise-upper">
                    + 11,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Александр</div>
                <div class="col-4">2018-08-15 11:56:57</div>
                <div class="col-3 prise-upper">
                    + 4,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Елена</div>
                <div class="col-4">2018-08-13 09:39:37</div>
                <div class="col-3 prise-upper">
                    + 6,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Наталия</div>
                <div class="col-4">2018-08-14 10:00:26</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Николай</div>
                <div class="col-4">2018-08-20 05:12:30</div>
                <div class="col-3 prise-upper">
                    + 4,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Юрий</div>
                <div class="col-4">2018-08-19 18:14:07</div>
                <div class="col-3 prise-upper">
                    + 6,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Любовь</div>
                <div class="col-4">2018-08-19 12:48:19</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Лариса</div>
                <div class="col-4">2018-08-18 19:23:13</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Василий</div>
                <div class="col-4">2018-08-17 21:11:57</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Eвгений</div>
                <div class="col-4">2018-08-15 08:12:40</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Катя</div>
                <div class="col-4">2018-08-16 07:08:24</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Рита</div>
                <div class="col-4">2018-08-18 15:27:19</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Тарас</div>
                <div class="col-4">2018-08-21 07:22:34</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Алла</div>
                <div class="col-4">2018-08-21 11:52:18</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Виктор</div>
                <div class="col-4">2018-08-22 22:39:36</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Василий</div>
                <div class="col-4">2018-08-22 04:12:38</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Александра</div>
                <div class="col-4">2018-08-16 17:44:33</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Анна</div>
                <div class="col-4">2018-08-13 11:13:23</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Илья</div>
                <div class="col-4">2018-08-14 17:22:15</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Валерия</div>
                <div class="col-4">2018-08-15 15:39:19</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Алевтина</div>
                <div class="col-4">2018-08-12 10:17:46</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Вика</div>
                <div class="col-4">2018-08-21 12:15:30</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Александр</div>
                <div class="col-4">2018-08-21 21:15:56</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Марианна</div>
                <div class="col-4">2018-08-18 07:47:08</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Тимофей</div>
                <div class="col-4">2018-08-16 15:02:08</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Олег</div>
                <div class="col-4">2018-08-20 20:59:00</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Максим</div>
                <div class="col-4">2018-08-19 16:21:01</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Владислав</div>
                <div class="col-4">2018-08-16 10:36:16</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Владимир</div>
                <div class="col-4">2018-08-15 13:57:48</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Олеся</div>
                <div class="col-4">2018-08-19 19:23:44</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Валентина</div>
                <div class="col-4">2018-08-16 08:37:21</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Дмитрий</div>
                <div class="col-4">2018-08-18 06:06:35</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Михаил</div>
                <div class="col-4">2018-08-17 13:04:41</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Андрей</div>
                <div class="col-4">2018-08-15 13:29:46</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Иван</div>
                <div class="col-4">2018-07-30 15:32:19</div>
                <div class="col-4">2018-08-20 04:04:50</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Игорь</div>
                <div class="col-4">2018-08-21 00:18:09</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Александр</div>
                <div class="col-4">2018-08-19 18:22:50</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Оксана</div>
                <div class="col-4">2018-08-16 09:16:59</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Денис</div>
                <div class="col-4">2018-08-17 08:43:40</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Юлия</div>
                <div class="col-4">2018-08-21 23:10:59</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Ангелина</div>
                <div class="col-4">2018-08-19 11:52:15</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Егор</div>
                <div class="col-4">2018-08-19 12:45:25</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Станислав</div>
                <div class="col-4">2018-08-16 04:33:43</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Валерий</div>
                <div class="col-4">2018-08-18 08:45:08</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Кристина</div>
                <div class="col-4">2018-08-13 23:32:59</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Сергей</div>
                <div class="col-4">2018-08-22 05:55:06</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Иван</div>
                <div class="col-4">2018-08-13 21:35:02</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Анна</div>
                <div class="col-4">2018-08-18 11:00:29</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Алина</div>
                <div class="col-4">2018-08-22 11:24:47</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Николай</div>
                <div class="col-4">2018-08-21 18:46:05</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Ангелина</div>
                <div class="col-4">2018-08-18 17:47:30</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Ольга</div>
                <div class="col-4">2018-08-19 11:57:05</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Артем</div>
                <div class="col-4">2018-08-20 05:27:59</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Григорий</div>
                <div class="col-4">2018-08-15 09:18:24</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Надежда</div>
                <div class="col-4">2018-08-16 23:11:19</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Юрий</div>
                <div class="col-4">2018-08-18 17:34:56</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Марина</div>
                <div class="col-4">2018-08-15 11:42:27</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Виталий</div>
                <div class="col-4">2018-08-22 07:31:47</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Марат</div>
                <div class="col-4">2018-08-16 19:46:22</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Александр</div>
                <div class="col-4">2018-08-13 09:54:07</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Вера</div>
                <div class="col-4">2018-08-16 12:08:06</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Виктория</div>
                <div class="col-4">2018-08-17 19:26:34</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Анастасия</div>
                <div class="col-4">2018-08-15 13:32:40</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Людмила</div>
                <div class="col-4">2018-08-22 18:54:20</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Виталий</div>
                <div class="col-4">2018-08-20 12:44:58</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Николай</div>
                <div class="col-4">2018-08-21 03:02:30</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Ирина</div>
                <div class="col-4">2018-08-21 10:30:08</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Олег</div>
                <div class="col-4">2018-08-22 02:35:57</div>
                <div class="col-3 prise-upper">

                </div>
            </div>
            <div class="row">
                <div class="col-4">Елена</div>
                <div class="col-4">2018-08-13 19:33:13</div>
                <div class="col-3 prise-upper">
                    + 3,0 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Виктория</div>
                <div class="col-4">2018-08-18 12:19:46</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Ольга</div>
                <div class="col-4">2018-08-20 16:38:56</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Оксана</div>
                <div class="col-4">2018-08-19 14:58:50</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>
            <div class="row">
                <div class="col-4">Владимир</div>
                <div class="col-4">2018-08-15 14:32:37</div>
                <div class="col-3 prise-upper">
                    + 1,5 $
                </div>
            </div>

        <?php else: ?>
            <?php foreach($levelOneChildren as $child) : ?>
                <?php if ($orders[$child->user_id]) : ?>
                    <?php foreach($orders[$child->user_id] as $order) : ?>
                        <div class="row">
                            <div class="col-4"><?=$child->account_name;?></div>
                            <div class="col-4"><?=$order['name'];?></div>
                            <div class="col-3 prise-upper">
                                <?='+ '.number_format($order['comission'],2,'.',' ').' $';?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="row">
                        <div class="col-4"><?=$child->account_name;?></div>
                        <div class="col-4"><?=(($child->product_name)?$child->product_name:'');?></div>
                        <div class="col-3 prise-upper">
                            <?=isset($totals[$child->id])?'+ '.number_format($totals[$child->id],2,'.',' ').' $':'';?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if (!$children) : ?>
	<div class="row">
		<div class="col-12">Здесь будут добавляться данные о покупателях, которые система привлекает на сумму вашего партнерского пакета.</div>
	</div>
	<?php endif; ?>
        <?php endif; ?>
    </div>
</div>
    <div class="rows-table-affiliate generation-parent-2">
        <div class="container">
            <div class="row title generation-2">
                <div class="col-12">
                    2-я генерация
                </div>
            </div>
            <div class="stats">
                <div><?=count($levelTwoChildren);?> человек</div>
                <div><?=$levelTwoSales;?> покупок</div>
                <div><?=$levelTwoComission;?> $</div>
            </div>
            <div class="table-inner">
                <?php

                ?>
                <?php $num = 0; ?>
                <?php for($i=0;$i<3; $i++) :?>
                <div class="row">
                    <?php for($j=0; $j < 6; $j++) :?>
                    <div class="col-sm-2 col-md-2 col-4"><?=(!empty($levelTwoChildren[$num])?$levelTwoChildren[$num]->account_name:'');?></div>
                        <?php $num++; ?>
                    <?php endfor; ?>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
   
     
         <div class="container" style="    font-family: Montserrat;"> <div class="row">
          <p>
          <b>Итак как увеличить Ваш доход с помощью Prizolove.</b>
          <br><b>1. Пассивный доход. Как его создать?</b>
Вы покупаете партнерский пакет и смотрите, как система зарабатывает для Вас без вашего участия. На деньги, которые Вы тратите на покупку пакета, мы проводим рекламные кампании и находим покупателей, которые будут совершать покупки на нашем сайте. Этих покупателей, их покупки и суммы Вашего дохода Вы видите в своем кабинете. Примерный расчет вашего дохода такой: каждый клиент в среднем делает 2 покупки в год. Вы получаете 3$ с каждой покупки.
<br>
<b>Ваши действия:</b> купить любой партнерский пакет, который подойдет вам по уровню планового дохода и наблюдать за действиями привлеченных на Ваши деньги пользователей. Если объем дохода и тип партнерства вам будет интересен, приобретайте партнерские пакеты сразу, так как количество их ограничено.
<br><br>
<b>2. Как получить доход с активным участием: Реферальная программа.</b><br>
В вашем личном кабинете есть реферальные ссылки, которые принадлежат только Вам. Любой человек, перешедший по этим ссылкам, будет зачислен системой в Вашу сеть и Вы увидите его в кабинете, но только после совершения им полезного действия: регистрация или покупка. С этого времени каждая их покупка принесет Вам доход.<br> 
<b>Типы реферальных ссылок:</b><br>
Первая ссылка ведет на регистрацию человека в prizolove.com <br>
Вторая на покупку партнерского пакета. <br>
<img src="https://prizolove.com/images/ssilki2.JPG">
<br>
С помощью этих ссылок Вы можете значительно увеличивать свой доход, особо при этом не напрягаясь. Просто передавайте эти ссылки своим друзьям, родственникам, знакомым и получайте доход с каждой их покупки.
<br>
<b>Ваши действия: </b><br>
1)Скопировать первую ссылку, передать друзьям. Рассказать, что здесь можно участвовать в конкурсах, выигрывать бонусы на покупки. Затем покупать товары народного потребления и проходить к розыгрышу более крупных призов. <br>
Первая покупка Вашего друга принесет Вам 4$, каждая последующая 3$.<br>
2)Скопировать вторую ссылку, передать друзьям, рассказать, как можно зарабатывать с партнерской программой. Покупка любого пакета по Вашей ссылке, принесет Вам 10% от стоимости купленного партнерского пакета. При этом, приглашенные Вами друзья получают в подарок по Вашей ссылке пакет Proba бесплатно, а затем, пройдя путь проб, принимают решение об оплате.
<br><br>
<b>3. Как начисляется доход.</b><br>
- По партнерским пакетам пассивного дохода Вы получаете 3$ с каждой покупки привлеченного на Ваши деньги клиента ничего при этом не делая.<br>
- По реферальной программе с активным доходом Вы получаете:<br>
    - 10% за каждый купленный по Вашей ссылке партнерский пакет <br>
    - 4$ за каждую первую и 3$ за каждую последующую покупку друзей, зарегистрированных по Вашей реферальной ссылке
<br><br>
<b>4. Что такое реферальная сеть.</b><br>
Партнерские пакеты начиная с Business открывают перед Вами возможность получать доход от линий генерации.<br/>
Представьте себе, что зарегистрированные Вами друзья получили точно такие же предложения и некоторые из них купили партнерские пакеты. Это вторая линия генерации.<br>
То же самое сделали люди, привлеченные на суммы их партнерских пакетов. Это третья линия генерации. И т.д.<br>
Различные партнерские пакеты дают возможность получать комиссию от покупок совершенных покупателями, привлеченными на партнерские пакеты до 5 линии генерации. Это увеличивает Ваш плановый доход в 10 - ки раз.<br>
От покупок 1 - й линии Вы получаете 3$<br>
От покупок 2 - й линии Вы получаете 5%<br>
От покупок 3 - й линии Вы получаете 2%<br>
От покупок 4 - й линии Вы получаете 1,5%<br>
От покупок 5 - й линии Вы получаете 1%<br>
Все это Вы видите в своем кабинете.
<br><br>
<a href="/350" style="color: #3498db;
    border-bottom: 1px solid #3498db;" target="_blank">Переходите к выбору партнерских пакетов.</a>
</p>
          </div>
  </div>
          
          

          
          
          
<div class="container">
  <div class="row">
    <style>
    .carousel-indicators .active {
        background-color: #2D46F9 !important;
opacity:1;
    }
.carousel-indicators{margin-top:10px;}
.carousel-indicators li{background-color:black;opacity:0.4;transition:.5s;}
    .testimot_border{
        position: relative;
background:#F3F3F3;
        border: 3px solid #F3F3F3 !important;;
        margin: 0px 0;
        border-radius: 10px;
margin-top:50px;
margin-bottom:60px;
        padding: 20px;
    }
    .testimot_border::before {
        content: '';
        position: absolute; /* Абсолютное позиционирование */
        left: 20px;
        top: -20px; /* Положение треугольника */
        border: 10px solid transparent; /* Прозрачные границы */
        border-bottom: 10px solid #F3F3F3 !important; /* Добавляем треугольник */
    }
    .container_testim {
        background-color: white;
    }

    .container_testim::after {
        content: "";
        clear: both;
        display: table;
    }

    .container_testim img {
        float: left;
        margin-right: 20px;
        border-radius: 50%;
    }

    .container_testim span {
        font-size: 20px;
        margin-right: 15px;
    }

    @media (max-width: 500px) {
    .testimot_border{ margin-top:0px;}
        .container_testim {
            text-align: center;
        }
        .container_testim img {
            margin: auto;
            float: none;
            display: block;
        }
    }
</style> 

  
  <h1 class="title" >Отзывы инвесторов</h1> 
<div style="width:100%;margin:10px 0;"></div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="container_testim">
                    <img src="images/alina.png" alt="Avatar" style="width:90px">
                    <p><span>Алина,</span>32 года</p>
                    <p>Экономист</p>
                    <div class="testimot_border">
                        <p>Наконец-то появилась понятная для меня компания. Все время хотела инвестировать не большие деньги, но что бы получать доход. Это Вам не форекс с непонятными цифрами и акциями. Тут мне понятно. На мои деньги проводят рекламу, приходят люди и регистрируются. Покупают, товары - я получаю комиссию. И главное, что интересно, заглядываю каждый день в свой кабинет и вижу сколько у меня дивидендов. Короче рекомендую. Вложила 1000 долларов, получаю 50-150$ в месяц.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container_testim">
                    <img src="images/petr.png" alt="Avatar" style="width:90px">
                    <p><span>Петр,</span>  27 лет, Луцк</p>
                    <p>Клиент-менеджер</p>
                    <div class="testimot_border">
                        <p>Что сказать? Ну на вложенные где то выходит процентов 10 у меня. Вложил чуть больше 3000$ Выплачивают вовремя. Ну +/- 3 дня от запроса. САм играю в акции от prizolove и сам покупаю товары. И как инвестор на каждую свою покупку получаю - 3$ скидки, ну приятно)</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container_testim">
                    <img src="images/vera.png" alt="Avatar" style="width:90px">
                    <p><span>Вера, </span>39 лет, Томск</p>
                    <p>Домохозяйка</p>
                    <div class="testimot_border">
                        <p>Я поверила после того как сама поиграла в prizolove.com увидела настоящих победителей акций. Купила пару товаров - часы, и овощерезку. И что то еще, не помню. Мне предложили инвестировать в клиентcкие базы SweepStake. Попробовала с 100 долларов, за пару месяцев увидела доход и вложила еще 500$.  Денег не много, но на коммунальные хватает.</p>
                    </div>
                </div>

            </div>
            <div class="item">
                <div class="container_testim">
                    <img src="images/sergei.png" alt="Avatar" style="width:90px">
                    <p><span>Сергей,</span> 31 год, Минск</p>
                    <p>Веб-мастер</p>
                    <div class="testimot_border">
                        <p>Вложил 10-ку сразу. Так как модель для меня прозрачна. Кабинет показывает заработок и я понимаю за что я получаю. Это не какие то лохотроновые акции, где я потерял. Тут понятно - вот клиенты покупают, вот комиссия капает. Моя прибыль в месяц практически всегда 1000 баксов в месяц. Доволен. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </div>
  
</div>          
        
          
<?php /* <div class="popup_enter hidden">
        <a href="#" class="close_popup"><i class="fas fa-times"></i></a>
            <div class="popup-content"></div>
        </div> */
?>