<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
JLoader::import('pl_lib.library');
$parentId = PlLibHelperAffiliate::getParent();
$events = PlLibHelperEvents::getInstance()->getLatestEvents();
require_once(JPATH_ROOT.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');
?>
<script>
    jQuery(document).ready(function() {
        jQuery('.copy').on('click', function(e) {
            var $this = jQuery(this);
            var $temp = jQuery("<input>");
            jQuery("body").append($temp);
            $temp.val(jQuery(this).closest('div.link-ref-block').text()).select();
            document.execCommand("copy");
            $this.tooltip({'placement': 'top', 'title': 'Скопировано','trigger':'click', delay: { "show": 500, "hide": 100 }});
            $temp.remove();
            e.preventDefault();
            return false;
        });
        jQuery('a.akciya').on('click', function(e) {
            var $this = jQuery(this);
            var $temp = jQuery("<input>");
            jQuery("body").append($temp);
            $temp.val($this.attr('href')).select();
            document.execCommand("copy");
            $this.tooltip({'placement': 'top', 'title': 'Скопировано','trigger':'click', delay: { "show": 500, "hide": 100 }});
            $temp.remove();
            e.preventDefault();
            return false;
        });
    });
</script>
<div class="referal-body">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-12 col-sm-12">

                <div class="title">Постройте свою реферальную сеть в нашем проекте.<br/>
                    Зарабатывайте от 500$ в месяц.</div>

                <div class="col-12 col-md-12 col-sm-12">
                    <div class="background-title">Как это работает?</div>
                    <div class="text">Вы партнер системы Prizolove. А это значит, что у вас есть ваша персональная партнерская ссылка.<br/>
                        Мы ее называем реферальная ссылка.<br/><br/>
                        Это «хвостик» <b>/?atid=<?=$parentId;?></b>, который добавляется к ссылкам на страницы prizolove.com по которому система понимает, что человек пришел от вас.
                    </div>

                    <div class="block-how">
                        <div class="wrapper">
                            <img src="https://prizolove.com/images/icon-1-referal.png">
                            <div class="text">Копируете ссылку на загадку, нажав на нее</div>
                        </div>
                        <div class="wrapper">
                            <img src="https://prizolove.com/images/icon-2-referal.png">
                            <div class="text">Передаете ее другу, он проходит испытание и регистрируется.</div>
                        </div>
                        <div class="wrapper">
                            <img src="https://prizolove.com/images/icon-3-referal.png">
                            <div class="text">Учет его покупок ведется в Вашем личном кабинете.</div>
                        </div>
                        <div class="wrapper">
                            <img src="https://prizolove.com/images/icon-4-referal.png">
                            <div class="text">Вы получаете деньги за каждую покупку.</div>
                        </div>
                    </div>

                    <br/>
                    <div class="background-title">Это открывает Вам прекрасные возможности!
                    </div>
                    <div class="text">1. Пригласите партнера. Дайте ему ссылку на бесплатный партнерский пакет, где он пройдет ваш путь. И когда он приобретет партнерский пакет, вы получите 10% от суммы его партнерского пакета. А так же 5% от покупок покупателей привлеченных за счет его партнерского пакета (мы называем это второй линией генерации) Приглашайте партнеров, это выгодно.<br/>
                        Вот ваша ссылка на бесплатный партнерский пакет. (ссылка) копировать.<br/>
                        Отправляйте ее напрямую или опубликуйте страницу на своей странице в социальной сети.<br/><br/>
                        2. Пригласите покупателя. Тем людям, которых больше заинтерсует - выиграть 10 000$ отгадывая загадки и участвуя в конкурсах. <br/>
                        <span>1. Отправьте им вашу ссылку на регистрацию покупателя <br/></span>

                        <div style="background:rgba(0,0,0,0.15);color:rgba(0,0,0,0.80);text-align:center;padding:50px 0;font-weight:600;margin-top:20px;">
                            <article>
                                <div class="row-fluid links-ref-block">
                                    <div class="col-12 link-ref">
                                        Ссылка на регистрацию:
                                        <div class="parent-qa"></div>
                                        <div class="link-ref-block"><?php echo str_replace('?atid=','?atid=',AffiliateHelper::get_account_link( $parentId )); ?><a href="#" class="copy"><i class="fas fa-copy"></i></a></div>
                                    </div>
                                    <div class="col-12 link-ref">
                                        Ссылка на регистрацию партнера:
                                        <div class="parent-qa"><i class="fas fa-question-circle question1-icon"></i>
                                            <div class="question-1">Отправьте эту ссылку друзьям. После их регистрации и первой покупки вы получите 4$. А каждая повторная покупка принесет 3$.</div>
                                        </div>
                                        <div class="link-ref-block"><?php echo str_replace('?atid=','350?atid=',AffiliateHelper::get_account_link( $parentId )); ?><a href="#" class="copy"><i class="fas fa-copy"></i></a></div>
                                    </div>

                                </div>
                            </article>
                        </div>
                        <br/>

                        <span>2. Опубликуйте в социальной сети вот эти загадки по одной в неделю и вы соберете большое количество ваших друзей в сеть.</span>
                        <div style="background:rgba(0,0,0,0.15);color:rgba(0,0,0,0.80);text-align:center;padding:50px 0;font-weight:600;margin-top:20px;">
                            <div class="block-list-akcii">
                            <div class="body text-center">
                                <?php foreach($events as $event) : ?>
                                    <a href="<?=$event->url?>?atid=<?=$parentId;?>" class="akciya">
                                        <div class="product_image_wrapper">
                                            <?php if ($event->image) : ?>
                                                <img src="/<?=$event->image;?>">
                                            <?php else: ?>
                                                <img src="https://prizolove.com/images/second.jpg">
                                            <?php endif; ?>
                                        </div>
                                        <div class="name"><?=$event->name;?></div>
                                        <?php
                                        if ($event->difficulty) :
                                            switch($event->difficulty) {
                                                case 'easy':
                                                    $difficultyLabel = 'COM_EVENT_EASY';
                                                    break;
                                                case 'medium':
                                                    $difficultyLabel = 'COM_EVENT_MEDIUM';
                                                    break;
                                                case 'hard':
                                                    $difficultyLabel = 'COM_EVENT_HARD';
                                                    break;
                                            }

                                            ?>
                                            <div class="slojnost">Сложность: <?=JText::_($difficultyLabel);?></div>
                                        <?php else: ?>
                                            <div class="slojnost">Сложность: низкая</div>
                                        <?php endif; ?>
                                    </a>
                                <?php endforeach?>
                            </div>
                            </div>
                        </div>

                        <br/><br/>
                        3. Расчет вашего дохода. Если вы зарегистрируете 30 человек. И каждый 10 из них сделает тоже самое. ТО ваш плановый доход за год вырастет до 500$ в месяц. Вот и ваш пассивный доход)<br/><br/>
                        4. Маркетинг условия:  <br/>
                        <span>От покупок 1 - й линии Вы получаете 3$</span><br/>
                        <span>От покупок 2 - й линии Вы получаете 5%</span><br/>
                        <span>От покупок 3 - й линии Вы получаете 2%</span><br/>
                        <span>От покупок 4 - й линии Вы получаете 1,5%</span><br/>
                        <span>От покупок 5 - й линии Вы получаете 1%</span><br/>
                        <br/>
                        5. Стоимость услуг для партнера. С первой вашей заработанной комиссии система спишет партнерский взнос 12$.
                    </div>
                    <br/><br/>
                </div>
            </div>
        </div>
    </div>
</div>









