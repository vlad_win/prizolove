<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument();

unset($doc->_scripts[$this->baseurl . 'media/jui/js/bootstrap.min.js']); // Remove joomla core bootstrap
//unset($doc->_scripts[$this->baseurl . '/media/jui/js/bootstrap.min.js']); // Remove joomla core bootstrap
unset($doc->_styleSheets[$this->baseurl . 'media/jui/css/bootstrap.min.css']);
$this->helix3 = helix3::getInstance();
$this->helix3->addCSS('bootstrap4.min.css')// CSS Files
->addJS('bootstrap4.min.js');// JS Files
JLoader::import('pl_lib.library');

$events = PlLibHelperEvents::getInstance()->getEvents();

$user = JFactory::getUser();
$parentId = PlLibHelperAffiliate::getParent();

//$doc = JFactory::getDocument();
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagebuilder.css');
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagecontainer.css');
?>
<style>






    .my-accounts-block{padding:50px 0;}
    .stat-block{background:rgba(241, 196, 15,0.2);width:100%;text-align:center;border:1px solid #dbca9d;border-radius:10px;}
    .stat-block div{width:33%;display:inline-block;padding:20px 0;}
    .stat-block .count{font-weight:700;font-size:50px;line-height:50px;color:#212121;}
    .stat-block .color-block{margin:5px 0;display:inline-block;padding:10px;text-transform:uppercase;font-weight:bold;border-radius:5px;color:white;font-size:14px;;}
    .ref{background:rgba(52, 152, 219,0.9);text-shadow:1px 2px 2px #3498db;}
    .convers{background:rgba(211, 84, 0,0.9);text-shadow:1px 2px 2px #d35400;}
    .com{background:rgba(142, 68, 173,0.9);text-shadow:1px 2px 2px #8e44ad;}

    .stat-block .days{font-size:12px;font-weight:500;color:black;opacity:.5;}


    .table-affiliate .row{text-align:left;background:white;font-family:"Montserrat",sans-serif;font-weight:bold;color:#2c2b2b;border-radius:7px;padding:15px 0px 15px 0px ;box-shadow:0px 2px 15px rgba(0,0,0,0.25);margin-bottom:10px;}

    /*.tabs-block{display:none;}*/
    .links-ref-block{text-align:center;margin:50px 0;background:#e74c3c;border-radius:10px;color:white;}
    .title-ref{position:relative;text-align:center;font-weight:bold;padding:10px;font-size:21px;color:white;}
    .copy{display:block;padding:5px 8px 5px 8px;position:absolute;top:50%;right:20px;text-align:center;margin-top:2px;color:white  !important;background:#7a231d;}
    .copy:hover{background:#a03531;}
    .copy:active{transform:scale(1.5);}
    .link-ref{padding:10px;font-weight:500;font-size:21px;}
    .link-ref-block{background:#c0392b;margin-top:10px;padding:10px 0;font-size:18px;}


    .spravka{text-align:left;background:#f1c40f;color:#272727 !important;font-size:14px;line-height:16px;font-weight:bold;display:inline-block;padding:20px 10px 20px 60px;border-radius:10px;box-shadow:0px 3px 0px #d6aa26;position:relative;}
    .spravka:hover{background:#F2C437;}
    .spravka:active{box-shadow:none;    background-color: #F2C437;
        border-color: rgb(129, 33, 19);
        border-top-width: 3px;
        border-bottom-width: 1px;
        box-shadow: inset 0px 2px 2px 0px rgba(0, 0, 0, 0.25);
        transform:translateY(2px);
    }
    .spravka::before{content:"";display:block;width:40px;height:40px;background:url(https://prizolove.com/images/agenda.png) no-repeat center center;background-size:contain;position:absolute;top:50%;left:10px;transform:translateY(-50%);}


    .rows-table-affiliate .container{background:white;font-family:"Montserrat",sans-serif;font-weight:500;color:#2c2b2b;border-radius:7px;padding:0px 15px 0px 15px;box-shadow:0px 2px 15px rgba(0,0,0,0.25);margin-bottom:50px;overflow:hidden;}

    .rows-table-affiliate .row{padding:10px 0;border-bottom:1px solid rgba(0,0,0,0.10);}

    .prise-upper{color:#08CB76;font-weight:bold;}







    .semititle{color:#ffef0f;font-weight:600;margin-bottom:50px;}
    .image-done{width:25px;}

    .level-block-table span{display:block; font-size: 30px;
        font-weight: 900;
        background: linear-gradient(0,#fc9e42,#ffdf2c);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }

    .level-blur{-webkit-filter: blur(3px);
        filter: blur(3px);}
    .level-not-active{position:relative;}
    .level-not-active::before{content:"Купите товар, чтобы активировать Сертификат";text-align:center;width:90%;color:#212121;font-size:16px;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);z-index:99999;font-weight:600;background:rgba(0,0,0,0.10);border:3px solid red;}
</style>
<?php
/*
<div class="title-block">
    <div class="overlay-title-block"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 align-vertical">
                <h1 class="title">Prizoland</h1>
                <p class="semititle">Список доступных Вам акций</p>
            </div>
        </div>



    </div>
</div>
*/
?>
<div style="margin-top:40px;" class="table-affiliate">
    <div class="container">
        <div class="row">
            <div class="col-3 col-sm-2">
                Конкурс
            </div>
            <div class="col-1 col-sm-2">
                Участие
            </div>

            <div class="col-2 col-sm-2">
                Приз
            </div>

            <div class="col-3 col-sm-2">
                Активированная покупка
            </div>
            <div class="col-3 col-sm-2">
                Следующий уровень
            </div>
        </div>
    </div>
</div>
<div class="rows-table-affiliate">
    <div class="container">
        <?php foreach($events as $event) : ?>
        <div class="row">
            <div class="col-3 col-sm-2"><a href="<?=$event->url;?>" target="_blank"><?=$event->name;?></a></div>
            <div class="col-1 col-sm-2"><img class="image-done" src="https://prizolove.com/images/cancel-cabinet.png"></div>
            <div class="col-2col-sm-2">

            </div>
            <div class="col-3 col-sm-2">

            </div>
            <div class="col-3 level-block-table">

            </div>
        </div>
        <?php endforeach; ?>
        <?php /*
        <div class="row">
            <div class="col-3"><a href="#" target="_blank">Рецепт чего изображен на картинке (284)</a></div>
            <div class="col-1"><img class="image-done" src="https://prizolove.com/images/checked-cabinet.png"></div>
            <div class="col-2">
                Шампунь 5STARS
            </div>
            <div class="col-3">
                <a href="#" target="_blank">PowerBank Solar 20000 mAh</a>
            </div>
            <div class="col-3 level-block-table">
                Участие в розыгрыше<span class="prize">250 000 грн</span>
            </div>
        </div>
        <div class="row">
            <div class="col-3"><a href="#" target="_blank">Найдите ПОДАРОК на картинке (353)</a></div>
            <div class="col-1"><img class="image-done" src="https://prizolove.com/images/cancel-cabinet.png"></div>
            <div class="col-2">
                500 гривен
            </div>
            <div class="col-3">

            </div>
            <div class="col-3 level-block-table level-not-active">
                <div class="level-blur">Участие в розыгрыше<span class="prize">250 000 грн</span></div>

            </div>
        </div>
        */ ?>





    </div>

</div>







