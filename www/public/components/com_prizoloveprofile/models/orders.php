<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class PrizoloveProfileModelOrders extends JModelList
{
    protected $_total = null;

    protected $_orders;

    function __construct()
    {
        parent :: __construct();

        $app = JFactory::getApplication();
        $jinput = $app->input;

        $option = $jinput->get('option');


        // Get pagination request variables
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', 5);
        $limitstart = $app->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0);

        $limitstart = $jinput->get('limitstart',0);

        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

    }

    public function getTotal()
    {
        // Load the content if it doesn't already exist
        if (empty($this->_total))
        {
            $query = $this->_buildQuery();
            $this->_total = $this->_getListCount($query);
        }
        return $this->_total;
    }

    public function getPagination()
    {
        // Load the content if it doesn't already exist
        if (empty($this->_pagination))
        {
            jimport('joomla.html.pagination');

            $total = $this->getTotal();
            $limitstart = $this->getState('limitstart');
            $limit = $this->getState('limit');

            $this->_pagination = new JPagination($total, $limitstart, $limit);
        }
        return $this->_pagination;
    }

    public function getLimit()
    {
        return $this->getState('limit');
    }

    public function getLimitstart()
    {
        return $this->getState('limitstart');
    }

    private function _buildQuery()
    {
        $user = JFactory::getUser();

        $userEmail = $user->get('email');

        $db = $this->getDBO();

        $query = '
                  SELECT po.*
                  FROM #__prizolove_customers pc
                  INNER JOIN #__prizolove_orders po ON po.customer_id = pc.customer_id
                  WHERE pc.customer_email = "'.$db->escape($userEmail).'"
                  GROUP BY po.order_id
                  ORDER BY po.order_id DESC
                  ';

        return $query;
    }

	public function getItems()
    {

        $query = $this->_buildQuery();

        $limitstart = $this->getState('limitstart');
        $limit = $this->getState('limit');

        $this->_orders = $this->_getList($query, $limitstart, $limit);

        return $this->_orders;
    }
}