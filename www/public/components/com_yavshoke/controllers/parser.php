<?php
defined('_JEXEC') or die;

class YavshokeControllerParser extends JControllerLegacy
{
	public function xml()
	{
		/**
		 * @var YavshokeModelImportPrizolove $model
		 */
		$model = $this->getModel('Importprizolove');
		foreach (\Parser\Factory\ListXml::$list as $item){
			$parser = new \Parser\ParserXml();
			$xml  = $parser->getXml($item[1]);
			if($xml === false){
				echo $item[1] .' - FALSE <br>';
			}
			$model->storeXML($xml, $item[0]);
			echo $item[0] .'<br>';
			echo $item[1] .'<br>';
		}
		exit(0);
	}
}
