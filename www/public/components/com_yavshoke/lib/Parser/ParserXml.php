<?php

namespace Parser;


use Parser\Factory\Offer;

class ParserXml
{

	protected $xml_data;
	protected $offer = [];

	final public function getXml(string $export) : array
	{
		try{
			if(!$export) throw new \Exception('no string to parse');
			$xml_file = file_get_contents($export);
			$this->xml_data = simplexml_load_string($xml_file);
			if($this->xml_data === false) return false;
			foreach ($this->xml_data->shop->offers->offer as $xml){
					array_push($this->offer, new Offer($xml));
			};
			return $this->offer;
		}catch (\Exception $exception){
			echo  $exception->getMessage();
			exit(1);
		}

	}
}