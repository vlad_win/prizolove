<?php

namespace Parser\Factory;


class ListXml
{

	public static $list = [
		[134, 'https://yavshoke.ua/media/export/brendobuv.xml'],
		[134, 'https://yavshoke.ua/media/export/zhenskobuv.xml'],
		[3, 'https://yavshoke.ua/media/export/posuda.xml'],
		[7, 'https://yavshoke.ua/media/export/bt.xml'],
		[135, 'https://yavshoke.ua/media/export/vazy.xml'],
		[136, 'https://yavshoke.ua/media/export/ljustry.xml'],
		[137, 'https://yavshoke.ua/media/export/textile.xml'],
		[9, 'https://yavshoke.ua/media/export/dom.xml'],
		[138, 'https://yavshoke.ua/media/export/dress.xml'],
	];

}