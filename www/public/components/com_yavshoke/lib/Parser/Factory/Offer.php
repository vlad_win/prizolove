<?php
/**
 * @package     Parser\Factory
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

namespace Parser\Factory;

/**
 * @package     Parser\Factory
 *
 * @since       version
 * @property string $url
 */
class Offer
{
	public $url;
	public $name;
	public $description;
	public $price;
	public $vendorCode;
	public $currencyId;
	public $categoryId;
	public $picture = [];
	public $delivery;
	public $vendor;
	public $code;
	public $param = [];

	public function __construct(\SimpleXMLElement $offer)
	{
		foreach (get_class_vars(self::class) as $key => $var){
			$this->{$key} = $offer->{$key}->__toString();
		}
	}

}
