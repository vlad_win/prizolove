<?php

class YavshokeModelImportprizolove extends JModelLegacy
{

	/**
	 * @param \Parser\Factory\Offer[] $xml
	 * @since version
	 */
	public function storeXML(array $xml, $key)
	{
	    $this->_db->truncateTable('#__prizolove_products');
		foreach ($xml as $item){
			$sql = $this->_db->getQuery(true);
			$sql->select('id')
				->from('#__prizolove_products')
				->where('id = '.$this->_db->q($item->code));
			$id = $this->_db->setQuery($sql)->loadResult();

			$category = $this->_db->getQuery(true)
				->delete('#__prizolove_product_categories_products')
				->where('product_id = '. $this->_db->q($item->code));
			$category = $this->_db->setQuery($category)->execute();

			$productObject = new stdClass();
			$productObject->id = $item->code;
			$productObject->name = $item->name;
			$productObject->type = 'product';
			$productObject->discount = 1;
			$productObject->image = $item->picture;
			$productObject->short_description = $item->description;
			$productObject->full_description = $item->description;
			$productObject->original_url = $item->url;
			$productObject->discount_percent = 0;
			$productObject->landing = null;
			$productObject->price = round($item->price * 1.5);
			$productObject->price_usd = intval($productObject->price)/25;
			$productObject->quantity = 99999;
			$productObject->status = 1;

			if($category === null || true){
				$sql = $this->_db->getQuery(true)
					->insert('#__prizolove_product_categories_products')
					->columns(['product_id', 'category_id'])
					->values(implode(',', [$productObject->id, $key]));
				$this->_db->setQuery($sql)->execute();
			}else{
				$sql = $this->_db->getQuery(true)
					->update('#__prizolove_product_categories_products')
					->set('category_id = ' . $key)
					->where('product_id = '. $this->_db->q($item->code));
				$this->_db->setQuery($sql)->execute();
			}

			if ($id === null){
				$this->_db->insertObject('#__prizolove_products', $productObject);
			}else{
				$this->_db->updateObject('#__prizolove_products', $productObject, ['id']);
			}
		}


	}


}