<?php
defined('_JEXEC') or die;

?>

<?php
    if ($user->id) :
?>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-4 col-md-7 col-sm-7 logo-block" style="vertical-align:middle;">
		<a href="/personal"><img src="https://prizolove.com/images/logotype-white.png" style="width:211px;display:inline-block;vertical-align:middle;"></a> <span style="vertical-align:middle;margin-left:5px;" class="name-user">Привет, <?=$user->name;?></span>
            </div>
            <div class="col-7 col-md-4 col-sm-4 user-block" style="vertical-align:middle;">
               <div class="inline">
                    <a href="/accounts"><i class="fas fa-gem"></i>  <span>Мой кабинет</span></a> <a href="/logout"><i class="fas fa-times"></i> <span>Выйти</span></a>
               </div>
                
            </div>        
	</div>
    </div>
</div>
<?php endif; ?>

