<?php
defined('_JEXEC') or die;

?>

<?php
    if ($user->id) :
?>
<?php 
	$app = JFactory::getApplication();
	if (PlLibHelperAffiliate::checkPartner()) {
		$businessLink = '/accounts';
		$businessName = 'Бизнес';
	}
	$link = '/personal';
	$name = 'Испытания';
?>


<header id="header-auth" class="auth">
    <div class="container">
      <div class="row">
        <div class="col-8 col-sm-4 col-lg-4 col-md-4 text-left">
          <a href="/personal" class="logotype"><img src="https://prizolove.com/images/Header/logo-white.svg"></a>
        </div>
         
        <div class="col-4 col-sm-8 col-lg-8 col-md-8 text-right">
          <div class="nav__tools">
            <a href="/personal" class="nav__link"><img src="images/Header/swords.svg">Испытания</a><a href="/383" class="nav__link"><img src="images/Header/archive.svg">Витрина</a><?php if (!empty($businessLink)) : ?><a href="<?=$businessLink;?>" class="nav__link"><img src="images/Header/money.svg"><?=$businessName;?></a><?php endif; ?></div><div class="nav__menu">
                 <div class="icon"><i class="fas fa-caret-down"></i></div>
                 <div class="menu">
                   <a href="/logout">Выйти</a>
                 </div>
                </div>
                
        </div>
      </div>
  </div>
</header>
                
                <style>
                @media screen and (max-width:575px){
                #sp-bottom{    padding: 50px 0 80px !important;}
              }
                </style>




<?php else: ?>
<script>
    jQuery(document).ready(function() {
        jQuery('.pl-popup-link').on('click', function() {
            var action = this.hash.substr(1);
            jQuery('#'+action).modal('show');
        });
        jQuery('.pl-modal form').on('submit',function(e) {
            e.preventDefault();
            var curModal = jQuery(this).closest('.pl-modal');
            var curForm = curModal.find('form');
            curForm.find('div.error-popup-enter').html('').addClass('d-none');
            var formData = (curForm.serialize());

            jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_ajax&plugin=ajaxlogin&format=json',
                dataType: 'json',
                data: formData
            }).done(function (data) {
				if (data.success == false) {
                    curForm.find('div.error-popup-enter').html(data.message).removeClass('d-none');
                }else{
                    window.location.href = 'personal';
                }
                //if (data.data[0].user_id != undefined && data.data[0].user_id > 0);
            });
        });
    });
</script>
  
<header id="header-notauth" class="notauth">
    <div class="container">
      <div class="row">
        <div class="col-8 col-sm-4 col-lg-4 col-md-4 text-left">
          <a href="/" class="logotype"><img src="https://prizolove.com/images/Header/logo-white.svg"></a>
        </div>
         
        <div class="col-4 col-sm-8 col-lg-8 col-md-8 text-right">
          <div class="block__button-sign">
            <a data-toggle="modal" href="#auth-popup" class="pl-popup-link sign-in">Вход</a>
            <a data-toggle="modal" href="#reg-popup" class="pl-popup-link sign-up">Регистрация</a>
          </div>
        </div>
      </div>
  </div>
</header>








<script type="text/javascript">
  $(document).ready(function () {
    $('ul[role="menu"]')
        .on('show.bs.collapse', function (e) {
        $(e.target).prev('a[role="menuitem"]').addClass('active');
    })
        .on('hide.bs.collapse', function (e) {
        $(e.target).prev('a[role="menuitem"]').removeClass('active');
    });

    $('a[data-toggle="collapse"]').click(function (event) {

        event.stopPropagation();
        event.preventDefault();

        var drop = $(this).closest(".dropdown");
        $(drop).addClass("open");

        $('.collapse.in').collapse('hide');
        var col_id = $(this).attr("href");
        $(col_id).collapse('toggle');

    });
});
</script>

          
          
<?php endif; ?>

