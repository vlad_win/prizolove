<?php
defined('_JEXEC') or die('Restricted access');

JLoader::import('pl_lib.library');
$countryCode = 'ua';
$countries = array('ua', 'ru', 'us', 'kz');
if (function_exists('geoip_country_code_by_name')) {
       	$countryCode = strtolower(geoip_country_code_by_name($_SERVER['REMOTE_ADDR']));
}
if (!in_array($countryCode, $countries)) $countryCode = 'ua';


$userFormData = json_decode($this->orderUserData, true);
$fullname = (!empty($userFormData['fullname']))?$userFormData['fullname']:'';
$city = (!empty($userFormData['city']))?$userFormData['city']:'';
$np_address = (!empty($userFormData['np_address']))?$userFormData['np_address']:'';
$phone = (!empty($userFormData['phone']))?$userFormData['phone']:'';
$fname = (!empty($userFormData['fname']))?$userFormData['fname']:'';
$lname = (!empty($userFormData['lname']))?$userFormData['lname']:'';
$mname = (!empty($userFormData['mname']))?$userFormData['mname']:'';
$email = (!empty($userFormData['email']))?$userFormData['email']:'';
$script = "
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '2066163586738295');
fbq('track', 'PageView');
</script>
<noscript><img height='1' width='1' style='display:none'
src='https://www.facebook.com/tr?id=2066163586738295&ev=PageView&noscript=1'
/></noscript>
<!-- End Facebook Pixel Code -->
";
$doc = JFactory::getDocument();
$doc->addCustomTag($script);
$app = JFactory::getApplication();
$productId = $app->input->get('pid', false, 'INT');
$user = JFactory::getUser();
$packets = [254,256,257,258,324];
if ($user->id) {
    if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
        echo 'This code can not work without the AcyMailing Component';
        return false;
    }
    $userClass = acymailing_get('class.subscriber');

    $fullname = $user->name;
    $email = $user->email;
    $acyUser = $userClass->get($email);
    $phone = $acyUser->phone;
}
if ($this->showBalance && $user->id) {   

    $comission = PlLibHelperBalances::getInstance()->getBalance($user->id);
    $balance = PlLibHelperRates::convertUSD($comission);
    $bonus = number_format($balance, 2,'.','');
} else {
    $bonus = 0;
}

?>
<style type="text/css">


  
  body{font-family:"Montserrat",arial,sans-serif;color:#333;font-weight:500 !important;}
*:focus{outline:none;}


.orderpage{margin:30px 0;}
.orderpage .container{max-width:768px;}


/* ЗАГОЛОВОК */
.orderpage .block_products .title,.orderpage .block_contact .title{font-size:18px;position:relative;padding-left:45px;font-weight:600;}
.orderpage .block_products .title::before,.orderpage .block_contact .title::before{content:"";position:absolute;width:35px;height:15px;left:0;top:50%;
  transform:translateY(-50%);background:#4bbe3f;}


/* БЛОК С ПЕРЕЧНЕМ ПРОДУКТОВ */
.product_parent{margin-top:15px;background:#FCF3D0;border:1px solid #dbca9d;box-shadow:0px 3px 0px #dbca9d;border-radius:5px;padding:0px 15px 0px 15px;}
.product{position:relative;padding:15px 0;border-bottom:1px solid #dbca9d;}
.product:last-child{border-bottom:none;}
.product div{display:inline-block;vertical-align:middle;}
.product .image_product img{max-width:75px;width:100%;}
.product .image_product{width:10%;overflow:hidden;}
.product .name_product{width:69%;font-size:18px;line-height:21px;font-weight:500;color:#333;padding-left:20px;}
.product .name_product .quantity{font-size:14px;opacity:0.6;}
.product .price_product{width:19%;font-size:18px;font-weight:500;}
.product .close-btn{position:absolute;right:10px;top:50%;margin-top:-12px;color:#333;transition:.5s;font-weight: bold;text-align:center;
    opacity:0.3;
    color: #333;
    width: 24px;
    height: 24px;
    font-size: 14px;
    line-height: 24px;
    border-radius: 50%;
    transition: 1s;
    z-index: 9999;
    cursor: pointer !important;}
.product .close-btn:hover{opacity:1;background: #e74c3c;
    color: white;
    transform: rotate(360deg);}


/* БЛОК ИТОГО К ОПЛАТЕ */
.total-price{font-weight:500;margin:15px 15px 0 15px;font-size:18px;}
.total-price .text{margin-right:20px;}
.total-price .price{font-weight:bold;font-size:18px;color:#e74c3c;}


/* БЛОК ВАШИ КОНТАКТНЫЕ ДАННЫЕ */
.block_contact{margin:30px 0;padding: 0;}
.input_block label{font-weight:500;}


/* СТИЛИЗАЦИЯ ИНПУТОВ */
.input_block{font-size:16px;color:#364A5D;margin:15px 15px 0 15px;}
.input_block label{width:31%;line-height:39px;display:inline-block;vertical-align:middle;}
.input_block label i{margin-right:7px;font-size:16px;}
.input_block input{width:60%;display: inline-block;
    height: 34px;
    padding: 6px 12px;
  transition:.5s;
    font-size: 16px;
  font-weight:500;
    line-height: 1.428571429;
    color: #333;
    background-color: transparent;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);   
}
.input_block input:focus{border-color: #66afe9;
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,0.6);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,0.6);}


/* СТИЛИЗАЦИЯ СПОСОБА ОПЛАТЫ */
.payment_block{margin:15px 15px 0px 15px;font-size:16px;}
.payment_block .method{display:inline-block;width:30%;color:#364A5D;}
.payment_block .check-f-i-field{display:inline-block;width:64%;color:black;}
.check-f-i-field label{cursor:pointer;transition:.5s;position:relative;font-weight:500;}
.check-f-i-field label:hover{color:#e74c3c;}


/* СТИЛИЗАЦИЯ КНОПКИ */
.block_contact .btn-block{margin:15px 0 0 31%;text-align:left;}
.block_contact .btn-order{font-size:16px;display:inline-block;color:white;vertical-align:middle;padding:7px 20px;font-weight:500;background: -webkit-linear-gradient(top,#79d670,#4bbe3f);box-shadow: 3px 3px 7px 0 rgba(105,206,95,.5), inset 0 -3px 0 0 #3a9731;    text-shadow: 0 -1px 0 rgba(0, 0, 0, .3);border-radius:5px;text-align:center;transition:.5s;border:none !important;cursor:pointer !Important;}
.block_contact .btn-order:hover{text-decoration:none !important;background:-webkit-linear-gradient(top,#69f95b,#1fc80d);}


/* БЛОК О СПЕЦИАЛИСТЕ */
.block_contact .specialist{text-align:left;font-size:12px;margin: 5px 0px 0px 31%;color:#333;}
.block_contact .specialist .mark{background:white;color:#e74c3c;}


/* БЛОК СЕКЬЮРНОСТИ */
.block_secure{font-size:10px;color:rgba(0,0,0,0.50);padding:30px 0 0 0;border-top:1px solid rgba(0,0,0,0.08);text-align:center;line-height:14px;}
.block_secure img{height:55px}
.order_img_container{    display:inline-box; display: -webkit-inline-box;}

/* MEDIA ЗАПРОСЫ */
@media screen and (max-width:750px){
  .input_block label{width:49%;}
  .input_block input{width:49%;}
  .payment_block .method{width:49%;}
  .payment_block .check-f-i-field{width:49%;}
  .block_contact .btn-block{margin:30px 0 0 0;text-align:center;}
  .block_contact .specialist{text-align:center;margin:5px 0px 0px 0px;padding:0 100px;}
  .total-price{margin:15px 0 0 0;}
  .total-price .text{margin-right:0px;}
  .input_block{margin:15px 0px 0 0px;}
  .payment_block{margin:30px 0px 0px 0px;}
}

@media screen and (max-width:690px){
  .product .close-btn{top:19%;}
}

@media screen and (max-width:628px){
  .input_block label{width:100%;}
  .input_block input{width:100%;}
  .payment_block .method{width:100%;}
  .payment_block .check-f-i-field{width:100%;}
  .block_contact .specialist{padding:0 0px;}
}

@media screen and (max-width:600px){
  .product .image_product{width:10%;}
  .product .name_product{width:58%;}
  .product .price_product{width:29%;text-align:right;}
}

@media screen and (max-width:470px){
  .product .image_product{width:29%;}
  .product .name_product{width:69%;}
  .product .price_product{width:100%;text-align:center;}
}
  
  
                                            .order-block{margin-top:50px;}
                                 
                                 .specialist{
                                   font-size:18px!important;
                                 }
                                 
                                
                                 .order_button_more{
                                   margin:0px 0 0 0;
                                   display:inline-block;
       padding: 5px 10px;
        color: #333;
    text-decoration: underline;
}
                                 .order_button_more:hover{color:#e74c3c;text-decoration:none;}
                                                                                 
                                                                                      
                                                                                    .attention_block::before{content: "";
    position: absolute;
    top: -9px;
    left: -15px;
    width: 35px;
    height: 40px;
    background: url(/images/closed-bank.png) no-repeat center center;
    background-size: contain;}
                                   .attention_block{padding: 10px 40px;
    font-weight: 500;
    font-size: 10px;
    position: relative;
    max-width: 100%;
    width: 70%;
    line-height: 15px;
    color: #716844;
    margin: 0px 0 10px 30%;
    background: #FCF3D0;
    border: 1px solid #dbca9d;
    box-shadow: 0px 3px 0px #dbca9d;
    border-radius: 5px;
    text-align: left;}
                       
                       @media screen and (max-width:750px){
                         
                              .attention_block{ width: 49%;
    margin-left: 50%;}
                       }
                       @media screen and (max-width:628px){
                         
                         .attention_block{ width: 100%;
    margin-left: 0%;}
                       }
</style>

        <div id="system-message-container">
            <?php
                $app = JFactory::getApplication();
                $messages = $app->getMessageQueue();
                foreach($messages as $message) { ?>
                    <div class="alert alert-<?=$message['type'];?> alert-dismissible fade show">
                        <i class="fas fa-check" style="padding-right:10px;"></i> <?=$message['message'];?>
                        <button data-dismiss="alert" aria-label="Close" class="btn close-btn btn-link"><i class="fas fa-times-circle"></i></button>
                    </div>
            <?php }

            ?>
        </div>
        
     
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">


<div class="orderpage">
  <div class="container">
    <div class="row">
      <form method="post" action="index.php?option=com_orderpage&task=checkout">
      <div class="block_products col-12 col-md-12 col-sm-12 col-lg-12">
        <div class="title">Данные о платеже</div> 

 	<?php
                    $cost = 0;
                    $discountPercent = 0;
                ?>
                <?php foreach($this->cart['items'] as $pid=>$item) : ?>

                    <?php
                        $cost += ($item['quantity']*$item['price']);
                        if ($item['discount_percent']) $discountPercent = $item['discount_percent'];
                    ?>

   
  <div id="sppb-addon-1526894067420" class="clearfix pid-<?=$pid;?>">
                        <div class="parent-check">
                  
   <div class="product_parent">
          <div class="product">
            <div class="image_product"><?php if ($item['image']) :?> <img src="<?=$item['image'];?>" alt="<?=$item['name'];?>"> <?php endif; ?></div>
            <div class="name_product"> <div class="check-f-i-field"><?=$item['name'];?>
                                <br/><span class="quantity">Кол-во: <?=$item['quantity'];?> шт. </span></div></div>
                  
            <div class="price_product"><span class="item-cost">
                  <?=number_format(PlLibHelperRates::recalc(($item['quantity']*$item['price']),false), 0,'.', ' ');?></span> <?=PlLibHelperRates::currencyLabel();?></div>
              
              
       <a data-product="<?=$pid;?>" href="javascript:void(0)" class="close-btn remove"><i class="fas fa-times"></i></a> 
  
                </div>
                    </div>

              <?php endforeach; ?>
                <?php if ($cost) : ?>
                  
                     <div id="sppb-addon-1526894067482" class="clearfix">
                        <div class="sppb-addon sppb-addon-text-block 0">
				<?php if ($bonus > 0 && $discountPercent) : ?>
                                <?php
                                $maxDiscount = intval(($cost/100)*$discountPercent);
                                if ($maxDiscount > $bonus) $maxDiscount = intval($bonus);
                                ?>
                                <div class="total-price">
                                    <span class="text">У вас бонусном счету:</span>
                                    <span class="price" style="color:#333 !important;"><?=$bonus;?> <sup><?=PlLibHelperRates::currencyLabel();?></sup></span>
                                </div>
                                <div class="total-price">
                                    <span class="text">Продавец готов принять:</span>
                                    <span class="price" style="color:#333 !important;"><?=$maxDiscount;?> <sup>грн</sup></span>
                                    <div class="input-wrapper" style="margin-top:10px;">
                                        <input max="<?=$maxDiscount;?>" name="bonus" type="number" placeholder="Введите сумму бонусов" value="<?=$maxDiscount;?>">
                                    </div>

                                </div>
                                <div class="total-price">
                                    <span class="text">Итого к оплате:</span>
                                    <span class="price"><span data-cost="<?=$cost;?>"><?=number_format(PlLibHelperRates::recalc($cost  - $bonus,false), 2,'.', ' ');?></span> <sup><?=PlLibHelperRates::currencyLabel();?></sup></span>

                                </div>
                            <?php else: ?>
                                <div class="total-price"><span class="text">Итого к оплате:</span>&nbsp;&nbsp;<strong> <span class="price"><?=number_format(PlLibHelperRates::recalc($cost,false), 0,'.', ' ');?> <sup>  <?=PlLibHelperRates::currencyLabel();?></sup></span></strong></div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                  
    
              </div>  
        </div>       
              
   
         
         
   <div class="block_contact col-12 col-md-12 col-sm-12 col-lg-12"> 
        <div class="title">Ваши контактные данные</div> 
       
           
         
         
          <div class="check-step-body" data-step="1">
         
         
        <div class="input_block">
            <label for="name_input"><i class="fas fa-user"></i> Имя и фамилия:</label>
 <input id="name_input" required autofocus="autofocus" class="input-text step1" type="text" name="fullname" value="<?=$fullname;?>">
        </div>
              
              
              
        <div class="input_block">
            <label for="phone_input"><i class="fas fa-phone"></i> Мобильный телефон:</label>
 <input id="phone_input" required autofocus="autofocus" placeholder="Ваш номер телефона" class="input-text step1" name="phone" type="tel" value="<?=$phone;?>">
        </div>
              
              
        <div class="input_block">
            <label for="email_input"><i class="fas fa-envelope"></i> Электронная почта:</label>   
<input id="email_input" required autofocus="autofocus" placeholder="Ваша эл. почта" class="input-text step1" name="email" type="email" value="<?=$email;?>">
        </div>
     
              
        <div class="payment_block">
          <div class="method">Способ оплаты:</div>
            <div class="check-f-i-field">
             <input type="radio" id="r4" name="payment" checked value="card" class="step2"/>
                                        <label for="r4"><span></span>Оплата картой Visa/MasterCard</label>
  </div>
    </div>
                                          
          </div>                                  
                   
   <div class="attention_block">
                                          Платеж за товар вы осуществляете на стороне сертифицированного, банковского процессинга. По зашифрованному протоколу.<b> Никаких данных о вашей карте компания Prizolove не получает и не хранит.</b>
                                          </div>                
  <div class="btn-block"><button id="order-submit" type="submit" data-step="2" class="btn-order">Заказ подтверждаю</button><a href="https://prizolove.com/362" class="order_button_more" target="_blank">Больше информации о пакетах</a></div>
   <div class="specialist"><span class="mark">*</span>
                                          
Письмо об активации пакета прийдет к вам на почту автоматически.
                                       
                                          
                                          </div>
                               
                                  
                        
                     
                          
          </form>
  </div>
                                          
                                          
                                          
                            
                                             
      <div class="block_secure col-12 col-md-12 col-sm-12 col-lg-12">
        Конфиденциальная информация о номере Вашей карточки, CVV2 коде и другие ее реквизиты передаются на наш сайт в зашифрованном состоянии. Для обмена информацией с Вами применяется промышленный стандарт SSL-шифрования с использованием стойкой криптографии (длина ключа 128 бит). <br/>
            <div class="order_img_container">                              
            <img src="https://prizolove.com/images/visa-secure.PNG">
        	<img src="https://prizolove.com/images/mastercard.PNG">
            <img src="https://prizolove.com/images/pci-dss.png">
      </div>
                                          </div>
</div>
  </div>
</div>                   
                                            
                                            
        </div>

        <script>
            jQuery(document).ready(function() {
                jQuery('input[name="phone"]').each(function () {
		<?php if ($countryCode == 'ua') :?>
                    jQuery(this).mask('+38(099) 999 99 99', {placeholder: "+38(0--) --- -- --"});
		<?php endif;  ?>        

                });		
		jQuery('input[name="bonus"]').on('input', function() {
                    var curDiscount = jQuery(this).val();
                    var cost = jQuery('span.price span').data('cost');
                    jQuery('span.price span').text(cost - curDiscount);
                });
                jQuery('#order-submit').on('click', function () {
                    let fPhone = jQuery('input[name="phone"]');
                    fPhone.val(fPhone.val().replace(/^(\+){1}|([^\+\d])/gm, ''));
                });
                jQuery('.parent-check a.remove').on('click', function() {
                    var removeButton = jQuery(this);
                    jQuery.ajax({
                        method: "POST",
                        url: "/index.php?option=com_ajax&plugin=cart&format=json",
                        dataType: 'json',
                        data: {pid: removeButton.data('product'), act: 'delete'}
                    }).done(function(data) {
                        removeButton.closest('.pid-' + removeButton.data('product')).remove();
                        var cartCost = 0;
                        jQuery('.item-cost span').each(function() {
                            cartCost = cartCost + parseFloat(jQuery(this).text());
                        });
                        jQuery('.cart-sum span').text(cartCost);
                        if (cartCost == 0) location.reload();
                    });
                    return false;
                });
                jQuery('button.btn-link-i[type="button"]').on('click', function() {
                    if (jQuery(this).attr('type') == 'submit') return;
                    var step = parseInt(jQuery(this).data('step'));
                    var filled = true;
                    jQuery('input.step' + step).each(function() {
                        this.setCustomValidity('');
                        if (jQuery(this).val() == '') {
                            filled = false;
                            jQuery(this).focus();
                            this.setCustomValidity('Заполните это поле');
                            this.reportValidity();

                        }
                    });
                    if (filled) jQuery('div[data-step="'+(step + 1)+'"]').removeClass('hidden');
                });

                jQuery('button.btn-link-i[type="submit"]').on('click', function() {
                    var step = parseInt(jQuery(this).data('step'));
                    var filled = true;
                    jQuery('input.step' + step).each(function() {
                        this.setCustomValidity('');
                        if (jQuery(this).val() == '') {
                            filled = false;
                            jQuery(this).focus();
                            this.setCustomValidity('Заполните это поле');
                            this.reportValidity();
                        }
                    });
                    if (!filled) return false;
		    fbq('track', 'Purchase', {
			value: 5,
			currency: '$',
		    });	
                });

                <?php /* jQuery('input[name="payment"]').on('change', function() {
            if (jQuery(this).val() == 'card') jQuery('#auth').modal('show');
        }); */ ?>

                <?php /* jQuery('#login').on('click',function(e){
            e.preventDefault();
                $username = jQuery('#username').val();
                $password = jQuery('#password').val();
            jQuery.ajax({
                    type: 'post',
                    url: 'index.php?option=com_ajax&plugin=ajaxlogin&format=json',
                    dataType: 'json',
                    data: {username: $username, password: $password}
            }).done(function(data) {
                    if (data.data[0].user_id != undefined && data.data[0].user_id > 0) jQuery('#auth').modal('hide');
            });


        }); */ ?>

            });
        </script>