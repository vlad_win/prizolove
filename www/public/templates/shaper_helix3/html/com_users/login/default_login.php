<style>
  #sp-bottom,#sp-section-2{display:none;}
.block_login-form{background:#34495E;min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;}
.popup_enter{margin:0px auto;}  
.logo-wrapper{width:100%;text-align:center;margin:20px 0;}
.logo-wrapper img{display:inline-block;}
.form-check{    text-align: left;}
.form-check #remember{margin:0 0 20px 0;width:auto;display:inline-block !important;position:relative;}
 input[type=password]{margin-bottom:0px !important;}
</style>
<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

//print_r(get_class_methods($this->form));

//die;

?>
  <div class="block_login-form">
  <div class="container">
<div class="row">
   <div class="col-sm-12 col-sm-offset-12 text-center"><div class="logo-wrapper"><a href="/"><img src="https://prizolove.com/images/logotype-white.png"></a>
  </div></div>
	<div class="col-sm-12 col-sm-offset-12 text-center">
		<div class="login<?php echo $this->pageclass_sfx?> popup_enter">
	
  
 
				<h1 class="title">
					Вход в Prizolove
				</h1>

			<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
			<div class="login-description">
			<?php endif; ?>

				<?php if ($this->params->get('logindescription_show') == 1) : ?>
					<?php echo $this->params->get('login_description'); ?>
				<?php endif; ?>

				<?php if (($this->params->get('login_image') != '')) :?>
					<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USERS_LOGIN_IMAGE_ALT')?>"/>
				<?php endif; ?>

			<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
			</div>
			<?php endif; ?>

			<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-validate">

				<?php /* Set placeholder for username, password and secretekey */
					$this->form->setFieldAttribute( 'username', 'hint', JText::_('Электронная почта') );
					$this->form->setFieldAttribute( 'password', 'hint', JText::_('JGLOBAL_PASSWORD') );
					$this->form->setFieldAttribute( 'secretkey', 'hint', JText::_('JGLOBAL_SECRETKEY') );
				?>
				<div class="inputs">
				<?php foreach ($this->form->getFieldset('credentials') as $field) : ?>
					<?php if (!$field->hidden) : ?>
						<div class="form-group">
							<div class="group-control">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
				</div>
				<?php if ($this->tfa): ?>
					<div class="form-group">
						<div class="group-control">
							<?php echo $this->form->getField('secretkey')->input; ?>
						</div>
					</div>
				<?php endif; ?>

				<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
					<div class="form-check">						
						<input id="remember" type="checkbox" name="remember" class="form-check-input" value="yes">
						<label class="form-check-label" for="remember">						
						<?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?>
						</label>
					</div>
				<?php endif; ?>

				<div class="form-group">
					<button type="submit" class="btn btn-form btn-submit">
						<?php echo JText::_('JLOGIN'); ?>
					</button>
				</div>

				<?php $return = $this->form->getValue('return', '', $this->params->get('login_redirect_url', $this->params->get('login_redirect_menuitem'))); ?>
 				<input type="hidden" name="return" value="<?php echo base64_encode($return); ?>" /> 
				<?php echo JHtml::_('form.token'); ?>

			</form>
			
			<div class="form-links">
                  <div class="enter_in">Впервые у нас?
						<a href="/registration">
						Зарегистрируйтесь!</a></div>
					<div class="enter_in">
						Не помните пароль?
						<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
						Восстановите его</a>
					
					</div>
				</ul>
			</div>
		</div>

		

	</div>
</div>
                          </div></div>
