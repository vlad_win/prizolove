<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
?>
<style>
    #sp-bottom,#sp-section-2{display:none;}
    .block_login-form{background:#34495E;min-height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;}
    .popup_enter{margin:0px auto;}
    .logo-wrapper{width:100%;text-align:center;margin:20px 0;}
    .logo-wrapper img{display:inline-block;}
    .form-check{    text-align: left;}
    .form-check #remember{margin:0 0 20px 0;width:auto;display:inline-block !important;position:relative;}
    input[type=password]{margin-bottom:0px !important;}
</style>

<div class="block_login-form">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-offset-12 text-center">
                <div class="logo-wrapper"><a href="/"><img src="https://prizolove.com/images/logotype-white.png"></a></div>
            </div>
            <div class="col-sm-12 col-sm-offset-12 text-center">
                <div class="login<?php echo $this->pageclass_sfx?> popup_enter">
                    <?php if ($this->params->get('show_page_heading')) : ?>
                        <h1>
                            <?php echo $this->escape($this->params->get('page_heading')); ?>
                        </h1>
                    <?php endif; ?>

                    <form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate">
                        <?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
                            <p><?php echo JText::_($fieldset->label); ?></p>
                            <?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
                                <div class="form-group">
                                    <?php echo $field->label; ?>
                                    <div class="group-control">
                                        <?php echo $field->input; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endforeach; ?>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary validate"><?php echo JText::_('JSUBMIT'); ?></button>
                        </div>
                        <?php echo JHtml::_('form.token'); ?>
                    </form>
                </div>



            </div>
        </div>
    </div>
</div>