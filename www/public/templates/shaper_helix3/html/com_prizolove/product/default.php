<?php                                                                                                   
defined('_JEXEC') or die('Restricted access.');
$app = JFactory::getApplication();
$document = JFactory::getDocument();
$user = JFactory::getUser();
$pid = $app->input->get('pid', 1, 'INT');
$product = PlLibHelperCatalog::getInstance()->getProductById($pid);
if (!$product) {
    JError::raiseError(404, JText::_("Page Not Found"));
}
if ($user->id) {
    $bonus = 0;
    $bonusAccount = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
    if ($bonusAccount->value) {
        $bonus = number_format(PlLibHelperRates::recalc(PlLibHelperRates::convertUSD($bonusAccount->value)),2,'.','');
    }
    $price = number_format(PlLibHelperRates::recalc(PlLibHelperRates::convertUSD($product->price_usd)),2,'.','');
    $priceClean = number_format(PlLibHelperRates::recalc(PlLibHelperRates::convertUSD($product->price_usd)),2,'.','');
    $maxDiscount = intval(($priceClean/100)*$product->discount_percent);

    if ($maxDiscount > $bonus) $maxDiscount = intval($bonus);
}
$hostName = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];

$document->setMetaData( 'og:type', 'product' );
$document->setMetaData( 'og:title', $product->name );
$document->setMetaData( 'og:image', $hostName.'/'.$product->image );
//$categories = PlLibHelperCatalog::getInstance()->getCategories();
//$products = PlLibHelperCatalog::getInstance()->getProductsByCategoryId($cid);
?>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,900" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">

<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a href="https://prizolove.com/">Главная</a> - <a href="https://prizolove.com/catalog">Каталог товаров</a> - <span><?=$product->name;?></span>
            </div>
        </div>
    </div>
</div>
<div class="product-block">
    <div class="container">
        <div class="row product-wrapper">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-12"><h1><?=$product->name;?></h1></div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-6 image-wrapper"><?php if ($product->image) :?><img src="<?=$product->image;?>"><?php endif;?></div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-6 about-product-wrapper">
              
                <div class="price-block">
		    <?php if ($product->status) :?>
                    <div class="price">
                        <span class="number"><?=number_format(PlLibHelperRates::recalc(PlLibHelperRates::convertUSD($product->price_usd)),2,'.','');?></span>
                        <span class="valute"><?=PlLibHelperRates::currencyLabel();?></span>


                    </div><a class="button" href="/orderpage?pid=<?=$product->id;?>"><img src="https://prizolove.com/images/cart-catalog.png">Купить</a>
                          
                    <?php /* <div class="quantity"><a href="#" class="plus"></a><input class="input-text"  value="1" type="text" ><a href="#" class="minus"></a></div> */ ?>

                    <br/>
                          <div class="bonuses-block">Продавец готов взять бонусами: <span class="number"><?=$maxDiscount;?></span> <sup>грн</sup></div> <div class="qa-product-block"><i class="fas fa-question-circle"></i>
                            <span class="block-how">Выигрывайте бонусы проходя испытания в вашем кабинете или приглашайте друзей - 5% от суммы на покупки за каждую регистрацию.</span>
                          </div>
		     <?php else: ?>
			<!-- нет в наличии -->
		     <?php endif;?>
                </div>
                <div class="sertificate-block"><img src="https://prizolove.com/images/diamond_red.png"> <span class="red">Закажи и прими участие в розыгрыше 10 000$.</span> После оплаты, Вы получите активированный Сертификат Финалиста на розыгрыш 10 000$, который будет автоматически отправлен на Вашу почту<?php /* <a href="#" class="see-more">Узнать больше</a> */ ?></div> 
                  <div class="share-block"><b>Поделитесь с друзьями</b>, каждая покупка по этой ссылке даст Вам 5% от суммы товара 
                          <div class="social-buttons">
                      


<script type="text/javascript">(function(w,doc) {
if (!w.__utlWdgt ) {
    w.__utlWdgt = true;
    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
}})(window,document);
</script>
    <div data-mobile-view="true" data-share-size="30" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1798746" data-mode="share" data-background-color="#ffffff" data-share-shape="round" data-share-counter-size="12" data-icon-color="#ffffff" data-mobile-sn-ids="fb.vk.ok." data-text-color="#000000" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="fb.vk.ok." data-preview-mobile="false" data-selection-enable="false" data-exclude-show-more="true" data-share-style="1" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons" ></div>
  

  
  

  


                </div> </div>          
                   
                            
                            
                            
                <div class="height"></div>
                            <div class="money-block">
                            <div class="left_label">Возврат товара</div><div class="right_label">
                            <div class="about-return">Если товар не соответствует описанию, вы можете договориться с продавцом о компенсации без необходимости возвращать товар.</div> 
                            </div>
                            </div>
                <div class="money-block">
                            <div class="left_label">Оплата</div><div class="right_label"> 
                           <div class="icon"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Слой_1" x="0px" y="0px" viewBox="0 0 222 141" style="enable-background:new 0 0 222 141;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
	.st1{fill:#F4B519;}
	.st2{fill:#2A2A6C;}
</style>
<title>Visa</title>
<path class="st0" d="M0,0h222v141H0V0z"/>
<path class="st1" d="M3.7,118.1h214.6v19.2H3.7V118.1z"/>
<path class="st2" d="M3.7,3.7h214.6v19.2H3.7V3.7z M109.2,44.1L97.8,97H84.2l11.3-52.9L109.2,44.1L109.2,44.1z M166.7,78.3l7.2-19.9  l4.1,19.9H166.7z M182,97h12.6l-11.1-52.9h-11.7c-2.6,0-4.9,1.5-5.8,3.9l-20.5,49h14.4l2.9-7.9h17.6L182,97z M146.3,79.7  c0.1-14-19.3-14.7-19.2-21c0-1.9,1.9-3.9,5.8-4.4c4.6-0.4,9.3,0.4,13.5,2.4l2.4-11.2c-4.1-1.5-8.4-2.3-12.8-2.3  c-13.5,0-23,7.2-23.1,17.5c-0.1,7.6,6.8,11.9,12,14.4c5.3,2.6,7.1,4.3,7.1,6.6c0,3.6-4.3,5.1-8.2,5.2c-4.9,0.1-9.7-1-14.1-3.3  L107.3,95c3.2,1.5,9.1,2.7,15.2,2.8C136.9,97.9,146.3,90.8,146.3,79.7 M89.6,44.1L67.4,97H53L42.1,54.8c-0.7-2.6-1.2-3.5-3.3-4.6  c-4.3-2.1-8.8-3.6-13.5-4.5l0.3-1.5h23.3c3.1,0,5.8,2.3,6.3,5.4L61,80.1l14.2-36H89.6"/>
</svg></div> <div class="icon"><svg xmlns="http://www.w3.org/2000/svg" id="Слой_1" data-name="Слой 1" viewBox="0 0 181.05 140.82">
  <g>
    <path d="M54.11,140.24v-9.33A5.53,5.53,0,0,0,48.27,125,5.75,5.75,0,0,0,43,127.65,5.46,5.46,0,0,0,38.14,125a4.91,4.91,0,0,0-4.35,2.21v-1.83H30.55v14.86h3.26V132a3.48,3.48,0,0,1,3.64-3.95c2.14,0,3.23,1.4,3.23,3.92v8.27h3.26V132a3.5,3.5,0,0,1,3.64-3.95c2.21,0,3.26,1.4,3.26,3.92v8.27h3.26Zm48.27-14.86H97.1v-4.51H93.84v4.51h-3v3h3v6.78c0,3.45,1.34,5.5,5.16,5.5a7.58,7.58,0,0,0,4-1.15l-0.93-2.77a6,6,0,0,1-2.86.84c-1.55,0-2.14-1-2.14-2.49v-6.71h5.28v-3ZM130,125a4.38,4.38,0,0,0-3.92,2.18v-1.8h-3.2v14.86h3.23v-8.33c0-2.46,1.06-3.82,3.11-3.82a5.27,5.27,0,0,1,2,.37l1-3.11a6.92,6.92,0,0,0-2.3-.4Zm-41.68,1.55A11.11,11.11,0,0,0,82.21,125c-3.76,0-6.22,1.8-6.22,4.76,0,2.42,1.8,3.92,5.13,4.38l1.55,0.22c1.77,0.25,2.61.71,2.61,1.55,0,1.15-1.18,1.8-3.39,1.8A7.91,7.91,0,0,1,77,136.17l-1.55,2.52a10.72,10.72,0,0,0,6.43,1.93c4.29,0,6.78-2,6.78-4.85s-2-4-5.19-4.45l-1.55-.22c-1.4-.19-2.52-0.47-2.52-1.46s1.06-1.74,2.83-1.74a9.57,9.57,0,0,1,4.66,1.27ZM174.86,125a4.38,4.38,0,0,0-3.92,2.18v-1.8h-3.2v14.86H171v-8.33c0-2.46,1.06-3.82,3.11-3.82a5.27,5.27,0,0,1,2,.37l1-3.11a6.92,6.92,0,0,0-2.3-.4Zm-41.65,7.77a7.51,7.51,0,0,0,7.23,7.78q0.35,0,.7,0a7.77,7.77,0,0,0,5.35-1.77l-1.55-2.61a6.53,6.53,0,0,1-3.89,1.34,4.77,4.77,0,0,1,0-9.51,6.53,6.53,0,0,1,3.89,1.34l1.55-2.61a7.77,7.77,0,0,0-5.35-1.77,7.51,7.51,0,0,0-7.92,7.07q0,0.35,0,.7v0.06Zm30.28,0v-7.43h-3.23v1.8a5.64,5.64,0,0,0-4.66-2.18,7.77,7.77,0,0,0,0,15.54,5.64,5.64,0,0,0,4.66-2.18v1.8h3.23v-7.37Zm-12,0a4.46,4.46,0,1,1,0,.35Q151.44,133,151.45,132.78Zm-39-7.77a7.77,7.77,0,1,0,.22,15.54h0a9,9,0,0,0,6.09-2.08l-1.55-2.39a7.07,7.07,0,0,1-4.32,1.55,4.12,4.12,0,0,1-4.45-3.64h11.05v-1.24c0-4.66-2.89-7.77-7.06-7.77Zm0,2.89a3.68,3.68,0,0,1,3.76,3.6h-7.77a3.82,3.82,0,0,1,3.95-3.61h0.06Zm81.08,4.91v-13.4h-3.23v7.77a5.64,5.64,0,0,0-4.66-2.18,7.77,7.77,0,0,0,0,15.54,5.64,5.64,0,0,0,4.66-2.18v1.8h3.23v-7.37Zm-12,0a4.46,4.46,0,1,1,0,.35Q181.49,133,181.5,132.81Zm-109.15,0v-7.43H69.11v1.8A5.64,5.64,0,0,0,64.45,125a7.77,7.77,0,0,0,0,15.54,5.64,5.64,0,0,0,4.66-2.18v1.8h3.23v-7.37Zm-12,0a4.29,4.29,0,1,1,0,.37q0-.18,0-0.37h0Zm138.59,5.27a1.49,1.49,0,0,1,1.4.89,1.45,1.45,0,0,1,0,1.15,1.49,1.49,0,0,1-.81.78,1.46,1.46,0,0,1-.59.12,1.55,1.55,0,0,1-1.4-.9,1.46,1.46,0,0,1,0-1.15,1.44,1.44,0,0,1,1.4-.87v0Zm0,2.63a1.1,1.1,0,0,0,.45-0.09,1.17,1.17,0,0,0,.36-0.25,1.12,1.12,0,0,0-.36-1.8,1.12,1.12,0,0,0-.45-0.09,1.17,1.17,0,0,0-.45.09,1.13,1.13,0,0,0-.37.25,1.15,1.15,0,0,0,.82,1.91v0Zm0.09-1.85a0.62,0.62,0,0,1,.4.12,0.39,0.39,0,0,1,.14.33,0.37,0.37,0,0,1-.11.28,0.54,0.54,0,0,1-.33.14l0.45,0.51H199.2l-0.42-.51h-0.14v0.51h-0.3v-1.37H199Zm-0.34.26v0.37H199a0.33,0.33,0,0,0,.19,0,0.16,0.16,0,0,0,0-.14,0.16,0.16,0,0,0,0-.14,0.33,0.33,0,0,0-.19,0h-0.34v-0.09Z" transform="translate(-21.19 -0.2)"/>
    <g>
      <path d="M87.23,12.16h49v88h-49v-88Z" transform="translate(-21.19 -0.2)" style="fill: #ff5f00"/>
      <path d="M90.34,56.16a55.86,55.86,0,0,1,21.37-44,56,56,0,1,0,0,88A55.86,55.86,0,0,1,90.34,56.16Z" transform="translate(-21.19 -0.2)" style="fill: #eb001b"/>
      <path d="M202.23,56.16a56,56,0,0,1-90.52,44,56,56,0,0,0,0-88A56,56,0,0,1,202.23,56.16ZM196.9,90.83V89h0.73V88.66h-1.85V89h0.73v1.8h0.39Zm3.59,0V88.66h-0.56l-0.65,1.55-0.65-1.55h-0.58v2.18h0.4V89.18l0.61,1.41h0.42l0.61-1.41v1.65h0.4Z" transform="translate(-21.19 -0.2)" style="fill: #f79e1b"/>
    </g>
  </g>
</svg></div></div></div>
                       <div class="money-block">
                       <div class="left_label">Доставка</div><div class="right_label"><div class="about-delivery">Осуществляется международная доставка<br/>иногда срок доставки может длиться 2-3 недели.</div></div></div>  
                 
  
                      <div class="money-block">
                      <div class="protect-buyer"><span class="title">Защита Покупателя</span><br/>
                              <ul>
                                 <li><b>Полный возврат</b> если Вы не получили товар</li>
                                 <li><b>Возврат платежа</b> при несоответствии описанию</li>
                              </ul</div>
                      </div>
                        </div>
    </div>
</div>
<div class="about-block row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="title">Описание продукта</div>
                <div class="text">
                    <?=$product->full_description;?>
                </div>
            </div>

</div>
 
                      
                    
                  
