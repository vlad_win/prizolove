<?php                                                                                                   
defined('_JEXEC') or die('Restricted access.');
$app = JFactory::getApplication();
$cid = $app->input->get('cid', 0, 'INT');
if ($cid)
    $products = PlLibHelperCatalog::getInstance()->getProductsByCategoryId($cid);
else
    $products = PlLibHelperCatalog::getInstance()->getProducts();

$categories = PlLibHelperCatalog::getInstance()->getCategories();

$parentId = PlLibHelperAffiliate::getParent();
$affiliateLink = '';
if ($parentId) $affiliateLink = '&atid='.$parentId;
$active_cid = JFactory::getApplication()->input->getInt('cid', 0)


?>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<script>
    jQuery(window).scroll(function() {
        if (jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()) {
            //$("body").append('<div class="big-box"><h1>Page ' + page + '</h1></div>');
            var numProducts = jQuery('.product').length;
            jQuery.ajax({
                method: "POST",
                url: "/index.php?option=com_ajax&plugin=catalog&format=json",
                dataType: 'json',
                data: {
                    num: numProducts
                }
            })
            .done(function(data) {
                if (data.data[0] != undefined) jQuery('.product-blocks-scroll').append(data.data[0]);

            })
            ;

        }
    });
</script>
    <?php
    $session = JFactory::getSession();
    $bonus = floatval($session->get('event_bonus',0));
    $session->clear('event_bonus');
    $disable = true;	
    if ($bonus && !$disable) :
	$user = JFactory::getUser();
   	$bonusAccount = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
    ?>
<style>
    .category-block .active{
        border-color: #e74c3c;
    }
/* Стили попапа */
.popup-bonuses-spis{font-family:"Montserrat",arial,sans-serif;font-weight:500;max-width:500px;background:white;min-height:100px;width:100%;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);border-radius:5px;box-shadow:0px 2px 8px rgba(0,0,0,0.15);text-align:center;padding:0 0px 20px 0px;z-index: 9999;}

.popup-bonuses-spis .topper{text-align:center;width:100%;padding:10px 0 20px 0;background:url(https://prizolove.com/images/topper_sert.png) no-repeat 0% 120%;background-size:cover;}
.popup-bonuses-spis .topper img{width:35px;margin-left:-30px;display: inline-block;}


.popup-bonuses-spis .icon-wrapper{display:inline-block;width:20px;vertical-align:middle;margin-right:5px;}
.popup-bonuses-spis h5{font-size:21px;font-weight:700;margin:0px 0 20px 0;}
.popup-bonuses-spis .stat{font-size:14px;line-height:21px;margin:10px 0;}
.popup-bonuses-spis .stat span{font-weight:700;font-size:16px;}
.popup-bonuses-spis .line{width:100%;height:1px;background:rgba(0,0,0,0.1);margin:20px 0;}

.popup-bonuses-spis .link{color:#e74c3c;margin-top:20px;display:inline-block !important;font-size:12px;text-decoration:none;border-bottom:1px solid #e74c3c;padding-bottom:5px;transition:.2s;width:160px;margin:20px auto 0 auto;}
.popup-bonuses-spis .link:hover{color:#c0392b !important;border-bottom:1px solid #c0392b;transform:translateY(-3px);}


/* Анимация */
@-webkit-keyframes rotateInUpLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

@keyframes rotateInUpLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

.rotateInUpLeft {
  -webkit-animation-duration:1s;
  animation-duration:1s;
  -webkit-animation-name: rotateInUpLeft;
  animation-name: rotateInUpLeft;
}



/* НОВЫЕ СТИЛИ ДИМА */
.popup-bonuses-spis .good{    text-align: center;
    font-size: 21px;
    font-weight: bold;
    color: #388E3C;
    font-family: "Montserrat",arial,sans-serif;
    margin-top: 20px;}
.popup-bonuses-spis .text-good{font-size:14px;}

.answer-good .fireworks{display:block;width:160px;height:160px;margin:10px auto 10px auto;position:relative;}
.answer-good .fireworks .ramka{display:block;font-family:"Montserrat",arial,sans-serif;text-align:center;background:url(https://prizolove.com/images/Ramka-1.png) no-repeat center center;width:160px;height:160px;background-size:cover;position:relative; -webkit-animation-name: tada;
  animation-name: tada;
-webkit-animation-duration: 1s;
  animation-duration: 1s; }

.answer-good .ramka .icon-block{width:125px;height:125px;text-align:center;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);background:black;border-radius:50%;overflow:hidden;background-color:#388E3C;color:white;}
.answer-good .ramka .icon-block h5{font-weight:800;font-size:40px;margin-bottom:0px;margin-top:25px;}
.answer-good .ramka .icon-block span{font-size:16px;font-weight:500;}





@-webkit-keyframes tada {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  10%,
  20% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
  }

  30%,
  50%,
  70%,
  90% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
  }

  40%,
  60%,
  80% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

@keyframes tada {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  10%,
  20% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
  }

  30%,
  50%,
  70%,
  90% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
  }

  40%,
  60%,
  80% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}



.answer-good .fireworks::before,.answer-good .fireworks::after{
  content:"";
  position:absolute;
  display:block;
  width:100px;
  height:100px;
  }
.answer-good .fireworks::before{left:-150px;top:35px;;background:url(https://prizolove.com/images/firework-left-popup.png) no-repeat center center;background-size:contain;transform:translateX(100px);-webkit-animation-duration: 1s;
  animation-duration: 1s; 
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;    -webkit-animation-name: leftFadeIn; 
            animation-name: leftFadeIn;}
.answer-good .fireworks::after{right:-150px;top:35px;
background:url(https://prizolove.com/images/firework-right-popup.png) no-repeat center center;background-size:contain;transform:translateX(-100px);-webkit-animation-duration: 1s;
  animation-duration: 1s; 
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;    -webkit-animation-name: rightFadeIn; 
            animation-name: rightFadeIn;}

@-webkit-keyframes leftFadeIn {
            0% {
               opacity: 0;
               transform:translateX(100px);
            }
            
            
            100% {
               opacity: 1; 
               transform:translateX(25px);
            }
         }

@keyframes leftFadeIn {
            0% {
               opacity: 0;
               transform:translateX(100px);
            }
            
            
            100% {
               opacity: 1; 
               transform:translateX(50px);
            }
         }
         
    
@-webkit-keyframes rightFadeIn {
            0% {
               opacity: 0;
               transform:translateX(-100px);
            }
            
            
            100% {
               opacity: 1; 
               transform:translateX(-25px);
            }
         }

@keyframes rightFadeIn {
            0% {
               opacity: 0;
               transform:translateX(-100px);
            }
            
            
            100% {
               opacity: 1; 
               transform:translateX(-50px);
            }
         }
         


.title-good .green-text{text-align:center;font-size:21px;font-weight:bold;color:#388E3C;font-family:"Montserrat",arial,sans-serif;margin-top:20px;}
.text-good{font-size:21px;text-align:center;font-family:"Montserrat",arial,sans-serif;color:#333;font-weight:500;}


.arrow-down{text-align:center;font-size:21px;font-family:Montserrat,arial,sans-serif;font-weight:500;}
.arrow-down .arrow{width:40px;height:40px;border-radius:50%;background:red;color:white;line-height:40px;text-align:center;font-size:25px;margin:10px auto 20px auto;animation-name: expandUp;
    -webkit-animation-name: expandUp; 
 
    animation-duration: 2s; 
    -webkit-animation-duration: 2s;
  
 -webkit-animation-iteration-count: infinite;
 animation-iteration-count: infinite;
    animation-timing-function: ease-in-out; 
    -webkit-animation-timing-function: ease-in-out;     
 
    visibility: visible !important; }

@keyframes expandUp {
    0% {
        transform: translateY(0px);
    }
    50%{
        transform: translateY(20px);
    }
   
    100% {
        transform: translateY(0px);
    }   
}
 
@-webkit-keyframes expandUp {
    0% {
        -webkit-transform: translateY(0px);
    }
    50%{
        -webkit-transform: translateY(20px);
    }
   
    100% {
        -webkit-transform: translateY(0px);
    }   
}

</style>
        <div id="bonuses-popup" class="modal fade pl-modal" role="dialog">
            <div class="modal-dialog">
                <div class="popup-bonuses-spis">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="topper">
     <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
    </div>
    <h5 class="good"><i class="fas fa-check-circle"></i> Супер! Ваш ответ верен</h5>
    <div class="text-good">
     <b class="your-sertificate">Сертификат Финалиста на розыгрыш 10 000$ ваш!</b><br/>Что бы он принял участие в розыгрыше нужна одна покупка<br/><br/>Пройдя испытание вы выиграли:
    </div>
    <div class="answer-good">
      <div class="fireworks">
      <div class="ramka">
        <div class="icon-block">
      <div style="width:20px;">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m31 175.859375v206.351563l45-45 45 45v-153.484376zm0 0" fill="#ff4d4d"></path><path d="m391 224.929688v157.28125l45-45 45 45v-187.71875zm0 0" fill="#ff001e"></path><path d="m391 241v-30c49.628906 0 90-40.371094 90-90v-60h-75v-30h106v90c0 66.167969-54.832031 120-121 120zm0 0" fill="#ff9f00"></path><path d="m112.824219 236.09375c-65.84375-17.285156-112.824219-76.949219-112.824219-145.09375v-60h106v30h-75v30c0 54.519531 36.78125 102.261719 89.441406 116.089844zm0 0" fill="#ffd400"></path><path d="m226 301h60v181h-60zm0 0" fill="#ffd400"></path><path d="m256 301h30v181h-30zm0 0" fill="#ff9f00"></path><path d="m91 0v166c0 90.902344 74.097656 165 165 165s165-74.097656 165-165v-166zm0 0" fill="#ffe470"></path><path d="m421 0v166c0 90.902344-74.097656 165-165 165v-331zm0 0" fill="#fdbf00"></path><path d="m348.101562 124-63.601562-9.300781-28.5-57.597657-28.5 57.597657-63.601562 9.300781 46.199218 45-11.097656 63.300781 57-30 57 30-11.097656-63.300781zm0 0" fill="#fdbf00"></path><path d="m301.902344 169 11.097656 63.300781-57-30v-145.199219l28.5 57.597657 63.601562 9.300781zm0 0" fill="#ff9f00"></path><path d="m361 482v30h-210v-30c0-16.5 13.5-30 30-30h150c16.5 0 30 13.5 30 30zm0 0" fill="#ffe470"></path><path d="m361 482v30h-105v-60h75c16.5 0 30 13.5 30 30zm0 0" fill="#fdbf00"></path></svg>
        </div>
      У вас на бонусном счету:<b><?=$bonus;?> $</b></div>
      </div>
      </div>
      </div>
	
    <div class="stat">На эту сумму будет уменьшена стоимость вашего заказа.</div>
    Всего - <?=$bonusAccount->value;?> бонусов
    <a href="/catalog" class="link">Приступайте к выбору!</a>
    <a href="/personal" class="link">Вернуться к загадкам</a>
                    </div>
                </div>
            </div>

        </div>
	</div>
	<script>jQuery('#bonuses-popup').modal('show');</script>
<?php endif;?>

<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 breadcrumbs-block">
                <a href="https://prizolove.com/">Главная</a> - <span>Каталог товаров</span>
            </div>
            <?php /* <div class="col-12 col-sm-12 col-md-4 col-lg-4 search-block"><input type="search" placeholder="Поиск по товарам"><input type="submit" value=""></div> */ ?>
        </div>
    </div>
</div>


<div class="slider-block">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 category-block">
                <?php foreach($categories as $category) : ?>
                <div class="sub-link">
                    <div class="name <?php if ($active_cid == $category->id) {echo 'active';} ?>">
                        <?=$category->svg;?>
                        <a href="<?=JRoute::_('index.php?option=com_prizolove&view=catalog&cid='.$category->id.$affiliateLink);?>"><?=$category->name;?></a>
                    </div>
                </div>
                <?php endforeach; ?>





            </div>

        </div>
    </div>
</div>
                  
<button class="mobile-category"><div class="title">Категории товаров</div><div class="text">Более 1000+ товаров </div></button>
                  <div class="category-block-mobile">
                  <?php foreach($categories as $category) : ?>
                <div class="sub-link">
                    <div class="name">
                        <?=$category->svg;?>
                        <a href="<?=JRoute::_('index.php?option=com_prizolove&view=catalog&cid='.$category->id.$affiliateLink);?>"><?=$category->name;?></a>
                    </div>
                </div>
                <?php endforeach; ?>
                  </div>
                  
<div class="catalog-block">
    <div class="container">
        <div class="row product-blocks">
            <?php 
		foreach($products as $key=>$product) :		
		if ($key == 8) :
	    ?>	      	
	    </div>
        <div class="sale-block">
            <div class="container">
                <div class="row">
                    <div class="col-12 title-block"><h2>Специальные предложения</h2></div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12 col-lg-6"><a href="/381" target="_blank" class="banner-block"></a></div>
                    <div class="col-12 col-md-6 col-sm-12 col-lg-6"><a href="/136" target="_blank" class="banner-block-2"></a></div>
                </div>
            </div>
        </div>
        <div class="row product-blocks product-blocks-scroll">
            <div class="col-12 col-md-6 col-sm-6 col-lg-3">
                <div class="product">
                    <a href="/catalog?pid=<?=$product->id.$affiliateLink;?>" class="overlay-link"></a>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><?php if ($product->image) :?><img src="<?=$product->image;?>"><?php endif;?></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper"><?=$product->name;?></div>
                        <div class="price-wrapper"><?=number_format(PlLibHelperRates::recalc(PlLibHelperRates::convertUSD($product->price_usd)),2);?> <span class="valute"><?=PlLibHelperRates::currencyLabel();?></span></div>
                    </div>
                </div>
            </div>
	    <?php else :?>
            <div class="col-12 col-md-6 col-sm-6 col-lg-3">
                <div class="product">
                    <a href="/catalog?pid=<?=$product->id.$affiliateLink;?>" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <?php /* <div class="top-icon">Топ продаж</div> */ ?>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><?php if ($product->image) :?><img src="<?=$product->image;?>"><?php endif;?></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper"><?=$product->name;?></div>
                        <div class="price-wrapper"><?=number_format(PlLibHelperRates::recalc(PlLibHelperRates::convertUSD($product->price_usd)),2);?> <span class="valute"><?=PlLibHelperRates::currencyLabel();?></span></div>
                    </div>
                </div>
            </div>
	    <?php endif;?>
            <?php endforeach; ?>

        </div>
    </div>

</div>
              

<?php /*
<div class="foryou-block">
    <div class="container">
        <div class="row">
            <div class="col-12 title-block"><h2>Рекомендуем вам</h2></div>
        </div>
        <div class="row">
            <div class="col-12"><div class="tabs">
                    <a href="#" class="active">Все категории</a> <a href="#">Часы</a> <a href="#">Все для кухни</a>
                    <a href="#">Красота и здоровье</a> <a href="#">Для водителей</a> <a href="#">Аксессуары</a> <a href="#">Гаджеты и техника</a> <a href="#">Зоотовары</a> <a href="#">Для дома и дачи</a> <a href="#">Спортивные товары</a>
                </div></div>
        </div>
    </div>
</div>
<div class="catalog-block-2 catalog-block">
    <div class="container">
        <div class="row product-blocks">
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">4 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">1 строка</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row product-blocks">
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="product">
                    <a href="#" class="overlay-link"></a>
                    <div class="diamond-icon"></div>
                    <div class="top-icon">Топ продаж</div>
                    <div class="wrapper-left">
                        <div class="image-wrapper"><img src="https://prizolove.com/images/airpods.png"></div>
                    </div>
                    <div class="wrapper-right">
                        <div class="name-wrapper">Беспроводная гарнитура AirPods</div>
                        <div class="price-wrapper">1 799 <span class="valute">грн</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
*/ ?>
              
              
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

              <script>
              $('.mobile-category').click(function(){
  $(".category-block-mobile").fadeToggle(100);
});
              </script>