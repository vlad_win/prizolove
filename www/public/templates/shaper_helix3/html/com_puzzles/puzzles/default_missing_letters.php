<?php
defined('_JEXEC') or die;
$user = JFactory::getUser();
?>
<style>
	.letter-space{display: inline-block; width: 10px;}
</style>
<div class="body_riddle">
  <div class="container bounceInUp">
    <div class="row">
      <div class="col-12">
      <form>
        <h3><?=$this->item->params['question'];?></h3>
        <div class="image-wrapper"><img src="/<?=$this->item->params['image'];?>"></div>
        <div class="choose">Введите ваш ответ <i class="far fa-hand-point-down"></i></div>
        <div class="main_riddle shake">
          <div class="letters-wrapper">
              <?php $chrArray = preg_split('//u', $this->item->params['template'], -1, PREG_SPLIT_NO_EMPTY); ?>
                  <?php
                  foreach($chrArray as $i=>$letter) {
                      if ($letter != '_' && $letter != ' ') {
                          echo '<div class="letter">'.$letter.'</div>';
                      } else {
                          if ($letter == ' ') {
                              echo '<span class="letter-space"></span>';
                          } else {
                              echo '<div class="letter"><input class="letter'.$i.'" type="text" name="puzzle['.$this->item->id.']['.$i.']" maxlength="1" autocomplete="off"></div>';
                          }
                      }
                  }
                  ?>
          </div>
        </div>
        
        
        <div class="btn-wrapper">
          <button class="btn-check"><i class="fas fa-star"></i> Проверить ответ <i class="fas fa-star"></i></button>
        </div>
        <?php if ($this->item->username) : ?>
        <div class="author-block">
             Автор Испытания: <?=$this->item->username;?>.
        </div>
        <?php endif;?>
      </form>

      </div>
    </div>
  </div>
</div>
<div id="popup-win" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup-bonuses-spis bounceInUp">
            <div class="topper">
                <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
            </div>
            <h5 class="good"><i class="fas fa-check-circle"></i> Супер! Ваш ответ верен</h5>
            
            <div class="answer-good">
                <div class="fireworks">
                    <div class="ramka">
                        <div class="icon-block"><h5><?=$this->item->bonuses;?></h5><span>ваш бонус</span></div>
                    </div>
                </div>
            </div>
            


            <div class="stat"><b>Внимание!</b><br/>
Больше испытаний = больше шансов словить приз: <b>1, 2, 5, 10, 20, 50 $, а также прикольные товарные призы.</b><br/><br/>

Удачи!
</div>
	    <?php if (!$user->id) :?>
	    <div class="stat" style="color: #e00;">Для получения бонусов вам необходимо авторизироваться</div>
	    <?php endif;?>
            <a href="javascript:void();" id="nextButton" class="button hidden">Следующее испытание</a>

	    <?php if ($user->id) :?>
            <a href="/autor" class="link" target="_blank">Загрузите свои испытания, обыграйте Prizolove. </a>
 	    <?php endif;?>
        </div>
    </div>
</div>
<div id="popup-lost" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup-bonuses-spis">
            <div class="topper">
                <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
            </div>
            <h5 class="notgood"><i class="fas fa-times-circle"></i> Ваш ответ не верен</h5>
            <div id="penalty">
                <div class="text-notgood">
                    <b class="your-sertificate">За непройденное испытание с вашего бонусного счета списано:</b>
                </div>
                <div class="answer-notgood">
                  <div class="fireworks">
                  <div class="ramka">
                    <div class="icon-block"><h5>-0.1</h5><span>бонуса</span></div>
                  </div>
                  </div>
                </div>

                <?php if ($this->item->username) : ?>
                    <div class="text-notgood">
                        <span class="your-sertificate">c бонусного счета в пользу автора испытания.<br />Автор Испытания: <?=$this->item->username;?>.</span><br/><br/>
                    </div>
                <?php endif; ?>
            </div>
            <a href="javascript:void();" id="tryAgain" class="button">Попробовать еще</a>
            <a href="javascript:void();" id="pass" class="link">Пропустить за 0.05 бонуса</a>
        </div>
    </div>
</div>
<script>
    jQuery(function ($) {
        $(document).ready(function () {
            $('#pass').on('click', function() {
                jQuery.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=events&task=pass&format=json",
                    dataType: 'json',
                    async: false,
                    data: {eid: <?=$this->item->id;?>, test: 532643775675}
                }).done(function(data) {
                    if (data.data[0].link != undefined) window.location = data.data[0].link;
                });
            });
            $('#tryAgain').on('click',function() {
	     	    $('#popup-lost').modal('hide');
	            return false;
	        });
            $('input[name^="puzzle"][class*="letter"]').on('keydown', function (event) {
                var questions = $('input[name*="puzzle"][class*="letter"]');
                var questionIndex = questions.index($(this));
                var key = event.keyCode || event.charCode;
                if (key == 8 || key == 46) {
                    if ((questionIndex - 1) >= 0 && $(this).val() == '') {
                        questions.eq(questionIndex - 1).focus();
                    }
                }
            });
        });
        $('input[name^="puzzle"][class*="letter"]').on('input', function () {
            var questions = $('input[name*="puzzle"][class*="letter"]');
            var questionIndex = questions.index($(this));
            ;
            if ((questionIndex < (questions.length - 1)) && $(this).val() != '') {
                questions.eq(questionIndex + 1).focus();
            }
        });
        $('button.btn-check').on('click', function() {
            var curForm = $('form').serialize();
            $.ajax({
                method: "POST",
                url: "/index.php?option=com_ajax&plugin=puzzles&format=json",
                dataType: 'json',
                data: curForm

            }).done(function(data) {
                if (data.data[0].correct == true) {
                    $('#popup-win #bonusAccount').addClass('hidden');
                    if (data.data[0].total > 0) {
                        $('#popup-win #bonusAccount span').text(data.data[0].total);
                        $('#popup-win #bonusAccount').removeClass('hidden');
                    }
		    $('#popup-win #nextButton').addClass('hidden');
                    if (data.data[0].nextLink != undefined && data.data[0].nextLink != false) {
                        $('#popup-win #nextButton').attr('href',data.data[0].nextLink).removeClass('hidden');
                    }
                    $('#popup-win .icon-block h5').text(data.data[0].bonuses);
                    $('#popup-win').modal('show');
                } else {
                    if (data.data[0].penalty > 0) {
                        $('#popup-lost #penalty').show();
                    } else {
                        $('#popup-lost #penalty').hide();
                    }
                    $('#popup-lost').modal('show');
                }
            });
            return false;
        });
    });
</script>
