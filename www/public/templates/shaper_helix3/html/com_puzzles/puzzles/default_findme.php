<?php
defined('_JEXEC') or die;
$imgParams = getimagesize(JUri::root().$this->item->params['findme']['image']);
$rightOffset = ceil(($this->item->params['findme']['left'])/ ($imgParams[0]/100));
$topOffset = ceil($this->item->params['findme']['top'] / ($imgParams[1]/100))-3;
$selectionWidth = ceil(($this->item->params['findme']['width'])/ ($imgParams[0]/100));
$selectionHeight = ceil(($this->item->params['findme']['height'])/ ($imgParams[0]/100));
$user = JFactory::getUser();
?>
<style>

</style>
 <div class="body_riddle">
<div class="container bounceInUp puzzle ">
    <div class="row">
        <div class="col">
           <h3><?=$this->item->params['findme']['question'];?></h3>
           <div class="choose">Нашли его? Жмите на него! <i class="far fa-hand-point-down"></i></div>

        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="puzzle__image find-me">
                <img width="100%" src="/<?=$this->item->params['findme']['image'];?>" alt="" id="puzzleImage">
                <div id="findMe"></div>
            </div>
            <div class="next-puzzle_link"><a href="javascript:void();" class="passPuzzle">Пропуcтить за 0.05 бонуса</a></div>
        </div>
    </div>
</div>
  </div>
  
<div id="popup-win" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup-bonuses-spis bounceInUp">
            <div class="topper">
                <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
            </div>
            <h5 class="good"><i class="fas fa-check-circle"></i> Супер! Ваш ответ верен</h5>
            
            <div class="answer-good">
                <div class="fireworks">
                    <div class="ramka">
                        <div class="icon-block"><h5><?=$this->item->bonuses;?></h5><span>ваш бонус</span></div>
                    </div>
                </div>
            </div>
            <div id="bonusAccount" class="hidden">У вас на бонусном счету:<strong><span></span> </strong></div>
            <div class="stat"><span>1</span> Бонус = <span><?=number_format(PlLibHelperRates::convertUSD(1),2,'.','');?></span> грн</div>

            <div class="stat"><b>Внимание!</b><br/>
Больше испытаний = больше шансов словить приз: <b>1, 2, 5, 10, 20, 50 $, а также прикольные товарные призы.</b><br/><br/>

Удачи!
</div>
	    <?php if (!$user->id) :?>
	    <div class="stat" style="color: #e00;">Для получения бонусов вам необходимо авторизироваться</div>
	    <?php endif;?>
            <a href="javascript:void();" id="nextButton" class="button hidden">Следующее испытание</a>

	    <?php if ($user->id) :?>
            <a href="/autor" class="link" target="_blank">Загрузите свои испытания, обыграйте Prizolove. </a>
 	    <?php endif;?>
        </div>
    </div>
</div>
<div id="popup-lost" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup-bonuses-spis">
            <div class="topper">
                <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
            </div>
            <h5 class="notgood"><i class="fas fa-times-circle"></i> Ваш ответ не верен</h5>
            <div id="penalty">
                <div class="text-notgood">
                    <b class="your-sertificate">За непройденное испытание с вашего бонусного счета списано:</b>
                </div>
                <div class="answer-notgood">
                    <div class="fireworks">
                        <div class="ramka">
                            <div class="icon-block"><h5>-0.1</h5><span>бонуса</span></div>
                        </div>
                    </div>
                </div>
                <?php if ($this->item->username) : ?>
                <div class="text-notgood">
                    <span class="your-sertificate">c бонусного счета в пользу автора испытания.<br />Автор Испытания: <?=$this->item->username;?>.</span><br/><br/>
                </div>
                <?php endif;?>
            </div>
            <a href="javascript:void();" id="tryAgain" class="button">Попробовать еще</a>
            <a href="javascript:void();" class="link passPuzzle">Пропуcтить за 0.05 бонуса</a>
        </div>
    </div>
</div>
<style>
    .puzzle__image {
        position: relative;
    }
    .puzzle__image img {
        widht: 100%;
        display: inline;
    }
    #findMe {
        position: absolute;
        top: <?=($topOffset+4);?>%;
        left: <?=$rightOffset?>%;
        z-index: 10;
        width:  <?=$selectionWidth;?>%;
        height: <?=$selectionHeight;?>%;
    }
</style>
<script>
    jQuery(function ($) {
		
        $(document).ready(function () {
			$('.passPuzzle').on('click', function() {
                jQuery.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=events&task=pass&format=json",
                    dataType: 'json',
                    async: false,
                    data: {eid: <?=$this->item->id;?>}
                }).done(function(data) {
                    if (data.data[0].link != undefined) window.location = data.data[0].link;
                });
			});
			$('#tryAgain').on('click',function() {
					$('#popup-lost').modal('hide');
					return false;
				});			
            $('#findMe').on('click', function(e) {
                jQuery.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=events&format=json",
                    dataType: 'json',
                    async: false,
                    data: {eid: <?=$this->item->id;?>}
                }).done(function(data) {
                    $('#popup-win #bonusAccount').addClass('hidden');
                    if (data.data[0].total > 0) {
                        $('#popup-win #bonusAccount span').text(data.data[0].total);
                        $('#popup-win #bonusAccount').removeClass('hidden');
                    }
		    $('#popup-win #nextButton').addClass('hidden');
                    if (data.data[0].nextLink != undefined && data.data[0].nextLink != false) {
                        $('#popup-win #nextButton').attr('href',data.data[0].nextLink).removeClass('hidden');
                    }
                    $('#popup-win').modal('show');
                });


				e.stopPropagation();
            });
			$('#puzzleImage').on('click', function() {
				$('#popup-lost #penalty').hide();
                jQuery.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=events&task=fail&format=json",
                    dataType: 'json',
                    async: false,
                    data: {eid: <?=$this->item->id;?>}
                }).done(function(data) {
                    $('#popup-lost #penalty').hide();
                    if (data.data[0].penalty > 0) {
                        $('#popup-lost #penalty').show();
                    }
                    $('#popup-lost').modal('show');
                });

			});	    	
        });
    });
</script>

