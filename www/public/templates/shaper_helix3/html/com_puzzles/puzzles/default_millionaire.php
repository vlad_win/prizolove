<?php
defined('_JEXEC') or die;
$user = JFactory::getUser();
?>
<style>
    .hidden {display: none;}
    .nextButton {background: #ccc;}
</style>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="puzzle__level">
            <img class="sppb-img-responsive" id="imgLevel" src="/images/level1111.jpg" alt="Image" title="">
        </div>
    </div>
    <?php

    ?>
    <?php foreach($this->item->params['questions'] as $key=>$question) : ?>
    <div class="container question-block<?=($key)?' hidden':'';?>" data-question="<?=$key;?>">
        <div class="row">
            <div class="col level-block__image">
                <img src="/<?=$question['image'];?>" alt="">
            </div>
            <div class="col level-block__about">
      <div class="about-level">
                <div class="level-block__title">
                    <?=$question['block_title'];?>
                </div>
                <div class="level-block__description">
                    <?=$question['block_description'];?>
                </div>
                <div class="downarrow"></div> 
      </div>
            </div>
        </div>
        <div class="row">
            <div class="col question__title topTitle">
                <?=$question['question_title'];?>
            </div>
        </div>
        <div class="row">
            <div class="col answer_block answer_block__answer1">
                <span class="number">A:</span> <span class="answer"><?=$question['answer1'];?></span>
            </div>
            <div class="col answer_block answer_block__answer2">
                <span class="number">В:</span> <span class="answer"><?=$question['answer2'];?></span>
            </div>
        </div>
        <div class="row">
            <div class="col answer_block answer_block__answer3">
                <span class="number">Б:</span> <span class="answer"><?=$question['answer3'];?></span>
            </div>
            <div class="col answer_block answer_block__answer4">
                <span class="number">Г:</span> <span class="answer"><?=$question['answer4'];?></span>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    <div class="row">
        <div class="col">
            <a id="errorButton" class="sppb-btn buttonNext_error sppb-btn-custom sppb-btn-lg sppb-btn-round hidden">Вы ответили не правильно! Попробуйте еще раз!</a>
            <a id="nextQuestion" class="sppb-btn  buttonNext sppb-btn-custom sppb-btn-lg sppb-btn-round hidden">Следующий вопрос <i class="fa fa-angle-double-right"></i></a>
            <a href="/366" id="activateCertificate" class="sppb-btn buttonNext3 sppb-btn-custom sppb-btn-lg sppb-btn-round hidden">Перейти к активации сертификата <i class="fa fa-angle-double-right"></i></a>
        </div>
    </div>
</div>
<div id="popup-win" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup-bonuses-spis bounceInUp">
            <div class="topper">
                <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
            </div>
            <h5 class="good"><i class="fas fa-check-circle"></i> Супер! Ваш ответ верен</h5>
            
            <div class="answer-good">
                <div class="fireworks">
                    <div class="ramka">
                        <div class="icon-block"><h5><?=$this->item->bonuses;?></h5><span>ваш бонус</span></div>
                    </div>
                </div>
            </div>
            <div id="bonusAccount" class="hidden">У вас на бонусном счету:<strong><span></span> </strong></div>
            <div class="stat"><span>1</span> Бонус = <span><?=number_format(PlLibHelperRates::convertUSD(1),2,'.','');?></span> грн</div>
            <div class="stat"><b>Внимание!</b><br/>
Больше испытаний = больше шансов словить приз: <b>1, 2, 5, 10, 20, 50 $, а также прикольные товарные призы.</b><br/><br/>

Удачи!
</div>
	    <?php if (!$user->id) :?>
	    <div class="stat" style="color: #e00;">Для получения бонусов вам необходимо авторизироваться</div>
	    <?php endif;?>
            <a href="javascript:void();" id="nextButton" class="button hidden">Следующее испытание</a>

	    <?php if ($user->id) :?>
            <a href="/autor" class="link" target="_blank">Загрузите свои испытания, обыграйте Prizolove. </a>
 	    <?php endif;?>
        </div>
    </div>
</div>
<div id="popup-lost" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup-bonuses-spis">
            <div class="topper">
                <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
            </div>
            <h5 class="notgood"><i class="fas fa-times-circle"></i> Ваш ответ не верен</h5>
            <div id="penalty">
                <div class="text-notgood">
                    <b class="your-sertificate">За непройденное испытание с вашего бонусного счета списано:</b>
                </div>
                <div class="answer-notgood">
                    <div class="fireworks">
                        <div class="ramka">
                            <div class="icon-block"><h5>-0.1</h5><span>бонуса</span></div>
                        </div>
                    </div>
                </div>
                <?php if ($this->item->username) : ?>
                <div class="text-notgood">
                    <span class="your-sertificate">c бонусного счета в пользу автора испытания.<br />Автор Испытания: <?=$this->item->username;?>.</span><br/><br/>
                </div>
                <?php endif; ?>
            </div>
            <a href="javascript:void();" id="tryAgain" class="button">Попробовать еще</a>
            <a href="javascript:void();" id="pass" class="link">Пропуcтить за 0.05 бонуса</a>
        </div>
    </div>
</div>
<script>
    var imageLevels = ['/images/level1111.jpg','/images/level222222.jpg','/images/level33333.jpg','/images/level44444.jpg'];
    jQuery(function ($) {
        $(document).ready(function () {
            $('#pass').on('click', function() {
                jQuery.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=events&task=pass&format=json",
                    dataType: 'json',
                    async: false,
                    data: {eid: <?=$this->item->id;?>, test: 67564746475634}
                }).done(function(data) {
                    if (data.data[0].link != undefined) window.location = data.data[0].link;
                });
            });
            var questionBlock = 0;
            $('#tryAgain').on('click',function() {
	     	    $('#popup-lost').modal('hide');
	            return false;
	        });
            $('.answer_block').on('click', function() {
                var curAnswer = $(this).find('span.answer').text();
                questionBlock = $(this).parents('.question-block').data('question');
                $.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=puzzles&format=json",
                    dataType: 'json',
                    data: {
                        'puzzle' : {
                                <?=$this->item->id;?>:{
                                    'questionId': questionBlock,
                                    'answer': curAnswer
                                }
                        }
                    }
                }).done(function(data) {
                    $('.sppb-btn').addClass('hidden');
                    if (data.data[0].nextQuestion == true) {
			            $('#imgLevel').attr('src',imageLevels[(questionBlock+1)]);
                        $('.question-block[data-question="'+ questionBlock +'"]').addClass('hidden');
	                    $('.question-block[data-question="'+ (questionBlock+1) +'"]').removeClass('hidden');
                    }
                    if (data.data[0].correct != undefined && data.data[0].correct == false) {
                        if (data.data[0].penalty > 0) {
	                        $('#popup-lost #penalty').show();
        	        } else {
                        	$('#popup-lost #penalty').hide();
                        }
                        $('#popup-lost').modal('show');
                    }
                    if (data.data[0].activate == true) {
                        $('#popup-win #bonusAccount').addClass('hidden');
                        if (data.data[0].total > 0) {
                            $('#popup-win #bonusAccount span').text(data.data[0].total);
                            $('#popup-win #bonusAccount').removeClass('hidden');
                        }
		    $('#popup-win #nextButton').addClass('hidden');
                    if (data.data[0].nextLink != undefined && data.data[0].nextLink != false) {
                            $('#popup-win #nextButton').attr('href',data.data[0].nextLink).removeClass('hidden');
                        }
                        $('#popup-win').modal('show');
                    }
                });
            });
            $('#nextQuestion').on('click', function() {
                $('.question-block[data-question="'+ questionBlock +'"]').addClass('hidden');
                $('.question-block[data-question="'+ (questionBlock+1) +'"]').removeClass('hidden');
            });
        });
        /*
        $('button.btn-check').on('click', function() {
            var curForm = $('form').serialize();
            $.ajax({
                method: "POST",
                url: "/index.php?option=com_ajax&plugin=puzzles&format=json",
                dataType: 'json',
                data: curForm

            }).done(function(data) {
                if (data.data[0].correct == true) {
                    $('#popup-win .icon-block h5').text(data.data[0].bonuses);
                    $('#popup-win').modal('show');
                } else {
                    if (data.data[0].penalty > 0) {
                        $('#popup-lost #penalty').show();
                    } else {
                        $('#popup-lost #penalty').hide();
                    }
                    $('#popup-lost').modal('show');
                }
            });
            return false;
        }); */
    });
</script>

