<?php
defined('_JEXEC') or die;
?>
<div class="header">
	<div class="container">
        <div class="row">
<?php
    if ($user->id) :
?>
       
    

            <div class="col-4 col-md-6 col-sm-6 logo-block">
				<img src="https://prizolove.com/images/logo-vertical-white.png">
			</div>
            <div class="col-7 col-md-6 col-sm-6 user-block">
                <div class="inline"><span class="name-user">Привет, <?=$user->username;?>!</span>
                    <br/><?php /* <a href="#"><i class="fas fa-cog"></i>  <span>Настройки</span></a> */?><a href="/logout"><i class="fas fa-times"></i> <span>Выйти</span></a>
                </div>
                <div class="inline avatar-block">
					<img src="https://prizolove.com/images/avatar-user.png">
				</div>
            </div>        
<?php else: ?>
        <a href="/login" class="login-link">Войти</a> / <a href="/profile?view=registration" class="login-link">Зарегистрироваться</a>
<?php endif; ?>
		</div>
    </div>
</div>
