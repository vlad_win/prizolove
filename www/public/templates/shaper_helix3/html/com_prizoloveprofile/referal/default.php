<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
JLoader::import('pl_lib.library');
$parentId = PlLibHelperAffiliate::getParent();
$events = PlLibHelperEvents::getInstance()->getLatestEvents(6, 'registration');
require_once(JPATH_ROOT.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');
?>
 
<script>
    jQuery(document).ready(function() {
        jQuery('a.copy').on('click', function(e) {
            var $this = jQuery(this);
            var $temp = jQuery("<input>");
            jQuery("body").append($temp);
            $temp.val(jQuery(this).closest('.link-ref-block').text()).select();
            document.execCommand("copy");
            $this.tooltip({'placement': 'top', 'title': 'Ссылка скопирована. Вставляйте ее при отправке в сообщения. Нажмите правую кнопку мыши и выберите вставить.','trigger':'click', delay: { "show": 500, "hide": 500 }});
            $temp.remove();
            e.preventDefault();
            return false;
        });
  

  jQuery('a.akciya').on('click', function(e) {
            var $this = jQuery(this);
            var $temp = jQuery("<input>");
            jQuery("body").append($temp);
            $temp.val(jQuery('.akciya[href]:find').closest('div.block-list-akcii').text()).select();
            document.execCommand("copy");
            $this.tooltip({'placement': 'top', 'title': 'Ссылка скопирована. Вставляйте ее при отправке в сообщения. Нажмите правую кнопку мыши и выберите вставить.','trigger':'click', delay: { "show": 500, "hide": 500 }});
            $temp.remove();
            e.preventDefault();
            return false;
        });
    
    });
</script>
  <style>
  .referal-body .title{    font-size: 30px;
    line-height: 40px;}
  .block-how{background: rgba(241, 196, 15,0.2);
             margin-top:20px;
    
    text-align: center;
           
             padding:20px 0;
    border: 1px solid #dbca9d;
    border-radius: 10px;}
  .block-how .wrapper{position:relative;}
.block-how .wrapper-1::before{content:"1";}
.block-how .wrapper-2::before{content:"2";}
.block-how .wrapper-3::before{content:"3";}
.block-how .wrapper-4::before{content:"4";}
  .block-how .wrapper::before{
    position: absolute;
    left: 15%;
    top: 35%;
    font-size: 80px;
    opacity: 0.1;
    z-index: -1;
    font-weight: bold;
    font-family: "Montserrat",sans-serif;
    transform: translate(-50%,-50%);}
.block-how .wrapper .text{font-size:12px;padding:10px 10px;  font-weight:500;}
.block-how .wrapper::after{content:"";font-family:"FontAwesome";position:absolute;right:0px;top:50%;transform:translateY(-50%);font-size:40px;color:#84B246;}
.block-how .last-wrapper::after{display:none !important;}
.i-block{font-weight:500;color:#364A5D;font-weight:bold;}
.block-how .wrapper img{z-index:9999;}
         .yellow-block{background: rgba(241, 196, 15,0.2);
             margin-top:20px;
    width: 100%;
    text-align: center;
           
             padding:20px 0;
    border: 1px solid #dbca9d;
    border-radius: 10px;}
         .link-ref{font-size:18px;}
         .tooltip-inner{max-width:300px;background:red !important;font-family:"Montserrat",sans-serif;font-weight:bold;}
         .tooltip .arrow{display:none !important;}
         .tooltip{    top: 150px !important;left: 0px !important;}
         .link-ref-block{background-color:#ffedb8;}
                         .link-ref .copy{color:#689539;font-weight:bold;transition:.5s;transform:scale(1) !important;}
                                         .yellow-block .akciya{background:white !important;}
                                         
                                         
                                         .block-list-akcii .akciya{vertical-align:top !important;}
  </style>
<div class="referal-body">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-12 col-sm-12">

                <div class="title">Постройте свою реферальную сеть в нашем проекте.<br/>
                    Зарабатывайте от 500$ в месяц. </div>

                <div class="col-12 col-md-12 col-sm-12">
                    <div class="background-title">Как это работает?</div>
                    <div class="text"> За каждую регистрацию по вашей ссылке, вы получаете 10$.  <br/>7$ на товары и 3$ на карту за каждую покупку человека зарегистрированного по вашей ссылке.<br/><br/>Вы партнер системы Prizolove. А это значит, что у вас есть ваша персональная партнерская ссылка. Мы ее называем реферальная ссылка.<br/><br/>
                        Это «хвостик» <b>/?atid=<?=$parentId;?></b>, который добавляется к ссылкам на страницы prizolove.com по которому система понимает, что человек пришел от вас.
                    </div>

                    <div class="block-how">
                        <div class="wrapper wrapper-1">
                            <img src="https://prizolove.com/images/icon-1-referal2.jpg">
                            <div class="text">Копируете ссылку на загадку, <br/>нажав на нее.</div>
                        </div>
                        <div class="wrapper wrapper-2">
                            <img src="https://prizolove.com/images/icon-2-referal-1.jpg">
                            <div class="text">Передаете ее другу, он проходит испытание и регистрируется.</div>
                        </div>
                        <div class="wrapper wrapper-3">
                            <img src="https://prizolove.com/images/icon-3-referal.png">
                            <div class="text">Учет его покупок ведется в Вашем личном кабинете.</div>
                        </div>
                        <div class="wrapper wrapper-4 last-wrapper">
                            <img src="https://prizolove.com/images/icon-4-referal.png">
                            <div class="text">Вы получаете деньги за каждую покупку.</div>
                        </div>
                    </div>

                    <br/><br/><br/>
                    <div class="background-title">Это открывает Вам прекрасные возможности!
                    </div>
                    <div class="text"><i class="i-block">1. Пригласите партнера.</i> Дайте ему ссылку на бесплатный партнерский пакет, где он пройдет автоматический путь ознакомления с продуктом. А после оплаты, вы получите 10% от суммы его партнерского пакета. Так же вы получите 5% от покупок, совершенных покупателями привлеченными за счет его партнерского пакета (мы называем это второй линией генерации). Приглашайте партнеров, это выгодно.<br/>
Отправляйте ссылку в сообщении или опубликуйте на своей странице в социальной сети. 
                           <div class="yellow-block">
                            <article>
                          <div class="row-fluid links-ref-block">
                                                                    <div class="col-12 link-ref">
                                        Ссылка на регистрацию партнера:
                                        
                                        <div class="link-ref-block"><?php echo str_replace('?atid=','35067084?atid=',AffiliateHelper::get_account_link( $parentId )); ?><a href="#" class="copy"  >Скопировать <i class="fas fa-copy"></i></a></div>
                                    </div>

                                </div>
                            </article>
                        </div>
                          <br/><br/>
                          
                        <i class="i-block">2. Пригласите покупателя.</i> Тем людям, которых больше заинтересует - выиграть 10 000$ отгадывая загадки и участвуя в конкурсах. <br/><br/>
                        <span>- Отправьте им вашу ссылку на регистрацию покупателя <br/></span>

                        <div class="yellow-block">
                            <article>
                                <div class="row-fluid links-ref-block">
                                    <div class="col-12 link-ref">
                                        Ссылка на регистрацию:
                                        
                                        <div class="link-ref-block"><?php echo str_replace('?atid=','?atid=',AffiliateHelper::get_account_link( $parentId )); ?><a href="#" class="copy" >Скопировать <i class="fas fa-copy"></i></a></div>
                                    </div>
                                    
                                </div>
                            </article>
                        </div>
                        <br/>

                        <span>- Опубликуйте в социальной сети вот эти загадки по одной в неделю и вы соберете большое количество ваших друзей в сеть. <b>(Нажмите на понравившуюся загадку, чтобы скопировать ее ссылку)</b></span>
                        
                            <div class="block-list-akcii yellow-block">
                            <div class="body text-center">
                                <?php foreach($events as $event) : ?>
                                    <a href="<?=$event->url?>?atid=<?=$parentId;?>" class="akciya">
                                        <div class="product_image_wrapper">
                                            <?php if ($event->image) : ?>
                                                <img src="/<?=$event->image;?>">
                                            <?php else: ?>
                                                <img src="https://prizolove.com/images/second.jpg">
                                            <?php endif; ?>
                                        </div>
                                        <div class="name"><?=$event->name;?></div>                                        
                                    </a>
                                <?php endforeach?>
                            </div>
                            </div>
                        </div>

                        <br/><br/>
                         <br/><br/><br/>
                    <div class="background-title">Расчет вашего дохода
                    </div> <br/><i class="i-block">Если вы зарегистрируете 30 человек.</i> И каждый 10-й из них сделает тоже самое. То ваш плановый доход за год вырастет до 500$ в месяц. (Вот и ваш пассивный доход)<br/><br/>
                        <i class="i-block">Расчетные данные:</i>  <br/>
                        <span>От покупок 1 - й линии Вы получаете 3$ за каждую покупку.</span><br/>
                        <span>От покупок 2 - й линии Вы получаете 5% от каждой покупки людей, приглашенных вашей первой линией.</span><br/>
                        <span>От покупок 3 - й линии Вы получаете 2% от каждой покупки.</span><br/>
                        <span>От покупок 4 - й линии Вы получаете 1,5% от каждой покупки.</span><br/>
                        <span>От покупок 5 - й линии Вы получаете 1% от каждой покупки.</span><br/>
                        <br/>
                         <i class="i-block">Стоимость услуг для партнера.</i> С первой вашей заработанной комиссии система спишет партнерский взнос 12$.
                    
                          
                    <br/><br/>
                          <b>Желаем Вам дохода и призов! Ваш Prizolove.</b>
                         <br/><br/> <br/><br/>
                </div>
                          </div>
            </div>
        </div>
    </div>
</div>





