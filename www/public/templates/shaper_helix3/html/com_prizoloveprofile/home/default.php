<style>

.autor-link{display: block;
    font-size: 16px;
    margin-top: 10px;
    color: #2FCC71;
    text-decoration: underline;}

.img_how{width:90%;display:inline-block;margin:10px auto 0 auto;}
@media screen and (max-width:768px){.img_how{display:none;}}
.product_block_wrapper{vertical-align:top;}
.product_block_wrapper .testimonial{color:#333;position:relative;}
.product_block_wrapper .testimonial::after{content:"";position:absolute;width:100%;height:100%;top:0;left:0;background:linear-gradient(0deg,white,rgba(255,255,255,0));}
.overlay{position:absolute;top:0;left:0;width:100%;height:110%;background:rgba(0,0,0,0.35);z-index:-1;}


.popup_registration, .popup_enter, .popup_lost, .popup_thankyou{overflow:hidden;position:relative;width:100%;border-radius:15px;padding:20px;max-width:400px;margin:50px auto; background:white;text-align:center;font-family:Montserrat,Arial,sans-serif;box-shadow:0px 0px 30px rgba(0,0,0,0.15);}

.close_popup{position:absolute;top:10px;right:10px;color:white;z-index:99999;font-weight:bold;background:white;color:#212121;width:30px;height:30px;font-size:14px;line-height:30px;border-radius:50%;transition:1s;}
.close_popup:hover{background:#e74c3c;color:white;transform: rotate(360deg);}

.popup_registration .title, .popup_enter .title, .popup_lost .title{color:#212121;font-weight:700;font-size:20px;}
.popup_registration .text{color:#212121;padding:20px 0;font-weight:500;}
.popup_registration .btn_winner,.popup_registration .btn_partner{text-align:left;display:block;width:100%;border:2px solid #dfdfdf;border-radius:10px;padding:10px 0;text-decoration:none;font-size:16px;font-weight:500;color:#212121;margin-bottom:20px;transition:.5s;}
.popup_registration .btn_winner:hover{color:#ff5754;border:2px solid #FF5754;}
.popup_registration .btn_partner:hover{color:#825d75;border:2px solid #825d75;}

.popup_registration, .popup_thankyou{padding:120px 20px 20px 20px;}

.popup_registration::before, .popup_thankyou::before{content:"";position:absolute;top:-25px;left:0;display:block;max-width:400px;width:100%;height:150px;background:url(https://prizolove.com/images/header-popup.png) no-repeat center center;background-size:contain;}

.popup_registration .enter_in, .popup_enter .enter_in, .popup_enter .lost_password, .popup_lost .enter_in{color:#212121;font-size:14px;font-weight:500;}
.popup_registration .enter_in a, .popup_enter .enter_in a,.popup_enter .lost_password a, .popup_lost .enter_in a{text-decoration:none;color:#48B750;font-weight:700;transition:.5s;}
.popup_registration .enter_in a:hover, .popup_enter .enter_in a:hover,.popup_enter .lost_password a:hover, .popup_lost .enter_in a:hover{color:#147334;}
.btn_winner img, .btn_partner img{width:40px;margin:0 20px;vertical-align:middle;}

.popup_enter .lost_password{margin-top:10px;}


.popup_enter .inputs, .popup_lost .inputs, .popup_registration .inputs{text-align:left;margin:30px 0px 15px 0;}
.popup_enter label, .popup_lost label, .popup_registration label{color:#39414b;font-size:16px;font-weight:600;}
.popup_enter input, .popup_lost input, .popup_registration input{width:100%;border-radius:10px;outline:none !important;padding:15px 0px 15px 15px;box-shadow:none;border:2px solid #dfdfdf;transition:.5s;margin:5px auto 20px auto;background:white !important;font-family:Montserrat,Arial,sans-serif;font-size:16px;font-weight:500;color:#212121;}
.popup_enter input:hover, .popup_lost input:hover, .popup_registration input:hover{border:2px solid #48B750;}
.popup_enter input:focus, .popup_lost input:focus, .popup_registration input:focus{border:2px solid #48B750;}

.popup_enter .btn-form, .popup_lost .btn-form, .popup_registration .btn-form{display:block;width:100%;text-align:center;background:#48B750;border-radius:10px;padding:15px 0;color:white;font-size:18px;font-weight:600;text-decoration:none;transition:.5s;}
.popup_enter .btn-form:hover, .popup_lost .btn-form:hover, .popup_registration .btn-form:hover{background:#147334;}


.header{padding:10px 0 !important;background:#34495e;font-family:Montserrat,Arial,sans-serif;}

.name-user{color:white;font-weight:500;font-family:Montserrat,Arial,sans-serif;font-size:18px;}
.inline{display:inline-block;vertical-align:middle;}
.inline img{margin-left:20px;}
.inline a{margin-left:20px;transition:.5s;}
.inline a span{color:white;border-bottom:1px dotted white;font-size:12px;}
.inline i{border-bottom:0px;color:white;font-size:12px;}
.inline a:hover{opacity:0.6;text-decoration:none;}

.enters{text-align:right;}
.enters img{width:20px;margin-right:10px;vertical-align:middle;}
.enters a{color:#ecf0f1;text-decoration:none;font-size:14px;display:inline-block;font-weight:600;padding: 7px 0;transition:.5s;}
.enters a:hover{color:#FF5754;}
.popup_thankyou img{width:100px;
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;    -webkit-animation-name: bounceIn;
    animation-name: bounceIn;position:relative;z-index:2;}
.popup_thankyou .image::before,.popup_thankyou .image::after{
    content:"";
    position:absolute;
    display:block;
    width:100px;
    height:100px;
}
.popup_thankyou .title{font-weight:700;font-size:21px;color:#3FB659;padding:20px 0;}
.popup_thankyou .text{font-size:14px;line-height:18px;font-weight:500;}
.popup_thankyou .image::before{left:0;top:120px;background:url(https://prizolove.com/images/firework-left-popup.png) no-repeat center center;background-size:contain;transform:translateX(100px);-webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;    -webkit-animation-name: leftFadeIn;
    animation-name: leftFadeIn;}
.popup_thankyou .image::after{right:0;top:120px;
    background:url(https://prizolove.com/images/firework-right-popup.png) no-repeat center center;background-size:contain;transform:translateX(-100px);-webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;    -webkit-animation-name: rightFadeIn;
    animation-name: rightFadeIn;}

@-webkit-keyframes bounceIn {
    0% {
        opacity: 0;
        -webkit-transform: scale(.3);
    }
    30% {
        opacity: 1;
        -webkit-transform: scale(1.15);
    }
    60% {
        -webkit-transform: scale(.9);
    }
    85% {
        opacity: 1;
        -webkit-transform: scale(1.15);
    }


    100% {
        -webkit-transform: scale(1);
    }
}

@keyframes bounceIn {
    0% {
        opacity: 0;
        transform: scale(.3);
    }
    30% {
        opacity: 1;
        transform: scale(1.15);
    }
    60% {
        transform: scale(.9);
    }
    85% {
        opacity: 1;
        -webkit-transform: scale(1.15);
    }
    100% {
        transform: scale(1);
    }
}



@-webkit-keyframes leftFadeIn {
    0% {
        opacity: 0;
        transform:translateX(100px);
    }


    100% {
        opacity: 1;
        transform:translateX(25px);
    }
}

@keyframes leftFadeIn {
    0% {
        opacity: 0;
        transform:translateX(100px);
    }


    100% {
        opacity: 1;
        transform:translateX(50px);
    }
}


@-webkit-keyframes rightFadeIn {
    0% {
        opacity: 0;
        transform:translateX(-100px);
    }


    100% {
        opacity: 1;
        transform:translateX(-25px);
    }
}

@keyframes rightFadeIn {
    0% {
        opacity: 0;
        transform:translateX(-100px);
    }


    100% {
        opacity: 1;
        transform:translateX(-50px);
    }
}


.sidebar-cabinet{position:absolute;height:100%;background:#34495e;width:7%;top:0;left:0;box-shadow:0px 2px 6px rgba(0,0,0,0.15);z-index:99999;text-align:center;min-height:100vh;}
.sidebar-cabinet img{width:50px;}
.sidebar-cabinet .menu-link{padding:10px;color:white;font-weight:700;display:flex;justify-content:center;min-height:110px;align-items:center;Flex-wrap:wrap;font-size:13px;}
.sidebar-cabinet .menu-link:hover{background:rgba(255,255,255,0.05);}
.sidebar-cabinet .active-menu{background:rgba(255,255,255,0.05);}
.sidebar-cabinet .active-menu .img-wrapper img{-webkit-filter: none;
  -moz-filter: none;
  -ms-filter: none;
  -o-filter: none;
  filter: none;
  filter: none; /* IE 6-9 */}
.sidebar-cabinet .menu-link:hover img{-webkit-filter: none;
  -moz-filter: none;
  -ms-filter: none;
  -o-filter: none;
  filter: none;
  filter: none; /* IE 6-9 */}
.sidebar-cabinet .img-wrapper img{-webkit-filter: grayscale(100%);
  -moz-filter: grayscale(100%);
  -ms-filter: grayscale(100%);
  -o-filter: grayscale(100%);
  filter: grayscale(100%);
  filter: gray;}

.img-wrapper{width:100%;}

.header{padding:20px 0;background:#34495e;}
.user-block{text-align:right;}
.name-user{color:white;font-weight:500;font-family:"Montserrat",sans-serif;font-size:18px;}
.inline{display:inline-block;vertical-align:middle;}
.inline img{margin-left:20px;}
.inline a{margin-left:20px;}
.inline a span{color:white;border-bottom:1px dotted white;font-size:12px;}
.inline i{border-bottom:0px;color:white;font-size:12px;}
.inline a:hover{opacity:0.6;text-decoration:none;}
.title-block{padding:10px 0 0 0;position:relative; color: #000;}

.overlay-title-block{position:absolute;top:0;left:0;width:100%;height:100%;background:url(https://prizolove.com/images/pattern.png) top left;opacity:0.04;}

.title{color:#000;font-weight:700;font-family:"Montserrat",sans-serif; margin:0 !important;}

.affiliate-menu{width:100%;text-align:center;}
.affiliate-menu a{border-top-left-radius:10px;border-top-right-radius:10px;display:inline-block;width:32%;text-align:center;padding:10px 20px;background:rgba(0,0,0,0.2);color:#000;font-weight:500;font-family:"Montserrat",sans-serif;}
.affiliate-menu .active{background:white;color:black; border: 1px solid rgba(0,0,0,0.2); border-bottom: none;}

.affiliate-menu a:hover{background:white;color:black;}



.my-accounts-block{padding:20px 0;}
.stat-block{background:rgba(241, 196, 15,0.2);width:100%;text-align:center;border:1px solid #dbca9d;border-radius:10px;}
.stat-block div{width:32%;vertical-align:top;display:inline-block;padding:20px 0;}
.stat-block .count{font-weight:700;font-size:40px;line-height:50px;color:#212121;}
.stat-block .color-block{margin:5px 0;display:inline-block;padding:10px;text-transform:uppercase;font-weight:bold;border-radius:5px;color:white;font-size:12px;}
.ref{background:rgba(52, 152, 219,0.9);text-shadow:1px 2px 2px #3498db;}
.convers{background:rgba(211, 84, 0,0.9);text-shadow:1px 2px 2px #d35400;}
.com{background:rgba(142, 68, 173,0.9);text-shadow:1px 2px 2px #8e44ad;}

.stat-block .days{font-size:12px;font-weight:500;color:black;opacity:.5;}


.table-affiliate .row{text-align:left;background:white;font-family:"Montserrat",sans-serif;font-weight:bold;color:#2c2b2b;border-radius:7px;padding:15px 0px 15px 0px ;box-shadow:0px 2px 15px rgba(0,0,0,0.25);margin-bottom:10px;}

/*.tabs-block{display:none;}*/
.links-ref-block{text-align:center;margin:0;color:#000;}
.title-ref{position:relative;text-align:center;font-weight:bold;padding:10px;font-size:21px;color:white;}
.copy{display:block;padding:5px 8px 5px 8px;position:absolute;top:50%;right:20px;text-align:center;margin-top:2px;/* color:white !important;background:#7a231d; */}
.copy:hover{ /* background:#a03531; */}
.copy:active{transform:scale(1.5);}
.link-ref{padding:10px;font-weight:500;font-size:21px;}
.link-ref-block{margin-top:10px;padding:10px 0;font-size:18px;}


.spravka{text-align:left;background:#f1c40f;color:#272727 !important;font-size:14px;line-height:16px;font-weight:bold;display:inline-block;padding:20px 10px 20px 60px;border-radius:10px;box-shadow:0px 3px 0px #d6aa26;position:relative;}
.spravka:hover{background:#F2C437;}
.spravka:active{
    box-shadow:none;
    background-color: #F2C437;
    border-color: rgb(129, 33, 19);
    border-top-width: 3px;
    border-bottom-width: 1px;
    box-shadow: inset 0px 2px 2px 0px rgba(0, 0, 0, 0.25);
    transform:translateY(2px);
}
.spravka::before{content:"";display:block;width:40px;height:40px;background:url(https://prizolove.com/images/agenda.png) no-repeat center center;background-size:contain;position:absolute;top:50%;left:10px;transform:translateY(-50%);}


.rows-table-affiliate .container{background:#FDF3D7;font-family:"Montserrat",sans-serif;font-weight:500;color:#2c2b2b;border-radius:7px;padding:0px 15px 0px 15px;box-shadow:0px 2px 15px rgba(0,0,0,0.25);margin-bottom:20px;overflow:hidden;}

.rows-table-affiliate .row{padding:10px 0;border-bottom:1px solid rgba(0,0,0,0.10);}

.prise-upper{color:#08CB76;font-weight:bold;}





.parent-qa{width:20px !important;padding-bottom:0px !important;display:inline-block;position:relative;}
.question1-icon,.question2-icon,.question3-icon,.question4-icon,.question5-icon{opacity:0.5;cursor:pointer;z-index:9999;display:inline-block;}
.question-1, .question-2, .question-3, .question-4, .question-5{position:absolute;width:300px !important;max-width:300px !important;font-size:14px;background:white;border-radius:10px; color:#212121;padding:10px;box-shadow:0px 0px 15px rgba(0,54,255,0.19); transform:translateY(-15px);transition:.5s;font-weight:500; z-index: 999999999; visibility: hidden; opacity: 0;}

.question-1{top:-360%;left:-135px;}
.question-2{top:-500%;left:-202px;width:450px !important;max-width:450px !important;}
.question-3{top:-280%;left:-135px;}
.question-4{top:-120%;left:-135px;}
.question-5{top:-90%;left:-135px;}
.question-1::after, .question-2::after, .question-3::after, .question-4::after , .question-5::after{
    content: ''; 
    position: absolute; /* Абсолютное позиционирование */
    left: 45%; bottom: -20px; /* Положение треугольника */
    border: 10px solid transparent; /* Прозрачные границы */
    border-top: 10px solid white; /* Добавляем треугольник */
   }

@media screen and (min-width:769px){
	.question1-icon:hover ~ .question-1, .question2-icon:hover ~ .question-2, .question3-icon:hover ~ .question-3, .question4-icon:hover ~ .question-4, .question5-icon:hover ~ .question-5{opacity:1 !important;transform:translateY(0px) !important;visibility:visible;} 
}
.question1-icon:hover ~ .question-1, .question2-icon:hover ~ .question-2, .question3-icon:hover ~ .question-3, .question4-icon:hover ~ .question-4, .question5-icon:hover ~ .question-5{opacity:1 !important;transform:translateY(0px) !important;visibility:visible;} 

.money-btn{background:#e74c3c;padding:10px 45px;display:inline-block;font-size:14px;color:white !important;text-transform:uppercase;font-weight:bold;border-radius:10px;margin:0px 0px 5px 0px;opacity:0.6;box-shadow:0px 3px 0px #c0392b;transition:.5s;}
.money-btn-a:hover{background:#c0392b;}
.money-btn-a:active{box-shadow:none;transform:translateY(2px);}





.alert-min0{position:relative;width:auto !important;padding-bottom:0px !important;display:inline-block;position:relative;padding-top:0px !important;}
.alert-block{position:absolute;top:50px;left:-70px;position:absolute;width:330px !important;font-size:14px;background:white;border-radius:10px;color:#212121;padding:20px;box-shadow:0px 0px 15px rgba(0,54,255,0.19);  /* transform:translateY(-15px);transition:.5s;*/z-index:9999;font-weight:500;}

.alert-block a{color:#3498db;border-bottom:1px solid #3498db;}
.alert-block a:hover{color:#2980b9;border-bottom:1px solid #2980b9;}

.alert-block::after{
    content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */
    left: 40%; top: -30px;
    border: 20px solid transparent;	border-bottom: 20px solid white;
  }

@media screen and (min-width:768px){.ref-links-accordion {
    margin:5px 0;
}}
                     
@media screen and (max-width:767px){
  .link-ref,.link-ref-block{font-size:16px !important;}
  .avatar-block{display:none !important;}
  .inline{width:100%;}
  .title-block{text-align:center;}
  .title-block .title{font-size:25px;}
  .links-ref-block{margin:10px auto;}
  .title-block .spravka{display:none;}
  .affiliate-menu a{width:100% !important;border-radius:10px;margin:5px 0;}
  .stat-block div{width:100%;}
  .link-ref-block .copy{position:relative;top:0;left:0;}
  .table-affiliate{font-size:10px;margin-top:20px;}
  .rows-table-affiliate{font-size:10px;}
  .question-1{left:-200px;}
  .question-2{width:300px !important;left:-180px;top:-550%;}
  .question-1, .question-2, .question-3, .question-4, .question-5, .alert-block{font-size:12px;height:auto;}
  .sidebar-cabinet{display:none;}
  .question-0 {top: 50px !important; left: -350px !important; }
  .question-5 { left: -250px !important; }
}
@media screen and (max-width:768px){
    .money-btn{padding:10px 20px;font-size:10px;}
    .stat-block .color-block{font-size:10px;}
    .stat-block div{width:32%;}
    .stat-block .count{font-size:30px;}
    .stat-block .days{font-size:8px;}
    .ref-links-accordion label { font-size: 16px;}
    .my-accounts-block {
        padding: 10px 0;
    }
    .ref-links-accordion label{font-size:16px !important;}
    .title{font-size:16px !important;}
    .name-user{font-size:16px !important}
    .title-block {padding:0px !important;}
    .ref-links-accordion {margin: 0px 0 !important;}
}
@media screen and max-width(768px) and min-width(445px){
	.ref-links-active article { height: 280px; height: 280px; }
    .ref-links-accordion input:checked ~ article { height: 280px; height: 280px;}
}

@media screen and (max-width:445px){
	.ref-links-active article { height: 305px; }
    .ref-links-accordion input:checked ~ article { height: 305px;}
}
.question-2 {
    top: -180px;
    z-index:9999;
}
.question-1 {
    top: -135px;
    z-index:9999;}




.alert-min0{position:relative;width:auto !important;padding-bottom:0px !important;display:inline-block;position:relative;padding-top:0px !important; border}
.alert-block{position:absolute;top:140%;left:-50%;position:absolute;width:330px !important;font-size:14px;background:white;border-radius:10px;color:#212121;padding:20px;box-shadow:0px 0px 15px rgba(0,54,255,0.19); /* transform:translateY(15px);transition:.5s */; z-index:99999;font-weight:500; visibility: hidden;opacity:0;}
.alert-block:hover {opacity: 1; visibility: visible;}
.alert-block a{color:#3498db;border-bottom:1px solid #3498db;}
.alert-block a:hover{color:#2980b9;border-bottom:1px solid #2980b9;}

.alert-block::after{
    content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */
    left: -35px; bottom: 40px;
    border: 20px solid transparent;	border-bottom: 20px solid white;
   }
.alert-block.left::before{    	
    left: 140px; top: -40px;    
	content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */    
    border: 20px solid transparent;	border-bottom: 20px solid white;
}   
.alert-block.left::after{        
	content: '';
    z-index:9999; 
    position: absolute; /* Абсолютное позиционирование */
    left: 0; bottom: 0;
    border: 0;
}   
a.money-btn:hover ~ .alert-block {opacity:1 !important;transform:translateY(0px) !important;visibility:visible;}

.links-ref-block{text-align:center;border-bottom-left-radius:10px;border-bottom-right-radius:10px;color:#000;}
.title-ref{position:relative;text-align:center;font-weight:bold;padding:10px;font-size:21px;color:white;}
.link-ref{padding:10px;font-weight:500;font-size:21px;}
.link-ref-block{margin-top:10px;padding:10px 0;font-size:18px;}

.parent-qa{width:5px !important;padding-bottom:0px !important;display:inline-block;position:relative;}


/* СТИЛИ ДЛЯ АККОРДЕОНА */

.ref-links-accordion{
    width: 100%;
}


.ref-links-accordion label{
    text-align:center;
    margin-bottom:0 !important;
    position:relative;
    z-index: 10;
    display: block;
    cursor: pointer;
    color: #000;
    font-size: 21px;
    padding:15px;
    font-weight:600;
    border-top-left-radius:10px;
    border-top-right-radius:10px;
    border-bottom-left-radius:10px;
    border-bottom-right-radius:10px;
    text-decoration: underline !important;	
}
.ref-links-accordion label:hover{
    text-decoration: none !important;	
}
/* .ref-links-accordion label:hover{background:#EB5449;} */



.ref-links-accordion input:checked + label,
.ref-links-accordion input:checked + label:hover{
    color: #000;
    border-top-left-radius:10px;
    border-top-right-radius:10px;
    border-bottom-left-radius:0px;
    border-bottom-right-radius:0px;

}


.ref-links-accordion input{
    display: none;
}

.ref-links-accordion article{
	background: #fff;
    overflow: hidden;
    height: 0;
    position: relative;
    z-index: 10;
    -webkit-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -moz-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -o-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -ms-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
}
.ref-links-accordion input:checked ~ article {
    -webkit-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -moz-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -o-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -ms-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    height: 250px;
    overflow: initial !important;
}

.ref-links-button{
    text-align:center;
	margin-bottom:0 !important;
	position:relative;
    
    display: block;
    cursor: pointer;
    color: #000;
    font-size: 16px;
	font-weight:500;
	text-decoration:underline !important;
	border-radius:10px;
}
.ref-links-button:hover{background:white;color:black;}
.ref-links-active article{
	background: #fff;
	-webkit-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -moz-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -o-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    -ms-transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    transition: height 0.3s ease-in-out, box-shadow 0.6s linear;
    height: 500px;
    overflow: initial !important;
}
                 
                 
                .parent-qa-0{width:5px !important;padding-bottom:0px !important;display:inline-block;position:relative;}
.question0-icon{opacity:0.5;cursor:pointer;z-index:9999999 !important;display:inline-block;}
.question-0{position:absolute;width:500px !important;max-width:500px !important;font-size:14px;background:white;border-radius:10px;color:#212121;padding:10px;box-shadow:0px 0px 15px rgba(0,54,255,0.19);z-index:999999 !important; transform:translateX(15px);transition:.5s;opacity:0;visibility:hidden;font-weight:500;}

.question-0{top:-20px;left:40px;}

.question-0::after{
    content: ''; 
    position: absolute; /* Абсолютное позиционирование */
    left: -20px; top: 20px; /* Положение треугольника */
    border: 10px solid transparent; /* Прозрачные границы */
    border-right: 10px solid white; /* Добавляем треугольник */
   }


.parent-qa-0:hover .question-0{opacity:1 !important;transform:translateX(0px) !important;visibility:visible;}

.question-0 a{text-decoration:underline !important;}
            .question-0 .line{width:40px;height:2px;background:black;margin:10px 0;opacity:0.2;}

.title-block .title{display:inline-block;}
.align-vertical{vertical-align:middle !important;}

/* Новые стили */
.to-continue-alert{margin-top:20px;margin-bottom:-10px;}
.rows-table-affiliate .title{text-align:center;font-size:16px;}
.rows-table-affiliate .generation-2{background:#94cc9a !important;}
.rows-table-affiliate .generation-3{background:#ffa62e !important;}
.generation-parent-2 .container, .generation-parent-3 .container{ padding-left:0;padding-right:0;color:black;}
.generation-parent-2 .table-inner, .generation-parent-3 .table-inner{padding-left:15px;padding-right:15px;}

.rows-table-affiliate .stats{text-align:center;padding:20px 0;}
.generation-parent-2 .stats{background:#addbad;border-bottom:1px solid #94cc9a;}
.generation-parent-2 .table-inner{background:#addbad;}
.generation-parent-3 .stats{background:#ffb566;border-bottom:1px solid #d88329;}
.generation-parent-3 .table-inner{background:#ffb566;}
.table-inner{text-align:center !important;}
.rows-table-affiliate .stats div{display:inline-block;width:32%;text-align:center;font-size:16px;font-weight:bold;}
.alert-date{
    font-weight:500;
    border-left:10px solid #e16a6a;
    border-color: #e16a6a;
    color: #ce2d2d;
    background: #e7c3c3;
}
                                  
     .money-btn{
opacity:0.9;
     }                             
                                  
                                  
                                  
/* Popup container - can be anything you want */
.popup_hap2 {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* The actual popup */
.popup_hap2 .popuptext {
    visibility: hidden;
    width: 250px;
    background-color: rgba(255,255,255,0.95);
    color: #212121;
  box-shadow:0px 2px 6px rgba(0,0,0,0.15);
    text-align: center;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
 font-weight:500;
    margin-left: -200px;
}

/* Popup arrow */
.popup_hap2 .popuptext::after {
    content: "";
    position: absolute;
    top: 100%;

    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: rgba(255,255,255,0.95) transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup_hap2 .show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}
                                  
                                  /* Popup container - can be anything you want */
.popup_hap3 {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* The actual popup */
.popup_hap3 .popuptext {
    visibility: hidden;
    width: 250px;
    background-color: rgba(255,255,255,0.95);
    color: #212121;
  box-shadow:0px 2px 6px rgba(0,0,0,0.15);
    text-align: center;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
 font-weight:500;
    margin-left: -200px;
}

/* Popup arrow */
.popup_hap3 .popuptext::after {
    content: "";
    position: absolute;
    top: 100%;

    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: rgba(255,255,255,0.95) transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup_hap3 .show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}

                                  
                                  /* Popup container - can be anything you want */
.popup_hap4 {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* The actual popup */
.popup_hap4 .popuptext {
    visibility: hidden;
    width: 250px;
    background-color: rgba(255,255,255,0.95);
    color: #212121;
  box-shadow:0px 2px 6px rgba(0,0,0,0.15);
    text-align: center;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
 font-weight:500;
    margin-left: -200px;
}

/* Popup arrow */
.popup_hap4 .popuptext::after {
    content: "";
    position: absolute;
    top: 100%;

    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: rgba(255,255,255,0.95) transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup_hap4 .show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}


/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
                                  
                                  
                                  .btn-pereiti{display:block;max-width:400px;width:100%;text-align:center;margin:0 auto 0px auto !important;}
                                  .green-button{text-align: center;
    color: white !important;
    display: block !important;
    background: #2ecc71;
    width: 100%;
    padding: 20px 0;
    border-radius: 10px;
    margin-bottom: 50px;
    font-weight: bold;
    font-family: "Montserrat",sans-serif;transition:.5s;}
                                  .green-button:hover{background:#27ae60;}
                                                      
    
                                                      .profile-body .title{    font-size: 30px;
    line-height: 40px;}
  .block-how{background: rgba(241, 196, 15,0.2);
             margin-top:20px;
    
    text-align: center;
           
             padding:20px 0;
    border-top: 1px solid #dbca9d;
      
    border-bottom: 1px solid #dbca9d;
    border-radius: 0px;}
  .block-how .wrapper{position:relative;}
.block-how .wrapper-1::before{content:"1";}
.block-how .wrapper-2::before{content:"2";}
.block-how .wrapper-3::before{content:"3";}
.block-how .wrapper-4::before{content:"4";}
  .block-how .wrapper::before{
    position: absolute;
    left: 15%;
    top: 35%;
    font-size: 80px;
    opacity: 0.1;
    font-weight: bold;
    font-family: "Montserrat",sans-serif;
    transform: translate(-50%,-50%);}
.block-how .wrapper .text{padding:10px 5px;    color: #39414b;
    font-weight: bold;
    font-size: 14px;}
.block-how .wrapper::after{content:"";font-family:"FontAwesome";position:absolute;right:0px;top:50%;transform:translateY(-50%);font-size:40px;color:#84B246;}
.block-how .last-wrapper::after{display:none !important;}
.i-block{font-weight:500;color:#364A5D;font-weight:bold;}
.block-how .wrapper img{z-index:9999;}
         .yellow-block{background: rgba(241, 196, 15,0.2);
             margin-top:20px;
    width: 100%;
    text-align: center;
           
             padding:20px 0;
    border: 1px solid #dbca9d;
    border-radius: 10px;}
         .link-ref{font-size:18px;}
         .tooltip-inner{max-width:300px;background:red !important;font-family:"Montserrat",sans-serif;font-weight:bold;}
         .tooltip .arrow{display:none !important;}
         .tooltip{    top: 150px !important;left: 0px !important;}
         .link-ref-block{background-color:#ffedb8;}
                         .link-ref .copy{color:#689539;font-weight:bold;transition:.5s;transform:scale(1) !important;}
                                         .yellow-block .akciya{background:white !important;}
                                         
                                         
                                         .block-list-akcii .akciya{vertical-align:top !important;}
                                         
                                         .block-how{width:100%;}
.block-how .wrapper{text-align:center;position:relative;display:inline-block !important;width:24%;vertical-align:top;}
.block-how img{max-width:150px;width:100%;margin:0 auto;display:block;}

                          .block-how .wrapper img{z-index:9999;}               
@media screen and (max-width:889px){.block-how .wrapper{width:49% !important;}}
                                                      
                                         
                                         
                                         /* NEW STYLE */
                                         .how-doing .block-list-akcii{margin: 10px 0 0 0;}
                                         .how-doing .block-list-akcii .title-block{
    padding: 5px 0;border-bottom:0px;}
                                         .how-doing .block-list-akcii .title-block p{margin-bottom:0 !important;}
    .how-doing .block-list-akcii .block-how {
    margin-top: 0px;
    padding: 5px 0;}
                                         .how-doing   .block-how{border-bottom:0 !important;}
                                      .how-doing   .block-how .wrapper .text {
    padding: 5px 5px;
    font-size: 12px;
    line-height: 1.5em;}
                                         
   .close-btn_100{
     position: absolute;
right: 13px;
color: red;
cursor: pointer;
   }
                                         
                                         .active-sert{font-size: 12px;
    background: #2ecc71;
    color: white;
                                                      display:inline-block;
    padding: 5px 8px;
    box-shadow: 0px 2px 0 #27ae60;
    margin-left: 30px;    transition: .2s;}
                                         .active-sert:hover{transform: translateY(-5px);
    box-shadow: 0 3px 0 #27ae60, 0 2px 10px #27ae60;color:white;}
                                         
</style>
<script>
    jQuery(document).ready(function() {
       jQuery('.alert').on('closed.bs.alert', function () {
            var messageId = jQuery(this).find('.close-btn').eq(0).data('messageId');
           jQuery.ajax({
               method: "POST",
               url: "/index.php?option=com_ajax&plugin=messages&format=json",
               dataType: 'json',
               data: {id: messageId }
           });
       });
	   jQuery('.ref-links-button').on('click', function() {
		   jQuery('.ref-links-accordion').toggleClass("ref-links-active");
	   });
       jQuery('.copy').on('click', function() {
		   var $this = jQuery(this);
           var $temp = jQuery("<input>");
           jQuery("body").append($temp);
           $temp.val(jQuery(this).closest('div.link-ref-block').text()).select();
           document.execCommand("copy");
           $this.notify('Скопировано', {
               position: "top right",
               autoHideDelay: 250,
               className: 'success',
               showDuration: 100,
               arrowShow: false
           });
           $temp.remove();
       });
    });
</script>
  
  
  <script type="text/javascript">
	jQuery(document).ready(function(){
        //Get current time
        var currentTime = new Date().getTime();
        //Add hours function
        Date.prototype.addHours = function(h) {    
           this.setTime(this.getTime() + (h*60*60*1000000)); 
           return this;   
        }
        //Get time after 24 hours
        var after24 = new Date().addHours(10).getTime();
        //Hide div click
       jQuery('.close-btn_100').click(function(){
            //Hide div
           jQuery('.block-list-alert_close').hide(800);
            //Set desired time till you want to hide that div
            localStorage.setItem('desiredTime', after24); 
        });
        //If desired time >= currentTime, based on that HIDE / SHOW
        if(localStorage.getItem('desiredTime') >= currentTime)
        {
            jQuery('.block-list-alert_close').hide();
        }
        else
        {
           jQuery('.block-list-alert_close').show();
        }
});

</script>
  
  
  
<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument();


JLoader::import('pl_lib.library');

$user = JFactory::getUser();
$userId = $user->id;
$parentId = PlLibHelperAffiliate::getParent();
$affiliateLink = '';
if ($parentId) $affiliateLink = '&atid='.$parentId;
                                         
                                         
                                         
if ($userId) $account = PlLibHelperUsers::getInstance()->getBonusAccount($userId);

$events = PlLibHelperEvents::getInstance()->getLatestEvents(18);
$products = PlLibHelperPersonal::getInstance()->getLatestProducts();


if ($parentId) {
	$children = PlLibHelperAffiliate::getChildren();

	$totals = PlLibHelperAffiliate::getTotal();

	$orders = PlLibHelperAffiliate::getOrders($parentId);

	$totalLeads = PlLibHelperAffiliate::getTotalLeads($parentId);

	$levelOneChildren = PlLibHelperAffiliate::getLevelOneChildren($parentId);

	$levelTwoChildren = array();
	if (count($levelOneChildren)) $levelTwoChildren = PlLibHelperAffiliate::getLevelTwoChildren($levelOneChildren);

	$levelTwoSales = PlLibHelperAffiliate::getLevelTwoSales($parentId);

	$levelTwoComission = PlLibHelperAffiliate::getLevelTwoComission($parentId);

	$expireDays = PlLibHelperAffiliate::getExpireDays();
	//$totalComission = PlLibHelperAffiliate::getTotalComission($user);
        $totalComission = PlLibHelperBalances::getInstance()->getBalance($user->id);
	$totalSales = PlLibHelperAffiliate::getTotalSales($user);

	$totalLeads = PlLibHelperAffiliate::getTotalLeads($parentId);
} else {
	$totalSales = 0;
	$totalLeads = 0;
	$totalComission = 0;
}

$totalEvents = count($events);
$completedEvents = 0;

foreach($events as $event) {
    if (!empty($event->status)) $completedEvents++;
}
$newEvents = $totalEvents - $completedEvents;

$messages = PlLibHelperMessages::getInstance()->getMessages($user->id);
//$doc = JFactory::getDocument();
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagebuilder.css');
//$doc->addStyleSheet(JPATH_ROOT.'/components/com_sppagebuilder/assets/css/sppagecontainer.css');
?>
<div class="profile-body">
    <div class="container">
  
 
 
        <div class="row">
   <div class="col-12 col-md-12 col-sm-12 col-lg-12 how-doing">
  <div class="block-list-akcii">
  
   <div class="block-list-alert_close">
  
  <div class="title-block">
      <div class="title">  Как выиграть супер-приз от 10 000$?                 

                           
                        </div>
  <a href="/autor" class="autor-link" target="_blank">Подать на рассмотрение свой вариант испытания</a>
 
  </div>
  <div class="block-how">
                        <div class="wrapper wrapper-1">
                            <img src="https://prizolove.com/images/icon-2-personal-11.jpg">
                            <div class="text">Пройдите доступное вам испытание.
И выиграйте бонус на покупку.</div>
                        </div>
                        <div class="wrapper wrapper-2">
                            <img src="https://prizolove.com/images/icon-2-profile-1.JPG">
                            <div class="text"> Потратите бонус и купите любой товар.</div>
                        </div>
                        <div class="wrapper wrapper-3">
                            <img src="https://prizolove.com/images/icon-2-personal-2.jpg">
                            <div class="text">К оплаченному заказу будет приложен сертификат на участие в розыгрыше.</div>
                        </div>
                        <div class="wrapper wrapper-4 last-wrapper">
                            <img src="https://prizolove.com/images/icon-2-profile-4.JPG">
                            <div class="text">В день розыгрыша пусть вам повезет! И приз ваш!</div>
                        </div>
                    </div>
  </div>
   </div>
  </div>
  
  
  
            <div class="col-12 col-md-9 col-sm-12 col-lg-9 wrapper-main">
                <div class="block-list-akcii">
                    <div class="title-block">
                    
  
  
  
   
  
  
  
  <div class="title">Пройдите эти испытания, дайте правильные ответы!</div>
  
         
                        <?php /* <div class="count">2 акции завершено</div> */ ?>
                    </div>
                    <div class="body text-center">
                        <?php 

			foreach($events as $event) : 
			if ($event->event_group > 0 and $event->user_status != 1) continue;
			?>
                            <a href="<?=Jroute::_('index.php?option=com_puzzles&view=puzzles&Itemid=1048&id='.$event->id.$affiliateLink);?>" class="akciya">
			    <?php if (empty($event->user_status)) : ?>
                            <div class="accept-puzzle new">NEW<div class="text">Новое испытание, вы тут не были.</div></div>
                            <?php else: ?>
                            <div class="accept-puzzle done"><i class="fas fa-check"></i><div class="text">Ура! Испытание пройдено!</div></div>
                            <?php endif;?>
				            <div class="product_image_wrapper">
                                <?php if ($event->image) : ?>
                                    <img src="/<?=$event->image;?>">
                                <?php else: ?>
                                    <img src="https://prizolove.com/images/second.jpg">
                                <?php endif; ?>
				            </div>
                            <div class="name"><?=$event->name;?></div>
			   <?php 
							if ($event->difficulty) : 
								switch($event->difficulty) {
									case 'easy':
										$difficultyLabel = 'COM_EVENT_EASY';
										break;
									case 'medium':
										$difficultyLabel = 'COM_EVENT_MEDIUM';
										break;
									case 'hard':
										$difficultyLabel = 'COM_EVENT_HARD';
										break;
								}
							
							?>
                            <div class="slojnost">Сложность: <?=JText::_($difficultyLabel);?></div>
                            <?php else: ?>
                            <div class="slojnost">Сложность: низкая</div>
                            <?php endif; ?>
                            </a><?php endforeach?>
                                 <div class="popup_hap2 btn-see-more" btn-see-more onclick="myFunction2()"><i class="fas fa-gem"></i> Дальше в Prizoland
                                        <span class="popuptext" id="myPopup2">Для открытия новых испытаний нужен хотя бы один активированный Сертификат Финалиста.<br/>Пройдите хотя бы одно испытание полностью!</span>
                              
                                      </div>

                          <script>
                          // When the user clicks on div, open the popup
                          function myFunction2() {
                              var popup = document.getElementById("myPopup2");
                              popup.classList.toggle("show");
                          }
                          </script>

                    </div>
                </div>
                         
                  <div class="wrapper-achieve lside">
      <div class="block-achieve">
        <div class="heading"><div class="img-wrapper"><img src="https://prizolove.com/images/blesk-3-1.png" class="blesk-3"><img src="https://prizolove.com/images/blesk-2-1.png" class="blesk-2"><img src="https://prizolove.com/images/blesk-1-1.png" class="blesk-1"><img src="https://prizolove.com/images/bag-money-lc-1.png" class="bag"></div><span>Ваши достижения:</span></div>
        <div class="main">
      <a class="sertificates">
        <div class="icon-wrapper">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<polygon style="fill:#4B9FD8;" points="256,461.104 205.072,495.904 205.072,367.472 256,367.472 "/>
<polygon style="fill:#3789C9;" points="256,461.104 306.928,495.904 306.928,367.472 256,367.472 "/>
<polygon style="opacity:0.05;fill:#010101;enable-background:new    ;" points="205.136,445.424 231.408,441.376 255.968,461.104   256.016,461.056 256.048,461.088 280.592,441.376 306.896,445.44 306.896,367.472 205.136,367.472 "/>
<path style="fill:#EBCEB8;" d="M496,16.096H16c-8.832,0-16,7.168-16,16v309.808c0,8.832,7.168,16,16,16h480c8.832,0,16-7.168,16-16  V32.096C512,23.248,504.832,16.096,496,16.096z"/>
<path style="fill:#FDF2E5;" d="M45.888,313.472c-26.432-15.6,6.272-34.272,6.272-64.976s-8.32-30.704-8.32-61.408  c0-30.752,8.32-30.752,8.32-61.504s-32.736-49.408-6.272-65.072c44.704-26.464,54.064,7.744,106.016,7.744  c52,0,52-8.32,103.984-8.32c52.032,0,52.032,8.32,104.064,8.32s61.36-34.176,106.176-7.744c26.432,15.6-6.272,34.272-6.272,64.976  s8.32,30.704,8.32,61.408c0,30.752-8.32,30.752-8.32,61.504s32.736,49.424,6.272,65.088c-44.704,26.464-54.064-7.744-106.016-7.744  c-52,0-52,8.32-103.984,8.32c-52.032,0-53.616-17.536-104.8-8.256C97.072,315.632,90.704,339.904,45.888,313.472z"/>
<g>
	<path style="fill:#84CA9B;" d="M426.208,284.448h-80.544c-4.416,0-8-3.584-8-8s3.584-8,8-8h80.544c4.416,0,8,3.584,8,8   S430.64,284.448,426.208,284.448z"/>
	<path style="fill:#84CA9B;" d="M166.32,284.448H85.792c-4.416,0-8-3.584-8-8s3.584-8,8-8h80.528c4.416,0,8,3.584,8,8   S170.736,284.448,166.32,284.448z"/>
	<path style="fill:#84CA9B;" d="M426.208,225.456H85.792c-4.416,0-8-3.584-8-8s3.584-8,8-8h340.416c4.416,0,8,3.584,8,8   S430.64,225.456,426.208,225.456z"/>
	<path style="fill:#84CA9B;" d="M426.208,166.448H85.792c-4.416,0-8-3.584-8-8s3.584-8,8-8h340.416c4.416,0,8,3.584,8,8   S430.64,166.448,426.208,166.448z"/>
	<path style="fill:#84CA9B;" d="M357.856,107.456H154.144c-4.416,0-8-3.584-8-8s3.584-8,8-8h203.712c4.416,0,8,3.584,8,8   S362.288,107.456,357.856,107.456z"/>
</g>
<polygon style="fill:#ED6362;" points="357.232,349.632 339.312,371.952 343.68,400.256 316.976,410.608 306.624,437.312   278.336,432.944 256,450.864 233.664,432.944 205.376,437.296 195.024,410.608 168.32,400.256 172.688,371.952 154.768,349.632   172.688,327.296 168.32,298.992 195.024,288.656 205.376,261.952 233.664,266.304 256,248.4 278.336,266.304 306.624,261.952   316.976,288.656 343.68,298.992 339.312,327.296 "/>
<path style="fill:#EF6F6F;" d="M271.424,339.312h-64.816c-4.4,0-8.064-3.648-8.064-8.064l0.096-0.56v-0.096  c-0.096-4.688,3.744-8.528,8.432-8.528h47.28c5.248,0,9.84-3.84,10.208-9.088c0.192-5.536-4.128-10.112-9.648-10.112H205.28  c-4.688,0-8.064-3.84-8.064-8.528c0,0,0,0,0-0.096s0-0.096,0-0.096c0-4.592,3.376-8.528,8.064-8.528h41.472  c5.152,0,9.744-3.84,10.032-8.992c0.288-5.616-4.128-10.128-9.552-10.128H203.6l-8.576,22.144l-26.704,10.352l4.368,28.304  l-17.92,22.336l17.92,22.336l-4.368,28.304l26.704,10.352l8.576,22.144h50.016c5.248,0,9.84-3.84,10.128-8.992  c0.288-5.536-4.128-10.128-9.552-10.128h-41.76c-5.536,0-9.936-4.592-9.648-10.112c0.288-5.152,4.96-9.088,10.128-9.088H252.8  c5.152-0.096,9.28-4.32,9.28-9.552c0-5.248-4.304-9.552-9.552-9.552h-49.072c-4.496,0-8.16-3.648-8.064-8.064v-0.752  c-0.288-4.304,3.184-8.064,7.584-8.064h67.904c5.248,0,9.84-3.84,10.208-8.992C281.264,343.904,276.944,339.312,271.424,339.312z"/>
<path style="fill:#FFCC5B;" d="M256,413.728c-35.344,0-64.096-28.752-64.096-64.096s28.752-64.096,64.096-64.096  s64.096,28.752,64.096,64.096S291.344,413.728,256,413.728z"/>
<path style="fill:#FDBC4B;" d="M245.072,292.464c-0.224,4.256,3.184,7.792,7.36,7.792h32.16c4.256,0,7.648,3.536,7.44,7.792  c-0.224,3.968-3.824,7.008-7.792,7.008h-30.72c-3.968,0.08-7.152,3.328-7.152,7.36c0,4.048,3.328,7.36,7.36,7.36h37.792  c3.472,0,6.288,2.816,6.208,6.208v0.576c0.224,3.328-2.448,6.208-5.84,6.208h-52.304c-4.048,0-7.584,2.96-7.872,6.928  c-0.144,4.256,3.168,7.792,7.44,7.792h49.92c3.392,0,6.208,2.816,6.208,6.208l-0.08,0.432v0.064c0.08,3.616-2.88,6.576-6.496,6.576  h-36.432c-4.048,0-7.584,2.96-7.872,7.008c-0.144,4.256,3.184,7.792,7.44,7.792h38.224c3.616,0,6.208,2.96,6.208,6.576  c0,0,0,0,0,0.08s0,0.08,0,0.08c0,3.536-2.592,6.56-6.208,6.56h-31.952c-3.968,0-7.504,2.96-7.728,6.928  c-0.224,4.336,3.168,7.792,7.36,7.792h0.816c34.128-1.376,61.504-29.504,61.504-63.968c0-35.344-28.752-64.096-64.096-64.096  c-1.472,0-2.912,0.128-4.352,0.224C248.16,286.288,245.264,288.912,245.072,292.464z"/>
<path style="fill:#FFFFFF;" d="M244.368,377.472L244.368,377.472c-2.24,0-4.4-0.896-5.984-2.48l-15.504-15.504  c-3.296-3.296-3.296-8.656,0-11.952s8.656-3.296,11.952,0l9.52,9.52l32.8-32.8c3.296-3.296,8.656-3.296,11.952,0  s3.296,8.656,0,11.952l-38.784,38.784C248.752,376.576,246.608,377.472,244.368,377.472z"/>
</svg>
        
        </div><span class="number"><?=($userId)?PlLibHelperEvents::getInstance()->getCertificates($userId):0;?></span> <span class="text">сертификатов</span>
          <div class="about">Пройдите доступные испытания, выиграйте сертификат на розыгрыш суперприза.</div></a><br/>
          <a class="sertificates">
          <span style="padding-left:30px;" class="number"><?=($userId)?PlLibHelperEvents::getInstance()->getCertificates($userId,1):0;?></span> <span class="text">активированных</span>
          <div class="about">Покупка любого товара активирует сертификат и он попадает в барабан при розыгрышах призов</div></a>
          <a class="active-sert" target="_blank" href="/383">Активировать сертификат</a>
      <div class="bonuses">
        <div class="icon-wrapper">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" xml:space="preserve">
<polygon style="fill:#CC2E48;" points="29,55 0,19 58,19 "/>
<polygon style="fill:#FC3952;" points="58,19 0,19 10,3 48,3 "/>
<polygon style="fill:#F76363;" points="42.154,19 48,3 10,3 15.846,19 "/>
<polygon style="fill:#F49A9A;" points="42,19 29,3 16,19 "/>
<polygon style="fill:#CB465F;" points="15.846,19 29,55 42.154,19 "/>
</svg>
        </div>
        <span class="number"><?=(!empty($account->value)?floatval($account->value):0);?></span> <span class="text">бонусов</span>
            <div class="about">Бонус равен эквиваленту USD$ На эту сумму может быть уменьшена покупка товара.
Покупка активирует сертифкат на розыгрыш 10 000$<br/><a href="/383" target="_blank" style="color:#E95D4B;text-decoration:underline;">Потратить свои бонусы</a>
            </div>
            </div>
               <a class="bonuses" style="padding:0px 0px 0px 0px !important;">
            
        <div class="icon-wrapper">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m31 175.859375v206.351563l45-45 45 45v-153.484376zm0 0" fill="#ff4d4d"/><path d="m391 224.929688v157.28125l45-45 45 45v-187.71875zm0 0" fill="#ff001e"/><path d="m391 241v-30c49.628906 0 90-40.371094 90-90v-60h-75v-30h106v90c0 66.167969-54.832031 120-121 120zm0 0" fill="#ff9f00"/><path d="m112.824219 236.09375c-65.84375-17.285156-112.824219-76.949219-112.824219-145.09375v-60h106v30h-75v30c0 54.519531 36.78125 102.261719 89.441406 116.089844zm0 0" fill="#ffd400"/><path d="m226 301h60v181h-60zm0 0" fill="#ffd400"/><path d="m256 301h30v181h-30zm0 0" fill="#ff9f00"/><path d="m91 0v166c0 90.902344 74.097656 165 165 165s165-74.097656 165-165v-166zm0 0" fill="#ffe470"/><path d="m421 0v166c0 90.902344-74.097656 165-165 165v-331zm0 0" fill="#fdbf00"/><path d="m348.101562 124-63.601562-9.300781-28.5-57.597657-28.5 57.597657-63.601562 9.300781 46.199218 45-11.097656 63.300781 57-30 57 30-11.097656-63.300781zm0 0" fill="#fdbf00"/><path d="m301.902344 169 11.097656 63.300781-57-30v-145.199219l28.5 57.597657 63.601562 9.300781zm0 0" fill="#ff9f00"/><path d="m361 482v30h-210v-30c0-16.5 13.5-30 30-30h150c16.5 0 30 13.5 30 30zm0 0" fill="#ffe470"/><path d="m361 482v30h-105v-60h75c16.5 0 30 13.5 30 30zm0 0" fill="#fdbf00"/></svg>
        </div>
        <span class="number"><?=$newEvents;?></span> <span class="text">новых испытаний</span>
          <div class="about">При успешном прохождении испытаний, открываются новые.</div>
          </a>
          
        </div>
      </div>
      
    </div>          
                            
                            
                            <!--     ================================================
                <div class="block-list-vitrina">
                    <div class="title-block">
                        <div class="title">Список доступных Вам товаров
                            <?php /* <div class="parent-qa"><i class="fas fa-question-circle question4-icon"></i>
                            <span class="question-4">Количество покупок совершенных покупателями вашей сети.</span></div> */ ?>
                        </div>
                    </div>
                    <div class="body text-center">
                        <?php foreach($products as $product) : ?>
                            <a href="<?=($product->landing)?($product->landing):'/186';?>" class="product_block_wrapper">
                                <?php /* <div class="free">Только в Prizolove</div> */ ?>
                                <div class="diamond_product"></div>
                                <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                                <div class="product_image_wrapper">
                                <?php if ($product->image) :?>
                                    <img class="product_image" src="<?=$product->image;?>">
                                <?php endif; ?>
                                </div>

                                <div class="title_name"><?=$product->name;?></div>
                                <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                                <div class="price">от <?=$product->price;?> грн.</div>

                            </a>
                        <?php endforeach; ?>
                        <?php /*
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/kitchen1.png">
                            <div class="title_name">Набор для кухни</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 407 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">
                            <div class="free">Только в Prizolove</div>
                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/kitchen1.png">
                            <div class="title_name">Набор для кухни</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 407 грн.</div>

                        </a>
                        <a href="#" class="product_block_wrapper">

                            <div class="diamond_product"></div>
                            <div class="diamond_text">Дает право активировать Сертификат Финалиста и участвовать в розыгрышах призов. Удачи ;)</div>
                            <img src="https://prizolove.com/images/dyxi.png">
                            <div class="title_name">Парфюмерия</div>
                            <div class="button"><img src="https://prizolove.com/images/cart-catalog.png"></div>
                            <div class="price">от 387 грн.</div>

                        </a> */ ?>
                                                 
      
                                                   <a href="/186" style="color:red!important;" class="btn-see-more">Посмотреть все товары</a>
                    </div>                     
                </div>
                            ==================================================END:: BLOCK   --> 
                            
                              
                      <div class="green-button-wrapper" style="padding-top:10px;background:#F0F4F9;">
                      
                       <a href="/referal" style="margin-bottom:0 !important;" class="green-button">Реферальная сеть! 10$ за регистрацию друга!</a>
                      
                      </div>
                              
                              
                              
                              <div class="block-list-vitrina">
          <div class="title-block">
          <div class="title"><a href="/136" style="font-weight: bold;color:#39414b !important;text-decoration:underline !important;">Список победителей</a></div>
          </div>
          <div class="body text-center">
           
            <a href="/136" class="product_block_wrapper winners text-center" style="padding-top:20px;">
  
 
 
  <img src="https://prizolove.com/images/misyats.png" style="margin: 0 auto;">
  <div class="title_name">Месяц Антонина</div>
                              <p class="testimonial"> Очень рада, что приняла участие в этом розыгрыше, и не только</p>
  
  
</a><a href="/136" class="product_block_wrapper winners text-center" style="padding-top:20px;">
  
 
 
  <img src="https://prizolove.com/images/david.jpg" style="margin: 0 auto;">
  <div class="title_name">Давид Марьян</div>
                              <p class="testimonial">Спасибо! Чудесные новости, конечно! Очень порадовали, благодарю! </p>
  
  
</a><a href="136" class="product_block_wrapper winners text-center" style="padding-top:20px;">
  
 
 
  <img src="https://prizolove.com/images/duma.jpg" style="margin: 0 auto;">
  <div class="title_name">Дума Евгения</div>
                              <p class="testimonial">Даже не знаю, как описать это.... Очень приятно, радостно. Я не </p>
  
  
</a>
               
         
        </div> 
        
       
      </div>
                              
                              
                              
                                       <div class="block-list-vitrina">
          <div class="title-block">
          <div class="title">Определение победителей ежемесячного розыгрыша</div>
          </div>
          <div class="body text-center">
           <iframe width="900" height="500" src="https://www.youtube.com/embed/VTygc1O7dN8?controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
           
               
         
        </div> 
        
       
      </div>
                          
                              
                              
                              
                              
                              
                              
                               <div class="block-list-vitrina">
                    <div class="title-block">
                        <div class="title"><a href="/accounts" style="font-weight: bold;color:#39414b !important;text-decoration:underline !important;">Мой бизнес</a>  </div>
                    </div>
                    <div class="body text-center">

                       <div class="stat-block">
                <div>
                    <span class="count"><?php if ($parentId == 3) :?>157<?php else :?><?php echo $totalLeads; ?><?php endif;?></span>
					<div class="parent-qa"><i class="fas fa-question-circle question3-icon"></i>
					<div class="question-3">Количество покупателей привлеченных системой в Вашу сеть. А так же покупатели зарегистрированные по вашим реферальным ссылкам.</div></div>
					<br/>
                    <span class="ref color-block">Человек</span><br/>
                    <?php /* <span class="days">За последние 30 дней</span> */ ?>
                </div>
                <div>
                    <span class="count"><?php if ($parentId == 3) :?>67<?php else :?><?php echo $totalSales; ?><?php endif; ?></span>
					<div class="parent-qa"><i class="fas fa-question-circle question4-icon"></i>
					<span class="question-4">Количество покупок совершенных покупателями вашей сети.</span></div><br/>					
                    <span class="convers color-block">Покупок</span><br/>
                    <?php /* <span class="days">За последние 30 дней</span> */ ?>
                </div>
                <div>
                    <span class="count"><?php if ($parentId == 3) :?>212,50<?php else :?><?php echo $totalComission; ?><?php endif;?> $</span>
					<div class="parent-qa"><i class="fas fa-question-circle question5-icon"></i>
					<span class="question-5">Ваш доход к выводу.</span></div><br/>
                    <?php if (intval($totalComission)) : ?>
                        <div class="alert-min0">
             
                       <span class="popup_hap3 money-btn" btn-see-more onclick="myFunction()">Вывести
                                        <span class="popuptext" id="myPopup3">
                      Что бы вывести средства вам нужно быть владельцем платного аккаунта. Посмотрите партнерские пакеты.
                      (<a target="_blank" href="https://prizolove.com/362">Выберите наиболее подходящий</a>.) 
                      Минимальный следующий <a target="_blank" href="https://prizolove.com/orderpage?pid=256">шаг</a>
                      </span>
                         </span>

                          <script>
                          // When the user clicks on div, open the popup
                          function myFunction() {
                              var popup = document.getElementById("myPopup3");
                              popup.classList.toggle("show");
                          }
                          </script>
                      
                      
                      
                      </div><br/>
                    <?php else: ?>
                      
                      
                        <div class="alert-min0">
                
                          <span class="popup_hap4 money-btn" btn-see-more onclick="myFunction()">Вывести
                                        <span class="popuptext" id="myPopup4">
                       К сожалению, у Вас нет суммы к выводу. <a href="https://prizolove.com/362" target="_blank">
                      Увеличьте партнерский пакет</a>, пригласите больше людей или немного подождите
                    
                      </span>
                                      </span>

                          <script>
                          // When the user clicks on div, open the popup
                          function myFunction() {
                              var popup = document.getElementById("myPopup4");
                              popup.classList.toggle("show");
                          }
                          </script>
                      
                      </div>
                      
                      
                      
                      <br/>
                    <?php endif;?>
                    <?php /* <span class="days">За последние 30 дней</span> */ ?>
                </div>

                    </div>
                </div>
                              
                      
                      
                </div>
                      
                      
                      
                      
                      
                      
 
               
                
               
                </div>
                     
                      
                      
               

<div class="col-12 col-sm-12 col-md-3 col-lg-3 wrapper-achieve rside">
      <div class="block-achieve">
        <div class="heading"><div class="img-wrapper"><img src="https://prizolove.com/images/blesk-3-1.png" class="blesk-3"><img src="https://prizolove.com/images/blesk-2-1.png" class="blesk-2"><img src="https://prizolove.com/images/blesk-1-1.png" class="blesk-1"><img src="https://prizolove.com/images/bag-money-lc-1.png" class="bag"></div><span>Ваши достижения:</span></div>
        <div class="main">
      <a class="sertificates">
        <div class="icon-wrapper">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<polygon style="fill:#4B9FD8;" points="256,461.104 205.072,495.904 205.072,367.472 256,367.472 "/>
<polygon style="fill:#3789C9;" points="256,461.104 306.928,495.904 306.928,367.472 256,367.472 "/>
<polygon style="opacity:0.05;fill:#010101;enable-background:new    ;" points="205.136,445.424 231.408,441.376 255.968,461.104   256.016,461.056 256.048,461.088 280.592,441.376 306.896,445.44 306.896,367.472 205.136,367.472 "/>
<path style="fill:#EBCEB8;" d="M496,16.096H16c-8.832,0-16,7.168-16,16v309.808c0,8.832,7.168,16,16,16h480c8.832,0,16-7.168,16-16  V32.096C512,23.248,504.832,16.096,496,16.096z"/>
<path style="fill:#FDF2E5;" d="M45.888,313.472c-26.432-15.6,6.272-34.272,6.272-64.976s-8.32-30.704-8.32-61.408  c0-30.752,8.32-30.752,8.32-61.504s-32.736-49.408-6.272-65.072c44.704-26.464,54.064,7.744,106.016,7.744  c52,0,52-8.32,103.984-8.32c52.032,0,52.032,8.32,104.064,8.32s61.36-34.176,106.176-7.744c26.432,15.6-6.272,34.272-6.272,64.976  s8.32,30.704,8.32,61.408c0,30.752-8.32,30.752-8.32,61.504s32.736,49.424,6.272,65.088c-44.704,26.464-54.064-7.744-106.016-7.744  c-52,0-52,8.32-103.984,8.32c-52.032,0-53.616-17.536-104.8-8.256C97.072,315.632,90.704,339.904,45.888,313.472z"/>
<g>
	<path style="fill:#84CA9B;" d="M426.208,284.448h-80.544c-4.416,0-8-3.584-8-8s3.584-8,8-8h80.544c4.416,0,8,3.584,8,8   S430.64,284.448,426.208,284.448z"/>
	<path style="fill:#84CA9B;" d="M166.32,284.448H85.792c-4.416,0-8-3.584-8-8s3.584-8,8-8h80.528c4.416,0,8,3.584,8,8   S170.736,284.448,166.32,284.448z"/>
	<path style="fill:#84CA9B;" d="M426.208,225.456H85.792c-4.416,0-8-3.584-8-8s3.584-8,8-8h340.416c4.416,0,8,3.584,8,8   S430.64,225.456,426.208,225.456z"/>
	<path style="fill:#84CA9B;" d="M426.208,166.448H85.792c-4.416,0-8-3.584-8-8s3.584-8,8-8h340.416c4.416,0,8,3.584,8,8   S430.64,166.448,426.208,166.448z"/>
	<path style="fill:#84CA9B;" d="M357.856,107.456H154.144c-4.416,0-8-3.584-8-8s3.584-8,8-8h203.712c4.416,0,8,3.584,8,8   S362.288,107.456,357.856,107.456z"/>
</g>
<polygon style="fill:#ED6362;" points="357.232,349.632 339.312,371.952 343.68,400.256 316.976,410.608 306.624,437.312   278.336,432.944 256,450.864 233.664,432.944 205.376,437.296 195.024,410.608 168.32,400.256 172.688,371.952 154.768,349.632   172.688,327.296 168.32,298.992 195.024,288.656 205.376,261.952 233.664,266.304 256,248.4 278.336,266.304 306.624,261.952   316.976,288.656 343.68,298.992 339.312,327.296 "/>
<path style="fill:#EF6F6F;" d="M271.424,339.312h-64.816c-4.4,0-8.064-3.648-8.064-8.064l0.096-0.56v-0.096  c-0.096-4.688,3.744-8.528,8.432-8.528h47.28c5.248,0,9.84-3.84,10.208-9.088c0.192-5.536-4.128-10.112-9.648-10.112H205.28  c-4.688,0-8.064-3.84-8.064-8.528c0,0,0,0,0-0.096s0-0.096,0-0.096c0-4.592,3.376-8.528,8.064-8.528h41.472  c5.152,0,9.744-3.84,10.032-8.992c0.288-5.616-4.128-10.128-9.552-10.128H203.6l-8.576,22.144l-26.704,10.352l4.368,28.304  l-17.92,22.336l17.92,22.336l-4.368,28.304l26.704,10.352l8.576,22.144h50.016c5.248,0,9.84-3.84,10.128-8.992  c0.288-5.536-4.128-10.128-9.552-10.128h-41.76c-5.536,0-9.936-4.592-9.648-10.112c0.288-5.152,4.96-9.088,10.128-9.088H252.8  c5.152-0.096,9.28-4.32,9.28-9.552c0-5.248-4.304-9.552-9.552-9.552h-49.072c-4.496,0-8.16-3.648-8.064-8.064v-0.752  c-0.288-4.304,3.184-8.064,7.584-8.064h67.904c5.248,0,9.84-3.84,10.208-8.992C281.264,343.904,276.944,339.312,271.424,339.312z"/>
<path style="fill:#FFCC5B;" d="M256,413.728c-35.344,0-64.096-28.752-64.096-64.096s28.752-64.096,64.096-64.096  s64.096,28.752,64.096,64.096S291.344,413.728,256,413.728z"/>
<path style="fill:#FDBC4B;" d="M245.072,292.464c-0.224,4.256,3.184,7.792,7.36,7.792h32.16c4.256,0,7.648,3.536,7.44,7.792  c-0.224,3.968-3.824,7.008-7.792,7.008h-30.72c-3.968,0.08-7.152,3.328-7.152,7.36c0,4.048,3.328,7.36,7.36,7.36h37.792  c3.472,0,6.288,2.816,6.208,6.208v0.576c0.224,3.328-2.448,6.208-5.84,6.208h-52.304c-4.048,0-7.584,2.96-7.872,6.928  c-0.144,4.256,3.168,7.792,7.44,7.792h49.92c3.392,0,6.208,2.816,6.208,6.208l-0.08,0.432v0.064c0.08,3.616-2.88,6.576-6.496,6.576  h-36.432c-4.048,0-7.584,2.96-7.872,7.008c-0.144,4.256,3.184,7.792,7.44,7.792h38.224c3.616,0,6.208,2.96,6.208,6.576  c0,0,0,0,0,0.08s0,0.08,0,0.08c0,3.536-2.592,6.56-6.208,6.56h-31.952c-3.968,0-7.504,2.96-7.728,6.928  c-0.224,4.336,3.168,7.792,7.36,7.792h0.816c34.128-1.376,61.504-29.504,61.504-63.968c0-35.344-28.752-64.096-64.096-64.096  c-1.472,0-2.912,0.128-4.352,0.224C248.16,286.288,245.264,288.912,245.072,292.464z"/>
<path style="fill:#FFFFFF;" d="M244.368,377.472L244.368,377.472c-2.24,0-4.4-0.896-5.984-2.48l-15.504-15.504  c-3.296-3.296-3.296-8.656,0-11.952s8.656-3.296,11.952,0l9.52,9.52l32.8-32.8c3.296-3.296,8.656-3.296,11.952,0  s3.296,8.656,0,11.952l-38.784,38.784C248.752,376.576,246.608,377.472,244.368,377.472z"/>
</svg>
        
        </div><span class="number"><?=($userId)?PlLibHelperEvents::getInstance()->getCertificates($userId):0;?></span> <span class="text">сертификатов</span>
          <div class="about">Сертификат дает право выбрать в витрине прикольный товар и обменять его на выигранные бонусы. А каждая покупка товара, дает шанс выиграть денежный приз от 100 до 10 000$</div></a><br/>
          <a class="sertificates">
          <span style="padding-left:30px;" class="number"><?=($userId)?PlLibHelperEvents::getInstance()->getCertificates($userId,1):0;?></span> <span class="text">участвуют в розыгрыше</span>
          <div class="about">Покупка любого товара активирует сертификат и он попадает в барабан при розыгрышах призов</div></a>
          <a class="active-sert" target="_blank" href="/383">Активировать сертификат</a>
      <div class="bonuses">
        <div class="icon-wrapper">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" xml:space="preserve">
<polygon style="fill:#CC2E48;" points="29,55 0,19 58,19 "/>
<polygon style="fill:#FC3952;" points="58,19 0,19 10,3 48,3 "/>
<polygon style="fill:#F76363;" points="42.154,19 48,3 10,3 15.846,19 "/>
<polygon style="fill:#F49A9A;" points="42,19 29,3 16,19 "/>
<polygon style="fill:#CB465F;" points="15.846,19 29,55 42.154,19 "/>
</svg>
        </div>
        <span class="number"><?=(!empty($account->value)?floatval($account->value):0);?></span> <span class="text">бонусов</span>
            <div class="about">Бонус равен эквиваленту USD$ На эту сумму может быть уменьшена покупка товара.
Покупка активирует сертифкат на розыгрыш 10 000$<br/><a href="/383" target="_blank" style="color:#E95D4B;text-decoration:underline;">Потратить свои бонусы</a>
            </div>
            </div>
               <a class="bonuses" style="padding:0px 0px 0px 0px !important;">
            
        <div class="icon-wrapper">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m31 175.859375v206.351563l45-45 45 45v-153.484376zm0 0" fill="#ff4d4d"/><path d="m391 224.929688v157.28125l45-45 45 45v-187.71875zm0 0" fill="#ff001e"/><path d="m391 241v-30c49.628906 0 90-40.371094 90-90v-60h-75v-30h106v90c0 66.167969-54.832031 120-121 120zm0 0" fill="#ff9f00"/><path d="m112.824219 236.09375c-65.84375-17.285156-112.824219-76.949219-112.824219-145.09375v-60h106v30h-75v30c0 54.519531 36.78125 102.261719 89.441406 116.089844zm0 0" fill="#ffd400"/><path d="m226 301h60v181h-60zm0 0" fill="#ffd400"/><path d="m256 301h30v181h-30zm0 0" fill="#ff9f00"/><path d="m91 0v166c0 90.902344 74.097656 165 165 165s165-74.097656 165-165v-166zm0 0" fill="#ffe470"/><path d="m421 0v166c0 90.902344-74.097656 165-165 165v-331zm0 0" fill="#fdbf00"/><path d="m348.101562 124-63.601562-9.300781-28.5-57.597657-28.5 57.597657-63.601562 9.300781 46.199218 45-11.097656 63.300781 57-30 57 30-11.097656-63.300781zm0 0" fill="#fdbf00"/><path d="m301.902344 169 11.097656 63.300781-57-30v-145.199219l28.5 57.597657 63.601562 9.300781zm0 0" fill="#ff9f00"/><path d="m361 482v30h-210v-30c0-16.5 13.5-30 30-30h150c16.5 0 30 13.5 30 30zm0 0" fill="#ffe470"/><path d="m361 482v30h-105v-60h75c16.5 0 30 13.5 30 30zm0 0" fill="#fdbf00"/></svg>
        </div>
        <span class="number"><?=$newEvents;?></span> <span class="text">новых испытаний</span>
          <div class="about">При успешном прохождении испытаний, открываются новые.</div>
          </a>
          
          <a class="bonuses" style="padding:0px 0px 0px 0px !important;">
            
        <div class="icon-wrapper">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m31 175.859375v206.351563l45-45 45 45v-153.484376zm0 0" fill="#ff4d4d"/><path d="m391 224.929688v157.28125l45-45 45 45v-187.71875zm0 0" fill="#ff001e"/><path d="m391 241v-30c49.628906 0 90-40.371094 90-90v-60h-75v-30h106v90c0 66.167969-54.832031 120-121 120zm0 0" fill="#ff9f00"/><path d="m112.824219 236.09375c-65.84375-17.285156-112.824219-76.949219-112.824219-145.09375v-60h106v30h-75v30c0 54.519531 36.78125 102.261719 89.441406 116.089844zm0 0" fill="#ffd400"/><path d="m226 301h60v181h-60zm0 0" fill="#ffd400"/><path d="m256 301h30v181h-30zm0 0" fill="#ff9f00"/><path d="m91 0v166c0 90.902344 74.097656 165 165 165s165-74.097656 165-165v-166zm0 0" fill="#ffe470"/><path d="m421 0v166c0 90.902344-74.097656 165-165 165v-331zm0 0" fill="#fdbf00"/><path d="m348.101562 124-63.601562-9.300781-28.5-57.597657-28.5 57.597657-63.601562 9.300781 46.199218 45-11.097656 63.300781 57-30 57 30-11.097656-63.300781zm0 0" fill="#fdbf00"/><path d="m301.902344 169 11.097656 63.300781-57-30v-145.199219l28.5 57.597657 63.601562 9.300781zm0 0" fill="#ff9f00"/><path d="m361 482v30h-210v-30c0-16.5 13.5-30 30-30h150c16.5 0 30 13.5 30 30zm0 0" fill="#ffe470"/><path d="m361 482v30h-105v-60h75c16.5 0 30 13.5 30 30zm0 0" fill="#fdbf00"/></svg>
        </div>
        <span class="number">0</span> <span class="text">придуманных вами</span>
          </a>
          
          <a class="bonuses" style="padding:0px 0px 0px 0px !important;">
            
        <div class="icon-wrapper">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m31 175.859375v206.351563l45-45 45 45v-153.484376zm0 0" fill="#ff4d4d"/><path d="m391 224.929688v157.28125l45-45 45 45v-187.71875zm0 0" fill="#ff001e"/><path d="m391 241v-30c49.628906 0 90-40.371094 90-90v-60h-75v-30h106v90c0 66.167969-54.832031 120-121 120zm0 0" fill="#ff9f00"/><path d="m112.824219 236.09375c-65.84375-17.285156-112.824219-76.949219-112.824219-145.09375v-60h106v30h-75v30c0 54.519531 36.78125 102.261719 89.441406 116.089844zm0 0" fill="#ffd400"/><path d="m226 301h60v181h-60zm0 0" fill="#ffd400"/><path d="m256 301h30v181h-30zm0 0" fill="#ff9f00"/><path d="m91 0v166c0 90.902344 74.097656 165 165 165s165-74.097656 165-165v-166zm0 0" fill="#ffe470"/><path d="m421 0v166c0 90.902344-74.097656 165-165 165v-331zm0 0" fill="#fdbf00"/><path d="m348.101562 124-63.601562-9.300781-28.5-57.597657-28.5 57.597657-63.601562 9.300781 46.199218 45-11.097656 63.300781 57-30 57 30-11.097656-63.300781zm0 0" fill="#fdbf00"/><path d="m301.902344 169 11.097656 63.300781-57-30v-145.199219l28.5 57.597657 63.601562 9.300781zm0 0" fill="#ff9f00"/><path d="m361 482v30h-210v-30c0-16.5 13.5-30 30-30h150c16.5 0 30 13.5 30 30zm0 0" fill="#ffe470"/><path d="m361 482v30h-105v-60h75c16.5 0 30 13.5 30 30zm0 0" fill="#fdbf00"/></svg>
        </div>
        <span class="number">0$</span> <span class="text">ваш доход за творчество</span>
          </a>
          
        </div>
      </div>
      
    </div>
                          

        </div>

                       
                      
    </div>
</div>

          
          <script>
var coll = document.getElementsByClassName("title-block");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  });
}
</script>
                
                      
                      






