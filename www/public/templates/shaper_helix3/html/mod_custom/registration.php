<?php defined('_JEXEC') or die; ?>



<div class="popup_registration page not-in-popup">


    <!-- Modal content-->
    <div class="modal-content">
        <form method="post" id="mod_custom_registration_1" action="/index.php?option=com_ajax&plugin=ajaxlogin&format=json">
            <div class="inputs">
                <div>
                    <input type="text" name="name" required placeholder="Ваше имя">
                </div>
                <div>
                    <input type="text" id="phone_mask" name="phone" required placeholder="Введите телефон для уведомления о призах">
                </div>
                <div>
                    <input data-validation="email" type="email" name="email" required placeholder="Электронная почта">
                </div>
                <div>
                    <input  type="password" name="password1" required placeholder="Пароль">
                </div>
                <div>
                    <input data-validation="confirmation" data-validation-confirm="password1" data-validation-error-msg="Пароли не совпадают" type="password" name="password2" required placeholder="Повторите пароль">
                </div>
                <input type="hidden" name="action" value="reg">
                <?php
                $uri = JFactory::getURI();
                $url = base64_encode($uri->toString());
                ?>
                <input type="hidden" value="<?=$url;?>" name="return">
                <button type="submit" class="btn btn-form btn-submit">Зарегистрироваться</button>
            </div>
            <div class="error-popup-enter" style="display: none;"></div>
            <?php echo JHtml::_('form.token'); ?>
            <?php echo JHtml::_('form.token'); ?>
            <?php
            JPluginHelper::importPlugin('captcha', 'recaptcha_invisible_cf');
            $dispathcer = JDispatcher::getInstance();
            $dispathcer->trigger('onInitCf');
            $onDisplayCf = $dispathcer->trigger('onDisplayCf');
            echo  $onDisplayCf[0];
            ?>
        </form>
        <div class="enter_in">Уже зарегистрированы? <button class="btn btn-link" data-toggle="modal" data-target="#auth-popup" data-dismiss="modal">Войти на сайт</button></div>
    </div>
</div>

<script>
    jQuery('#mod_custom_registration_1').on('submit', function (e) {
        e.preventDefault();
        gtag('event', 'click', {
            'event_category': 'click',
            'event_action': 'submit'
        });
        jQuery.ajax({
            method: "POST",
            url: "/forms/process.php",
            dataType: 'json',
            data: jQuery(this).serialize()
        });
        jQuery.ajax({
            method: "POST",
            url: "/index.php?option=com_ajax&plugin=ajaxlogin&format=json",
            dataType: 'json',
            data: jQuery(this).serialize(),
            success: function (data) {
                if (data.data[0].error) {
                    jQuery('#mod_custom_registration_1 .error-popup-enter').html(data.data[0].error).show();
                }else{
                    window.location.href = '/personal';
                }
            }
        }
        );

    });
</script>

  <style>
  .popup_registration.not-in-popup{padding: 0px !important;margin:0 !important;background: none !important;}
  .popup_registration.not-in-popup .btn-form{background:#f1223f !important;}
  .form-error{font-size: 18px; color: #ff0303}
  .popup_registration .inputs{
      margin-bottom: 8px;
  }
  .popup_registration .inputs > div{
      margin-bottom: 20px;
  }
  </style>
<script>
    jQuery(document).ready(function () {
        jQuery.validate({
            modules : 'security',
            form: '#mod_custom_registration_1',
            lang: 'ru'
        });
        jQuery('#phone_mask').mask('+38(099) 999 99 99');

    });

</script>