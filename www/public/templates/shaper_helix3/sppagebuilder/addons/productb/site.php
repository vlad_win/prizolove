<?php
/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2016 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
 */
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

JLoader::import('pl_lib.library');

class SppagebuilderAddonProductb extends SppagebuilderAddons{

    public function render() {
        $session = JFactory::getSession();
        $cart = json_decode($session->get('plCart', json_encode(array())), true);

        $class = (isset($this->addon->settings->class) && $this->addon->settings->class) ? $this->addon->settings->class : '';
        $title = (isset($this->addon->settings->title) && $this->addon->settings->title) ? $this->addon->settings->title : '';

        $product = (isset($this->addon->settings->product) && $this->addon->settings->product) ? $this->addon->settings->product : '';


        $description = (isset($this->addon->settings->description) && $this->addon->settings->description) ? $this->addon->settings->description : '';

	    $currency = (isset($this->addon->settings->currency) && $this->addon->settings->currency) ? $this->addon->settings->currency : '';
        $discount = (isset($this->addon->settings->discount) && $this->addon->settings->discount) ? $this->addon->settings->discount : '';
        $price = (isset($this->addon->settings->price) && $this->addon->settings->price) ? $this->addon->settings->price : '';
        $best_choice = (isset($this->addon->settings->best_choice) && $this->addon->settings->best_choice) ? $this->addon->settings->best_choice : '';

        $price_block = (isset($this->addon->settings->price_block) && $this->addon->settings->price_block) ? $this->addon->settings->price_block : '';

        if ($product) {
            $db = JFactory::getDbo();
            $query = "SELECT `*` FROM #__prizolove_products WHERE `id` = ".intval($product);

            $db->setQuery($query);

            $product = $db->loadObject();
        }

        if (empty($price) && !empty($product->price_usd)) {
            $price = number_format(PlLibHelperRates::convertUSD($product->price_usd), 0, '.','');
        } else if (empty($price) && !empty($product->price)) {
            $price = $product->price;
        }


        $currencyLabel = 'грн';
        
        if ($currency
            && file_exists(JPATH_ROOT.'/currencies.txt')
            && file_exists(JPATH_ROOT.'/currencies_labels.txt')
            && function_exists('geoip_country_code_by_name')
        ) {
            $countryCode = strtolower(geoip_country_code_by_name($_SERVER['REMOTE_ADDR']));
            $currencyCountries = json_decode(file_get_contents((JPATH_ROOT.'/currencies_countries.txt')), true);

            if (!empty($currencyCountries[$countryCode])) {
                $currencyCode = $currencyCountries[$countryCode];
                $currencyRates = json_decode(file_get_contents((JPATH_ROOT . '/currencies.txt')), true);
                $currencyLabels = json_decode(file_get_contents((JPATH_ROOT . '/currencies_labels.txt')), true);
                if (!empty($currencyLabels[$currencyCode])) {
                    $currencyLabel = $currencyLabels[$currencyCode];
                    $price = number_format($price / $currencyRates[$currencyCode], '0', ".", " ");
                }
            }

        }

        if ($price_block) {
            $regex = '/{price}/i';

            preg_match($regex, $price_block, $match);

            if ($match) {
                $price_block = preg_replace("|$match[0]|", $price, $price_block, 1);
            }
        }

        if ($discount && $price_block) {
            $discountMultiplier = (floatval($discount) / 100);
            $priceWithoutDiscount = ceil((floatval($price/(1 - $discountMultiplier))/10)*10);

            $regex = '/{discount}/i';

            preg_match($regex, $price_block, $match);

            if ($match) {
                $price_block = preg_replace("|$match[0]|", $discount, $price_block, 1);
            }

            $regex = '/{price_without_discount}/i';

            preg_match($regex, $price_block, $match);

            if ($match) {
                $price_block = preg_replace("|$match[0]|", $priceWithoutDiscount, $price_block, 1);
            }
        }



        $output1 = '<div class="paket">
                        <div class="topper '.(($best_choice)?'best-topper':'').'">'.$title.'</div>
                        <div class="inner">
                        <div class="vklad">'.$price_block.' '.$currencyLabel.'</div><div class="summa">Цена пакета</div>
                        '.$description.'
                        <div class="btn-wrapper"><a href="/orderpage?pid='.$product->id.'" class="btn-buy">Заказать "'.$title.'"</a></div>                        
                        </div>
                        </div>';

        return $output1;
    }

    public function scripts() {

    }

    public function stylesheets() {

    }

    public function css() {

        $css = '';
        return $css;

    }
    public static function getTemplate() {

        $output = '';
        return $output;
    }

}
