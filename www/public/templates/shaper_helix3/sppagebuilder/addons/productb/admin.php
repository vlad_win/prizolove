<?php
/**
* @package SP Page Builder
* @author JoomShaper http://www.joomshaper.com
* @copyright Copyright (c) 2010 - 2016 JoomShaper
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

$db = JFactory::getDbo();
$query = "SELECT `*` FROM #__prizolove_products";

$db->setQuery($query);

$products = $db->loadObjectList();
$arProducts = array();
foreach($products as $product) {
    $arProducts[$product->id] = $product->name;
}

SpAddonsConfig::addonConfig(
	array(
		'type'=>'content',
		'addon_name'=>'sp_productb',
		'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCTB'),
		'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCTB_DESC'),
		'category'=>'Media',
		'attr'=>array(
			'general' => array(
				'admin_label'=>array(
					'type'=>'text',
					'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_ADMIN_LABEL'),
					'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_ADMIN_LABEL_DESC'),
					'std'=> ''
				),

				'title'=>array(
					'type'=>'text',
					'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_TITLE'),
					'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_TITLE_DESC'),
					'std'=>  ''
				),

                'product'=>array(
                    'type'=>'select',
                    'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT'),
                    'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_DESC'),
                    'values'=>$arProducts,
                ),

                'description'=>array(
                    'type'=>'editor',
                    'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_DESCRIPTION'),
                    'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_DESCRIPTION_DESC'),
                    'std'=>'',
                ),

                'currency'=>array(
                    'type'=>'checkbox',
                    'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_CURRENCY'),
                    'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_CURRENCY'),
                    'std'=>0,
                ),

                'best_choice'=>array(
                    'type'=>'checkbox',
                    'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_BEST_CHOICE'),
                    'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_BEST_CHOICE'),
                    'std'=>0,
                ),

                'discount'=>array(
                    'type'=>'text',
                    'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_DISCOUNT'),
                    'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_DISCOUNT_DESC'),
                    'std'=>''
                ),

                'price'=>array(
                    'type'=>'text',
                    'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_PRICE'),
                    'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_PRICE_DESC'),
                    'std'=>''
                ),

                'price_block'=>array(
                    'type'=>'editor',
                    'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_PRICE_BLOCK'),
                    'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_PRODUCT_PRICE_BLOCK_DESC'),
                    'std'=>'{price},{discount}'
                ),

				'class'=>array(
					'type'=>'text',
					'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_CLASS'),
					'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_CLASS_DESC'),
					'std'=>''
				),

			),
		),
	)
);
