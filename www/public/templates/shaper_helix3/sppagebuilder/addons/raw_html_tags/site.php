<?php
/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2018 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

JLoader::import('pl_lib.library');

class SppagebuilderAddonRaw_html_tags extends SppagebuilderAddons{

	public function render() {

		$class = (isset($this->addon->settings->class) && $this->addon->settings->class) ? $this->addon->settings->class : '';
		$title = (isset($this->addon->settings->title) && $this->addon->settings->title) ? $this->addon->settings->title : '';
		$heading_selector = (isset($this->addon->settings->heading_selector) && $this->addon->settings->heading_selector) ? $this->addon->settings->heading_selector : 'h3';

		//Options
		$html = (isset($this->addon->settings->html) && $this->addon->settings->html) ? $this->addon->settings->html : '';

        $regex = '/{bonus}/i';

        preg_match($regex, $html, $match);

        if ($match) {
			$session = JFactory::getSession();
			if ($_SERVER['REMOTE_ADDR'] == '92.249.126.51')  {
				$app = JFactory::getApplication();
				echo $app->get('event_bonus');
			}
			$bonus = $session->get('event_bonus',0);
			$html = preg_replace("|$match[0]|", $bonus , $html);
		}

        $regex = '/{referal0}/i';

        preg_match($regex, $html, $match);
	$parentLink = '';

        if ($match) {
            $parentId = PlLibHelperAffiliate::getParent();
            if ($parentId) {
                $parentLink = '?atid='.$parentId;                
            }
	    $html = preg_replace("|$match[0]|", $parentLink, $html);
        }

        $regex = '/{referal1}/i';

        preg_match($regex, $html, $match);

        if ($match) {
            $parentId = PlLibHelperAffiliate::getParent();
            if ($parentId) {
                $parentLink = '&atid='.$parentId;                
            }
            $html = preg_replace("|$match[0]|", $parentLink, $html);
        }


		//Output
		if($html) {
			$output  = '<div class="sppb-addon tst sppb-addon-raw-html ' . $class . '">';
			$output .= ($title) ? '<'.$heading_selector.' class="sppb-addon-title">' . $title . '</'.$heading_selector.'>' : '';
			$output .= '<div class="sppb-addon-content">';
			$output .= $html;
			$output .= '</div>';
			$output .= '</div>';

			return $output;
		}

		return;
	}

	public static function getTemplate() {

		$output = '
			<div class="sppb-addon sppb-addon-raw-html {{ data.class }}">
				<# if( !_.isEmpty( data.title ) ){ #><{{ data.heading_selector }} class="sppb-addon-title sp-inline-editable-element" data-id={{data.id}} data-fieldName="title" contenteditable="true">{{{ data.title }}}</{{ data.heading_selector }}><# } #>
				<div class="sppb-addon-content sp-inline-editable-element" data-id={{data.id}} data-fieldName="html" contenteditable="true">
					{{{ data.html }}}
				</div>
			</div>
		';

		return $output;
	}

}
