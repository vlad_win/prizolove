<?php
/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2016 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
 */
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

class SppagebuilderAddonCart extends SppagebuilderAddons{

    private function plural_form($number) {
        $after = array('товар','товара','товаров');
        $cases = array(2,0,1,1,1,2);
        return $number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
    }

    public function render() {
        $app		= JFactory::getApplication();
        $user		= JFactory::getUser();
        $groups		= implode(',', $user->getAuthorisedViewLevels());
        $lang 		= JFactory::getLanguage()->getTag();
        $clientId 	= (int) $app->getClientId();

        $session = JFactory::getSession();
        $cart = json_decode($session->get('plCart', json_encode(array())), true);

        $db	= JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('m.id, m.title, m.module, m.position, m.ordering, m.content, m.showtitle, m.params');
        $query->from('#__modules AS m');
        $query->where('m.published = 1');

        $query->where('m.id = 456');

        $date = JFactory::getDate();

        $now = $date->toSql();

        $nullDate = $db->getNullDate();

        $query->where('(m.publish_up = '.$db->Quote($nullDate).' OR m.publish_up <= '.$db->Quote($now).')');
        $query->where('(m.publish_down = '.$db->Quote($nullDate).' OR m.publish_down >= '.$db->Quote($now).')');

        $query->where('m.access IN ('.$groups.')');
        $query->where('m.client_id = '. $clientId);

        // Filter by language
        if ($app->isSite() && $app->getLanguageFilter()) {
            $query->where('m.language IN (' . $db->Quote($lang) . ',' . $db->Quote('*') . ')');
        }

        // Set the query
        $db->setQuery($query);
        $module = $db->loadObject();


        $class = (isset($this->addon->settings->class) && $this->addon->settings->class) ? $this->addon->settings->class : '';
        $title = (isset($this->addon->settings->title) && $this->addon->settings->title) ? $this->addon->settings->title : '';
        $heading_selector = (isset($this->addon->settings->heading_selector) && $this->addon->settings->heading_selector) ? $this->addon->settings->heading_selector : 'h3';

        //Options





		$output = '<div id="popup-cart">';
        $output .= '<h3>Корзина</h3>';
        $output .= '<table class="table cart">';
        $cost = 0;
        $num = 0;

        if (!empty($cart['items'])) {
            $output .= '<tr>';
            $output .= '<th>Фото</th>';
            $output .= '<th>Название</th>';
            $output .= '<th>Количество</th>';
            $output .= '<th>Цена</th>';
            $output .= '<th>Итого</th>';
            $output .= '<th>Удалить</th>';
            $output .= '</tr>';
            foreach($cart['items'] as $id=>$item) {
                $num += $item['quantity'];
                $cost += $item['quantity'] * $item['price'];
                $output .= '<tr class="cart-row" data-id="'.$id.'">';
                $output .= '<td><img src="'.$item['image'].'" alt="'.$item['name'].'" ></td>';
                $output .= '<td class="cart__product__title"><span>'.$item['name'].'</span></td>';
                $output .= '<td><a href="#" class="cart__plus" data-act="plus">+</a><input class="cart__count" value="'.$item['quantity'].'" maxlength="3"><a href="#" class="cart__minus" data-act="minus">-</a></div></td>';
                $output .= '<td class="cart__col__price">'.$item['price'].'</td>';
                $output .= '<td class="cart__col__quantity">'.$item['quantity'] * $item['price'].'</td>';
                $output .= '<td><a data-act="delete" class="cart__delete" href="javascript:void(0)"><img src="/images/cart1.png" alt=""></a></td>';
                $output .= '</tr>';
            }
            $output .= '<tr>';
            $output .= '<td>В корзине:</td>';
            $output .= '<td colspan="5" class="cart__total-num">'.$this->plural_form($num).'</td>';
            $output .= '</tr>';
            $output .= '<tr>';
            $output .= '<td colspan="5">Итого к оплате</td>';
            $output .= '<td class="cart__total-cost">'.$cost.'</td>';
            $output .= '</tr>';

        }
        $output .= '</table>';
        $output .= '<div class="cart__buttons'.((!$num)?' hidden':'').'">';
        $output .= '<a id="continue-ordering" class="sppb-btn  sppb-btn-custom sppb-btn-round">Продолжить покупки</a>';
        $output .= '<a href="#form322" id="process-order" class="sppb-btn button-order sppb-btn-custom sppb-btn-round">Оформить заказ</a>';
        $output .= '</div>';
        $output .= '<div class="cart__order-form">';
        $output .= JModuleHelper::renderModule($module, array('style' => 'none'));
        $output .= '</div>';
        $output .= '</div>';







        return $output;
    }




    public static function getTemplate() {
        $output = '
		<#
			var image_overlay = 0;
			if(!_.isEmpty(data.overlay_color)){
				image_overlay = 1;
			}
			var open_lightbox = parseInt(data.open_lightbox);
			var title_font_style = data.title_fontstyle || "";

			var alt_text = data.alt_text;

			if(_.isEmpty(alt_text)){
				if(!_.isEmpty(data.title)){
					alt_text = data.title;
				}
			}
		#>
		<style type="text/css">
			<# if(open_lightbox && data.overlay_color){ #>
				#sppb-addon-{{ data.id }} .sppb-addon-image-overlay{
					background-color: {{ data.overlay_color }};
				}
			<# } #>

			#sppb-addon-{{ data.id }} img{
				border-radius: {{ data.border_radius }}px;
			}
		</style>
		<# if(data.image){ #>
			<div class="sppb-addon sppb-addon-single-image {{ data.position }} {{ data.class }}">
				<# if( !_.isEmpty( data.title ) && data.title_position != "bottom" ){ #><{{ data.heading_selector }} class="sppb-addon-title">{{ data.title }}</{{ data.heading_selector }}><# } #>
				<div class="sppb-addon-content">
					<div class="sppb-addon-single-image-container">
						<# if(image_overlay && open_lightbox) { #>
							<div class="sppb-addon-image-overlay"></div>
						<# } #>
						<# if(open_lightbox) { #>
							<a class="sppb-magnific-popup sppb-addon-image-overlay-icon" data-popup_type="image" data-mainclass="mfp-no-margins mfp-with-zoom" href=\'{{ data.image }}\'>+</a>
						<# } #>
			
						<# if(!open_lightbox) { #>
							<a target="{{ data.target }}" href=\'{{ data.link }}\'>
						<# } #>

						<# if(data.image.indexOf("http://") == -1 && data.image.indexOf("https://") == -1){ #>
							<img class="sppb-img-responsive" src=\'{{ pagebuilder_base + data.image }}\' alt="{{ alt_text }}" title="{{ data.title }}">
						<# } else { #>
							<img class="sppb-img-responsive" src=\'{{ data.image }}\' alt="{{ alt_text }}" title="{{ data.title }}">
						<# } #>

						<# if(!open_lightbox) { #>
							</a>
						<# } #>

					</div>
				</div>
				<# if( !_.isEmpty( data.title ) && data.title_position == "bottom" ){ #><{{ data.heading_selector }} class="sppb-addon-title">{{ data.title }}</{{ data.heading_selector }}><# } #>
			</div>
		<# } #>
		';

        return $output;
    }

}
