<?php
/**
 * @package Helix3 Framework
 * Template Name - Shaper Helix3
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2017 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
 */
//no direct accees
defined('_JEXEC') or die('restricted access');

$user = JFactory::getUser();
JPluginHelper::importPlugin('captcha', 'recaptcha_invisible_cf');
$dispathcer = JDispatcher::getInstance();
$dispathcer->trigger('onInitCf');



$countries = array('ua', 'ru', 'us', 'kz');
$countryCode = 'ua';
if (function_exists('geoip_country_code_by_name')) {
       	$countryCode = strtolower(geoip_country_code_by_name($_SERVER['REMOTE_ADDR']));
}
if (!in_array($countryCode, $countries)) $countryCode = 'ua';

$currentSession = JFactory::getSession();
$fbMissedEmail = $currentSession->get('fbMissedEmail', false);

if ($fbMissedEmail) $currentSession->clear('fbMissedEmail');

$plVisitor = file_get_contents('forms/counter');

$doc = JFactory::getDocument();
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();

$lang = $app->getLanguage();
if (is_object($menu)) {
	$lang_code = $menu->language;

	if ($lang_code == 'en-GB') {
		$newLang = JLanguage::getInstance($lang_code);
		$app->loadLanguage($newLang);
		$app->set('menuLanguage', $app->getLanguage());
		$lang = $app->get('menuLanguage');
	}
}

$inputCookie  = $app->input->cookie;



$jinput = $app->input;

$option = $jinput->get('option', false);

$confirmationScript = false;
$pageId = 'custom';

$gaEventsIds = array(79, 210, 51);

if ($option == 'com_sppagebuilder') {
    $pageId = $jinput->get('id', false, 'INT');
    if ($pageId == 173) {
        $confirmationScript = true;
    }
}

$pageCookies = array(272,273);
if (in_array($pageId, $pageCookies)) {
    $inputCookie->set($name = '_bns', $value = base64_encode('Бонус: 100 грн'));
}

if ($pageId == 275) {
    $inputCookie->set($name = '_bns', $value = base64_encode('Бонус: 2 сертификата в заказе'));
}

if ($pageId == 290) {
	$valueBase64 = base64_encode('Бонус: +2 сертификата');
    $inputCookie->set($name = '_bns', $value = $valueBase64);
}


JHtml::_('jquery.framework');
JHtml::_('bootstrap.framework'); //Force load Bootstrap
unset($doc->_scripts[$this->baseurl . '/media/jui/js/bootstrap.min.js']); // Remove joomla core bootstrap
//Load Helix
$helix3_path = JPATH_PLUGINS . '/system/helix3/core/helix3.php';

if (file_exists($helix3_path)) {
    require_once($helix3_path);
    $this->helix3 = helix3::getInstance();
} else {
    die('Please install and activate helix plugin');
}

//Coming Soon
if ($this->helix3->getParam('comingsoon_mode'))
    header("Location: " . $this->baseUrl . "?tmpl=comingsoon");

//Class Classes
$body_classes = '';
if ($this->helix3->getParam('sticky_header')) {
    $body_classes .= ' sticky-header';
}

$body_classes .= ($this->helix3->getParam('boxed_layout', 0)) ? ' layout-boxed' : ' layout-fluid';

if (isset($menu) && $menu) {
    if ($menu->params->get('pageclass_sfx')) {
        $body_classes .= ' ' . $menu->params->get('pageclass_sfx');
    }
}

//Body Background Image
if ($bg_image = $this->helix3->getParam('body_bg_image')) {

    $body_style = 'background-image: url(' . JURI::base(true) . '/' . $bg_image . ');';
    $body_style .= 'background-repeat: ' . $this->helix3->getParam('body_bg_repeat') . ';';
    $body_style .= 'background-size: ' . $this->helix3->getParam('body_bg_size') . ';';
    $body_style .= 'background-attachment: ' . $this->helix3->getParam('body_bg_attachment') . ';';
    $body_style .= 'background-position: ' . $this->helix3->getParam('body_bg_position') . ';';
    $body_style = 'body.site {' . $body_style . '}';

    $doc->addStyledeclaration($body_style);
}

//Body Font
$webfonts = array();

if ($this->params->get('enable_body_font')) {
    $webfonts['body'] = $this->params->get('body_font');
}

//Heading1 Font
if ($this->params->get('enable_h1_font')) {
    $webfonts['h1'] = $this->params->get('h1_font');
}

//Heading2 Font
if ($this->params->get('enable_h2_font')) {
    $webfonts['h2'] = $this->params->get('h2_font');
}

//Heading3 Font
if ($this->params->get('enable_h3_font')) {
    $webfonts['h3'] = $this->params->get('h3_font');
}

//Heading4 Font
if ($this->params->get('enable_h4_font')) {
    $webfonts['h4'] = $this->params->get('h4_font');
}

//Heading5 Font
if ($this->params->get('enable_h5_font')) {
    $webfonts['h5'] = $this->params->get('h5_font');
}

//Heading6 Font
if ($this->params->get('enable_h6_font')) {
    $webfonts['h6'] = $this->params->get('h6_font');
}

//Navigation Font
if ($this->params->get('enable_navigation_font')) {
    $webfonts['.sp-megamenu-parent'] = $this->params->get('navigation_font');
}

//Custom Font
if ($this->params->get('enable_custom_font') && $this->params->get('custom_font_selectors')) {
    $webfonts[$this->params->get('custom_font_selectors')] = $this->params->get('custom_font');
}

$this->helix3->addGoogleFont($webfonts);

//Custom CSS
if ($custom_css = $this->helix3->getParam('custom_css')) {
    $doc->addStyledeclaration($custom_css);
}

//Custom JS
if ($custom_js = $this->helix3->getParam('custom_js')) {
    $doc->addScriptdeclaration($custom_js);
}

//preloader & goto top
$doc->addScriptdeclaration("\nvar sp_preloader = '" . $this->params->get('preloader') . "';\n");
$doc->addScriptdeclaration("\nvar sp_gotop = '" . $this->params->get('goto_top') . "';\n");
$doc->addScriptdeclaration("\nvar sp_offanimation = '" . $this->params->get('offcanvas_animation') . "';\n");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>"
      lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <meta name="interkassa-verification" content="37a11472a60ca58137f98686898bc68d" />
    <meta name="interkassa-verification" content="c75df4970a5db1265de118e059958901" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>
        var plCounter = 1;
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112941077-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push({
                userId: <?php echo @$user->id;?>,
                event: 'authentication'
            });
        }
        gtag('js', new Date());

        gtag('config', 'UA-112941077-1');
    </script>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KBP7JRW');</script>
    <script>
        <?php if ($pageId == 79) :?>
        ga('require', 'GTM-M2PQLPZ');
        <?php endif; ?>
    </script>
    <!-- End Google Tag Manager -->
    <?php
    if ($favicon = $this->helix3->getParam('favicon')) {
        $doc->addFavicon(JURI::base(true) . '/' . $favicon);
    } else {
        $doc->addFavicon($this->helix3->getTemplateUri() . '/images/favicon.ico');
    }
    ?>
    <!-- head -->
    <jdoc:include type="head"/>
    <?php
    $megabgcolor = ($this->helix3->PresetParam('_megabg')) ? $this->helix3->PresetParam('_megabg') : '#ffffff';
    $megabgtx = ($this->helix3->PresetParam('_megatx')) ? $this->helix3->PresetParam('_megatx') : '#333333';

    $preloader_bg = ($this->helix3->getParam('preloader_bg')) ? $this->helix3->getParam('preloader_bg') : '#f5f5f5';
    $preloader_tx = ($this->helix3->getParam('preloader_tx')) ? $this->helix3->getParam('preloader_tx') : '#f5f5f5';

    // load css, less and js
    $this->helix3->addCSS('bootstrap4.min.css, font-awesome.min.css, animate.min.css, custom.css')// CSS Files
    ->addJS('popper.min.js, bootstrap4.min.js, jquery.sticky.js, main.js, jquery.maskedinput.min.js, notify.min.js, analytics.js')// JS Files
    ->lessInit()->setLessVariables(array(
        'preset' => $this->helix3->Preset(),
        'bg_color' => $this->helix3->PresetParam('_bg'),
        'text_color' => $this->helix3->PresetParam('_text'),
        'major_color' => $this->helix3->PresetParam('_major'),
        'megabg_color' => $megabgcolor,
        'megatx_color' => $megabgtx,
        'preloader_bg' => $preloader_bg,
        'preloader_tx' => $preloader_tx,
    ))
//        ->addLess('legacy/bootstrap', 'legacy')
        ->addLess('master', 'template');

    //RTL
    if ($this->direction == 'rtl') {
        $this->helix3->addCSS('bootstrap-rtl.min.css')
            ->addLess('rtl', 'rtl');
    }

    $this->helix3->addLess('presets', 'presets/' . $this->helix3->Preset(), array('class' => 'preset'));

    //Before Head
    if ($before_head = $this->helix3->getParam('before_head')) {
        echo $before_head . "\n";
    }
    ?>
    <?php if ($pageId == 350) : ?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '2066163586738295');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=2066163586738295&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
    <?php endif; ?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

</head>

<body class="<?php echo $this->helix3->bodyClass($body_classes); ?> off-canvas-menu-init">
    <?php if ($pageId == 350) : ?>
<script>
	jQuery(document).ready(function() {
	jQuery('a.btn-buy').on('click', function() {
		fbq('track', 'Lead', {
		value: 2,
		currency: '$',
		});
	});
	});
</script>
<!-- End Facebook Pixel Code -->
    <?php endif; ?>
<!-- <div class="overlay"></div> -->
<div id="auth-popup" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup_enter">

            <!-- Modal content-->
            <div class="modal-content">
                <a href="#" class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                <div class="title">Вход в Prizolove</div>
                <form method="post">
                <div class="inputs">
                    <label for="email">Электронная почта</label><br>
                    <input class="validate-username required" type="text" name="username" required>
                    <label  for="password">Пароль</label><br>
                    <input class="validate-password required" type="password" name="password" required>                    
                    <button type="submit" class="btn btn-form btn-submit">Войти</button>
                </div>
                <div class="error-popup-enter d-none"></div>
                <div class="enter_in">Впервые у нас? <button class="btn btn-link" data-toggle="modal" data-target="#reg-popup" data-dismiss="modal">Зарегистрируйтесь!</button></div>
                <div class="lost_password">Не помните пароль? <button class="btn btn-link" data-toggle="modal" data-target="#remind-popup" data-dismiss="modal">Восстановите его</button></div>
                <input type="hidden" name="action" value="auth">
		<?php
                        $uri = JFactory::getURI();
                        $url = base64_encode($uri->toString());
                ?>
                <input type="hidden" value="<?=$url;?>" name="return">
                <?php echo JHtml::_('form.token'); ?>
                </form>
            </div>
        </div>

    </div>
</div>
<div id="reg-popup" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup_registration">
            <a href="#" class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="title">Добро пожаловать в Prizolove!</div>
                <form method="post" action="/index.php?option=com_ajax&plugin=ajaxlogin&format=json">
                    <div class="inputs">
                        <input type="text" name="name" required placeholder="Ваше имя">
                        <input type="email" name="email" required placeholder="Электронная почта">
                        <input type="password" name="password1" required placeholder="Пароль">
                        <input type="password" name="password2" required placeholder="Повторите пароль">
                        <input type="hidden" name="action" value="reg">
			<?php
        	                $uri = JFactory::getURI();
                	        $url = base64_encode($uri->toString());
	                ?>
	                <input type="hidden" value="<?=$url;?>" name="return">
                        <button type="submit" class="btn btn-form btn-submit">Зарегистрироваться</button>
                    </div>
                    <div class="error-popup-enter d-none"></div>
                    <?php echo JHtml::_('form.token'); ?>
                    <?php
                        $onDisplayCf = $dispathcer->trigger('onDisplayCf');
                        echo  $onDisplayCf[0];
                    ?>
                </form>
                <div class="enter_in">Уже зарегистрированы? <button class="btn btn-link" data-toggle="modal" data-target="#auth-popup" data-dismiss="modal">Войти на сайт</button></div>
            </div>
        </div>
    </div>
</div>
<div id="thankyou-popup" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup_thankyou">
            <a href="#" class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
            <div class="modal-content">
                <div class="pp-image"><img src="https://prizolove.com/images/popup-check-thankyou.png"></div>
                <div class="pp-title">Ваши данные приняты!</div>
                <div class="pp-text">На Вашу электронную почту отправлено письмо. Пожалуйста, подтвердите свой e-mail, нажав на кнопку <b>"Подтвердить e-mail"</b></div>
            </div>
        </div>
    </div>
</div>
<div id="remind-popup" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup_lost">
            <!-- Modal content-->
            <div class="modal-content">
                <a href="#" class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                <div class="title">Восстановление пароля</div>
                <form method="post">
                    <div class="inputs">
                        <label for="email">Электронная почта</label><br>
                        <input type="email" name="jform[email]" required>
                        <button type="submit" class="btn btn-form btn-submit">Напомнить пароль!</button>
                    </div>
                    <div class="enter_in">Вспомнили пароль? <button class="btn btn-link" data-toggle="modal" data-target="#auth-popup" data-dismiss="modal">Войти на сайт</button></div>
                    <input type="hidden" name="action" value="remind">
                    <?php echo JHtml::_('form.token'); ?>
                </form>
            </div>
        </div>

    </div>
</div>
<div id="remind-manual-popup" class="modal fade pl-modal" role="dialog">
    <div class="modal-dialog">
        <div class="popup_enter">
            <!-- Modal content-->
            <div class="modal-content">
                <a href="#" class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                <div class="title">Восстановление пароля</div>
                <div>
                    <p>На Ваш email была отправлена инструкция по восстановлению пароля.</p>
                </div>
            </div>
        </div>

    </div>
</div>
    <?php
    $session = JFactory::getSession();
    $userEventStat = json_decode($session->get('userEventsStat'), true);
    $session->clear('userEventsStat');
    if (!empty($userEventStat) && !empty($userEventStat['openedEvents']))  :
        $bonusAccount = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
    ?>
        <div id="new-events" class="modal fade pl-modal" role="dialog">
            <div class="modal-dialog">
                <div class="popup-bonuses-spis">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="topper">
                            <img class="rotateInUpLeft" src="https://prizolove.com/images/diamond_red.png">
                        </div>
                        <h5>Открыты новые испытания!</h5>
                        <div class="stat">
                            <div class="icon-wrapper">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m31 175.859375v206.351563l45-45 45 45v-153.484376zm0 0" fill="#ff4d4d"></path><path d="m391 224.929688v157.28125l45-45 45 45v-187.71875zm0 0" fill="#ff001e"></path><path d="m391 241v-30c49.628906 0 90-40.371094 90-90v-60h-75v-30h106v90c0 66.167969-54.832031 120-121 120zm0 0" fill="#ff9f00"></path><path d="m112.824219 236.09375c-65.84375-17.285156-112.824219-76.949219-112.824219-145.09375v-60h106v30h-75v30c0 54.519531 36.78125 102.261719 89.441406 116.089844zm0 0" fill="#ffd400"></path><path d="m226 301h60v181h-60zm0 0" fill="#ffd400"></path><path d="m256 301h30v181h-30zm0 0" fill="#ff9f00"></path><path d="m91 0v166c0 90.902344 74.097656 165 165 165s165-74.097656 165-165v-166zm0 0" fill="#ffe470"></path><path d="m421 0v166c0 90.902344-74.097656 165-165 165v-331zm0 0" fill="#fdbf00"></path><path d="m348.101562 124-63.601562-9.300781-28.5-57.597657-28.5 57.597657-63.601562 9.300781 46.199218 45-11.097656 63.300781 57-30 57 30-11.097656-63.300781zm0 0" fill="#fdbf00"></path><path d="m301.902344 169 11.097656 63.300781-57-30v-145.199219l28.5 57.597657 63.601562 9.300781zm0 0" fill="#ff9f00"></path><path d="m361 482v30h-210v-30c0-16.5 13.5-30 30-30h150c16.5 0 30 13.5 30 30zm0 0" fill="#ffe470"></path><path d="m361 482v30h-105v-60h75c16.5 0 30 13.5 30 30zm0 0" fill="#fdbf00"></path></svg>
                            </div> Пройдено <span><?=$userEventStat['totalFinished'];?></span> испытаний
                        </div>
                        <div class="stat">
                            <div class="icon-wrapper">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" xml:space="preserve">
                                    <polygon style="fill:#CC2E48;" points="29,55 0,19 58,19 "></polygon>
                                    <polygon style="fill:#FC3952;" points="58,19 0,19 10,3 48,3 "></polygon>
                                    <polygon style="fill:#F76363;" points="42.154,19 48,3 10,3 15.846,19 "></polygon>
                                    <polygon style="fill:#F49A9A;" points="42,19 29,3 16,19 "></polygon>
                                    <polygon style="fill:#CB465F;" points="15.846,19 29,55 42.154,19 "></polygon>
                                </svg>
                            </div>
                            Выиграно - <span><?=$userEventStat['winBonuses'];?></span> бонусов                            
			            </div>

                        <div class="line"></div>
                        <div class="stat">Открыто <span><?=$userEventStat['openedEvents'];?></span> новых испытаний, за них списано <span><?=$userEventStat['spentBonuses'];?></span> бонуса </div>
                        <div class="stat"><span>1</span> Бонус = <span><?=number_format(PlLibHelperRates::convertUSD(1),2,'.','');?></span> грн</div>
                        У вас на бонусном счету:<strong><span><?=$bonusAccount->value;?></span> $</strong>
                        <?php if ($option == 'com_puzzles') :?>
                            <a href="javascript:void();" data-dismiss="modal" class="button">Перейти к загадке</a>
                        <?php endif;?>
                        <a href="/catalog" class="link">Можно тратить на покупки</a>
                    </div>
                </div>
            </div>

        </div>
        <script>
            jQuery('#new-events').modal('show');
        </script>
    <?php endif;?>
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1521478467974668',
            xfbml      : true,
            version    : 'v3.0'
        });
    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/ru_RU/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<?php /* if ($pageId == 79) : ?>
			<!-- MarketGid Sensor --> <script type="text/javascript"> (function() { var d = document, w = window; w.MgSensorData = w.MgSensorData || []; w.MgSensorData.push({cid:278722, lng:"ru", nosafari:true , project: "a.marketgid.com"}); var l = "a.marketgid.com"; var n = d.getElementsByTagName("script")[0]; var s = d.createElement("script"); s.type = "text/javascript"; s.async = true; var dt = !Date.now?new Date().valueOf():Date.now(); s.src = "//" + l + "/mgsensor.js?d=" + dt; n.parentNode.insertBefore(s, n); })(); </script> <!-- /MarketGid Sensor -->
		<?php endif;   */ ?>
<!-- Google Tag Manager (noscript) -->
<?php if ($pageId == 79) :?>
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
            {'GTM-M2PQLPZ':true});
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-112941077-1', 'auto');
        ga('require', 'GTM-M2PQLPZ');
        ga('send', 'pageview');
    </script>
<?php endif; ?>
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KBP7JRW"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="body-wrapper">
    <div class="body-innerwrapper">
        <?php $this->helix3->generatelayout(); ?>
    </div> <!-- /.body-innerwrapper -->
</div> <!-- /.body-innerwrapper -->
<?php /*
        <!-- Off Canvas Menu -->
        <div class="offcanvas-menu">
            <a href="#" class="close-offcanvas" aria-label="Close"><i class="fa fa-remove" aria-hidden="true"></i></a>
            <div class="offcanvas-inner">
                <?php if ($this->helix3->countModules('offcanvas')) { ?>
                  <jdoc:include type="modules" name="offcanvas" style="sp_xhtml" />
                <?php } else { ?>
                  <p class="alert alert-warning">
                    <?php echo JText::_('HELIX_NO_MODULE_OFFCANVAS'); ?>
                  </p>
                <?php } ?>
            </div> <!-- /.offcanvas-inner -->
        </div> <!-- /.offcanvas-menu -->
		*/ ?>
<?php
if ($this->params->get('compress_css')) {
    $this->helix3->compressCSS();
}

$tempOption = $app->input->get('option');
// $tempView       = $app->input->get('view');

if ($this->params->get('compress_js') && $tempOption != 'com_config') {
    $this->helix3->compressJS($this->params->get('exclude_js'));
}

//before body
if ($before_body = $this->helix3->getParam('before_body')) {
    echo $before_body . "\n";
}
?>

<jdoc:include type="modules" name="debug"/>

<!-- Preloader -->
<jdoc:include type="modules" name="helixpreloader"/>

<!-- Go to top -->
<?php if ($this->params->get('goto_top')) { ?>
    <a href="javascript:void(0)" class="scrollup" aria-label="<?php JText::_('HELIX_GOTO_TOP'); ?>">&nbsp;</a>
<?php } ?>
<script>
	
	//========================================== Change favicon/title =============
	var title = document.title;
		var setinter = '';
		
		     function startAnimate(){
				var favicon_images = [
                    'https://prizolove.com//images/favicon.ico',
                    'https://prizolove.com//images/green-diamond.png'
                ],
    image_counter = 0; // To keep track of the current image
					setinter =	setInterval(function() {
					jQuery("link[rel='icon']").remove();
					jQuery("link[rel='shortcut icon']").remove();
					jQuery("head").append('<link rel="icon" href="' + favicon_images[image_counter] + '" type="image/gif">'); 
					if(image_counter == favicon_images.length -1)
						image_counter = 0;
					else
						image_counter++;
				}, 1000);}
		
			 jQuery(window).mouseenter(function () {
				  jQuery("link[rel='shortcut icon']").attr("href", "https://prizolove.com//images/favicon.ico");
                  document.title = title;
				  clearInterval(setinter);
        });
	
		   jQuery(window).mouseleave(function () {
				jQuery("link[rel='shortcut icon']").attr("href", "https://prizolove.com//images/green-diamond.png");
				document.title = "Ловите свой приз!"; 
				startAnimate();
        });
	//==========================================END:: Change favicon/title =============
	
	
	
    var plVisitor = <?=$plVisitor;?>;
    var plPageId = '<?=$pageId;?>';
    var questionId;
    var questionNameSend;
    jQuery(function ($) {
		$('#new-events').modal('show');
        <?php if ($fbMissedEmail) : ?>
        $(document).ready(function () {
            var prevText = $('.cf-img-above').html();
            prevText = '<div class="social-error">К учетной записи социальной сети не привязан имейл. Зарегистрируйтесь пожалуйста через форму</div>' + prevText;
            $('.cf-img-above').html(prevText);

            $('html,body').animate({
                scrollTop: $('.cf-init').find('input[name="cf[name]"]').eq(0).offset().top
            }, 1000);
        });
        <?php endif;?>
        function getParticipants() {
            if ($('.participants').length) {
                $.ajax({
                    url: "/forms/ps.php"
                }).done(function (data) {
                        var participants = parseInt(data);
                        if (participants) {
                            $('.participants').data('digit', participants);
                            $('.participants').sppbanimateNumbers(participants, false, 1000);
                        }
                    }
                );
            }
        }

        getParticipants();
        setInterval(getParticipants, 60000);
        cartInit();
        $('#pl-cart').on('click', function() {
            if ($(this).hasClass('active')) {
                $("#rstbox_17").trigger("open");
            }
        });

        $('#continue-ordering').on('click', function() {
            $("#rstbox_17").trigger("close");
        });

        $('.button-ajax-order').on('click', function() {
            var productId = $(this).data('product-id');
            var act = $(this).data('act');
            $.ajax({
                method: "POST",
                url: "/index.php?option=com_ajax&plugin=cart&format=json",
                dataType: 'json',
                data: {
                    pid: productId,
                    act: act
                }
            })
                .done(function(data) {

                    if (data.data[0].table != undefined) {
                        $('.cart__buttons').removeClass('hidden');
                        $('table.cart').html(data.data[0].table);
                        cartCalculate();
                        cartInit();
                    }
                    if (data.data[0].error) {
                        alert(data.data[0].error);
                    }
                })
            ;
        });

        $('.button-oneclick-order').on('click', function() {
            var productId = $(this).data('product-id');
            var ocHref = $(this).attr('href');
	    ocHref = ocHref + '?pid=' + productId;	
            window.location = ocHref;
	    return false;	
            /* $.ajax({
                method: "POST",
                url: "/index.php?option=com_ajax&plugin=oneclick&format=json",
                dataType: 'json',
                data: {
                    pid: productId
                }
            })
                .done(function(data) {
                    window.location = ocHref;
                })
            ;   
            return false; */
        });
        $('a[href*=#]:not([href=#])').on('click', function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
        jQuery('.akeeba-sociallogin-button-facebook').on('click', function () {
            gtag('event', 'click', {
                'event_category': 'click_fb',
                'event_action': 'submit'
            });
        });
        jQuery('.akeeba-sociallogin-button-google').on('click', function () {
            gtag('event', 'click', {
                'event_category': 'click_gmail',
                'event_action': 'submit'
            });
        });
        if (plCounter > 1) {
            $('#button').hide();
            $('#div1').show();
        }
        if (plCounter > 0 && plCounter < 3) {
            $('.button-grey').removeClass('hidden');
            $('.button-grey').on('click', function () {
                location.reload();
            });
        }

        $(".scroll-to-form").on('click', function () {
            $('html, body').animate({
                scrollTop: $('form').eq(0).offset().top
            }, 1000);
        });
        <?php if (in_array($pageId, $gaEventsIds)) :  ?>
        jQuery('[type="submit"]').on('click', function () {
            gtag('event', 'click', {
                'event_category': 'click',
                'event_action': 'submit'
            });
            $.ajax({
                method: "POST",
                url: "/forms/process.php",
                dataType: 'json',
                data: $(this).parents('form').serialize()
            });
        });
        <?php endif; ?>
        <?php /* if ($pageId == 79) :  ?>
			$('[type="submit"]').on('click', function() {
				ga('send', 'event', 'button', 'registration');
			});
			$('form').on("submit", function() {
				_mgq.push(["MgSensorInvoke", "registration"]);
			});
			<?php endif;  */ ?>
        $('input[name="cf[name]"]').on('input', function () {
            var nameSpcsRE = /^\s|\s$/;
            this.setCustomValidity('');
            if (nameSpcsRE.test($(this).val())) {
                $(this).val($(this).val().replace(/^\s/, ''));
                $(this).val($(this).val().replace(/\s{1,}/ig, ' '));
                this.setCustomValidity('Лишние пробелы');
            }
            var nameLenRE = /^.{0,30}$/;
            if (!nameLenRE.test($(this).val())) {
                this.setCustomValidity('Не более 30 символов');
            }
            var nameLetRE = /^[a-я\s]{0,30}$/ig;
            if (!nameLetRE.test($(this).val())) {
                this.setCustomValidity('Только буквы');
            }
        });
        $('input[name="cf[email]"]').on('input', function () {
            var emailRE = /^.{0,64}$/;
            this.setCustomValidity('');
            if (!emailRE.test($(this).val())) {
                this.setCustomValidity('Не более 64 символов');
            }
        });
        $('input[name*="questions"], input[name*="cf"]').on('drop', function () {
            return false;
        });
        $('input[name*="squestions"]').on('input', function () {
            var completed = true;
            var qParentBlock = $(this).parents('[class*="answer"]');
            qParentBlock.find('.button').not('.disabled').addClass('disabled');
            qParentBlock.find('input[name*="squestions"]').each(function () {
                /* if ($(this).attr('maxlength') != $(this).val().length ) {
                 completed = false;
                 } */
                if ($(this).val().length < 1) {
                    completed = false;
                }
            });
            if (completed) qParentBlock.find('.button').removeClass('disabled');
        });
        $('input[name*="questions"]').on('input', function () {
            if ($(this).data('question') != undefined) {
                questionId = $(this).data('question');
                questionNameSend = $(this).attr('name');
            }
            if (questionId) {
                $(this).closest('.sppb-column').find('a.questions').not('.disabled').addClass('disabled');
            } else {
                $('a.questions').not('.disabled').addClass('disabled');
            }
            var completed = true;
            if (!$(this).hasClass('numeric')) {
                var questionRE = /^[a-я]{1,}$/gi;
                this.setCustomValidity('');
                if (questionRE.test($(this).val()) == false) {
                    $(this).val($(this).val().replace(/[^a-я]+/gi, ''));
                    completed = false;
                }
            } else {
                numericRE = /^[\d]{1,}$/;
                this.setCustomValidity('');
                if (numericRE.test($(this).val()) == false) {
                    $(this).val($(this).val().replace(/[^\d]+/gi, ''));
                    completed = false;
                }
            }


            var questionName = $(this).attr('name');
            var questionClass = $(this).attr('class');

            var curQuestion = $(this);
            $('input[name="' + questionName + '"]').not(this).each(function () {
                if (questionClass == $(this).attr('class'))
                    $(this).val(curQuestion.val());
            });
            if (questionId) {
                //if (curQuestion.attr('maxlength') != curQuestion.val().length) {
		if (curQuestion.val().length < 1) {
                    completed = false;
                }
            } else {
                $('input[name*="questions"]').not('.numeric').each(function () {
                    if ($(this).val().length < 1) {
//                    if ($(this).attr('maxlength') != $(this).val().length) {
                        completed = false;
                    }
                });
            }
            if (completed && !questionId) {
                $('a.questions').removeClass('disabled');
            }
            if (completed && questionId) {
                curQuestion.closest('.sppb-column').find('a.questions').removeClass('disabled');
            }
        });

        $('input[name^="questions"][class*="letter"]').on('keydown', function (event) {
            var questions = $('input[name*="questions"][class*="letter"]');
            var questionIndex = questions.index($(this));
            var key = event.keyCode || event.charCode;
            if (key == 8 || key == 46) {
                if ((questionIndex - 1) >= 0 && $(this).val() == '') {
                    questions.eq(questionIndex - 1).focus();
                }
            }
        });
        $('input[name^="questions"][class*="letter"]').on('input', function () {
            var questions = $('input[name*="questions"][class*="letter"]');
            var questionIndex = questions.index($(this));
            ;
            if ((questionIndex < (questions.length - 1)) && $(this).val() != '') {
                questions.eq(questionIndex + 1).focus();
            }
        });
        $('a.questions').on('click', function () {
            var data = {};
            if (questionNameSend) {
                var question = $('input[name*="' + questionNameSend + '"]').eq(0).attr('name');
                var answer = $('input[name*="' + questionNameSend + '"]').eq(0).val();
                data[question] = answer;

            } else {
                $('input[name*="questions"]').each(function () {
                    var question = $(this).attr('name');
                    var answer = $(this).val();
                    data[question] = answer;
                });
            }
            $.ajax({
                method: "POST",
                url: "/index.php?option=com_ajax&plugin=questions&format=json",
                dataType: 'json',
                data: data

            })
                .success(function (data) {
                    //$('<a href="'+data.data[0]+'" target="_blank">&nbsp;</a>')[0].click();
                    //window.open(data.data[0], '_blank');
                    window.location.href = data.data[0];
                })
                .done(function (msg) {

                });
        });
        $('[class*="answer"]').find('a.button').on('click', function () {
            var data = {};
            var questions = $(this).parents('[class*="answer"]').find('input[name*="squestion"]');
            questions.each(function () {
                data[$(this).attr('name')] = $(this).val();
            });
            //data[question.attr('name')] = question.val();

            $.ajax({
                method: "POST",
                url: "/index.php?option=com_ajax&plugin=questions&format=json",
                dataType: 'json',
                data: data

            })
                .success(function (data) {
                    if (data.data[0].hide) {
                        var hide = data.data[0].hide;
                        $(hide).hide();
                    }
                    if (data.data[0].show) {
                        var show = data.data[0].show;
                        $(show).removeClass('hidden');
                    }
                    if (data.data[0].click_after) {
                        var click_after = data.data[0].click_after;
                        if (show) {
                            $(show).find('a.button').on('click', function () {
                                $(show).hide();
                                $(click_after).removeClass('hidden');
                            });
                        }
                    }
                })
                .done(function (msg) {

                });
            return false;
        });
        $('a.button-order').on('click', function (e) {
            e.preventDefault();
            var hash = $(this).attr('href').split('#')[1];
            $('.convertforms.' + hash).toggleClass('animated');
            $(this).hide();
            return false;
        });
        $('[id*="#cf_"]').on("onFail", function () {
            let cfResponse = $(this).find('.cf-response').html();
            $(this).find('.cf-response').html(cfResponse.replace(/SyntaxError[\s\S]+?<br>/, ''));
        });


        $('.form-173').on("onBeforeSubmit", function (data) {
            var cfResponse = $(this).find('div.cf-response');
            $.ajax({
                method: "POST",
                url: "/forms/visitor.php?" + "?_=" + jQuery.now()
            }).success(function (data) {
                plVisitor = data;
            });
        });
        $('.form-173').on("afterTask", function (data) {
            if (plVisitor) {
                var cfResponse = $(this).find('div.cf-response');
                var cfResponseHTML = $(this).find('div.cf-response').html().replace('1002', plVisitor)
                cfResponse.html(cfResponseHTML);
                $('#sp-component').html($(this));
            }
        });
        $('.form-173').on("afterTask", function (data) {
            FB.XFBML.parse();
        });








        // $(document).ready(function(){
        //     $("#my_image").attr("src",'//graph.facebook.com/1732000743558198/picture?width=400&height=400');
        //
        // });



        //=================== facebook button =========================
        //==============================================================

        $('.form-173').on("afterTask", function (data) {
            $(document).ready(function(){
                $(".share_class").on('click',function(){
                    share_me();

                });
            });
            function share_me() {
                FB.ui({
                        method: 'share',
                        href: 'https://prizolove.com',
                    },
                    function(response){
                        if(response) {
                            self.location.href = 'https://prizolove.com/277'
                        }else{
                            self.location.href = 'https://prizolove.com/101'

                        }
                    });
            }
        });







        window.fbAsyncInit = function() {
            FB.init({
                appId      : '2065706520362598',
                cookie     : true,
                xfbml      : true,
                version    : 'v3.0'
            });
            FB.AppEvents.logPageView();
        };

        $(document).ready(function(){
            $(".share_class").on('click',function(){
                share_me();
            });
        });






//    ================================== Facebook Avatar ==========

        


        function share_me() {
            FB.ui({
                    method: 'share',
                    href: 'https://prizolove.com',
                },
                function(response){
                    if(response) {

                        $("#sppb-addon-1527686004608").fadeIn(1);
                        function funcShowPrize() {
                            $("#section-id-1527687375850").fadeIn(3000);
                            $("#sppb-addon-1527686004608").remove();//Скрыть счетчик

                        }
                        $(function () {
                            $({numberValue: 0}).animate({numberValue: 100}, {
                                duration: 5000, // Скорость анимации, где 500 = 0,5 одной секунды, то есть 500 миллисекунд
                                easing: "linear",
                                step: function(val) {
                                    $(".price_timer_priz").html(Math.ceil(val)); // Блок, где необходимо сделать анимацию
                                }
                            });
                        });
                        $(function () {
                            $("#section-id-1527672615913").fadeOut(1);//Скрыть блок с кнопкой FB
                            $("#sppb-addon-1524241153485").fadeOut(1);//Скрыть текст
                            setTimeout(funcShowPrize,6000);
                        });


                    }else{

                    }
                });
        }
        //=================== END:: facebook button ==================


        //************************ Animate page 277 timer ************************ //

        $("#section-id-1527687375850").fadeOut(0);//Скрыть блок с формой
        $("#sppb-addon-1527686004608").fadeOut(0);//Скрыть счетчик

        var randomArray = new Array('.image-hidden2','.image-hidden');//Масив з класами призов
        var RandElement = randomArray[Math.floor(Math.random()*(randomArray.length))];//Рандомный выбор поиза

        $(RandElement).fadeOut(0);

        //************************ END:: Animate page 277 timer ************************ //

        //************************ Random pize ************************ //

        function myRandomFunction() {
            $(".demo").innerHTML = Math.random();
        }
        //************************ END::  Random pize ************************ //





        //=================== facebook button Page 282  ==================
        function okrowkaShare(){
            FB.ui({
                    method: 'share',
                    href: 'https://prizolove.com/284',
                },
                function(response){
                    if(response) {
                        self.location.href = 'https://prizolove.com/282'
                    }else{
                        self.location.href = 'https://prizolove.com/282'
                    }
                });
        }
        $(document).ready(function() {
            $(".share_class_1").on('click', function () {
                okrowkaShare();
            });
        });
        //===================END::  facebook button Page 282  ==================



        //=================== facebook button Page 381  ==================
        function okrowkaShare(){
            FB.ui({
                    method: 'share',
                    href: 'https://prizolove.com/381',
                },
                function(response){
                    if(response) {
                        self.location.href = 'https://prizolove.com/381'
                    }else{
                        self.location.href = 'https://prizolove.com/381'
                    }
                });
        }
        $(document).ready(function() {
            $(".share_butt_99").on('click', function () {
                okrowkaShare();
            });
        });
        //===================END::  facebook button Page 381  ==================




        //=================== facebook button Page 286  ==================
        function okrowkaShare(){
            FB.ui({
                    method: 'share',
                    href: 'https://prizolove.com/285',
                },
                function(response){
                    if(response) {
                        self.location.href = 'https://prizolove.com/286'
                    }else{
                        self.location.href = 'https://prizolove.com/286'
                    }
                });
        }
        $(document).ready(function() {
            $(".share_class_2").on('click', function () {
                okrowkaShare();
            });
        });
        //===================END::  facebook button Page 286  ==================

        //=================== facebook button Page 278  ==================
        function okrowkaShare(){
            FB.ui({
                    method: 'share',
                    href: 'https://prizolove.com/290',
                },
                function(response){
                    if(response) {
                        self.location.href = 'https://prizolove.com/278'
                    }else{
                        self.location.href = 'https://prizolove.com/278'
                    }
                });
        }
        $(document).ready(function() {
            $(".share_class_4").on('click', function () {
                okrowkaShare();
            });
        });
        //===================END::  facebook button Page 289  ==================

	        //=================== facebook button Page 278  ==================
        function okrowkaShare5(){
            FB.ui({
                    method: 'share',
                    href: 'https://prizolove.com/296',
                },
                function(response){
                    if(response) {
                        self.location.href = 'https://prizolove.com/297'
                    }else{
                        self.location.href = 'https://prizolove.com/297'
                    }
                });
        }
        $(document).ready(function() {
            $(".share_class_5").on('click', function () {
                okrowkaShare5();
            });
        });
        //===================END::  facebook button Page 289  ==================



        //=================== facebook button Page FEYA ==================
        function okrowkaShare(){
            FB.ui({
                    method: 'share',
                    href: 'https://prizolove.com/293',
                    picture: 'https://prizolove.com/images/2.png',
                },
                function(response){
                    if(response) {
                        self.location.href = 'https://prizolove.com/282'
                    }else{
                        self.location.href = 'https://prizolove.com/282'
                    }
                });
        }
        $(document).ready(function() {
            $(".share_class_33").on('click', function () {
                okrowkaShare();
            });
        });
        //===================END::  facebook button Page FEYA ==================



        if ($('input[name="cf[phone]"]').length) {
            $('input[name="cf[phone]"]').each(function () {
		<?php if ($countryCode == 'ua') :?>
			$(this).mask('+38(099) 999 99 99', {placeholder: "+--(---) --- -- --"});
		<?php endif;  ?>        
            });
            $('form').on('submit', function () {
                let fPhone = $(this).find('input[name="cf[phone]"]');
                if (fPhone.length) {
                    fPhone.val(fPhone.val().replace(/^(\+){1}|([^\+\d])/gm, ''));
                }
            });
        }
        if (typeof kName !== 'undefined') {
            $('input[name="cf[name]"]').val(kName);
        }
        if (typeof kEmail !== 'undefined') {
            $('input[name="cf[email]"]').val(kEmail);
        }
        if (typeof kPhone !== 'undefined') {
            $('input[name="cf[phone]"]').val(kPhone);
        }
        function cartCalculate(){

            var cartCost = 0;
            var productNum = 0;
            $('table.cart tr.cart-row').each(function() {
                var rowPrice = parseFloat($(this).find('td.cart__col__price').text());
                var rowQuantity = parseInt($(this).find('input.cart__count').val());
                productNum = productNum + rowQuantity;
                $(this).find('td.cart__col__quantity').text(rowPrice * rowQuantity);
                cartCost = cartCost + (rowPrice * rowQuantity);
            });

            if (productNum) {
                $('#pl-cart .pl-cart__product-num, .cart__total-num').text(productNum);
            } else {
                $('#pl-cart .pl-cart__product-num, .cart__total-num').text('');
            }

            if (productNum && !$('#pl-cart').hasClass('active')) $('#pl-cart').addClass('active');

            if (!productNum && $('#pl-cart').hasClass('active')) $('#pl-cart').removeClass('active');

            $('.cart__total-cost').text(cartCost);
        }

        function cartInit() {
            $('.cart__plus, .cart__minus').on('click', function() {
                var cartBtn = $(this);
                var productId = $(this).parents('tr').data('id');
                var act = $(this).data('act');
                var cartInput = cartBtn.parents('tr').find('input.cart__count');
                $.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=cart&format=json",
                    dataType: 'json',
                    data: {pid: productId, act: act}
                })
                    .done(function(data) {
                        if (act == 'minus') {
                            var cartInputVal = parseInt(cartInput.val()) - 1;
                            if (cartInputVal > 0) {
                                cartInput.val(cartInputVal);
                            } else {
                                cartBtn.parents('tr').remove();
                            }
                            cartCalculate();
                        } else {
                            cartInput.val(parseInt(cartInput.val()) + 1);
                            cartCalculate();
                        }
                        if (data.data[0].error) {
                            alert(data.data[0].error);
                        }
                    })
                ;
                return false;
            });
            $('.cart__count').on('change', function() {
                var productId = $(this).parents('tr').data('id');
                var num = $(this).val();
                $.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=cart&format=json",
                    dataType: 'json',
                    data: {pid: productId, num: num}
                })
                    .done(function(data) {
                        if (data.data[0].error) {
                            alert(data.data[0].error);
                        }
                    })
                ;
            });
            $('.cart__delete').on('click', function() {
                var cartBtn = $(this);
                var productId = $(this).parents('tr').data('id');
                var act = $(this).data('act');
                $.ajax({
                    method: "POST",
                    url: "/index.php?option=com_ajax&plugin=cart&format=json",
                    dataType: 'json',
                    data: {pid: productId, act: act}
                })
                    .done(function(data) {
                        cartBtn.parents('tr').remove();
                        cartCalculate();
                        if (data.data[0].error) {
                            alert(data.data[0].error);
                        }
                    })
                ;
            });
        }
	$('#btn-1528298385834').on('click', function() {
              var userConfirm = confirm('<?=$lang->_('CONFIRM_MONEY_ARE_YOU_SURE');?>');
              if (!userConfirm) return false;
	});
	 /*  $('.button-see').on('click', function(e){
              var userConfirm = confirm('Вы уверены? Игра может захватить Вас?');
              if (!userConfirm) return false;
          }); */

    });
    jQuery.fn.checkboxesCount = function (options) {

        var settings = jQuery.extend({
            min: 3,
            max: 3,
            message: 'Набор должен состоять из трех средств'
        }, options);

        $this = jQuery(this);
        $this.each(function () {
            let ccForm = jQuery(this);

            ccForm.find('form div.submit button').eq(0).on('click', function (e) {
                let checkedInput = ccForm.find('form input[name="cf[things][]"]:checked').length;

                if ((checkedInput > settings.max) || (checkedInput < settings.min)) {
                    ccForm.find('div.cf-checkbox-group').eq(0).notify(settings.message, {position: "top left"});
                    e.preventDefault();
                }

            });
        })
        return this;

    };


    //============================ Вікно пуш повідомлення з вказаними параметрами =================
    // function notifyMe() {
    //     var notification = new Notification("СУПЕР новость! На кону 10,000грн!", {
    //         tag: "ache-mail",
    //         body: "14.06 Розыгрыш 10,000 грн. Успейте получить шанс на победу!",
    //         icon: "https://prizolove.com/images/diamonds.png"
    //     });
    //     function clickFunc() {
    //         location.href='https://prizolove.com/290'
    //     }
    //     notification.onclick = clickFunc;
    // }

    //============================ Умови показу повідомлення =================
    // function notifSet() {
    //     if (!("Notification" in window)) {
    //         alert("Ваш браузер не поддерживает уведомления");//Перевіряємо чи підтримує повідомлення браузер
    //     }
    //
    //     else if (Notification.permission === "granted") {
    //         setTimeout(notifyMe, 20000)//Якщо користувач вже давав згоду на отримання повідомлень то показуємо повідомлення
    //     }
    //
    //     else if (Notification.permission !== "denied") { //Якщо користувач ще не давав згоду на отримання повідомлень
    //         Notification.requestPermission(function (permission) {//то ми питаємо дозволу на показ
    //
    //
    //             if (!('permission' in Notification)) {
    //                 Notification.permission = permission; //Значення яке дав нам користувач
    //
    //                 if (permission === 'granted') { // Якщо дав до ми визиваэмо функцыю показу повыдомлення
    //                     setTimeout(notifyMe, 2000) // Функція виводу повідомлення
    //                 }
    //             }
    //
    //         })
    //     }
    // }


	jQuery(document).ready(function() {			
			jQuery('.categoryTitle').on('click', function() {				
				var catClass = /catalog.+/ig.exec(jQuery(this).attr('class'));          
				jQuery('.categoryBlock').each(function() {
					if (!jQuery(this).hasClass('not-active')) jQuery(this).addClass('not-active'); 
				});
				jQuery('div.'+catClass).removeClass('not-active');                                                                      
				jQuery('.allProducts').addClass('not-active');
				jQuery('html,body').animate({
					scrollTop: jQuery('div.'+catClass).offset().top
				}, 250);

			});
			jQuery('a.categoryHide').on('click', function() {
				jQuery(this).parents('.sppb-section').addClass('not-active');
				jQuery('.allProducts').removeClass('not-active');
				jQuery('html,body').animate({
					scrollTop: jQuery('.allProducts').offset().top
				}, 250);
			});
			categoryBgImage();
	});
	jQuery(window).resize(function() {
		categoryBgImage();
	});
	function categoryBgImage() {
		if (jQuery( window ).width() < 992) {
			jQuery('.category-bg-image').each(function() {
				var image = jQuery(this).data('image');
				if (image) {
					jQuery(this).parents('div.blockProduct').css('background-image', 'url(' + image + ')');
					jQuery(this).parents('div.blockProduct').find('.sppb-addon-single-image-container').hide();
				}
			});
		}
	}
    function verifyEventAnswer(id) {
        jQuery.ajax({
            method: "POST",
            url: "/index.php?option=com_ajax&plugin=events&format=json",
            dataType: 'json',
	    async: false,
            data: {eid: id}
        });
    }




</script>

</body>
</html>
