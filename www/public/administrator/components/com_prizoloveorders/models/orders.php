<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorldList Model
 *
 * @since  0.0.1
 */
class PrizoloveOrdersModelOrders extends JModelList
{
    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @see     JControllerLegacy
     * @since   1.6
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                'date', 'a.date',
                'order_id', 'a.order_id',
                'order_url', 'a.order_url',
				'cusomter_phone', 'c.customer_phone',
				'address_delivery', 'a.address_delivery',
				'product_name', 'a.product_name',
				'click_id', 'a.click_id',
				'comment', 'a.comment',
                'sent', 'a.sent',
            );
        }

        parent::__construct($config);
    }

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Initialize variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

        // Filter by search in title
        $search = $this->getState('filter.search');
		$date_start = $this->getState('filter.date_start');
		$date_end = $this->getState('filter.date_end');		
		$state = $this->getState('filter.sent');
		
		
		
		// Create the base select statement.
		$query->select('a.order_id, a.bonuses, a.order_url, c.customer_email, a.product_name, a.address_delivery, a.click_id, a.date, a.customer_id, c.customer_name, c.customer_phone, a.comment, a.customer_comment, a.sent')
			  ->from($db->quoteName('#__prizolove_orders', 'a'))
              ->join('LEFT', $db->quoteName('#__prizolove_customers', 'c') . ' ON a.customer_id = c.customer_id')
              ->group('a.order_id')
        ;

        // Join over the clients.
        /* $query->select($db->quoteName('cl.name', 'client_name'))
            ->select($db->quoteName('cl.purchase_type', 'client_purchase_type'))
            ->join('LEFT', $db->quoteName('#__banner_clients', 'cl') . ' ON cl.id = a.cid'); */

        if (!empty($search))
        {
            $search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
            $query->where('
				(c.customer_email LIKE ' . $search . ') OR
				(a.order_id LIKE ' . $search . ') OR
				(a.product_name LIKE ' . $search . ') OR
				(a.address_delivery LIKE ' . $search . ') OR				
				(a.customer_id LIKE ' . $search . ') OR
				(c.customer_name LIKE ' . $search . ') OR
				(c.customer_phone LIKE ' . $search . ') OR
				(a.order_url LIKE ' . $search . ') OR
				(a.comment LIKE ' . $search . ') 
			');			
        }
		
		if (!empty($state)) {
			$state = implode(',', $state);
			$query->where('a.sent IN ('.$state.')');
		}
		
		if (!empty($date_start) && !empty($date_end)) {
			$query->where('a.date >= "'.$date_start.':00:00:00" AND a.date <= "'.$date_end.':23:59:59"');
		}
		
        $listOrdering  = $this->state->get('list.ordering', 'a.order_id');
        $orderDirn     = $this->state->get('list.direction', 'DESC');
	
        $query->order($db->escape($listOrdering) . ' ' . $db->escape($orderDirn));		

		return $query;
	}


}
