<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 

class PrizoloveOrdersControllerOrders extends JControllerAdmin
{

	public function getModel($name = 'Order', $prefix = 'PrizoloveOrdersModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
	public function export()
	{
		$statuses = [ 
					  1 => 'отправлен',
					  5 => 'оплачен',
					  7 => 'к отправке',
                      8 => 'возврат товара',
					  9 => 'забрал товар',
					  0 => 'новый',
					  2 => 'не дозвон'
					];				  
			
		$model = $this->getModel();

		$this->addHeaders();

		$rows = $model->export();
		echo '"Joomla\'s Order ID";"Date";"Personal data 1";"Personal data 2";"Personal data 3";"Good\'s name";"Address";"Comment";"Landing Url";"Order\'s status";'."\r\n";

		foreach($rows as $row) {
			$row->sent = $statuses[$row->sent];
			foreach($row as $col) {								
				$col = @iconv('UTF-8','WINDOWS-1251',htmlspecialchars($col));
				echo '"'.$col.'";';				
			}			
			echo "\r\n";
		}

	        exit();
		
	}

	function addHeaders($fileName = 'export'){
		$fileName = substr(preg_replace('#[^a-z0-9_-]#i','_',$fileName),0,50);
 		@ob_clean();

		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		header("Content-Disposition: attachment; filename=".$fileName.".csv");

		header("Content-Transfer-Encoding: binary");
	}


}