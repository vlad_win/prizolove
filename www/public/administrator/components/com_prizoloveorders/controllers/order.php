<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * HelloWorlds Controller
 *
 * @since  0.0.1
 */
class PrizoloveOrdersControllerOrder extends JControllerForm
{
	public function batch($model = null)
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('order', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_prizoloveorders&view=orders' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}
}