ALTER TABLE `#__prizolove_orders` 
ADD `order_hash` VARCHAR(10) NULL DEFAULT NULL AFTER `sent`, 
ADD `discount` FLOAT NULL DEFAULT NULL AFTER `order_hash`, 
ADD `status` VARCHAR(20) NULL DEFAULT NULL AFTER `discount`;