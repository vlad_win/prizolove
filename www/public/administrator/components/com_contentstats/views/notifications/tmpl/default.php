<?php 

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author              Germinal Camps
# copyright           Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license            http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites:           http://www.JoomlaThat.com
# Technical Support:  Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); 

$hours = $this->params->get('hours', '+00:00');

?>

<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-horizontal">

  <div id="j-sidebar-container" class=" ">
    <?php echo $this->sidebar; ?>
  </div>

  <div id="j-main-container" >

    <div class="navbar filter-bar">
    <div class="navbar-inner">

      <input type="text" name="keywords" id="keywords" value="<?php echo $this->keywords;?>" class="text_area"  placeholder="<?php echo JText::_( 'TYPE_TO_SEARCH' ); ?>" />
      
      <div class="btn-group ">
        <button class="btn tip hasTooltip btn-inverse" type="submit" onclick="this.form.submit();" title="<?php echo JText::_('GO'); ?>"><i class="icon-search"></i></button>
        <button class="btn tip hasTooltip" type="button" onclick="resetFilter();" title="<?php echo JText::_('RESET'); ?>"><i class="icon-remove"></i></button>
      </div>
     
    </div></div>

    <table class="table table-striped">
      <thead>
        <tr>
          <th width="5" class="hidden-phone"> <?php echo JHTML::_( 'grid.sort', 'ID', 'st.id', $this->lists['order_Dir'], $this->lists['order']); ?> </th>
          <th width="20" class="hidden-phone"> <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
          </th>
          <th class="hidden-phone"> <?php echo JHTML::_( 'grid.sort', 'TITLE', 'nt.name', $this->lists['order_Dir'], $this->lists['order']); ?> </th>
          <th class="hidden-phone"> <?php echo JText::_('TYPE_OF_ACTION'); ?> </th>
          <th class="hidden-phone"> <?php echo JHTML::_( 'grid.sort', 'FREQUENCY', 'nt.frequency', $this->lists['order_Dir'], $this->lists['order']); ?> </th>
          <th class="hidden-phone"> <?php echo JHTML::_( 'grid.sort', 'NEXT_NOTIFICATION', 'nt.nextdate', $this->lists['order_Dir'], $this->lists['order']); ?> </th>
          <th class="hidden-phone"> <?php echo JText::_('RECIPIENTS'); ?> </th>
          <th width="5" class="hidden-phone"><?php echo JHTML::_( 'grid.sort', 'ACTIVE', 'nt.active', $this->lists['order_Dir'], $this->lists['order']); ?></th>
        </tr>
      </thead>
      <?php

    $db = JFactory::getDBO();

    for ($i=0, $n=count( $this->items ); $i < $n; $i++) {
    $row = &$this->items[$i];
    $checked  = JHTML::_('grid.id',   $i, $row->id );
    $link_edit     = JRoute::_( 'index.php?option=com_contentstats&controller=notification&task=edit&cid[]='. $row->id);

    if (strpos($row->target, ',') === false) {
      //single
      $query = "SELECT * FROM #__users WHERE id = ".(int)$row->target;
      $db->setQuery($query);
      $target = $db->loadObject();
      $recipients = $target->name;
    }
    else{
      $recipients = count(explode(",", $row->target));
    }

    if($row->active){
      $publicat = JHTML::image('administrator/components/com_contentstats/assets/images/tick.png','Active');
      $link_publicat = JRoute::_('index.php?option=com_contentstats&controller=notification&task=unpublish&cid[]='. $row->id); 
    }
    else{
      $publicat = JHTML::image('administrator/components/com_contentstats/assets/images/publish_x.png','Not Active');
      $link_publicat = JRoute::_('index.php?option=com_contentstats&controller=notification&task=publish&cid[]='. $row->id); 
    }

    ?>
      <tr>
        <td class="hidden-phone"><?php echo $row->id; ?></td>
        <td class="hidden-phone"><?php echo $checked; ?></td>
        <td>
          <a rel="tooltip" title="<?php echo JText::_('EDIT'); ?>" href="<?php echo $link_edit; ?>"><?php echo $row->name; ?></a>
        </td>
        <td class="hidden-phone"><?php echo ContentstatsHelper::getTypeName($row->component, $row->type); ?></td>
        <td class="hidden-phone"><?php echo JText::_($row->frequency); ?></td>
        <td class="hidden-phone"><?php echo JHTML::_('date', strtotime($row->nextdate . ' ' .$hours .' hours'), JText::_('DATE_FORMAT_LC2')); ?></td>
        <td class="hidden-phone"><?php echo $recipients; ?></td>
        <td align="center" class="hidden-phone"><a href="<?php echo $link_publicat; ?>"><?php echo $publicat; ?></a></td>
        
      </tr>
      <?php } ?>
  
    </table>

    <div align="center" class="pagination"><?php echo $this->pagination->getLimitBox(); ?><br><br> <?php echo $this->pagination->getListFooter();?></div>
  </div>
  <input type="hidden" name="option" value="com_contentstats" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="controller" value="notification" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>