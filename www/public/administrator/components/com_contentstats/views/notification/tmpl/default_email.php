<?php 

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); 

$hours = $this->params->get('hours', '+00:00');

$user = JFactory::getUser($this->item->user_id);

?>
<?php if( $this->notification->body ){ ?>
<?php echo $this->notification->body; ?><br><br>
<?php } ?>

<strong><?php echo JText::_('COMPONENT'); ?></strong>: <?php echo $this->item->component; ?><br>
<strong><?php echo JText::_('TYPE_OF_ACTION'); ?></strong>: <?php echo ContentstatsHelper::getTypeName($this->notification->component, $this->notification->type); ?><br>
<strong><?php echo JText::_('ITEM'); ?></strong>: <?php echo $this->item_name($this->item->component, $this->item->type, $this->item->reference_id, $this->item->id, $this->item->value, $this->item->valuestring); ?><br>
<strong><?php echo JText::_('ITEM_ID'); ?></strong>: <?php echo $this->item->reference_id; ?><br>
<strong><?php echo JText::_('USER'); ?></strong>: <?php echo $user->id ? $user->name . " (".$user->username.")" : JText::_('UNREGISTERED'); ?><br>
<strong><?php echo JText::_('USER_ID'); ?></strong>: <?php echo $this->item->user_id; ?><br>
<strong><?php echo JText::_('DATE'); ?></strong>: <?php echo JHTML::_('date', strtotime($this->item->date_event . ' ' .$hours .' hours'), JText::_('DATE_FORMAT_LC2')); ?><br>
<br>
<strong><?php echo JText::_('IP'); ?></strong>: <?php echo $this->item->ip; ?><br>
<strong><?php echo JText::_('COUNTRY'); ?></strong>: <?php echo $this->item->country; ?><br>
<strong><?php echo JText::_('STATE'); ?></strong>: <?php echo $this->item->state; ?><br>
<strong><?php echo JText::_('CITY'); ?></strong>: <?php echo $this->item->city; ?><br>