<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author          Germinal Camps
# copyright       Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license        http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites:       http://www.JoomlaThat.com
# Technical Support:  Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); 
?>
<div id="cs-app">
  <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data"  class="form-horizontal form-validate">
    <div class="span6">
      <fieldset class="adminform">
        <legend><?php echo JText::_( 'DETAILS' ); ?></legend>

        <div class="control-group">
          <label class="control-label" for="name"> <?php echo JText::_( 'NAME' ); ?></label>
          <div class="controls">
            <input  class="inputbox required" 
                    type="text" 
                    name="name" 
                    id="name" 
                    v-model="notification.name"
                    required="required"
                    placeholder="<?php echo JText::_( 'NAME' ); ?>"
            />
            <span class="help-block"><?php echo JText::_( 'NAME_EXPLANATION' ); ?></span>
          </div>

        </div>

        <div class="control-group">
          <label class="control-label" for="body"> <?php echo JText::_( 'MESSAGE' ); ?></label>
          <div class="controls">
            <textarea  class="inputbox" 
                    type="text" 
                    name="body" 
                    id="body" 
                    v-model="notification.body"
                    placeholder="<?php echo JText::_( 'MESSAGE' ); ?>"
            >
            </textarea>
            <span class="help-block"><?php echo JText::_( 'MESSAGE_EXPLANATION' ); ?></span>
          </div>

        </div>

        <div class="control-group">
          <label class="control-label" for="active"> <?php echo JText::_( 'ACTIVE' ); ?></label>
          <div class="controls">
            <div class="btn-group" id="sign_group">
    
            <label class="btn" for="publish_1" ><?php echo JText::_('JYES');?>
              <input class="radio_toggle" type="radio" value="1" name="active" id="publish_1" <?php if($this->notification->active == 1) echo "checked='checked'";?> />
            </label>
            <label class="btn" for="publish_0"><?php echo JText::_('JNO');?>
            <input class="radio_toggle" type="radio" value="0" name="active" id="publish_0" <?php if(!$this->notification->active) echo "checked='checked'";?> /> </label>

            </div>
          </div>
          
        </div>

      </fieldset>
      <fieldset class="adminform">
        <legend><?php echo JText::_( 'WHAT_TO_NOTIFY' ); ?></legend>
        
        <div class="control-group">
          <label class="control-label" for="component"> <?php echo JText::_( 'CONTENT_PROVIDER' ); ?></label>
          <div class="controls">
            <select name="component" id="component" v-model="notification.component">
              <option v-for="component in components" 
                      :value="component.value"
              >
              {{ component.text }}
              </option>
              
            </select>  
          </div>
        </div>

        <div class="control-group">
          <label class="control-label" for="type"> <?php echo JText::_( 'TYPE' ); ?></label>
          <div class="controls">
            <select name="type" id="type" v-model="notification.type">
              <option v-for="(type, index) in types[notification.component]" 
                      :value="index"
              >
              {{ type }}
              </option>
              
            </select> 
            <span class="help-block"><?php echo JText::_( 'TYPE_EXPLANATION' ); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label class="control-label" for="reference_id"> <?php echo JText::_( 'ITEM_ID' ); ?></label>
          <div class="controls">
            <input  class="inputbox input-small" 
                    type="text" 
                    name="reference_id" 
                    id="reference_id" 
                    v-model="notification.reference_id"
                    placeholder="<?php echo JText::_( 'ITEM_ID' ); ?>"
            />
            <span class="help-block"><?php echo JText::_( 'ITEM_ID_EXPLANATION' ); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label class="control-label" for="user_id"> <?php echo JText::_( 'USER_ID' ); ?></label>
          <div class="controls">
            <input  class="inputbox input-small" 
                    type="text" 
                    name="user_id" 
                    id="user_id" 
                    v-model="notification.user_id"
                    placeholder="<?php echo JText::_( 'USER_ID' ); ?>"
            />
            <span class="help-block"><?php echo JText::_( 'USER_ID_EXPLANATION' ); ?></span>
          </div>
        </div>
        
      </fieldset>
      
    </div>
    <div class="span6">

      <fieldset class="adminform">
        <legend><?php echo JText::_( 'WHEN_TO_NOTIFY' ); ?></legend>
        <div class="control-group">
          <label class="control-label" for="frequency"> <?php echo JText::_( 'FREQUENCY' ); ?></label>
          <div class="controls">
            <select name="frequency" id="frequency" v-model="notification.frequency">
              <option value="everytime"><?php echo JText::_( 'EVERY_TIME' ); ?></option>
              <option value="daily"><?php echo JText::_( 'DAILY' ); ?></option>
              <option value="weekly"><?php echo JText::_( 'WEEKLY' ); ?></option>
              <option value="monthly"><?php echo JText::_( 'MONTHLY' ); ?></option>
              
            </select>  
            <span class="help-block"><?php echo JText::_( 'WHEN_EXPLANATION' ); ?></span>
            <span class="help-block" v-show="notification.frequency == 'everytime'">
              <div class="alert">
                <?php echo JText::_( 'EVERY_TIME_WARNING' ); ?>
              </div>
            </span>

          </div>
        </div>

        <div class="control-group" v-show="notification.frequency == 'weekly'">
          <label class="control-label" for="params_weekday" > <?php echo JText::_( 'WHICH_DAY_WEEK' ); ?></label>
          <div class="controls">
            <select name="jform[params][weekday]" id="params_weekday" v-model="notification.params.weekday">
              <?php for ($i=0; $i < 7; $i++) { ?>
              <option value="<?php echo $i; ?>" ><?php echo JText::_( 'WEEKDAY_'. $i); ?></option>
              <?php } ?>
              
            </select>  
            <span class="help-block"><?php echo JText::_( 'WHICH_WEEKDAY_EXPLANATION' ); ?></span>
            
          </div>
        </div>

        <div class="control-group" v-show="notification.frequency == 'monthly'">
          <label class="control-label" for="params_monthday" > <?php echo JText::_( 'WHICH_DAY_MONTH' ); ?></label>
          <div class="controls">
            <select name="jform[params][monthday]" id="params_monthday" v-model="notification.params.monthday">
              <?php for ($i=1; $i < 33; $i++) { ?>
              <option value="<?php echo $i; ?>" ><?php echo JText::_( 'MONTHDAY_'. $i); ?></option>
              <?php } ?>
              
            </select>  
            <span class="help-block"><?php echo JText::_( 'WHICH_MONTHDAY_EXPLANATION' ); ?></span>
            
          </div>
        </div>

        <div class="control-group" v-show="notification.frequency != 'everytime'">
          <label class="control-label" for="params_time" > <?php echo JText::_( 'AT_WHAT_TIME' ); ?></label>
          <div class="controls">
            <select name="jform[params][time]" id="params_time" v-model="notification.params.time">
              <?php for ($i=0; $i < 24; $i++) { ?>
              <option value="<?php echo $i; ?>" ><?php echo JText::_( 'TIME_'. $i); ?></option>
              <?php } ?>
              
            </select>  
            <span class="help-block"><?php echo JText::_( 'WHAT_TIME_EXPLANATION' ); ?></span>
            
          </div>
        </div>

        <div class="control-group" v-show="notification.frequency != 'everytime'">
          <label class="control-label" for="nextdate"> <?php echo JText::_( 'NEXT_NOTIFICATION' ); ?></label>
          <div class="controls">
            <?php echo JHTML::calendar($this->notification->nextdate, "nextdate", "nextdate", "%Y-%m-%d %H:%M:%S", array("class" => "date_item input-medium")); ?>
            <span class="help-block"><?php echo JText::_( 'NEXT_NOTIFICATION_EXPLANATION' ); ?></span>
          </div>
        </div>

      </fieldset>

      <fieldset class="adminform">
        <legend><?php echo JText::_( 'WHOM_TO_NOTIFY' ); ?></legend>

        <div class="control-group">
          <label class="control-label" for="search_user"> <?php echo JText::_( 'ADD_NEW_USERS' ); ?> </label>
          <div class="controls">
            
            <div class="input-append ">
              <input type="text" name="search_user" id="search_user" value="" size="30" placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER'); ?>" />
              <input type="button" class="btn btn-inverse" id="button_search_users" value="<?php echo JText::_('SEARCH_USER'); ?>" />
            </div>

            <div>

              <button class="btn btn-default"
                      v-for="user in search_results"
                      @click.prevent="addToRecipientList(user)"
              >
              {{ user.name }} ({{ user.email }})
              </button>

            </div>

          </div>
        </div>

        <div class="control-group">
          <label class="control-label"> <?php echo JText::_( 'CURRENT_USERS' ); ?> </label>
          <div class="controls">
            
            <div v-for="user in notification.current_users">
              <span class="label label-info">
              {{ user.name }} ({{ user.email }}) <i class="icon icon-remove icon-white removeicon" @click.prevent="removeFromRecipientList(user)"></i>
              </span><br>
            </div>

            <input type="hidden" name="target" id="target" v-model="current_user_ids" />

          </div>
        </div>

        <div class="control-group">
          <label class="control-label" for="search_email"> <?php echo JText::_( 'ADD_NEW_EMAILS' ); ?> </label>
          <div class="controls">
            
            <div class="input-append ">
              <input  type="email" 
                      name="search_email" 
                      id="search_email" 
                      v-model="inputemail" 
                      size="30" 
                      placeholder="<?php echo JText::_('TYPE_EMAIL'); ?>" 
              />
              <input  type="button" 
                      class="btn btn-inverse" 
                      id="button_add_email" 
                      value="<?php echo JText::_('ADD_EMAIL'); ?>"
                      @click.prevent="addToEmailList()"
              />
            </div>

          </div>
        </div>

        <div class="control-group">
          <label class="control-label"> <?php echo JText::_( 'CURRENT_EMAILS' ); ?> </label>
          <div class="controls">
            
            <div v-for="email in notification.current_emails">
              <span class="label label-info">
              {{ email }} <i class="icon icon-remove icon-white removeicon" @click.prevent="removeFromEmailList(email)"></i>
              </span><br>
            </div>

            <input type="hidden" name="target_emails" id="target_emails" v-model="current_emails" />

          </div>
        </div>

      </fieldset>

      <fieldset class="adminform">
        <legend><?php echo JText::_( 'HOW_TO_NOTIFY' ); ?></legend>

        <div class="control-group">
          <label class="control-label"> <?php echo JText::_( 'INCLUDE_CSV' ); ?> </label>
          <div class="controls">

            <div class="btn-group">

              <label class="btn" for="csv_1" ><?php echo JText::_('JYES');?>
                <input  class="radio_toggle" 
                        type="radio" 
                        value="1" 
                        name="jform[params][csv]" 
                        id="csv_1" 
                        <?php if($this->notification->params->csv == 1) echo "checked='checked'";?>
                />
              </label>
              <label class="btn" for="csv_0"><?php echo JText::_('JNO');?>
                <input  class="radio_toggle" 
                        type="radio" 
                        value="0" 
                        name="jform[params][csv]" 
                        id="csv_0" 
                        <?php if($this->notification->params->csv == 0) echo "checked='checked'";?>
                /> 
              </label>

            </div>

            <span class="help-block"><?php echo JText::_( 'INCLUDE_CSV_EXPLANATION' ); ?></span>

          </div>

        </div>
      </fieldset>
      
    </div>
    <input type="hidden" name="option" value="com_contentstats" />
    <input type="hidden" name="id" value="<?php echo $this->notification->id; ?>" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="controller" value="notification" />
  </form>
</div>

<script>

  var vm = {
    components: <?php echo json_encode($this->components); ?>,
    types: <?php echo json_encode($this->types); ?>,
    notification: <?php echo json_encode($this->notification); ?>,
    search_results: [],
    current_user_ids: [],
    inputemail: ""
  };
  new Vue({
    el: '#cs-app',
    data: vm,
    computed: {

        current_user_ids: function () {
          var ids = [];
          for (var i = this.notification.current_users.length - 1; i >= 0; i--) {
            ids.push(this.notification.current_users[i].id);
          };
          return ids.join();
        },

        current_emails: function () {
          var emails = [];
          for (var i = this.notification.current_emails.length - 1; i >= 0; i--) {
            emails.push(this.notification.current_emails[i]);
          };
          return emails.join();
        }

    },
    methods: {
      addToRecipientList: function (user) {
        this.notification.current_users.push(user);
      },
      removeFromRecipientList: function (user) {
        removeByAttr(this.notification.current_users, 'id', user.id); 
      },
      addToEmailList: function () {
        this.notification.current_emails.push(this.inputemail);
        this.inputemail = "";
        jQuery('#search_email').focus();
      },
      removeFromEmailList: function (email) {
        var index = this.notification.current_emails.indexOf(email);

        if (index > -1) {
           this.notification.current_emails.splice(index, 1);
        }
      }
    }
    
  });

</script>
