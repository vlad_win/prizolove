<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author        Germinal Camps
# copyright       Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license        http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites:       http://www.JoomlaThat.com
# Technical Support:  Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

$hours = $this->params->get('hours', '+00:00');

?>
<?php if( $this->notification->body ){ ?>
<?php echo $this->notification->body; ?><br><br>
<?php } ?>

<strong><?php echo JText::_('COMPONENT'); ?></strong>: <?php echo $this->notification->component; ?><br>
<strong><?php echo JText::_('TYPE_OF_ACTION'); ?></strong>: <?php echo ContentstatsHelper::getTypeName($this->notification->component, $this->notification->type); ?><br>
<strong><?php echo JText::_('ITEM'); ?></strong>: <?php echo $this->notification->reference_id ? $this->notification->reference_id : JText::_('JALL'); ?><br>
<strong><?php echo JText::_('USER'); ?></strong>: <?php echo $this->notification->user_id ? $this->notification->user_id : JText::_('JALL'); ?><br>
<br>
<strong><?php echo JText::_('FROM'); ?></strong>: <?php echo JHTML::_('date', strtotime($this->notification->date_in . ' ' .$hours .' hours'), JText::_('DATE_FORMAT_LC2')); ?><br>
<strong><?php echo JText::_('TO'); ?></strong>: <?php echo JHTML::_('date', strtotime($this->notification->date_out . ' ' .$hours .' hours'), JText::_('DATE_FORMAT_LC2')); ?><br>
<br>
<strong><?php echo JText::_('TOTAL'); ?></strong>: <?php echo $this->notification->total; ?><br>
<br>
<?php if( count($this->items) ){ ?>
<table border="0" cellpadding="10" cellspacing="0" class="cs_table">
  <thead>
    <tr>
      <th width="5"> <?php echo JText::_('ID'); ?></th>

      <th> <?php echo JText::_('COMPONENT'); ?></th>
      <th> <?php echo JText::_('USER'); ?></th>
      <th> <?php echo JText::_('USER_ID'); ?></th>

      <th> <?php echo JText::_('TYPE_OF_ACTION'); ?></th>
      <th> <?php echo JText::_('ITEM'); ?></th>
      <th> <?php echo JText::_('ITEM_ID'); ?></th>
      <th> <?php echo JText::_('DATE'); ?></th>
      <th> <?php echo JText::_('IP'); ?> </th>

      <th> <?php echo JText::_('Country'); ?></th>
      <th> <?php echo JText::_('State'); ?> </th>
      <th> <?php echo JText::_('City'); ?> </th>
    </tr>
  </thead>
  <?php

  $limit = (int)$this->params->get('max_email_rows', 50);

	for ($i=0, $n=count( $this->items ); $i < $n && $i < $limit; $i++)	{
		$row = &$this->items[$i];

		if(isset($this->countrynames[$row->country])) $countryname = $this->countrynames[$row->country] ;
		else $countryname = "";

	?>
  <tr>
    <td><?php echo $row->id; ?></td>
    <td><?php echo $row->component; ?></td>
    <td><?php if($row->user_id){ echo $row->username; } else { echo JText::_('UNREGISTERED'); } ?></td>
    <td><?php echo $row->user_id; ?></td>
    <td><?php echo $this->types[$row->component][$row->type] ? $this->types[$row->component][$row->type] : $row->type ; ?></td>
    <td><?php echo $this->item_name($row->component, $row->type, $row->reference_id, $row->id, $row->value, $row->valuestring); ?></td>
    <td><?php echo $row->reference_id; ?></td>
    <td><?php echo JHTML::_('date', strtotime($row->date_event . ' ' .$hours .' hours'), JText::_('DATE_FORMAT_LC2')); ?></td>
    <td><?php if($row->ip == "::1" || $row->ip == "127:0:0:1") echo JText::_('localhost'); else echo $row->ip; ?></td>

    <td><?php echo ucwords(strtolower($countryname)); ?></td>
    <td><?php echo ucwords(strtolower($row->state)); ?></td>
    <td><?php echo ucwords(strtolower($row->city)); ?></td>
  </tr>
  <?php } ?>

</table>
<?php } ?>
<br><br>
<strong><?php echo JText::_('NEXT_NOTIFICATION'); ?></strong>: <?php echo JHTML::_('date', strtotime($this->notification->nextdate . ' ' .$hours .' hours'), JText::_('DATE_FORMAT_LC2')); ?><br>
