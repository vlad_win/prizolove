<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class NotificationsViewNotification extends JViewLegacy
{

	public $_path = array(
		'template' => array(),
		'helper' => array()
	);
	
	public $_layout = 'default';
	
	public $template = "";

	function display($tpl = null)
	{

		$document	= JFactory::getDocument();
		
		$notification		= $this->get('Data');
		$components = ContentstatsHelper::getComponents() ;
		$types = ContentstatsHelper::getTypes() ;
		$countries = ContentstatsHelper::countries() ;

		$isNew		= ($notification->id < 1);

		$text = $isNew ? JText::_( 'New' ) : JText::_( 'Edit' );
		JToolBarHelper::title( JText::_( 'NOTIFICATION' ).': <small><small>[ '.$text.' ]</small></small>','notification' );
		
		JToolBarHelper::apply();
		JToolBarHelper::save();
		
		if ($isNew)  {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel( 'cancel', 'Close' );
		}

		$this->assignRef('notification', $notification);
		$this->assignRef('components', $components);
		$this->assignRef('countries', $countries);
		$this->assignRef('types', $types);

		parent::display($tpl);
	}

	function item_name($component, $type, $reference_id, $entry_id, $value = 0, $valuestring = ""){
		
		if(!isset($this->item_names[$component][$type][$reference_id]) || !$reference_id || $value || $valuestring){
		
			$dispatcher   = JDispatcher::getInstance();
			$plugin_ok = JPluginHelper::importPlugin('contentstats', $component);
			$results = $dispatcher->trigger('getItemName_'.$component, array($reference_id, $type, $entry_id));
			
			$this->item_names[$component][$type][$reference_id] = $results[0];
		}
		
		return $this->item_names[$component][$type][$reference_id] ;
		
	}
	
}