<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author              Germinal Camps
# copyright           Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license            http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites:           http://www.JoomlaThat.com
# Technical Support:  Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class StatsViewItems extends JViewLegacy
{
	var $item_names = array();
	
	function display($tpl = null)
	{
		JToolBarHelper::title(   JText::_( 'ENTRY_MANAGER' ), 'items' );

		$user = JFactory::getUser();

        if ($user->authorise('core.delete', 'com_contentstats')) {
            JToolBarHelper::deleteList(JText::_( 'SURE_ENTRIES' ));
        }

		if ($user->authorise('core.admin', 'com_contentstats') || $user->authorise('core.options', 'com_contentstats'))
		{
			JToolBarHelper::preferences( 'com_contentstats');
		}

		ContentstatsHelper::addSubmenu('items');
		$this->sidebar = JHtmlSidebar::render();
		
		$document	= JFactory::getDocument();
		
		$mainframe = JFactory::getApplication();

		$main_plugin_ok = JPluginHelper::importPlugin('system', 'contentstats');
		if(!$main_plugin_ok){
			$mainframe->enqueueMessage(JText::_('ALERT_SYSTEM_PLUGIN'));
		}
		$plugin_ok = JPluginHelper::importPlugin('contentstats');
		if(!$plugin_ok){
			$mainframe->enqueueMessage(JText::_('ALERT_EXTENSION_PLUGINS'));
		}

		$params = JComponentHelper::getParams( 'com_contentstats' );

		if($main_plugin_ok){
		
			$pagination = $this->get('Pagination');
			$keywords = $this->get('keywords');
			$components = $this->get('Components');
			$component_id = $this->get('ComponentId');
			$items = $this->get('Data');	
			$types = $this->get('Types');	
			$item_id = $this->get('ItemId');
			$type_id = $this->get('TypeId');
			$user_id = $this->get('UserId');
			$date_in = $this->get('DateIn');
			$date_out = $this->get('DateOut');
			$countries = $this->get('Countries');
			$country_id = $this->get('CountryId');
			$icons = $this->get('Icons');
		
			// push data into the template
			$this->assignRef('items', $items);	
			$this->assignRef('types', $types);	
			$this->assignRef('pagination', $pagination);
			$this->assignRef('keywords', $keywords);
			$this->assignRef('item_id', $item_id);
			$this->assignRef('type_id', $type_id);
			$this->assignRef('user_id', $user_id);
			$this->assignRef('date_in', $date_in);
			$this->assignRef('date_out', $date_out);
			$this->assignRef('params', $params);
			$this->assignRef('icons', $icons);

			$countrynames = ContentstatsHelper::countries() ;
			
			$this->assignRef('countrynames', $countrynames);
			
			$lists['order_Dir'] = $this->get('FilterOrderDir') ;
			$lists['order']     = $this->get('FilterOrder') ;
			
			$component_name = "";
			$thecountryname = "";

			// get list of sections for dropdown filter
			$javascript = "";//'onchange="this.form.submit();"';
			$lists['component_id'] = "<option value='0'>-- ".JText::_( 'ALL_COMPONENTS' )." --</option>";
			
			foreach($components as $component){
				//echo $component_id; die;
				if($component["component"] === $component_id) { $selected = "selected"; $component_name = $component["component"];}
				else $selected = "";
				$lists['component_id'] .= "<option value='".$component["component"]."' $selected>".$component["component"]."</option>";
			}
			$lists['component_id'] = "<select name='component_id' id='component_id' ".$javascript." >".$lists['component_id']."</select>";
			
			//country
			$lists['country_id'] = "<option value='0'>-- ".JText::_( 'ALL_COUNTRIES' )." --</option>";
			
			foreach($countries as $country){
				//echo $component_id; die;
				if($country["country"] === $country_id) { $selected = "selected"; if(isset($countrynames[$country["country"]])) $thecountryname = $countrynames[$country["country"]] ; }
				else $selected = "";
				if(isset($country["country"])) {
					if(isset($countrynames[$country["country"]])) $countryname = $countrynames[$country["country"]] ;
					else $countryname = "";
					$lists['country_id'] .= "<option value='".$country["country"]."' $selected>".ucwords(strtolower($countryname))."</option>";
				}
			}
			$lists['country_id'] = "<select name='country_id' id='country_id' ".$javascript.">".$lists['country_id']."</select>";
			
			$this->assignRef('lists', $lists);

			$this->assignRef('component_name', $component_name);
			$this->assignRef('country_name', $thecountryname);
			
			//$tpl = $params->get('viewformat');
			if(JRequest::getVar('layout') == "delete") $tpl = NULL;
		}
		else{
			$tpl = "alert";
		}
		
		parent::display($tpl);
	}
	
	function item_name($component, $type, $reference_id, $entry_id, $value = 0, $valuestring = ""){
		
		if(!isset($this->item_names[$component][$type][$reference_id]) || !$reference_id || $value || $valuestring){
		
			$dispatcher   = JDispatcher::getInstance();
			$plugin_ok = JPluginHelper::importPlugin('contentstats', $component);
			$results = $dispatcher->trigger('getItemName_'.$component, array($reference_id, $type, $entry_id));
			
			$this->item_names[$component][$type][$reference_id] = $results[0];
		}
		
		return $this->item_names[$component][$type][$reference_id] ;
		
	}

	function countries(){
		return ContentstatsHelper::countries();
	}

}