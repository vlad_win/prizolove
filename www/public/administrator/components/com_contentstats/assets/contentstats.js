function filter_options(component, type, reference_id){
	$('keywords').value = component ;
	$('type_id').value = type ;
	$('item_id').value = reference_id ;
	
	$('adminForm').submit();
}
function filter_user(user_id){
	$('user_id').value = user_id ;
	
	$('adminForm').submit();
}

jQuery(document).ready(function() {
  	jQuery("time.timeago").timeago();

  	jQuery( "#button_search_users" ).click(function() {
		search_users();
	});
});

function resetFilter() {
	jQuery('#keywords').val('');
	jQuery('#type_id').val('');
	jQuery('#item_id').val('');
	jQuery('#user_id').val('');
	jQuery('#component_id').val(0);
	jQuery('#country_id').val(0);
	jQuery('#date_in').val('');
	jQuery('#date_out').val('');

	jQuery('#adminForm').submit();
}

function search_users() {
	var searchword = jQuery('#search_user').val();
	var url = "index.php?option=com_contentstats&controller=notification&task=search_users&searchword=" + searchword ;

	jQuery.ajax({
		url: url,
		dataType: 'json',
		success: function(data)
		{
			vm.search_results = data;
		}
	});
}

var removeByAttr = function(arr, attr, value){
    var i = arr.length;
    while(i--){
       if( arr[i] 
           && arr[i].hasOwnProperty(attr) 
           && (arguments.length > 2 && arr[i][attr] === value ) ){ 

           arr.splice(i,1);

       }
    }
    return arr;
}