<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author                Germinal Camps
# copyright             Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license              http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites:             http://www.JoomlaThat.com
# Technical Support:    Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class NotificationsControllerNotifications extends NotificationsController
{
	
	function __construct()
	{
		JRequest::setVar('view', 'notifications');
		parent::__construct();

	}
	
	function remove()
	{
        $user = JFactory::getUser();
        if ($user->authorise('core.delete', 'com_contentstats')) {
            $model = $this->getModel('notifications');
            if(!$model->delete_items()) {
                $msg = JText::_( 'ERROR_DELETING_ENTRIES' );
            } else {
                $msg = JText::_( 'ENTRIES_DELETED' );
            }

            $this->setRedirect( 'index.php?option=com_contentstats&controller=notifications', $msg );
        } else {
            $link = 'index.php?option=com_contentstats&controller=notifications';
            $msg = JText::_('YOU_CANNOT_DELETE');

            $this->setRedirect($link, $msg);
        }

	}
  
}
