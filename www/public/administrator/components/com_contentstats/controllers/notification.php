<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class NotificationsControllerNotification extends NotificationsController
{

	function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask( 'add'  , 	'edit' );
		$this->registerTask( 'apply',	'save' );
	}

	function edit()
	{
		JRequest::setVar( 'view', 'notification' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);

		parent::display();
	}

	function save()
	{
		$model = $this->getModel('notification');

		if ($id = $model->store()) {
			$msg = JText::_( 'NOTIFICATION_SAVED' );
		} else {
			$msg = JText::_( 'ERROR_SAVING_NOTIFICATION' );
		}

		$task = JRequest::getCmd( 'task' );
		
		switch ($task)
		{
			case 'apply':
				$link = 'index.php?option=com_contentstats&controller=notification&task=edit&cid[]='. $id ;
				break;

			case 'save':
			default:
				$link = 'index.php?option=com_contentstats&controller=notifications';
				break;
		}
		
		$this->setRedirect($link, $msg);
	}

	function remove()
	{
		$model = $this->getModel('notification');
		if(!$model->delete()) {
			$msg = JText::_( 'ERROR_DELETE_NOTIFICATIONS' );
		} else {
			$msg = JText::_( 'NOTIFICATIONS_DELETED' );
		}

		$this->setRedirect( 'index.php?option=com_contentstats&controller=notifications', $msg );
	}

	function cancel()
	{
		$msg = JText::_( 'OPERATION_CANCELLED' );
		$this->setRedirect( 'index.php?option=com_contentstats&controller=notifications', $msg );
	}

	function search_users(){
		$mainframe = JFactory::getApplication();

		$db = JFactory::getDBO();
		$keywords = $db->escape(JRequest::getVar("searchword"));
		
		$where_clause[] = ' ( u.username LIKE "%'.$keywords.'%" OR  u.name LIKE "%'.$keywords.'%"OR  u.email LIKE "%'.$keywords.'%" ) ';
		
		// Build the where clause of the content record query
		$where_clause = (count($where_clause) ? ' WHERE '.implode(' AND ', $where_clause) : '');
		
		$query = ' SELECT u.* FROM #__users AS u ' . $where_clause ;
		$db->setQuery($query);
		$users = $db->loadObjectList();

		echo json_encode($users) ;

		$mainframe->close();
	}

	function publish()
	{
		$model = $this->getModel('notification');
		if(!$model->publish()) {
			$msg = JText::_( 'ERROR_PUBLISHING_NOTIFICATIONS' );
		} else {
			$msg = JText::_( 'NOTIFICATIONS_PUBLISHED' );
		}

		$this->setRedirect( 'index.php?option=com_contentstats&controller=notifications', $msg );
	}
	
	function unpublish()
	{
		$model = $this->getModel('notification');
		if(!$model->unpublish()) {
			$msg = JText::_( 'ERROR_UNPUBLISHING_NOTIFICATIONS' );
		} else {
			$msg = JText::_( 'NOTIFICATIONS_UNPUBLISHED' );
		}

		$this->setRedirect( 'index.php?option=com_contentstats&controller=notifications', $msg );
	}
}