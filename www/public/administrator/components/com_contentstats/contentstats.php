<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author              Germinal Camps
# copyright           Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license            http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites:           http://www.JoomlaThat.com
# Technical Support:  Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//new for Joomla 3.0
if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_contentstats'.DS.'helpers'.DS.'helpers.php');

if (!JFactory::getUser()->authorise('core.manage', 'com_contentstats'))
{
    return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

require_once( JPATH_COMPONENT.DS.'controller.php' );

// Require specific controller if requested
if($controller = JRequest::getWord('controller', 'items')) {

	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if (file_exists($path)) {
		require_once $path;
	} else {
		$controller = 'Items';
	}
}

switch($controller){

	case "notifications": 
	case "notification":
		
		$notifications = true;
		$prefix	= 'Notifications';
		break;
	
	default:
		$items = true ;
		$prefix	= 'Stats';
		$controller	= 'Items';
		break;
}

$lang = JFactory::getLanguage();
$lang->load('com_contentstats', JPATH_SITE);

$document = JFactory::getDocument();

//Vue
JHtml::_('jquery.framework');
$document->addScript('components/com_contentstats/assets/contentstats.js');
$document->addScript('components/com_contentstats/assets/jquery-timeago/jquery.timeago.js');
$document->addScript("components/com_contentstats/assets/js/vue.min.js");

//load CSS
$document->addStyleSheet('components/com_contentstats/assets/contentstats.css');
$document->addStyleSheet("components/com_contentstats/assets/bootstrap/css/bootstrap-glyphicons.css");
		
// Create the controller
$classname	= $prefix.'Controller'.$controller;

$controller	= new $classname( );

// Perform the Request task
$controller->execute( JRequest::getVar( 'task' ) );

// Redirect if set by the controller
$controller->redirect();