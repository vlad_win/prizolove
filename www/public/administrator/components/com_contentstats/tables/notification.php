<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// No direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.filesystem.file');

class TableNotification extends JTable
{

	var $id = null;
	var $component = null;
	var $value = null;
	var $valuestring = null;
	var $reference_id = null;
	var $type = null;
	var $ip = null;
	var $user_id = null;
	var $country = null;
	var $target = null;
	var $target_emails = null;
	var $params = null;
	var $nextdate = null;
	var $frequency = null;
	var $body = null;
	var $active = null;

	function __construct(& $db) {
		parent::__construct('#__content_statistics_notifications', 'id', $db);
	}
	
	function check(){

		return true;
	}
	  
}