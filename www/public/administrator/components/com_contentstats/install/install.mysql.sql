--
-- Estructura de la taula `#__content_statistics_notifications`
--

CREATE TABLE `#__content_statistics_notifications` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `component` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  `valuestring` varchar(255) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country` varchar(15) NOT NULL,
  `params` text NOT NULL,
  `target` text NOT NULL,
  `target_emails` text NOT NULL,
  `nextdate` datetime NOT NULL,
  `frequency` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB ;

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `#__content_statistics_notifications`
--
ALTER TABLE `#__content_statistics_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `component` (`component`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `type` (`type`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `country` (`country`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `#__content_statistics_notifications`
--
ALTER TABLE `#__content_statistics_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;