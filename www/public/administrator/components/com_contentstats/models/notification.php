<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class NotificationsModelNotification extends JModelLegacy
{

	function __construct()
	{
		parent::__construct();

		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}

	function setId($id)
	{
		$this->_id		= $id;
		$this->_data	= null;
	}

	function &getData()
	{
		$db = $this->getDBO();

		// Load the data
		if (empty( $this->_data )) {
			$query = $db->getQuery(true);
			$query
				->select('no.*')
				->from($db->qn('#__content_statistics_notifications', 'no'))
				->where($db->qn('id'). ' = ' . $this->_id);
			$db->setQuery( $query );
			$this->_data = $db->loadObject();

		}
		if (!$this->_data) {
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->name = "";
			$this->_data->body = "";
			$this->_data->active = 1;
			$this->_data->frequency = "daily";

			$this->_data->component = "";
			$this->_data->type = "";
			$this->_data->reference_id = "";
			$this->_data->user_id = "";
			
			$params = new stdClass();
			$params->time = 0;
			$params->weekday = 0;
			$params->csv = 1;

			$this->_data->params = $params;
			$this->_data->current_users = [];
			$this->_data->current_emails = [];
		}
		else{
			$this->_data->params = json_decode($this->_data->params);
			if(!empty($this->_data->target)){

				$query = $db->getQuery(true);
				$query
					->select('u.*')
					->from($db->qn('#__users', 'u'))
					->where($db->qn('id'). ' IN ('.$this->_data->target.')');

				$db->setQuery( $query );
				$this->_data->current_users = $db->loadObjectList();

			}
			else $this->_data->current_users = [];

			if(!empty($this->_data->target_emails)){
				$this->_data->current_emails = explode(",", $this->_data->target_emails);
			}
			else $this->_data->current_emails = [];

			if(!$this->_data->reference_id) $this->_data->reference_id = "";
			if(!$this->_data->user_id) $this->_data->user_id = "";
			
		}
		return $this->_data;
	}
	
	function store()
	{	
		$row = $this->getTable();

		$data = JRequest::get( 'post' );

		$data['params'] = json_encode($data['jform']['params']);

		if(!$data["nextdate"] ){

			$data["nextdate"] = ContentstatsHelper::getRealNextNotificationDate($data);

		}
		
		// Bind the form fields to the album table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		// Make sure the hello record is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Store the web link table to the database
		if (!$row->store()) {
			$this->setError( $row->getErrorMsg() );
			return false;
		}

		return $row->id;
	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row = $this->getTable();

		if (count( $cids )) {
			foreach($cids as $cid) {
				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}
		}
		return true;
	}

	function getMatchingInstantNotifications($entry){

		$db = $this->getDBO();

		$query = $db->getQuery(true);

		if ($entry->component) {
			$query->where($db->qn('no.component') . ' = '. $db->q($entry->component));
		}
		if ($entry->type) {
			$query->where($db->qn('no.type') . ' = '. (int)$entry->type);
		}
		if ($entry->reference_id) {
			$query->where('( ' . $db->qn('no.reference_id') . ' = '. (int)$entry->reference_id . ' OR '.$db->qn('no.reference_id').' = 0 ) ');
		}
		if ($entry->user_id) {
			$query->where('( ' . $db->qn('no.user_id') . ' = '. (int)$entry->user_id . ' OR '.$db->qn('no.user_id').' = 0 ) ');
		}

		$query->where($db->qn('no.frequency') . ' = '. $db->q('everytime'));
		$query->where($db->qn('no.active') . ' = 1 ');

		$query
			->select('no.*')
			->from($db->qn('#__content_statistics_notifications', 'no'));
				 
		$db->setQuery( $query );
		$notifications = $db->loadObjectList();

		foreach($notifications as $notification){
			
			$this->sendMail($notification, $entry);
			
		}
		
		return count($invoices);
	}

	function getDelayedNotifications(){

		$params = JComponentHelper::getParams( 'com_contentstats' );

		$db = $this->getDBO();

		$query = $db->getQuery(true);
		$query
			->select('no.*')
			->from($db->qn('#__content_statistics_notifications', 'no'))
			->where($db->qn('no.nextdate') . ' <= '. $db->q(date("Y-m-d H:i:s")))
			->where($db->qn('no.frequency') . ' != ' . $db->q('everytime'))
			->where($db->qn('no.active') . ' = 1 ');
				 
		$db->setQuery( $query );
		$notifications = $db->loadObjectList();

		foreach($notifications as $notification){
			
			//set the next recurrency date
			//swith for getNextNotificationDate if we want all the dates in between to be calculated and sent.
			$nextdate = ContentstatsHelper::getRealNextNotificationDate($notification);
			
			$query = $db->getQuery(true);
			$query
				->update('#__content_statistics_notifications')
				->set($db->qn('nextdate') . ' = ' . $db->q($nextdate))
				->where($db->qn('id') . ' = '. (int)$notification->id );

			$db->setQuery( $query );
			$db->query();

			//get the data
			$query = $db->getQuery(true);

			if ($notification->component) {
				$query->where($db->qn('st.component') . ' = '. $db->q($notification->component));
			}
			if ($notification->type) {
				$query->where($db->qn('st.type') . ' = '. (int)$notification->type);
			}
			if ($notification->reference_id) {
				$query->where($db->qn('st.reference_id') . ' = '. (int)$notification->reference_id);
			}
			if ($notification->user_id) {
				$query->where($db->qn('st.user_id') . ' = '. (int)$notification->user_id);
			}

			switch ($notification->frequency) {
				case 'daily':
					$date_in = $notification->nextdate ." - 1 days" ;
					break;
				case 'weekly':
					$date_in = $notification->nextdate ." - 1 weeks" ;
					break;
				case 'monthly':
					$date_in = $notification->nextdate ." - 1 months" ;
					break;
				case 'yearly':
					$date_in = $notification->nextdate ." - 1 years" ;
					break;
				
				default:
					$date_in = $notification->nextdate ;
					break;
			}

			$date_in = strtotime($date_in) ;
			$date_in = date("Y-m-d H:i:s", $date_in) ;

			$notification->date_in = $date_in;
			$notification->date_out = $notification->nextdate;
			$notification->nextdate = $nextdate;

			$notification->params = json_decode($notification->params);

			$query->where($db->qn('st.date_event') . ' >= '. $db->q($date_in));
			$query->where($db->qn('st.date_event') . ' <= '. $db->q($notification->nextdate));
			
			$limit = (int)$params->get('max_email_rows', 50);
			$limit_csv = (int)$params->get('maxrows', 5000);

			$query
				->select(array('st.*', $db->qn('u.name', 'username')))
				->from($db->qn('#__content_statistics', 'st'))
				->leftJoin($db->qn('#__users', 'u') . ' ON (' . $db->qn('u.id') . ' = ' . $db->qn('st.user_id') . ')')
				->order($db->qn('st.id') . ' DESC');
		
			if($notification->params->csv){
				$items = $this->_getList($query, 0, $limit_csv);
			}
			else{
				if($limit) $items = $this->_getList($query, 0, $limit);
				else $items = $this->_getList($query);
			}

			$notification->total = $this->_getListCount($query);

			$this->sendMail($notification, $notification, $items);
			
		}
		
		return count($notifications);
	}

	function sendMail($notification, $item, $items = false){

		$db = JFactory::getDBO();

		$lang = JFactory::getLanguage();
		$lang->load('com_contentstats', JPATH_ADMINISTRATOR);

		$params = JComponentHelper::getParams( 'com_contentstats' );
		
		$from = $params->get('email_email') ;
		$from_name = $params->get('email_name') ;

		$subject = $notification->name ;

		$view		= $this->getNotificationView();
		
		$view->notification		= $notification;
		$view->item				= $item;
		$view->items			= $items;
		$view->types			= ContentstatsHelper::getTypes();
		$view->params			= $params;
		$view->countrynames 	= ContentstatsHelper::countries();
		
		$view->_path['template'][1] = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'views'.DS.'notification'.DS.'tmpl' ;
		
		if($items !== false){	
			$plantilla = 'email_detailed' ;
		}
		else {
			$plantilla = 'email' ;
		}
		
		$message = $view->loadTemplate( $plantilla );

		$toMails = array();

		//recipients
		if($notification->target){
			$query = $db->getQuery(true);
			$query
				->select($db->qn('email'))
				->from($db->qn('#__users', 'u'))
				->where($db->qn('id'). ' IN ('.$notification->target.')');
			$db->setQuery( $query );
			$toMails = $db->loadColumn();
		}

		//recipient emails
		if($notification->target_emails){
			$toMails2 = explode(",", $notification->target_emails);
			$toMails = array_merge($toMails, $toMails2);
		}

		$toMails = array_unique($toMails);
		
		$mail = JFactory::getMailer();
		$mail->addRecipient($toMails);
		$mail->setsender(array($from, $from_name));
		$mail->setSubject($subject);
		$mail->setbody($message);
		$mail->isHTML(true);

		if($notification->params->csv){

			$filename = str_replace(" ", "_", preg_replace('/[^A-Za-z0-9\. _]/', '', strtolower($subject))) . "_" . date('Y_m_d') . ".csv";
			$location = JPATH_CACHE.DS.$filename;
			$csv = $this->getCSVfilepath($items, $location, $view);

			if($csv) $mail->addAttachment($location, $filename) ; 

		}
		
		//send only if there are recipients
		if(!empty($toMails)) $sent = $mail->Send();
		else $sent = false;

		return $sent ;
		
	}

	function getCSVfilepath($items, $location, $view){

		$params = JComponentHelper::getParams( 'com_contentstats' );

		$max = $params->get('maxrows', 5000);
		
		$total = count($items);

		if($total > $max){

			return false;

		}
		else{
		
			$content = $view->loadTemplate( 'csv' );
			
			return file_put_contents($location, $content);
		
		}

	}
	
	function getNotificationView($type = "html")
	{
		if (!class_exists( 'NotificationsViewNotification' ))
		{
			// Build the path to the model based upon a supplied base path
			$path = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'views'.DS.'notification'.DS.'view.'.$type.'.php';
			$false = false;

			// If the model file exists include it and try to instantiate the object
			if (file_exists( $path )) {
				require_once( $path );
				if (!class_exists( 'NotificationsViewNotification' )) {
					JError::raiseWarning( 0, 'View class NotificationsViewNotification not found in file.' );
					return $false;
				}
			} else {
				JError::raiseWarning( 0, 'View NotificationsViewNotification not supported. File not found.' );
				return $false;
			}
		}

		$view = new NotificationsViewNotification();
		return $view;
	}

	function publish()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'default', 'array' );

		if (count( $cids )) {
			foreach($cids as $cid) {

				$query = ' UPDATE #__content_statistics_notifications SET active = 1 WHERE id = '. (int)$cid . ' LIMIT 1 ';
				$this->_db->setQuery($query);
				$this->_db->query();
			}
		}
		return true;
	}

	function unpublish()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'default', 'array' );

		if (count( $cids )) {
			foreach($cids as $cid) {
				$query = ' UPDATE #__content_statistics_notifications SET active = 0 WHERE id = '. (int)$cid . ' LIMIT 1 ';
				$this->_db->setQuery($query);
				$this->_db->query();
			}
		}
		return true;
	}

}