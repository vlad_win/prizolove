<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2013 JoomlaContentStatistics.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaContentStatistics.com
# Technical Support:	Forum - http://www.JoomlaContentStatistics.com/forum
-------------------------------------------------------------------------*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NotificationsModelNotifications extends JModelLegacy
{

	var $_data;
	var $_total = null;
	var $_pagination = null;
	var $_keywords = null;

	function __construct(){
		parent::__construct();

		$mainframe = JFactory::getApplication();

		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('contentstats.notifications.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest('contentstats.notifications.limitstart', 'limitstart', 0, 'int');
		$keywords = $mainframe->getUserStateFromRequest('contentstats.notifications.keywords','keywords','','keywords');
		
		$filter_order     = $mainframe->getUserStateFromRequest('contentstats.notifications.filter_order', 'filter_order', 'nt.id', 'cmd' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest('contentstats.notifications.filter_order_Dir', 'filter_order_Dir', 'DESC', 'word' );
		
		$this->setState('filter_order', $filter_order);
		$this->setState('filter_order_Dir', $filter_order_Dir);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		
		$this->setState('keywords', $keywords);
		
	}
	
	function getTotal()
	{
 	// Load the content if it doesn't already exist
		if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);	
		}
		return $this->_total;
	}

	function getPagination()
	{
 	// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
		return $this->_pagination;
	}


	function getKeywords(){
		if (empty($this->_keywords)) {
			$this->_keywords = $this->getState('keywords')	;
		}
		return $this->_keywords;
	}
	
	function getFilterOrder(){
		return  $this->getState('filter_order') ;
	}

	function getFilterOrderDir(){
		return  $this->getState('filter_order_Dir') ;
	}

	function _buildContentOrderBy()
	{

		$filter_order     = $this->getState('filter_order' ) ;
		$filter_order_Dir = $this->getState('filter_order_Dir') ;

		$orderby = ' ORDER BY '.$filter_order.' '.$filter_order_Dir . ' ';

		return $orderby;
	}

	function _buildQuery()
	{
		
		$keywords = $this->getKeywords();
		
		$where_clause = array();

		if ($keywords != "")
			$where_clause[] = ' ( nt.name LIKE "%'.$keywords.'%" ) ';
		
		$orderby = $this->_buildContentOrderBy();
		
		// Build the where clause of the content record query
		$where_clause = (count($where_clause) ? ' WHERE '.implode(' AND ', $where_clause) : '');

		$query = ' SELECT nt.* '
				. ' FROM #__content_statistics_notifications AS nt '
				. $where_clause
				. $orderby
		;
		
		return $query;
	}
	
	function getTypes(){
		if (empty( $this->_types )){

			$params = JComponentHelper::getParams( 'com_contentstats' );
			$format = $params->get('viewformat');
			if($format == "stream") $stream = true ;
			else $stream = false ;
			
			$dispatcher   = JDispatcher::getInstance();
			$plugin_ok = JPluginHelper::importPlugin('contentstats');
			$results = $dispatcher->trigger('getTypes', array($stream));
			
			$this->_types = array();
			
			foreach($results as $result){
				$this->_types[$result->component] = $result->options;
				$this->_icons[$result->component] = isset($result->icons) ? $result->icons : array();	
			}
			
		}
		
		return $this->_types;

	}

	function getIcons(){
		return $this->_icons;
	}
	
	function getData(){

		if (empty( $this->_data )){
			$query = $this->_buildQuery();

			if(JRequest::getVar('task') == "export") $this->_data = $this->_getList($query);
			else $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			
		}

		return $this->_data;

	}
	function getComponents()
	{
		if (empty( $this->components )){
			$query = 	' SELECT DISTINCT component '
			. ' FROM #__content_statistics '
			.' ORDER BY component ' 
			;
			$this->_db->setQuery( $query );
			$this->components = $this->_db->loadAssocList('component');
		}
		
		return $this->components;

	}
	
	function getCountries()
	{
		if (empty( $this->countries )){
			$query = 	' SELECT DISTINCT country '
			. ' FROM #__content_statistics '
			.' ORDER BY country ' 
			;
			$this->_db->setQuery( $query );
			$this->countries = $this->_db->loadAssocList('country');
		}
		
		return $this->countries;

	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row = $this->getTable('notification');
		
		if (count( $cids )) {
			foreach($cids as $cid) {
				
				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}
		}
		return true;
	}
}