<?php
defined('_JEXEC') or die;

/**
 * View class for a list of banners.
 *
 * @since  1.6
 */
class OrderPageViewOrderPage extends JViewLegacy
{
	
	public function display($tpl = null)
	{		

		$this->addToolBar();
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors), 500);
		}

		

		$this->sidebar = JHtmlSidebar::render();

		return parent::display($tpl);
	}

	
	protected function addToolbar()
	{
		JToolBarHelper::preferences('com_orderpage');
		
		
	}	
}
