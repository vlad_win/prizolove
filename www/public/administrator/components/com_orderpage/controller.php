<?php

defined('_JEXEC') or die;


class OrderPageController extends JControllerLegacy
{
	
	public function display($cachable = false, $urlparams = array())
	{	
		
		$view   = $this->input->get('view', 'orderpage');
		$layout = $this->input->get('layout', 'default');
		
		return parent::display();
	}
}
