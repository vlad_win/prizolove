<?php

// No direct access
defined('_JEXEC') or die('Restricted access');

?>
<form action="<?php echo JRoute::_('index.php?option=com_prizolove&layout=edit&id=' . (int) $this->item->id); ?>"
      method="post" name="adminForm" id="adminForm">
    <div class="form-horizontal">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_PRIZOLOVE_PROMO_ITEMS_PROMO_ITEM_DETAILS'); ?></legend>
            <div class="row-fluid">
                <div class="span6">
                    <?php foreach ($this->form->getFieldset() as $field): ?>
                        <?php
                        $dataShowOn = '';
                        $groupClass = $field->type === 'Spacer' ? ' field-spacer' : '';
                        ?>
                        <?php if ($field->showon) : ?>
                            <?php JHtml::_('jquery.framework'); ?>
                            <?php JHtml::_('script', 'jui/cms.js', array('version' => 'auto', 'relative' => true)); ?>
                            <?php $dataShowOn = ' data-showon=\'' . json_encode(JFormHelper::parseShowOnConditions($field->showon, $field->formControl, $field->group)) . '\''; ?>
                        <?php endif; ?>
                        <div class="control-group"<?php echo $dataShowOn; ?>>
                            <div class="control-label"><?php echo $field->label; ?></div>
                            <div class="controls"><?php echo $field->input; ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </fieldset>
    </div>
    <input type="hidden" name="task" value="promoitem.edit" />
<?php echo JHtml::_('form.token'); ?>