<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_CREDITFORM
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

JHtml::_('formbehavior.chosen', 'select');


$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.order_id';

?>

<form action="index.php?option=com_prizoloveorders&view=orders" method="post" id="adminForm" name="adminForm">
    <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
                <?php //echo JText::_('COM_PRIZOLOVE_ORDERS_NUM'); ?>
                <?php echo JHtml::_('searchtools.sort', 'COM_PRIZOLOVE_ORDERS_NUM', 'a.order_id', $listDirn, $listOrder, null, 'desc'); ?>
            </th>			
			<th width="3%">
                <?php echo JText::_('COM_PRIZOLOVE_ORDERS_DATE'); ?>
			</th>
			<th width="3%">
				<?php echo JText::_('COM_PRIZOLOVE_ORDERS_CUSTOMER_EMAIL') ;?>
			</th>
			<th width="12%">
				<?php echo JText::_('COM_PRIZOLOVE_ORDERS_PRODUCT_NAME'); ?>
			</th>
			<th width="12%">
				<?php echo JText::_('COM_PRIZOLOVE_ORDERS_CLICK_ID'); ?>
			</th>
			<th width="12%">
				<?php echo JText::_('COM_PRIZOLOVE_ORDERS_ADDRESS_DELIVERY'); ?>
			</th>
			<th width="12%">
				<?php echo JText::_('COM_PRIZOLOVE_ORDERS_COMMENT'); ?>
			</th>
			<th width="5%">
				<?php echo JText::_('COM_PRIZOLOVE_ORDER_SENT_LABEL'); ?>
			</th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
                        $link = JRoute::_('index.php?option=com_prizoloveorders&task=order.edit&order_id=' . $row->order_id);
                ?>
    
					<tr>
						<td><?php echo $row->order_id ?></td>
						<td>
            <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_PRIZOLOVE_ORDERS_EDIT_ORDER'); ?>">
								<?php echo $row->date; ?>
							</a>								
						</td>
                        <td align="center">
							<?php echo $row->customer_phone .', '. $row->customer_name.', '. $row->customer_email; ?>
						</td>
						<td align="center">
							<?php echo $row->product_name; ?>
						</td>                        
						<td align="center">
							<?php echo $row->click_id; ?>
						</td>                        
						<td align="center">
							<?php echo $row->address_delivery; ?>
						</td>                        
						<td align="center">
							<?php echo $row->comment; ?>
						</td>                        
						<td align="center">
							<?php 
							switch($row->sent) {
								case 1: echo JText::_('COM_PRIZOLOVE_ORDER_SENT');break;
								case 2: echo JText::_('COM_PRIZOLOVE_CUSTOMER_NOT_ACCESSABLE');break;
								default: JText::_('COM_PRIZOLOVE_ORDER_NEW');break;
							}
							?>
						</td>                        
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>

    
	<?php echo JHtml::_('form.token'); ?>
</form>

