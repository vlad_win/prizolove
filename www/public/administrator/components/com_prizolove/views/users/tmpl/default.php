<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_CREDITFORM
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

JHtml::_('formbehavior.chosen', 'select');


$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.subid';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_prizolove&task=users.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

?>

<form action="index.php?option=com_prizolove&view=users" method="post" id="adminForm" name="adminForm">
    <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
                <?php //echo JText::_('COM_PRIZOLOVE_USERS_NUM'); ?>
                <?php echo JHtml::_('searchtools.sort', 'COM_PRIZOLOVE_SUBID', 'a.subid', $listDirn, $listOrder, null, 'desc'); ?>
            </th>			
			<th width="2%">
                <?php echo JText::_('COM_PRIZOLOVE_USERS_DATE'); ?>
			</th>
			<th width="2%">
				<?php echo JText::_('COM_PRIZOLOVE_USERS_CONFIRMED_DATE'); ?>
			</th>			
			<th width="3%">
				<?php echo JText::_('COM_PRIZOLOVE_USERS_EMAIL') ;?>
			</th>
			<th width="3%">
				<?php echo JText::_('COM_PRIZOLOVE_USERS_NAME') ;?>
			</th>
			<th width="3%">
				<?php echo JText::_('COM_PRIZOLOVE_USERS_NUM_LISTS') ;?>
			</th>
			<th width="3%">
				<?php echo JText::_('COM_PRIZOLOVE_USERS_CONFIRMED'); ?>
			</th>			
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="4">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
				<td colspan="2">
					<?php echo $this->pagination->getResultsCounter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
					//$link = JRoute::_('index.php?option=com_prizoloveorders&task=order.edit&order_id=' . $row->order_id);
				?>
    
					<tr>
						<td><?php echo $row->subid ?></td>						
                        <td align="center">
							<?php echo date('Y.m.d H:i:s',$row->created); ?>
						</td>
						<td align="center">
							<?php if ($row->confirmed_date) echo date('Y.m.d H:i:s',$row->confirmed_date); ?>							
						</td>            
						<td align="center">
							<?php echo $row->email; ?>
						</td>                        
						<td align="center">
							<?php echo $row->name; ?>
						</td>                        
						<td align="center">
							<?php echo $row->lists; ?>
						</td>                        
						<td align="center">
							<?php echo $row->confirmed; ?>
						</td>                        					
						            
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>

    
	<?php echo JHtml::_('form.token'); ?>
</form>
<script>
    jQuery(function($) {
        $('.js-stools-btn-clear').on('click', function() {
            $('input[name="task"]').val('');
        });
    });
</script>
