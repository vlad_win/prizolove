<?php

// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::root(true) . '/templates/shaper_helix3/imgareaselect/css/imgareaselect-default.css');
$doc->addScript(JUri::root(true) . '/templates/shaper_helix3/imgareaselect/scripts/jquery.imgareaselect.pack.js');
$data = $this->form->getFieldset();
$presets = array();
?>
<form action="<?php echo JRoute::_('index.php?option=com_prizolove&layout=edit&id=' . (int) $this->item->id); ?>"
      method="post" name="adminForm" id="adminForm">
    <div class="form-horizontal">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_PRIZOLOVE_EVENTS_EVENT_DETAILS'); ?></legend>
            <div class="row-fluid">
                <div class="span6">
					<?php foreach ($this->form->getFieldset() as $field): ?>
                        <?php
                        $dataShowOn = '';
                        $groupClass = $field->type === 'Spacer' ? ' field-spacer' : '';
                        ?>
                        <?php if ($field->showon) : ?>
                            <?php JHtml::_('jquery.framework'); ?>
                            <?php JHtml::_('script', 'jui/cms.js', array('version' => 'auto', 'relative' => true)); ?>
                            <?php $dataShowOn = ' data-showon=\'' . json_encode(JFormHelper::parseShowOnConditions($field->showon, $field->formControl, $field->group)) . '\''; ?>
                        <?php endif; ?>
                        <div class="control-group"<?php echo $dataShowOn; ?>>
                            <div class="control-label"><?php echo $field->label; ?></div>
                            <div class="controls"><?php echo $field->input; ?></div>
                            <?php
                                if ($field->fieldname == 'findme' && $field->value['image']) {
                                    $imgParams = getimagesize(JUri::root().''.$field->value['image']);

                                    if ($field->value['width']
                                        && $field->value['height']
                                        && $field->value['top']
                                        && $field->value['left']
                                    ) {
                                        $presets = $field->value;
                                    }
                                    echo '<div style="width: '.$imgParams[0].'px;"><img id="imageFindMe"src="/'.$field->value['image'].'" alt=""></div>';
                                }
                            ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </fieldset>
    </div>
    <input type="hidden" name="task" value="product.edit" />

<?php echo JHtml::_('form.token'); ?>
<script>
    <?php if ($presets) : ?>
        var leftOffset = <?=$presets['left'];?>;
        var topOffset = <?=$presets['top'];?>;
        var selectionWidth = <?=$presets['width'];?>;
        var selectionHeight = <?=$presets['height'];?>;
        /* alert(jQuery('#imageFindMe').width() - leftOffset);
        alert(jQuery('#imageFindMe').width() - (leftOffset - selectionWidth));
        alert(jQuery('#imageFindMe').height() - topOffset);
        alert(jQuery('#imageFindMe').height() - (topOffset - selectionHeight)); */
    <?php endif; ?>

    jQuery('img#imageFindMe').imgAreaSelect({
        handles: true,
        <?php if ($presets) : ?>
        x1: leftOffset,
        y1: topOffset,
        x2: leftOffset + selectionWidth,
        y2: topOffset +  selectionHeight,
        <?php endif; ?>
        onSelectEnd: function(img, selection) {
            var leftOffset = selection.x1;
            var topOffset = selection.y1;
            jQuery('input[name="jform[findme][width]"]').val(selection.width);
            jQuery('input[name="jform[findme][height]"]').val(selection.height);
            jQuery('input[name="jform[findme][top]"]').val(topOffset);
            jQuery('input[name="jform[findme][left]"]').val(leftOffset);
        }
    });
</script>
