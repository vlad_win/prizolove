<?php

defined('_JEXEC') or die('Restricted access');

class PrizoloveControllerBonus extends JControllerForm
{
    public function batch($model = null)
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Set the model
        $model = $this->getModel('bonus', '', array());

        // Preset the redirect
        $this->setRedirect(JRoute::_('index.php?option=com_prizolove&view=bonuses' . $this->getRedirectToListAppend(), false));

        return parent::batch($model);
    }
}