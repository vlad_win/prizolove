<?php

defined('_JEXEC') or die('Restricted access');
 

class PrizoloveControllerUsers extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Users', $prefix = 'PrizoloveModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

    public function export()
    {
        $model = $this->getModel();

        $this->addHeaders();

        $rows = $model->export();

        echo '"Subid";"Registration Date";"Confirmation Date";"Email";"Name";"Phone";"Lists";"Confirmed";'."\r\n";

        foreach($rows as $row) {

            foreach($row as $colName=>$col) {
                if ($col && ($colName == 'created' || $colName == 'confirmed_date')) {
                    $col = date('Y-m-d H:i:s', $col);
                }
                $col = @iconv('UTF-8','WINDOWS-1251',htmlspecialchars($col));
                echo '"'.$col.'";';
            }
            echo "\r\n";
        }

        exit();

    }

    function addHeaders($fileName = 'users')
    {
        $fileName = substr(preg_replace('#[^a-z0-9_-]#i','_',$fileName),0,50);
        @ob_clean();

        header("Pragma: public");
        header("Expires: 0"); // set expiration time
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        header("Content-Disposition: attachment; filename=".$fileName.".csv");

        header("Content-Transfer-Encoding: binary");
    }
}