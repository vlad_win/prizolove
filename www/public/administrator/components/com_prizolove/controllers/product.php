<?php

defined('_JEXEC') or die('Restricted access');

class PrizoloveControllerProduct extends JControllerForm
{
    public function batch($model = null)
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Set the model
        $model = $this->getModel('product', '', array());

        // Preset the redirect
        $this->setRedirect(JRoute::_('index.php?option=com_prizolove&view=products' . $this->getRedirectToListAppend(), false));

        return parent::batch($model);
    }
}