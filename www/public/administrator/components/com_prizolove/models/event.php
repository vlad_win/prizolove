<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


class PrizoloveModelEvent extends JModelAdmin
{
    /**
     * Method to get a table object, load it if necessary.
     *
     * @param   string  $type    The table name. Optional.
     * @param   string  $prefix  The class prefix. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return  JTable  A JTable object
     *
     * @since   1.6
     */
    public function getTable($type = 'Event', $prefix = 'PrizoloveTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the record form.
     *
     * @param   array    $data      Data for the form.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  mixed    A JForm object on success, false on failure
     *
     * @since   1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm(
            'com_prizolove.event',
            'event',
            array(
                'control' => 'jform',
                'load_data' => $loadData
            )
        );

        if (empty($form))
        {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState(
            'com_prizolove.edit.event.data',
            array()
        );

        if (empty($data))
        {
            $data = $this->getItem();
        }
        switch ($data->puzzle_type) {
            case 'missing_letters':
                $data = $this->restoreMissingLettersParams($data);
                break;
            case 'images':
                $data = $this->restoreImagesParams($data);
                break;
            case 'millionaire':
                $data = $this->restoreMillionaireParams($data);
                break;
            case 'findme':
                $data = $this->restoreFindme($data);
                break;
        }


        return $data;
    }

    protected function prepareTable($table)
    {
        $date = JFactory::getDate();
        $table->modified    = $date->toSql();

        if (empty($table->id)) {
            $table->created    = $date->toSql();
        }
    }

    public function save($data)
    {
        switch($data['puzzle_type']) {
            case 'missing_letters':
                $data = $this->processMissingLetters($data);
                break;
            case 'images':
                $data = $this->processImages($data);
                break;
            case 'millionaire':
                $data = $this->processMillionaire($data);
                break;
            case 'findme':
                $data = $this->processFindme($data);
                break;
        }
        /* if ($data['categories']) {
            $this->updateCategories($data['categories'], $data['id']);
        }
        */
        return parent::save($data);
    }

    public function processMissingLetters($data)
    {
        $data['params'] = array();

        if ($data['missing_letters']['image']) $data['params']['image'] = $data['missing_letters']['image'];
        if ($data['missing_letters']['question']) $data['params']['question'] = $data['missing_letters']['question'];
        if ($data['missing_letters']['template']) $data['params']['template'] = $data['missing_letters']['template'];
        if ($data['missing_letters']['answer']) $data['params']['answer'] = $data['missing_letters']['answer'];
        $data['params'] = json_encode($data['params']);
        return $data;

    }

    public function restoreMissingLettersParams($data)
    {
        foreach($data->params as $key=>$value) {
            $data->missing_letters[$key] = $value;
        }
        return $data;
    }

    public function restoreImagesParams($data)
    {
        if (!empty($data->params['images'])) {
            foreach($data->params['images'] as $key=>$image) {
                $data->images['image'.$key] = $image;
            }
        }
        if (!empty($data->params['images_params'])) $data->images_params = $data->params['images_params'];
        return $data;
    }

    public function restoreMillionaireParams($data)
    {
        if (!empty($data->params['questions'])) {
            $data->millionaire_question = $data->params['questions'];
        }
        return $data;
    }

    public function restoreFindme($data)
    {
        if (!empty($data->params['findme'])) {
            $data->findme = $data->params['findme'];
        }
        return $data;
    }

    public function processImages($data)
    {
        $data['params'] = array();
        if ($data['images']) {
            foreach($data['images'] as $image) {
                $data['params']['images'][] = $image;
            }
        }
        if ($data['images_params']) $data['params']['images_params'] = $data['images_params'];
        $data['params'] = json_encode($data['params']);
        return $data;
    }

    public function processMillionaire($data)
    {
        $data['params'] = array();

        if ($data['millionaire_question']) {
            foreach($data['millionaire_question'] as $question) {
                $data['params']['questions'][] = $question;
            }
        }
        $data['params'] = json_encode($data['params']);

        return $data;
    }

    public function processFindme($data)
    {
        $data['params'] = array();
        if ($data['findme']) {

            $data['params']['findme'] = $data['findme'];

        }
        $data['params'] = json_encode($data['params']);

        return $data;
    }



}