<?php
defined('_JEXEC') or die('Restricted access');


class PrizoloveModelUsers extends JModelList
{
    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @see     JControllerLegacy
     * @since   1.6
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                'date', 'a.created',
                'subid', 'a.subid',
            );
        }

        parent::__construct($config);
    }

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Initialize variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

        // Filter by search in title
        $search = $this->getState('filter.search');

		$date_start = $this->getState('filter.date_start');
		$date_end = $this->getState('filter.date_end');	
		
		$confirmed = $this->getState('filter.confirmed','-1');	
		
		
		

		// Create the base select statement.
		$query->select('a.subid, a.created, a.confirmed_date, a.email, a.name, a.phone, COUNT(c.subid) lists, a.confirmed')
			  ->from($db->quoteName('#__acymailing_subscriber', 'a'))                      
			  ->join('LEFT', $db->quoteName('#__acymailing_listsub', 'c') . ' ON a.subid = c.subid AND c.status = 1')
			  ->group('a.subid')
	        ;

        if ($date_start == '0000-00-00 00:00:00') unset($date_start);
        if ($date_end == '0000-00-00 00:00:00') unset($date_end);

        if (!empty($search))
        {
            $search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
            $query->where('(a.email LIKE ' . $search . ')');
        }
	
		if (!empty($date_start) && !empty($date_end)) {
			
			$date_start = new DateTime($date_start);
			$date_end = new DateTime($date_end . ' 23:59:59');
			
			$query->where('a.created >= '.$date_start->getTimestamp().' AND a.created <= '.$date_end->getTimestamp());
		} else if (!empty($date_start)) {			
			
			$date_start = new DateTime($date_start);
			$query->where('a.created >= '.$date_start->getTimestamp());
		} else if (!empty($date_end)) {
			$date_end = new DateTime($date_end . ' 23:59:59');
			$query->where('a.created <= '.$date_end->getTimestamp());
		}	

		switch($confirmed) {
			case 0: 
				$query->where('a.confirmed_date = 0');
				break;
			case 1: 
				$query->where('a.confirmed_date > 0');
				break;			
			case -2: 
				$query->where('a.confirmed_date > 0');
				$query->having('COUNT(c.subid) = 0');
				break;			
		}
		
        $listOrdering  = $this->state->get('list.ordering', 'subid');
        $orderDirn     = $this->state->get('list.direction', 'DESC');

        $query->order($db->escape($listOrdering) . ' ' . $db->escape($orderDirn));

		return $query;
	}

    public function export()
    {
        $jinput = JFactory::getApplication()->input;

        $filters = $jinput->getArray(
            array(
                'filter'=> array(
                    'search' => 'string',
                    'date_start' => 'string',
                    'date_end' => 'string',
                    'confirmed' => 'int'
                )
            )
        );

        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);


        $query->select('a.subid, a.created, a.confirmed_date, a.email, a.name, a.phone, COUNT(c.subid) lists, a.confirmed')
            ->from($db->quoteName('#__acymailing_subscriber', 'a'))
            ->join('LEFT', $db->quoteName('#__acymailing_listsub', 'c') . ' ON a.subid = c.subid AND c.status = 1')
            ->group('a.subid')
        ;

        if (!empty($filters['filter']['search']))
        {
            $search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($filters['filter']['search']), true) . '%'));
            $query->where('(c.email LIKE ' . $search . ')');
        }

        if (!empty($filters['filter']['date_start']) && !empty($filters['filter']['date_end'])) {

            $date_start = new DateTime($filters['filter']['date_start'] . ' 00:00:00');
            $date_end = new DateTime($filters['filter']['date_end'] . ' 23:59:59');

            $query->where('a.created >= '.$date_start->getTimestamp().' AND a.created <= '.$date_end->getTimestamp());
        }



        switch($filters['filter']['confirmed']) {
            case 0:
                $query->where('a.confirmed_date = 0');
                break;
            case 1:
                $query->where('a.confirmed_date > 0');
                break;
            case -2:
                $query->where('a.confirmed_date > 0');
                $query->having('COUNT(c.subid) = 0');
                break;
        }

        $db->setQuery($query);

        return $db->loadObjectList();
    }


}
