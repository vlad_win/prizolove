<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

class PrizoloveTableEvent extends JTable
{
    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__pl_events', 'id', $db);
    }
}
