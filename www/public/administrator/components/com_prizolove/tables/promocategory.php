<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

class PrizoloveTablePromocategory extends JTable
{
    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__prizolove_promo_categories', 'id', $db);
    }
}
