<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

class PrizoloveTableCategory extends JTable
{
    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__prizolove_product_categories', 'id', $db);
    }
}
