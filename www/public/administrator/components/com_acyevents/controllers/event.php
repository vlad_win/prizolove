<?php defined('_JEXEC') or die;

class AcyEventsControllerEvent extends JControllerForm
{
    public function getModel($name = 'Event', $prefix = 'AcyEventsModel', $config = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $config);

        return $model;
    }

    public function save($key = null, $urlVar = null)
    {
	    $data = $this->input->post->get('jform', array(), 'array');
	    $data['participant'] = 1;
        if($data['frequency'] == 0){
	        $data['next_at'] = date(DateTime::ISO8601, strtotime($data['start_at']));

        }else{
	        $begin = new DateTime( $data['start_at'] );
	        $end = new DateTime( 'now' );
	        $end = $end->modify( '+1 '.AcyEventsHelper::getFrequency( intval($data['frequency'])) );
	        $interval = DateInterval::createFromDateString('1 '.AcyEventsHelper::getFrequency( intval($data['frequency']) ));
	        $daterange = new DatePeriod($begin, $interval ,$end);
	        foreach($daterange as $date){
		        $data['next_at']  = $date->format(DateTime::ISO8601);
	        }
        }

	    $this->input->post->set('jform', $data);
	    return parent::save($key, $urlVar);
    }



}