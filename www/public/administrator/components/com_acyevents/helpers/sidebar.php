<?php
defined('_JEXEC') or die('Restricted access');


abstract class AcyEventsSideBar extends JHelperContent
{


    public static function addSubmenu($submenu)
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_ACYE_TITLE_DEFAULT'),
            'index.php?option=com_acyevents',
            $submenu == 'default'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_ACYE_MENU_EVENTS'),
            'index.php?option=com_acyevents&view=events',
            $submenu == 'events'
        );

        JHtmlSidebar::addEntry(
            JText::_('COM_ACYE_MENU_HISTORY'),
            'index.php?option=com_acyevents&view=history',
            $submenu == 'history'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_ACYE_MENU_CERTIFICATE'),
            'index.php?option=com_acyevents&view=certificate',
            $submenu == 'certificate'
        );
	    JHtmlSidebar::addEntry(
		    JText::_('JOPTIONS'),
		    'index.php?option=com_config&view=component&component=com_acyevents',
		    $submenu == 'options'
	    );

    }

}