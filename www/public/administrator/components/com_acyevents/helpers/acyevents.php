<?php
defined('_JEXEC') or die('Restricted access');


abstract class AcyEventsHelper extends JHelperContent
{


    public static function addSubmenu($submenu)
    {
        JHtmlSidebar::addEntry(
            'Event',
            'index.php?option=com_acyevents',
            $submenu == 'default'
        );

        JHtmlSidebar::addEntry(
            'History',
            'index.php?option=com_events&view=history',
            $submenu == 'history'
        );
    }
    
    public static function getFrequency(int $frequency): string
    {
        switch ($frequency){
            case 1: return 'DAY';
            case 2: return 'WEEK';
            case 3: return 'MONTH';
            default: return '';
        }
    }

    public static function getFund($sum = 0, $amount = 0, $max = 0)
    {
    	$funds = floatval($sum) * floatval($amount) / 100;
    	return ($funds < $max)?$funds:$max;
    }

	/***
	 *
	 * @inheritdoc https://www.acyba.com/acymailing/64-acymailing-developer-documentation.html#sendsingleemail
	 *
	 * @since version
	 */
    public static function sendWinner($data, $letter_id)
    {
	    if($data == null || $letter_id == null ) return null;

	    $mailer = self::AcyMailer();
	    $mailer->report = true;
	    $mailer->trackEmail = true;
	    $mailer->autoAddUser = true;
	    $mailer->checkConfirmField = false;
	    $mailer->checkPublished = false;
	    $mailer->addParam('certificate', $data->certificate_id);
	    $mailer->addParam('name', $data->name);
	    $mailer->addParam('amount', 100);
	    $mailer->sendOne($letter_id, 'max@codevery.com');
	    return $mailer->reportMessage;
    }

    public static function sendCertificate($data, $letter_id)
    {
        $mailer = self::AcyMailer();
        $mailer->report = true;
	    $mailer->trackEmail = true;
	    $mailer->autoAddUser = true;
	    $mailer->checkConfirmField = false;
	    $mailer->checkPublished = false;
	    $mailer->addParam('certificate', $data->certificate);
        $mailer->sendOne($letter_id, 'max@codevery.com');
    }

	/**
	 * @return  acymailerHelper
	 * @since version
	 */
    protected static function AcyMailer()
    {
	    if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
		    echo 'This code can not work without the AcyMailing Component';
		    return false;
	    }
	    return acymailing_get('helper.mailer');
    }


}