<?php defined('_JEXEC') or die; ?>

<div id="j-sidebar-container" class="span2">
    <?php echo JHtmlSidebar::render(); ?>
</div>
<div id="j-main-container" class="span10">

    <form action="index.php?option=com_acyevents&view=event" method="post" id="adminForm" name="adminForm">
        <table class="table table-container table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th><?php echo JText::_('COM_ACYE_NAME'); ?></th>
                <th><?php echo JText::_('COM_ACYE_FREQUENCY'); ?></th>
                <th><?php echo JText::_('COM_ACYE_START_AT'); ?></th>
                <th><?php echo JText::_('COM_ACYE_NEXT_AT'); ?></th>
                <th><?php echo JText::_('COM_ACYE_FUND'); ?></th>
                <th><?php echo JText::_('JPUBLISHED'); ?></th>
                <th><?php echo JText::_('HITS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($this->items as $item) : ?>
                <tr>
                    <td><?php echo $item->id; ?></td>
                    <td>
                        <a href="<?php echo JRoute::_('index.php?option=com_acyevents&view=event&id=' . $item->id); ?>"><?php echo $item->title; ?></a>
                    </td>
                    <td><?php echo JText::_('COM_ACYE_FREQUENCY_' . AcyEventsHelper::getFrequency($item->frequency)); ?></td>
                    <td><?php echo $item->start_at; ?></td>
                    <td><?php echo $item->next_at; ?></td>
                    <td>
                        <span><?php echo $item->fund_percent; ?>%</span>;
                        <span>Max: </span><span><?php echo $item->fund_max; ?></span>
                    </td>
                    <td>
                        <?php echo ($item->published == 1) ? '<i class="icon icon-publish green"></i>' : '<i class="icon icon-minus "></i>'; ?>
                    </td>
                    <td><?php echo $item->hits;?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>
        <div>
            <?php echo strtotime(DateTime::ISO8601, time()); ?>
        </div>
    </form>
</div>
