<?php defined('_JEXEC') or die;

class AcyEventsViewEvents extends JViewLegacy
{
    protected $form = null;
    
    public $items;

    public function display($tpl = null)
    {
        AcyEventsSideBar::addSubmenu('events');

        $this->items = $this->get('Events');


        JToolbarHelper::title(JText::_('COM_ACYE_TITLE_EVENTS'), 'wand');
	    JToolbarHelper::custom('manualcron', 'flash', 'health', JText::_('COM_ACYE_CRONJOB_MANUAL'), false);
	    JToolbarHelper::addNew();
        return parent::display($tpl);
    }


}
