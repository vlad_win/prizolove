<?php defined('_JEXEC') or die; ?>
<div id="j-sidebar-container" class="span2">
    <?php echo JHtmlSidebar::render(); ?>
</div>
<div id="j-main-container" class="span10">
    <form action="<?php echo JRoute::_('index.php?option=com_acyevents&view=event&id=' . (int)$this->item->id); ?>"
          method="post" name="adminForm" id="adminForm">
        <div class="form-horizontal">
            <?php echo $this->form->renderFieldset('event');?>

        </div>
        <input type="hidden" name="task" value="event.edit"/>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>