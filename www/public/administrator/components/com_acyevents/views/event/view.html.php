<?php defined('_JEXEC') or die;

class AcyEventsViewEvent extends JViewLegacy
{
    protected $form = null;
    
    public $item = null;

    public function display($tpl = null)
    {
        AcyEventsSideBar::addSubmenu('events');

        $this->form = $this->get('Form');
        $this->item = $this->get('Item');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode('<br />', $errors));

            return false;
        }

        JToolbarHelper::title(JText::_('COM_ACYE_TITLE_EVENT'), 'wand');
        JToolbarHelper::cancel('event.cancel');
        JToolbarHelper::save('event.save');
        JToolbarHelper::apply('event.apply');
        return parent::display($tpl);
    }


}
