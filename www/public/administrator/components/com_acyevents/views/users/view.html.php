<?php defined('_JEXEC') or die;

class AcyEventsViewUsers extends JViewLegacy
{
    protected $form = null;
    
    public $items;

    public function display($tpl = null)
    {
        JToolbarHelper::title(JText::_('COM_ACYE_TITLE_USERS'), 'users');
        AcyEventsSideBar::addSubmenu('users');


        $this->items = $this->get('Users');


        return parent::display($tpl);
    }


}
