<?php defined('_JEXEC') or die; ?>

<div id="j-sidebar-container" class="span2">
    <?php echo JHtmlSidebar::render(); ?>
</div>
<div id="j-main-container" class="span10">

    <table class="table table-container table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th><?php echo JText::_('COM_ACYE_USER_NAME'); ?></th>
            <th><?php echo JText::_('PHONE'); ?></th>
            <th><?php echo JText::_('EMAIL'); ?></th>
            <th><?php echo JText::_('COM_ACYE_PAYS'); ?></th>
            <th><?php echo JText::_('COM_ACYE_CERTIFICATE'); ?></th>
            <th><?php echo JText::_('COM_ACYE_REGISTERED'); ?></th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($this->items as $item) : ?>
            <tr>
                <td><?php echo $item->id; ?></td>
                <td><?php echo $item->user_name ?></td>
                <td><?php echo $item->user_phone ?></td>
                <td><?php echo $item->user_mail ?></td>
                <td><?php echo $item->user_pays ?></td>
                <td><?php echo strtoupper($item->user_sertificate) ?></td>
                <td><?php echo $item->created_at ?></td>
            </tr>
		<?php endforeach; ?>
        </tbody>
    </table>
</div>

