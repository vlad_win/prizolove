<?php defined('_JEXEC') or die;

class AcyEventsViewDefault extends JViewLegacy{

    public function display($tpl = null)
    {
        JToolbarHelper::title(JText::_('COM_ACYE_TITLE_EVENTS'), 'info-circle');
        JToolbarHelper::custom('manualcron', 'flash', 'health', JText::_('COM_ACYE_CRONJOB_MANUAL'), false);
        AcyEventsSideBar::addSubmenu('default');
        return parent::display($tpl);
    }


}