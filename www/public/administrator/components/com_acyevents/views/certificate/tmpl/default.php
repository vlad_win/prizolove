<?php defined('_JEXEC') or die; ?>

<div id="j-sidebar-container" class="span2">
    <?php echo JHtmlSidebar::render(); ?>
</div>
<div id="j-main-container" class="span10">

    <table class="table table-container table-bordered">
        <thead>
        <tr>
            <th><?php echo JText::_('COM_ACYE_CERTIFICATE'); ?></th>
            <th><?php echo JText::_('COM_ACYE_PAYS'); ?></th>
            <th><?php echo JText::_('COM_ACYE_REGISTERED'); ?></th>
            <th><?php echo JText::_('COM_ACYE_USER_NAME'); ?></th>
            <th><?php echo JText::_('EMAIL'); ?></th>
            <th><?php echo JText::_('PHONE'); ?></th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($this->items as $item) : ?>
            <?php if ($item->expired == 1){
                echo '<tr style="background: #e6ffe5">';
            }else{
                echo '<tr>';
            }?>
                <td><?php echo $item->certificate_id; ?></td>
                <td><?php echo round(floatval($item->paid_amount), 2); ?></td>
                <td><?php echo $item->activated_at ?></td>
                <td><?php echo $item->name ?></td>
                <td><?php echo $item->email ?></td>
                <td><?php echo $item->phone ?></td>
            </tr>
		<?php endforeach; ?>
        </tbody>
    </table>
</div>

