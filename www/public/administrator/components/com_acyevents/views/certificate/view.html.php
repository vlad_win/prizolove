<?php defined('_JEXEC') or die;

class AcyEventsViewCertificate extends JViewLegacy
{
    protected $form = null;
    
    public $items;

    public function display($tpl = null)
    {
        JToolbarHelper::title(JText::_('COM_ACYE_TITLE_CERTIFICATE'), 'users');
        AcyEventsSideBar::addSubmenu('certificate');


        $this->items = $this->get('CertificateList');


        return parent::display($tpl);
    }


}
