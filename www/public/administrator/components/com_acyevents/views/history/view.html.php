<?php defined('_JEXEC') or die;

class AcyEventsViewHistory extends JViewLegacy
{
    protected $form = null;
    
    public $items;

    public function display($tpl = null)
    {
        AcyEventsSideBar::addSubmenu('history');
        JToolbarHelper::title(JText::_('COM_ACYE_TITLE_HISTORY'), 'book');

        $this->items = $this->get('History');


        return parent::display($tpl);
    }


}
