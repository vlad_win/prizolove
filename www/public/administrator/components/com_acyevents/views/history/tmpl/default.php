<?php defined('_JEXEC') or die; ?>

<div id="j-sidebar-container" class="span2">
    <?php echo JHtmlSidebar::render(); ?>
</div>
<div id="j-main-container" class="span10">

    <table class="table table-container table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th><?php echo JText::_('COM_ACYE_NAME'); ?></th>
            <th><?php echo JText::_('COM_ACYE_EVENT_DATE'); ?></th>
            <th><?php echo JText::_('COM_ACYE_WINNER'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($this->items as $item) : ?>
            <tr>
                <td><?php echo $item->history_id; ?></td>
                <td><?php echo $item->title; ?></td>
                <td><?php echo $item->history_created_at; ?></td>
                <td>
                    <?php echo implode(json_decode($item->user_data), '<br>');?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
