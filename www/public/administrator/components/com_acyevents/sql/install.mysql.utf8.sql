DROP TABLE IF EXISTS `#__acyevents_event`;

CREATE TABLE `#__acyevents_event` (
	`id`        INT(11)       NOT NULL AUTO_INCREMENT,
	`published`    tinyint(1)        NOT NULL DEFAULT 1,
	`title`     VARCHAR (64)  NOT NULL,
	`acymailing_template_id`     smallint(5)  NOT NULL,
	`description`     TEXT  NOT NULL,
	`frequency`     tinyint(1)  NOT NULL COMMENT '0 - once; 1 - every day; 2 - week; 3 - month; 4 - annually',
	`start_at`     DATETIME  NULL,
	`next_at`     DATETIME  NULL,
	`participant`     smallint(1)  NOT NULL DEFAULT 1,
	`fund_percent`     tinyint(2)  NOT NULL DEFAULT 0,
	`fund_max`     INT(11)  NOT NULL DEFAULT 0,
	`created_at`     TIMESTAMP     DEFAULT CURRENT_TIMESTAMP,
	`hits`     INT(10)      DEFAULT 0,
	PRIMARY KEY (`id`)
)
	AUTO_INCREMENT = 0
	DEFAULT CHARSET =utf8;


DROP TABLE IF EXISTS `#__acyevents_history`;

CREATE TABLE `#__acyevents_history` (
	`id`        INT(11)       NOT NULL AUTO_INCREMENT,
	`event_id`        INT(11)       NOT NULL,
	`user_id`        INT(11)       NOT NULL,
	`user_fund`        int (11)       NOT NULL,
	`user_notification`        varchar (64)       NOT NULL,
	`user_data`        TEXT     NOT NULL,
	`created_at`        TIMESTAMP     DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
	AUTO_INCREMENT = 0
	DEFAULT CHARSET =utf8;


DROP TABLE IF EXISTS  `#__acyevents_users`;

CREATE TABLE `#__acyevents_certificate` (
	`certificate_id`        INT(10)       NOT NULL,
	`expired`        tinyint(1)       DEFAULT 0,
	`activated_at`        TIMESTAMP       DEFAULT CURRENT_TIMESTAMP
)
	DEFAULT CHARSET =utf8;