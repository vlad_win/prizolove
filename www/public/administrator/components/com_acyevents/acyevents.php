<?php defined('_JEXEC') or die;
JLoader::register('AcyEventsHelper', JPATH_COMPONENT . '/helpers/acyevents.php');
JLoader::register('AcyEventsSideBar', JPATH_COMPONENT . '/helpers/sidebar.php');

$controller = JControllerLegacy::getInstance('AcyEvents');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();