<?php defined('_JEXEC') or die;

class AcyEventsModelHistory extends JModelLegacy
{


    public function getHistory()
    {
        $sql = $this->_db->getQuery(true)
            ->select(['*', 'thistory.id as history_id', 'thistory.created_at AS history_created_at'])
            ->from($this->_db->qn('#__acyevents_history', 'thistory'))
	        ->leftJoin($this->_db->qn('#__acyevents_event', 'tevent'). 'ON(thistory.event_id = tevent.id)')
	        ->leftJoin($this->_db->qn('#__acyevents_certificate', 'tcert'). 'ON(thistory.user_id = tcert.certificate_id)')
	        ->order('history_id DESC');
        return $this->_db->setQuery($sql, 0, 100)->loadObjectList();
    }




}