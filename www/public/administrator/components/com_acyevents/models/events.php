<?php defined('_JEXEC') or die;

class AcyEventsModelEvents extends JModelLegacy
{


    public function getEvents()
    {
        $sql = $this->_db->getQuery(true)
            ->select('*')
            ->from('#__acyevents_event');
        return $this->_db->setQuery($sql)->loadObjectList();
    }




}