<?php defined('_JEXEC') or die;

class AcyEventsModelEvent extends JModelAdmin
{

    public function getTable($name = 'Event', $prefix = 'Table', $options = array())
    {
        return parent::getTable($name, $prefix, $options);
    }

    public function getForm($data = array(), $loadData = true)
    {
        $form = $this->loadForm(
            'com_acyevents.event',
            'event',
            array(
                'control' => 'jform',
                'load_data' => $loadData
            )
        );
        if (empty($form))
        {
            return false;
        }

        return $form;
    }

    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState(
            'com_acyevents.event.event.data',
            array()
        );
        if (empty($data))
        {
            $data = $this->getItem();
        }
        return $data;
    }


}
