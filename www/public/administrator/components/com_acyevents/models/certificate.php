<?php defined('_JEXEC') or die;

class AcyEventsModelCertificate extends JModelLegacy
{

	public function getTable($name = 'Certificate', $prefix = 'Table', $options = array())
	{
		return parent::getTable($name, $prefix, $options);
	}

	public function getCertificateList()
    {
        return $this->getCertificate(false);
    }

    public function getCertificate($certificate_id)
    {
    	if($certificate_id === null) return null;
	    $sql = $this->_db->getQuery(true)
		    ->select('*')
		    ->from($this->_db->qn('#__acyevents_certificate', 'acys'))
		    ->leftJoin('#__pl_users_certificates AS cert ON cert.id = acys.certificate_id')
		    ->leftJoin('#__users AS usr ON cert.user_id = usr.id')
		    ->leftJoin('#__acymailing_subscriber AS mailing ON mailing.userid = usr.id')
		    ->order('acys.activated_at DESC');
	    if($certificate_id){
	    	$sql->where('acys.certificate_id = '.$this->_db->q($certificate_id));
	    	return $this->_db->setQuery($sql)->loadObject();
	    }
	    return $this->_db->setQuery($sql,0, 100)->loadObjectList();
    }





}