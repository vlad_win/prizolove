<?php defined('_JEXEC') or die;

class AcyEventsModelUsers extends JModelLegacy
{

	public function getTable($name = 'Users', $prefix = 'Table', $options = array())
	{
		return parent::getTable($name, $prefix, $options);
	}

	public function getUsers()
    {
        $sql = $this->_db->getQuery(true)
            ->select('*')
            ->from('#__acyevents_users')
	        ->order('id DESC ');
        return $this->_db->setQuery($sql,0, 100)->loadObjectList();
    }

    public function addUser()
    {
    	$table = $this->getTable();
    	$table->bind(
    		array(
    			'user_sys_id' => 1,
    			'user_name' => md5(rand(1000,9999)),
			    'user_phone' => rand(111222333),
			    'user_mail' => 'asdasd@sadasd.com',
			    'user_pays' => rand(100,2000),
			    'user_sertificate' => bin2hex(random_bytes(5))
		    )
	    );
    	$table->store();

    }




}