<?php defined('_JEXEC') or die;

class TablePlCertificate extends JTable
{
    protected $certificate;

    public function __construct(&$db)
    {
        parent::__construct('#__pl_users_certificates', 'certificate_id', $db);
    }

    public function certificate($certificate_hash)
    {
        $this->certificate = $this->load($certificate_id);
        $sql = $this->_db->getQuery(true)
            ->select('*')
            ->from('#__pl_users_certificates as cert')
            ->where('cert.certificate_id = '.$this->_db->q($certificate_id))
            ->order('cert.id DESC');
        return $this->_db->setQuery($sql)->loadObject();
    }

}