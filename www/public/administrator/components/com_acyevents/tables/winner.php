<?php defined('_JEXEC') or die;

class AcyeventsTableWinner extends JTable
{
    public function __construct(&$db)
    {
        parent::__construct('#__acyevents_history', 'id', $db);
    }

    public function last()
    {
	    $sql = $this->_makeQuery()
		    ->order('history_id DESC');
	    return $this->_db->setQuery($sql, 0, 1)->loadObject();
    }

    public function history()
    {
    	$sql = $this->_makeQuery()
		    ->order('history_id DESC');
	    return $this->_db->setQuery($sql, 0, 100)->loadObjectList();
    }




    protected function _makeQuery()
    {
	    return $this->_db->getQuery(true)
		    ->select(['*', 'thistory.id as history_id', 'thistory.created_at AS history_created_at'])
		    ->from($this->_db->qn('#__acyevents_history', 'thistory'))
		    ->leftJoin($this->_db->qn('#__acyevents_event', 'tevent'). 'ON(thistory.event_id = tevent.id)')
		    ->leftJoin($this->_db->qn('#__acyevents_certificate', 'tcert'). 'ON(thistory.user_id = tcert.certificate_id)');
    }

}