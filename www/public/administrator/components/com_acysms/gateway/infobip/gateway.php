<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_infobip_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $senderid;
	public $waittosend = 0;
	public $connectionInformations;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $name = 'Infobip';
	public $domain = 'api.infobip.com';


	public function openSend($message, $phone){


		$authentication = new stdClass();
		$authentication->username = $this->username;
		$authentication->password = $this->password;

		$object = new stdClass();
		$object->authentication = $authentication;

		$messages = array();
		$oneMessageObject = new stdClass();
		$oneMessageObject->text = $message;

		if($this->unicodeChar($oneMessageObject->text)){
			$oneMessageObject->datacoding = '8';
		}

		$recipient = new stdClass();
		$recipient->gsm = $phone;


		$oneMessageObject->recipients = array();
		$oneMessageObject->recipients[] = $recipient;

		$oneMessageObject->sender = $this->senderid;
		$oneMessageObject->type = 'longSMS';

		$messages[] = $oneMessageObject;

		$object->messages = $messages;

		$fsockParameter = "POST /api/v3/sendsms/json HTTP/1.1\r\n";
		$fsockParameter .= "Host: api.infobip.com\r\n";
		$fsockParameter .= "Content-type: application/json\r\n";
		$fsockParameter .= "Content-length: ".strlen(json_encode($object))."\r\n\r\n";
		$fsockParameter .= json_encode($object);

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}


	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SENDER_ID'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]" id="senderprofile_senderid" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'Infobip').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=infobip&pass='.$config->get('pass').'</li>';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'Infobip').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=infobip&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}


	protected function interpretSendResult($result){

		if(!strpos($result, '200')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$resultObject = json_decode($res);

		if(empty($resultObject)){
			$this->errors[] = $res;
			return false;
		}

		$return = true;
		foreach($resultObject->results as $oneResult){
			if($oneResult->status != '0'){
				$this->errors[] = 'InfoBip interpretSendResult : '.$this->getErrors($oneResult->status);
				$return = false;
			}
		}

		if(!$return) return false;
		return true;
	}

	protected function getErrors($errNo){
		$errors = array();

		$errors['-1'] = ' Error in processing the request';
		$errors['-2'] = 'Not enough credits on a specific account';
		$errors['-3'] = 'Targeted network is not covered on specific account';
		$errors['-5'] = 'Username or password is invalid';
		$errors['-6'] = 'Destination address is missing in the request';
		$errors['-10'] = 'Username is missing in the request';
		$errors['-11'] = 'Password is missing in the request';
		$errors['-13'] = 'Number is not recognized by Infobip platform';
		$errors['-22'] = 'Incorrect XML format, caused by syntax error';
		$errors['-23'] = 'General error, reasons may vary';
		$errors['-26'] = 'General API error, reasons may vary';
		$errors['-27'] = 'Invalid scheduling parametar';
		$errors['-28'] = 'Invalid PushURL in the request';
		$errors['-30'] = 'Invalid APPID in the request';
		$errors['-33'] = 'Duplicated MessageID in the request';
		$errors['-34'] = 'Sender name is not allowed';
		$errors['-99'] = 'Error in processing request, reasons may vary';

		return isset($errors[$errNo]) ? 'Error '.$errNo.': '.$errors[$errNo] : 'Unknown error : '.$errNo;
	}

	public function deliveryReport(){

		$apiAnswer = new stdClass();

		$postData = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		$xml = simplexml_load_string($postData);

		$status = array();
		$status['NOT_SENT'] = array(0, "Not Sent");
		$status['SENT'] = array(1, "Sent");
		$status['NOT_DELIVERED'] = array(-1, "Not Delivered");
		$status['DELIVERED'] = array(5, "Delivered");
		$status['NOT_ALLOWED'] = array(-1, "Not allowed");
		$status['INVALID_DESTINATION_ADDRESS'] = array(-1, "Invalid Destination Address");
		$status['INVALID_SOURCE_ADDRESS'] = array(-1, "Invalid Source Address");
		$status['ROUTE_NOT_AVAILABLE'] = array(-1, "Route Not Available");
		$status['NOT_ENOUGH_CREDITS'] = array(-1, "Not Enough Credits");
		$status['REJECTED'] = array(-1, "Rejected");
		$status['INVALID_MESSAGE_FORMAT'] = array(-1, "Invalid Message Format");

		$completed_time = (string)$xml->message['donedate'];

		$messageStatus = (string)$xml->message['status'];
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 'DELIVERED'){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = (string)$xml->message['id'];
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';
		$apiAnswer->statsdetails_sms_id = $smsId;

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		return $apiAnswer;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = strtotime(JRequest::getString("when", ''));

		$apiAnswer->answer_body = JRequest::getString("text", '');

		$sender = JRequest::getString("sender", '');
		$msisdn = JRequest::getString("receiver", '');

		if(!empty($sender)) $apiAnswer->answer_from = '+'.$sender;
		if(!empty($msisdn)) $apiAnswer->answer_to = '+'.$msisdn;

		$apiAnswer->answer_sms_id = JRequest::getString("messageid", '');

		return $apiAnswer;
	}


}
