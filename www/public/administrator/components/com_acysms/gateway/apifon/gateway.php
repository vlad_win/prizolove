<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_apifon_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $senderid;
	public $domain;

	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = false;

	public $name = 'Apifon';

	public function openSend($message, $phone){
		$config = ACYSMS::config();

		$msgContent = new stdClass();
		$msgContent->sender_id = "sms";
		if(!empty($this->senderid) && preg_match('#^[0-9]{1,16}$|^[a-zA-Z0-9]{1,11}$#Uis', $this->senderid)) $msgContent->sender_id = $this->senderid;
		$msgContent->text = $message;
		$msgContent->dc = 0;

		$subs = array();
		$sub = new stdClass();
		$sub->number = $this->checkNum($phone);
		$subs[] = $sub;

		$sendRequest = new stdClass();
		$sendRequest->callback_url = ACYSMS_LIVE.'index.php?option=com_acysms&gateway=apifon&pass='.$config->get('pass');
		$sendRequest->subscribers = $subs;
		$sendRequest->message = $msgContent;
		$json = utf8_encode(json_encode($sendRequest));

		$dateTime = new DateTime();
		$strDate = $dateTime->format('D, d M Y H:i:s T');

		$strToSign = "POST\n/services/sms/send\n".$json."\n".$strDate;
		$signature = base64_encode(hash_hmac('SHA256', $strToSign, $this->secret, true));

		$fsockParameter = "POST /services/sms/send HTTP/1.1\r\n";
		$fsockParameter .= "Host: ".$this->domain."\r\n";
		$fsockParameter .= "Content-type: application/json; charset=utf-8\r\n";
		$fsockParameter .= "Authorization: ApifonWS " . $this->token . ":" . $signature."\r\n";
		$fsockParameter .= "X-ApifonWS-Date: " . $strDate."\r\n";
		$fsockParameter .= "Content-length: ".strlen($json)."\r\n\r\n";
		$fsockParameter .= $json;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you chose is blocked on your server. You should try to select the port 80';
		}
		return $result;
	}

	protected function interpretSendResult($result){


		if(strpos($result, '200 OK') === false){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else{
			$result = trim(substr($result, strpos($result, "\r\n\r\n")));
		}

		if(strpos($result, 'ERROR') !== false){
			$decodedResult = json_decode($result, true);
			$this->errors[] = $decodedResult['result_info']['status_code'].' => '.$decodedResult['result_info']['description'];
			return false;
		}

		return true;
	}

	public function displayConfig(){
		?>
		<script type="text/javascript">
			<!--
				function checkSenderID(value){
					var filter = /^[0-9]{1,16}$|^[a-zA-Z0-9]{1,11}$/i;
					return filter.test(value)
				}
			-->
		</script>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_TOKEN'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][token]"
						   id="senderprofile_token" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->token, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SECRET'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][secret]"
						   id="senderprofile_secret" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->secret, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SENDER_ID'); ?></label>
				</td>
				<td>
					<input type="text" onchange="if(checkSenderID(this.value)){this.style.border = 'none';}else{this.style.border = '2px solid red';alert('The sender ID must be 1 to 16 digits, or an alphanumeric word of 1 to 11 characters');}" name="data[senderprofile][senderprofile_params][senderid]"
						   id="senderprofile_senderid" class="inputbox" minlength="1" maxlength="16" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_domain"><?php echo JText::_('SMS_DOMAIN') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][domain]"
						   id="senderprofile_domain" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->domain, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_port"><?php echo JText::_('SMS_PORT') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][port]"
						   id="senderprofile_port" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(empty($this->port) ? 80 : $this->port, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
		</table>
		<?php
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	public function getBalance(){
		$dateTime = new DateTime();
		$strDate = $dateTime->format('D, d M Y H:i:s T');

		$strToSign = "POST\n/services/balance\n\n".$strDate;
		$signature = base64_encode(hash_hmac('SHA256', $strToSign, $this->secret, true));

		$fsockParameter = "POST /services/balance HTTP/1.1\r\n";
		$fsockParameter .= "Host: ".$this->domain."\r\n";
		$fsockParameter .= "Content-type: application/json; charset=utf-8\r\n";
		$fsockParameter .= "Authorization: ApifonWS " . $this->token . ":" . $signature."\r\n";
		$fsockParameter .= "X-ApifonWS-Date: " . $strDate."\r\n";
		$fsockParameter .= "Content-length: 0\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you chose is blocked on your server. You should try to select the port 80';
		}

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$res = json_decode($res, true);

		if(empty($res['balance'])) return false;
		return array("default" => $res['balance']);
	}

	private function displayBalance(){

		$balance = $this->getBalance();

		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	public function deliveryReport(){




		$status = array();
		$status[0] = array();
		$status[1] = array();
		$status[2] = array(3,4);
		$status[3] = array(1);
		$status[4] = array(19);
		$status[5] = array(7);
		$status[-1] = array(5,8,18,21,22,23,24,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118);
		$status[-2] = array();

		$apiAnswer = new stdClass();

		$postData = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		$decodedResponse = json_decode($postData, true);

		$messageStatus = empty($decodedResponse['data'][0]['status']['code']) ? 0 : $decodedResponse['data'][0]['status']['code'];

		if(!empty($decodedResponse['data'][0]['timestamp'])) $apiAnswer->statsdetails_received_date =  $decodedResponse['data'][0]['timestamp'];

		$apiAnswer->statsdetails_sms_id = $decodedResponse['data'][0]['messageId'];

		foreach($status as $key => $possibleStatus){
			if(in_array($messageStatus, $possibleStatus)){
				$apiAnswer->statsdetails_status = $key;
				break;
			}
		}

		if(!isset($apiAnswer->statsdetails_status)){
			$apiAnswer->statsdetails_status = -99;
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$decodedResponse['data'][0]['status']['text'];
		}else{
			$apiAnswer->statsdetails_error[] = $decodedResponse['data'][0]['status']['text'];
		}

		return $apiAnswer;
	}
}
