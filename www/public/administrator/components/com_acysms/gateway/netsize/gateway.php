<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_netsize_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;

	public $senderid;

	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $domain = 'prosvc.netsize.com';
	public $port = 80;

	public $name = 'NetSize';

	public function openSend($message, $phone){

		$params = array();

		$params['sLogin'] = $this->username;
		$params['sPassword'] = $this->password;
		$params['sOADC'] = $this->senderid;

		$params['nNotification'] = 7;
		$params['nSplit'] = 1;

		$params['sTarget'] = '+'.$this->checkNum($phone);
		$params['sMessage'] = $message;

		$params['Output'] = 'XMLLongTags';

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /SMS/Send HTTP/1.1\r\n";
		$fsockParameter .= "Host: prosvc.netsize.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();

		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SENDER_ID'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]" id="senderprofile_senderid" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'Netsize').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=netsize&pass='.$config->get('pass').'</li>';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'Netsize').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=netsize&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(preg_match('#<Code>(.*)</Code>#Ui', $res, $explodedResults)){
			if($explodedResults[1] != 0 && $explodedResults[1] != 51000){
				$this->errors[] = $res;
				return false;
			}
		}
		if(preg_match('#<IdTicket>(.*)</IdTicket>#Ui', $res, $explodedResults)){
			$this->smsid = $explodedResults[1];
		}

		return true;
	}


	protected function getErrors($errNo){
		$errors = array();
		$errors['0'] = 'In progress (a normal message submission, with no error encountered so far).';
		$errors['1'] = 'Scheduled (see Scheduling below).';
		$errors['22'] = 'Internal fatal error';
		$errors['23'] = 'Authentication failure';
		$errors['24'] = 'Data validation failed';
		$errors['25'] = 'You do not have sufficient credits';
		$errors['26'] = 'Upstream credits not available';
		$errors['27'] = 'You have exceeded your daily quota';
		$errors['28'] = 'Upstream quota exceeded';
		$errors['40'] = 'Temporarily unavailable';
		$errors['201'] = 'Maximum batch size exceeded';

		return isset($errors[$errNo]) ? 'Error '.$errNo.': '.$errors[$errNo] : 'Unknown error : '.$errNo;
	}

	public function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status[2] = array(-1, "Not sent");
		$status[3] = array(3, "Sent to the operator");
		$status[4] = array(3, "Delivered to the operator");
		$status[5] = array(5, "Delivered");
		$status[6] = array(-1, "Upstream credits not available");

		$completed_time = JRequest::getVar("P5", '');

		$messageStatus = JRequest::getVar("P3", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 5){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = JRequest::getVar("P1", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getString("received_time", '');

		$apiAnswer->answer_body = JRequest::getString("message", '');

		$sender = JRequest::getString("sender", '');
		$msisdn = JRequest::getString("msisdn", '');

		if(!empty($sender)) $apiAnswer->answer_from = '+'.$sender;
		if(!empty($msisdn)) $apiAnswer->answer_to = '+'.$msisdn;

		$apiAnswer->answer_sms_id = JRequest::getString("referring_batch_id", '');

		return $apiAnswer;
	}

	public function closeRequest(){
		echo 'status=0';
	}
}
