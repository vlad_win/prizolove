<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php


class ACYSMSGateway_mobyt_gateway extends ACYSMSGateway_default_gateway{

	public $userLogin;
	public $password;
	public $smsQty;
	public $smsSender;
	public $operation = 'TEXT';


	public $domain = 'smsweb.mobyt.it';
	public $port = 80;


	public $name = 'Mobyt';

	public $creditsUrl = 'https://www.mobyt.fr/tarifs.php';


	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = false;


	public function openSend($message, $phone){

		$phoneWithoutPlus = $this->checkNum($phone);
		$bigSmsId = microtime().$phoneWithoutPlus;
		$this->smsid = substr($bigSmsId, -20);

		if(!empty($this->smsSender)){
			if(preg_match('#[a-zA-Z]#', $this->smsSender)){
				if(strlen($this->smsSender) < 1 || strlen($this->smsSender) > 11){
					$this->errors[] = "Sender name too short or too long (NB : the sender name have to contains between 1 & 11 characters)";
					return false;
				}
			}else{
				if(strlen($this->smsSender) > 16){
					$this->errors[] = "Sender number too long (NB : the sender number have to contains less than 16 characters)";
					return false;
				}
			}
		}

		$params = array();
		$params['id'] = $this->userLogin;
		$params['password'] = '';
		$params['operation'] = $this->operation;
		$params['rcpt'] = $phone;
		$params['data'] = $message;
		$params['from'] = $this->smsSender;
		$params['qty'] = $this->smsQty;
		$params['act'] = $this->smsid;
		$params['ticket'] = md5($this->userLogin.$this->operation.$phone.$this->smsSender.$message.$this->password);

		$stringToPost = '';

		foreach($params as $aParam => $value){
			$value = urlencode($value);
			$stringToPost .= '&'.$aParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, "&");

		$fSockParameter = "POST /sms-gw/sendsmart HTTP/1.1\r\n";
		$fSockParameter .= "Host: smsweb.mobyt.it\r\n";
		$fSockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fSockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fSockParameter .= $stringToPost;

		return $this->sendRequest($fSockParameter);
	}

	public function displayConfig(){
		$config = ACYSMS::config();

		$qtyData = array();
		$qtyData[] = JHTML::_('select.option', 'a', 'Automatic Quality', 'value', 'text');
		$qtyData[] = JHTML::_('select.option', 'll', 'Low Quality', 'value', 'text');
		$qtyData[] = JHTML::_('select.option', 'l', 'Medium Quality', 'value', 'text');
		$qtyData[] = JHTML::_('select.option', 'h', 'High Quality', 'value', 'text');


		$qtyOption = JHTML::_('select.genericlist', $qtyData, 'data[senderprofile][senderprofile_params][smsQty]', 'class="inputbox chzn-done" style="width:200px;"', 'value', 'text', @$this->smsQty);

		?>
		<table>
			<tr>
				<td class="key">
					<label for="senderprofile_userLogin"><?php echo JText::_('SMS_USERNAME') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][userLogin]" id="senderprofile_userLogin" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->userLogin, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input type="password" name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_smsSender"><?php echo JText::_('SMS_FROM') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][smsSender]" id="senderprofile_smsSender" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->smsSender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_smstype"><?php echo JText::_('SMS_TYPE') ?></label>
				</td>
				<td>
					<?php echo $qtyOption; ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>


		</table>


		<?php

		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '
		<ul id="gateway_addresses">';
			echo '
			<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'Mobyt').'<br/>'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=mobyt&pass='.$config->get('pass').'</li>
			';
			echo '
		</ul>';
		}
	}


	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			$this->errors[] = "We were not able to find the API answer";
			return false;
		}

		if(strpos($res, 'OK') === false){
			$this->errors[] = "Error ".$res;

			return false;
		}
		return true;
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}


	public function getBalance(){

		$operation = 'GETMESS';


		$fsockParameter = "GET /sms-gw/sendsmart?id=".$this->userLogin."&operation=".$operation."&ticket=".md5($this->userLogin.$operation.$this->password)." HTTP/1.1\r\n";
		$fsockParameter .= "Host: ".$this->domain."\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			$this->errors[] = "We were not able to find the API answer for the balance";
			return false;
		}

		if(strpos($res, 'OK') === false){
			$this->erros[] = $res;
			return false;
		}


		$resExploded = explode(' ', $res);

		return array('default' => $resExploded[1]);
	}

	private function displayBalance(){

		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	public function deliveryReport(){
		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['Delivered (Consegnato)'] = array(5, 'Consegnato');
		$status['Not delivered'] = array(-1, 'Not delivered');
		$status['Unknown subscriber'] = array(-1, 'Numero inesistente');
		$status['VP exceed'] = array(-2, 'Destinatario Irraggiungibile');
		$status['Teleservice not provisioned'] = array(-1, 'Numero non abilitato in ricezione');
		$status['Operator Barring'] = array(-1, 'Numero bloccato dall\'operatore');
		$status['MT number is unknown in the MT network\'s HLR'] = array(-1, 'Numero sconosciuto nell\'Home Location Register');


		$statusRequest = JRequest::getVar('status', '');

		$requestDate = JRequest::getVar('date', '');
		$requestTime = JRequest::getVar('time', '');
		$idSms = JRequest::getVar('act', '');

		$receptionTime = ACYSMS::getTime($requestDate.' '.$requestTime);

		if(empty($statusRequest)) $apiAnswer->statsdetails_error[] = 'Empty status received';

		if($statusRequest == 'Delivered (Consegnato)'){
			if(empty($receptionTime)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $receptionTime;
		}

		if(empty($idSms)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';


		if(!isset($status[$statusRequest])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$statusRequest;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$statusRequest][0];
			$apiAnswer->statsdetails_error[] = $status[$statusRequest][1];
		}

		$apiAnswer->statsdetails_sms_id = $idSms;

		return $apiAnswer;
	}
}

