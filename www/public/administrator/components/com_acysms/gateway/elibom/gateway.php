<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_elibom_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $senderid;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $domain = 'ssl://www.elibom.com';
	public $port = 443;

	public $name = 'Elibom';
	public $creditsUrl = 'https://elibom.com/products/sms?section=prices-coverage';

	public function openSend($message, $phone){

		$data = array("destination" => $this->checkNum($phone), "text" => $this->password);

		$fsockParameter = "POST /messages HTTP/1.1\r\n";
		$fsockParameter .= "Host: www.elibom.com \r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Authorization: Basic ".base64_encode($this->username.":".$this->password)."\r\n";
		$fsockParameter .= "Content-length: ".strlen(json_encode($data))."\r\n\r\n";
		$fsockParameter .= json_encode($data);

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
	}

	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$jsonObject = json_decode($res);

		if(!empty($jsonObject->deliveryToken)) return true;

		$this->errors[] = print_r($jsonObject, true);
		return false;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getString("date", '');

		$apiAnswer->answer_body = JRequest::getString("text", '');

		$sender = JRequest::getString("from", '');
		if(!empty($sender)) $apiAnswer->answer_from = '+'.$sender;

		return $apiAnswer;
	}
}
