<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_sms4geeks_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $senderid;
	public $route;
	public $domain = 'sms4geeks.appspot.com';
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;

	public $name = 'SMS 4 Geeks';

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]"
						   id="senderprofile_username" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password"
						   class="inputbox" type="password" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_port"><?php echo JText::_('SMS_PORT') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][port]" id="senderprofile_port"
						   class="inputbox" type="text" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->port, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
		</table>
		<?php
	}

	public function openSend($message, $phone){

		$params = array();

		$encodeHelper = ACYSMS::get('helper.encoding');

		$params['msisdn'] = $this->checkNum($phone);
		$params['username'] = $encodeHelper->change($this->username, 'UTF-8', 'ISO-8859-1');
		$params['password'] = $encodeHelper->change($this->password, 'UTF-8', 'ISO-8859-1');
		$params['msg'] = $encodeHelper->change($message, 'UTF-8', 'ISO-8859-1');

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost .= '&action=out';
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /smsgateway HTTP/1.1\r\n";
		$fsockParameter .= "Host: sms4geeks.appspot.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	protected function interpretSendResult($result)
	{
		if (!strpos($result, '200 OK')) {
			$this->errors[] = 'Error 200 KO => ' . $result;
			return false;
		} else {
			$res = trim(substr($result, strpos($result, "\r\n\r\n")));
		}

		if(strpos($res, 'OK') !== 0) {
			$this->errors[] = $res;
			return false;
		}

		return true;
	}
}
