<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php


class ACYSMSGateway_smsgatewaydk_gateway extends ACYSMSGateway_default_gateway{

	public $apikey;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;

	public $domain = 'restapi.smsgateway.dk';
	public $port = 80;


	public $name = 'SmsGateway.dk';

	public function openSend($message, $phone){

		$messageObject = new stdClass();
		$messageObject->message = new stdClass();

		$messageObject->message->recipients = $this->checkNum($phone);
		$messageObject->message->sender = $this->from;
		$messageObject->message->message = $message;

		if($this->unicodeChar($messageObject->message->message)){
			$messageObject->message->format = 'UNICODE';
			$messageObject->message->charset = 'UTF-8';
		}

		$stringToPost = json_encode($messageObject);

		$fsockParameter = "POST /v2/message.json?apikey=".$this->apikey." HTTP/1.1\r\n";
		$fsockParameter .= "Host: restapi.smsgateway.dk\r\n";
		$fsockParameter .= "Content-type: application/json\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		return $this->sendRequest($fsockParameter);
	}


	public function displayConfig(){

		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_apikey"><?php echo 'API KEY'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][apikey]" id="senderprofile_apikey" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->apikey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_from"><?php echo JText::_('SMS_FROM') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][from]" id="senderprofile_from" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->from, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
	}

	protected function interpretSendResult($res){
		if(!strpos(strtolower($res), '201 created') && !strpos($res, '200 OK')){
			$res = substr($res, strpos($res, "\r\n\r\n"));
			$answer = json_decode($res);

			if(!empty($answer->message)){
				$this->errors[] = 'Error 200 KO => '.$answer->message;
			}else $this->errors[] = 'Error 200 KO => '.$res;

			return false;
		}

		$res = substr($res, strpos($res, "\r\n\r\n"));
		$answer = json_decode($res);

		if(isset($answer->details->state) && $answer->details->state == "DONE"){
			return true;
		}else{
			$this->errors[] = $res;
		}
	}

	function checkNum($phone){
		return $phone;
	}
}
