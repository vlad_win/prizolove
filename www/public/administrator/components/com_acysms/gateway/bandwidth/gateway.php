<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_bandwidth_gateway extends ACYSMSGateway_default_gateway{

	public $secret;
	public $token;
	public $userId;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $name = 'Bandwidth';
	public $creditsUrl = 'https://www.bandwidth.com/messaging/';

	public $domain = "ssl://api.catapult.inetwork.com";
	public $port = 443;

	public function openSend($message, $phone){

		$params = new stdClass();
		$config = ACYSMS::config();

		$params->to = $this->checkNum($phone);
		$params->from = $this->senderid;
		$params->text = $message;
		$params->receiptRequested = "all";
		$params->callbackUrl = ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=bandwidth&pass='.$config->get('pass');

		$stringToPost = json_encode($params);

		$fsockParameter = "POST /v1/users/".$this->userId."/messages HTTP/1.1\r\n";
		$fsockParameter .= "Host: api.catapult.inetwork.com\r\n";
		$fsockParameter .= "Content-type: application/json\r\n";
		$fsockParameter .= "Authorization: Basic ".base64_encode($this->token.":".$this->secret)."\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo "From" ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]" id="senderprofile_senderid" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_token"><?php echo "Token" ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][token]" id="senderprofile_token" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->token, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_secret"><?php echo "Secret" ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][secret]" id="senderprofile_secret" class="inputbox" type="text" style="width:200px;" value="<?php echo htmlspecialchars(@$this->secret, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_userId"><?php echo "User ID" ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][userId]" id="senderprofile_userId" class="inputbox" type="text" style="width:200px;" value="<?php echo htmlspecialchars(@$this->userId, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'Bandwidth').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=bandwidth&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}


	protected function interpretSendResult($result){

		if(strpos(strtolower($result), '201 created')){
			if(preg_match("#Location: .*\/messages\/(.*)#", $result, $matches)){
				$this->smsid = trim($matches[1]);
			}
		}
		return true;

		$res = trim(substr($result, strpos($result, "\r\n\r\n")));
		$resObject = json_decode($res);
		$this->errors[] = $resObject->message;
		return false;
	}

	public function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status[0] = array(5, "Message Delivered to Carrier");
		$status[187] = array(-1, "Spam Detected");
		$status[188] = array(-1, "Spam Detected");
		$status[189] = array(-1, "Spam Detected");
		$status[482] = array(-1, "Loop Detected");
		$status[600] = array(-1, "Destination Carrier Queue Full");
		$status[610] = array(-1, "submit_multi failed");
		$status[620] = array(-1, "Destination App Error");
		$status[630] = array(-1, "Message not acknowledge");
		$status[700] = array(-1, "Invalid Service Type");
		$status[720] = array(-1, "Invalid Destination Address");
		$status[740] = array(-1, "Invalid Source Number");
		$status[999] = array(-1, "Unknown Error");

		$raw_data = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		$dataObject = json_decode($raw_data);

		$completed_time = $dataObject->time;

		$messageStatus = $dataObject->deliveryCode;
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 0){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = $dataObject->messageId;
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getVar("time", '');

		$apiAnswer->answer_body = JRequest::getVar("text", '');

		$apiAnswer->answer_from = JRequest::getVar("from", '');
		$apiAnswer->answer_to = JRequest::getVar("to", '');

		$apiAnswer->answer_sms_id = JRequest::getVar("messageId", '');

		return $apiAnswer;
	}
}
