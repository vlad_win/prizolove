<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_farapayamak_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $from;
	public $to;
	public $text;
	public $waittosend = 0;
	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = true;
	public $domain = "www.farapayamak.ir";
	public $port = 80;
	public $name = 'farapayamak';
	public $type = "soap";
	public $creditsUrl = 'http://farapayamak.ir/en/link/detail/SMS-panel/9/view/';

	public function openSend($message, $phone){
		$params = array();
		$params['username'] = $this->username;
		$params['password'] = $this->password;
		$params['to'] = $this->checkNum($phone);
		$params['from'] = $this->sender;
		$params['text'] = iconv('UTF-8', 'UTF-8//TRANSLIT', $message);
		$params['isflash'] = false;
		$params['udh'] = "";
		$params['recId'] = array(0);
		$params['status'] = 0x0;
		if(!is_array($params['to'])){
			$params['to'] = array($params['to']);
		}
		try{
			$client = new SoapClient('http://api.payamak-panel.com/post/send.asmx?wsdl');
			return @$client->SendSms($params)->SendSmsResult;
		}catch(SoapFault $ex){
			return 'SoapFault';
		}
	}

	public function readResult_soap($res){
		return $res;
	}

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo @$this->username; ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo @$this->password; ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_sender"><?php echo JText::_('SMS_FROM') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][sender]" id="senderprofile_sender" class="inputbox" style="width:200px;" value="<?php echo @$this->sender; ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
	}


	protected function interpretSendResult($result){
		if($result == 1){
			return true;
		}else{
			$this->errors[] = 'err:'.$result;
		}
	}
}
