<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.4.0
 * @author	acyba.com
 * @copyright	(C) 2009-2017 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php 

class ACYSMSGateway_orangecameroon_gateway extends ACYSMSGateway_default_gateway{ 

  public $username; 
  public $password; 
  public $clientid; 
  public $client_secret; 
  public $applicationid; 
  public $authorization_header; 

  public $waittosend = 0; 

  public $sendMessage = true; 
  public $deliveryReport = false; 
  public $answerManagement = false; 

  public $name = 'Orange Cameroon'; 
  public $port = 80; 
  public $domain = 'api.orange.com'; 

  public $creditsUrl = ''; 

  function __construct(){ 
    $this->connectionInformations = array('bulksms.vsms.net' => 'bulksms.vsms.net', 'bulksms.2way.co.za' => 'bulksms.2way.co.za', 'usa.bulksms.com' => 'usa.bulksms.com', 'bulksms.com.es' => 'bulksms.com.es', 'bulksms.de' => 'bulksms.de', 'bulksms.co.uk' => 'www.bulksms.co.uk'); 
  } 

  public function openSend($message, $phone){ 

    $body = new stdClass(); 

    $body->outboundSMSMessageRequest = new stdClass(); 
    $body->outboundSMSMessageRequest->address = "tel:".$this->checkNum($phone); 
    $body->outboundSMSMessageRequest->senderAddress = "tel:".$this->senderid; 

    $body->outboundSMSMessageRequest->outboundSMSTextMessage = new stdClass(); 
    $body->outboundSMSMessageRequest->outboundSMSTextMessage->message = $message; 
    $stringToPost = json_encode($body); 

    $fsockParameter = "POST /smsmessaging/v1/outbound/".urlencode("tel:+".$this->senderid)."/requests HTTP/1.1\r\n"; 
    $fsockParameter .= "Host: api.orange.com \r\n"; 
    $fsockParameter .= "Authorization: Basic ".base64_encode($this->clientid.":".$this->client_secret)."\r\n"; 
    $fsockParameter .= "Content-Type: application/json;charset=UTF-8\r\n"; 
    $fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n"; 
    $fsockParameter .= $stringToPost; 

    new dbug($fsockParameter); 
    exit; 

    $result = $this->sendRequest($fsockParameter); 
    if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){ 
      $this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80'; 
    } 
    return $result; 
  } 

  public function displayConfig(){ 
    $config = ACYSMS::config(); 
Bug Fix => Conversation page SQL Query issue 
