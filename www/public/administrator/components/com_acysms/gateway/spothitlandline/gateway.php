<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_spothitlandline_gateway extends ACYSMSGateway_default_gateway{

	public $apikey;
	public $senderid;
	public $reecoute;
	public $miseenrelation;
	public $boitevocale;
	public $stop;
	public $voix;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $domain = 'www.spot-hit.fr';
	public $port = 80;

	public $name = 'Spot-Hit (Message Vocal)';
	public $creditsUrl = 'http://www.spot-hit.fr/sms-professionnel';


	public function openSend($message, $phone){

		$params = array();

		$params['key'] = $this->apikey;
		$params['type'] = $this->calltype;
		$params['message'] = '';
		$params['destinataires'] = $this->checkNum($phone);
		$params['expediteur'] = $this->senderid;
		$params['texte'] = $message;
		$params['voix'] = $this->voix;
		$params['detection_repondeur'] = 1;
		$params['reecoute'] = $this->reecoute;
		$params['mise_relation'] = $this->miseenrelation;
		$params['boite_vocale'] = $this->boitevocale;
		$params['stop'] = $this->stop;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /api/envoyer/vocal HTTP/1.1\r\n";
		$fsockParameter .= "Host: www.spot-hit.fr\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){

		$calltypeData = array();
		$calltypeData[] = JHTML::_('select.option', 'direct_repondeur', 'Direct Répondeur (sans appel)', 'value', 'text');
		$calltypeData[] = JHTML::_('select.option', 'appels', 'Appel', 'value', 'text');
		$calltypeOptions = JHTML::_('select.genericlist', $calltypeData, "data[senderprofile][senderprofile_params][calltype]", 'size="1" class="chzn-done" style="width:225px;"', 'value', 'text', @$this->calltype);

		$voiceData = array();
		$voiceData[] = JHTML::_('select.option', 'Philippe', 'Philippe', 'value', 'text');
		$voiceData[] = JHTML::_('select.option', 'Agnes', 'Agnes', 'value', 'text');
		$voiceData[] = JHTML::_('select.option', 'Damien', 'Damien', 'value', 'text');
		$voiceData[] = JHTML::_('select.option', 'Eva', 'Eva', 'value', 'text');
		$voiceData[] = JHTML::_('select.option', 'Helene', 'Helene', 'value', 'text');
		$voiceData[] = JHTML::_('select.option', 'John', 'John', 'value', 'text');
		$voiceData[] = JHTML::_('select.option', 'Loic', 'Loic', 'value', 'text');
		$voiceOption = JHTML::_('select.genericlist', $voiceData, "data[senderprofile][senderprofile_params][voix]", 'size="1" class="chzn-done" style="width:100px;"', 'value', 'text', @$this->voice);
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo 'Expéditeur'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]" id="senderprofile_senderid" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_apikey"><?php echo 'API Key'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][apikey]" id="senderprofile_apikey" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->apikey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_type"><?php echo 'Type'; ?></label>
				</td>
				<td>
					<?php echo $calltypeOptions; ?>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_voix"><?php echo 'Voix'; ?></label>
				</td>
				<td>
					<?php echo $voiceOption; ?>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_miseenrelation"><?php echo 'Mise en relation (Avec le numéro spécifié.)' ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][miseenrelation]" id="senderprofile_miseenrelation" class="inputbox" type="text" style="width:200px;" value="<?php echo htmlspecialchars(@$this->miseenrelation, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_reecoute"><?php echo 'Réecoute (Permettant au destinataire de réécouter votre message.)'; ?></label>
				</td>
				<td>
					<input type="checkbox" name="data[senderprofile][senderprofile_params][reecoute]" id="senderprofile_reecoute" <?php echo !empty($this->reecoute) ? 'checked' : ''; ?> />
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_boitevocale"><?php echo 'Boite Vocale (Permettant au destinataire de vous laisser un message)' ?></label>
				</td>
				<td>
					<input type="checkbox" name="data[senderprofile][senderprofile_params][boitevocale]" id="senderprofile_boitevocale" <?php echo !empty($this->boitevocale) ? 'checked' : ''; ?> />
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_stop"><?php echo 'Stop (Possibilité de s\'opposer à la réception de messages de votre part)' ?></label>
				</td>
				<td>
					<input type="checkbox" name="data[senderprofile][senderprofile_params][stop]" id="senderprofile_stop" <?php echo !empty($this->stop) ? 'checked' : ''; ?> />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			if(strpos(strtolower($result), '302 found')){
				$this->errors[] = 'Error 302 Found => Your access informations should be invalids';
				return false;
			}
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$res = json_decode($res);
		if(empty($res->id) || empty($res->resultat)){
			$this->errors[] = $this->getErrors($res->erreurs);
			return false;
		}
		if(!empty($res->id)) $this->smsid = $res->id;

		if(!empty($res->resultat) && $res->resultat == 1) return true;

		$this->errors[] = $this->getErrors($res);
		return false;
	}

	public function getBalance(){
		$fsockParameter = "GET /api/credits?key=".$this->apikey." HTTP/1.1\r\n";
		$fsockParameter .= "Host: www.spot-hit.fr \r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		if(!strpos($result, '200 OK')){
			if(strpos(strtolower($result), '302 found')){
				$this->errors[] = 'Error 302 Found => Your access informations should be invalids';
				return false;
			}
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));


		$res = json_decode($res);

		if(isset($res->premium) && isset($res->lowcost)){

			return array("LowCost" => $res->lowcost, "Premium" => $res->premium);
		}else{
			ACYSMS::enqueueMessage($this->getErrors($res->erreurs), 'error');
			return false;
		}
	}

	private function displayBalance(){
		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}


		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance['LowCost'].' Low Cost, '.$balance['Premium'].' Premium'), 'message');
	}


	protected function getErrors($errNo){
		$errArray = explode(',', $errNo);

		$errors = array();
		$errors['1'] = 'Type de SMS non spécifié ou incorrect';
		$errors['2'] = 'Le message est vide';
		$errors['3'] = 'Le message contient plus de 160 caractères';
		$errors['4'] = 'Aucun destinataire valide n\'est renseigné';
		$errors['5'] = 'Numéro interdit: seuls les envois en France Métropolitaine sont autorisés pour les SMS Low Cost';
		$errors['6'] = 'Numéro de destinataire invalide';
		$errors['7'] = 'Votre compte n\'a pas de formule définie';
		$errors['8'] = 'L\'expéditeur ne peut contenir que 11 caractères';
		$errors['9'] = 'Le système a rencontré une erreur, merci de nous contacter';
		$errors['10'] = 'Vous ne disposez pas d\'assez de SMS pour effectuer cet envoi';
		$errors['11'] = 'L\'envoi des message est désactivé pour la démonstration';
		$errors['12'] = 'Votre compte a été suspendu. Contactez-nous sur info@spot-hit.fr pour plus d\'informations';

		$return = '';
		foreach($errArray as $oneError){
			$return .= isset($errors[$oneError]) ? 'Error '.$oneError.': '.$errors[$oneError] : 'Unknown error : '.$oneError;
			$return .= '<br />';
		}
		return $return;
	}

	public function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status[1] = array(5, "Envoyé et bien reçu");
		$status[2] = array(-1, "Envoyé et non reçu");
		$status[3] = array(1, "En cours");
		$status[4] = array(-1, "Echec");

		$messageStatus = JRequest::getVar("statut", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';

		$completed_time = JRequest::getVar("date_update", '');
		if($messageStatus == 1){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = JRequest::getVar("id_message", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getString("date", '');

		$apiAnswer->answer_body = JRequest::getString("message", '');

		$sender = JRequest::getString("numero", '');

		if(!empty($sender)) $apiAnswer->answer_from = $sender;

		$apiAnswer->answer_sms_id = JRequest::getString("source", '');

		return $apiAnswer;
	}

	public function getNumber(){
		return JRequest::getString("numero", '');
	}
}
