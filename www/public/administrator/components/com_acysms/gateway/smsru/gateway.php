<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_smsru_gateway extends ACYSMSGateway_default_gateway{
	public $api_id = '';
	public $login = '';
	public $password = '';

	protected $_responseCode = array('send' => array('100' => 'Message is accepted to send', '200' => 'Incorrect api_id', '201' => 'Not enough money', '202' => 'Incorrect recipient', '203' => 'No text messages', '204' => 'The name of the sender is not agreed with the administration', '205' => 'The message is too long (more than 5 SMS)', '206' => 'Exceeded the daily limit for sending messages', '207' => 'On this number can not send messages', '208' => 'Time parameter is incorrect', '210' => 'Use GET, POST should be used where', '211' => 'The method was not found', '220' => 'Service is temporarily unavailable, please try later',),

									 'status' => array('-1' => 'Message not found', '100' => 'Message in the queue', '101' => 'Message to the operator', '102' => 'Your post (in transit)', '103' => 'Message delivered', '104' => 'Can not be reached: the lifetime has expired', '105' => 'Can not be reached: Removed from operator', '106' => 'Can not be reached: failed to phone', '107' => 'Can not be reached: unknown cause', '108' => 'Can not be reached: rejected', '200' => 'Incorrect api_id', '210' => 'Use GET, POST should be used where', '211' => 'The method was not found', '220' => 'Service is temporarily unavailable, please try later',),

									 'balance' => array('100' => 'Request is made', '200' => 'Incorrect api_id', '210' => 'Use GET, POST should be used where', '211' => 'The method was not found', '220' => 'Service is temporarily unavailable, please try later',),

									 'limit' => array('100' => 'Request is made', '200' => 'Incorrect api_id', '210' => 'Use GET, POST should be used where', '211' => 'The method was not found', '220' => 'Service is temporarily unavailable, please try later',),

									 'cost' => array('100' => 'Message is accepted to send', '200' => 'Incorrect api_id', '202' => 'Incorrect recipient', '206' => 'Exceeded the daily limit for sending messages', '207' => 'On this number can not send messages', '210' => 'Use GET, POST should be used where', '211' => 'The method was not found', '220' => 'Service is temporarily unavailable, please try later',),

									 'check' => array('100' => 'Number and password are the same', '300' => 'Invalid token (may have expired, or your IP has changed)', '301' => 'Wrong password or user not found')

	);

	public $domain = "sms.ru";
	public $port = 80;

	public $name = 'sms.ru';
	public $creditsUrl = 'http://sms.ru/?panel=price';

	public function openSend($message, $phone){

		$params = array();

		$params['api_id'] = $this->api_id;
		$params['username'] = $this->username;
		$params['password'] = $this->password;
		$params['to'] = $this->checkNum($phone);
		$params['text'] = $message;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /sms/send HTTP/1.1\r\n";
		$fsockParameter .= "Host: sms.ru \r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}

		return $result;
	}

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo 'API ID'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][api_id]" id="senderprofile_api_id" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->api_id, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][login]" id="senderprofile_login" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->login, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>

		</table>
		<?php
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if($res == 100){
			return true;
		}else if(!empty($this->_responseCode['send'][substr($res, 0, 3)])){
			$this->errors[] = $this->_responseCode['send'][substr($res, 0, 3)].' '.substr($res, 3);
		}else    $this->errors[] = $res;
		return false;
	}

	public function getBalance(){
		$fsockParameter = "GET /my/balance?api_id=".$this->api_id." HTTP/1.1\r\n";
		$fsockParameter .= "Host: sms.ru\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$errors = array();
		$errors['100'] = 'Запрос выполнен. На второй строчке вы найдете ваше текущее состояние баланса. request is made. You will find your current state of balance on the second line';
		$errors['200'] = 'Неправильный api_id / Wrong api_id';
		$errors['210'] = 'Используется GET, где необходимо использовать POST / You should use the method post, instead of the method get';
		$errors['211'] = 'Метод не найден / The method was not found';
		$errors['220'] = 'Сервис временно недоступен, попробуйте чуть позже. / The service has not worked, try again later';
		$errors['300'] = 'Неправильный token (возможно истек срок действия, либо ваш IP изменился) / Wrong token (maybe expired, or your IP has changed)';
		$errors['301'] = 'Неправильный пароль, либо пользователь не найден / Wrong password or the user was not found';
		$errors['302'] = 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс) / The user has registered, but the account has not been confirmed (the user did not enter the code, received in the SMS, when registering)';


		if(strpos($res, "\n")){
			list($code, $balance) = explode("\n", $res);
		}else $code = $res;

		if($code == "100"){
			return array("default" => strip_tags($balance));
		}else{
			$errorMsg = isset($errors[$code]) ? 'Error '.$code.': '.$errors[$code] : 'Unknown error : '.$code;
			ACYSMS::enqueueMessage($errorMsg, 'error');
			return false;
		}
	}

	private function displayBalance(){
		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['delivered'] = array(5, "Message arrived to handset.");
		$status['sent'] = array(-2, "Message sent.");
		$status['failed'] = array(-1, " Message failed to be delivered.");


		$completed_time = JRequest::getVar("delivered", '');

		$messageStatus = JRequest::getVar("status", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';

		if($messageStatus == 'delivered'){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_error[] = 'Unknow completed_time';
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = strtotime($completed_time);
		}


		$smsId = JRequest::getVar("id", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}
		$apiAnswer->statsdetails_sms_id = (string)$smsId;

		return $apiAnswer;
	}
}
