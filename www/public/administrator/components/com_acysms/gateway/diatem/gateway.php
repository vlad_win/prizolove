<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_diatem_gateway extends ACYSMSGateway_default_gateway{


	public $waittosend = 0;


	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $domain = "smssrv.diatem.net";
	public $port = 8800;

	public $name = 'Diatem (France only)';


	public function openSend($message, $phone){

		$params = array();


		$params['PhoneNumber'] = $this->checkNum($phone);
		$params['Text'] = $message;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST / HTTP/1.1\r\n";
		$fsockParameter .= "Host: smssrv.diatem.net\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;


		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$this->senderprofile_params = array();
		echo 'Your IP needs to be whitelisted by the Diatem team to be able to send SMS.';
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(strpos($res, "Message Submitted")){
			if(preg_match('#MessageID=(.*),#Ui', $res, $explodedResults)){
				$this->smsid = $explodedResults[1];
				return true;
			}
		}
		return false;
	}

	function checkNum($phone){
		return str_replace('+33', '0', $phone);
	}
}
