<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_octopush_gateway extends ACYSMSGateway_default_gateway{

	public $userLogin;
	public $apiKey;
	public $smsType;
	public $smsSender;
	public $smsReplies;
	public $waittosend;

	public $domain = "ssl://www.octopush-dm.com";
	public $port = 443;
	public $name = 'Octopush';
	public $creditsUrl = 'http://www.octopush.com/tarifs-sms';


	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = false;


	public function openSend($message, $phone){

		if(!empty($this->smsSender)){
			if(strlen($this->smsSender) < 3 || strlen($this->smsSender) > 11){
				$this->errors[] = "Nom d'expéditeur trop court (Rappel : le nom d'expéditeur doit contenir entre 3 et 11 caractères)";
				return false;
			}
		}


		if(empty($this->smsSender) && $this->smsType == "FR"){
			$this->errors[] = "En mode premium, vous devez spécifier un nom d'expéditeur";
			return false;
		}
		$params = array();
		$params['user_login'] = $this->userLogin;
		$params['api_key'] = $this->apiKey;
		$params['sms_recipients'] = $phone;
		$params['sms_text'] = $message;
		$params['sms_type'] = $this->smsType;
		$params['sms_sender'] = $this->smsSender;
		$params['request_mode'] = 'real';
		$params['with_replies'] = $this->smsReplies;

		$stringToPost = '';
		foreach($params as $aParam => $value){
			$value = urlencode($value);
			$stringToPost .= '&'.$aParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fSockParameter = "POST /api/sms HTTP/1.1\r\n";
		$fSockParameter .= "Host: www.octopush-dm.com\r\n";
		$fSockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fSockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fSockParameter .= $stringToPost;

		return $this->sendRequest($fSockParameter);
	}

	public function displayConfig(){

		$config = ACYMSMS::config();
		$typeData = array();
		$typeData[] = JHTML::_('select.option', 'XXX', 'LowCost', 'value', 'text');
		$typeData[] = JHTML::_('select.option', 'FR', 'Premium', 'value', 'text');
		$typeData[] = JHTML::_('select.option', 'WWW', 'Monde', 'value', 'text');


		$typeOption = JHTML::_('select.genericlist', $typeData, 'data[senderprofile][senderprofile_params][smsType]', 'class="inputbox chzn-done" style="width:100px;"', 'value', 'text', @$this->smsType);
		?>
		<table>
			<tr>
				<td class="key">
					<label for="senderprofile_userLogin"><?php echo JText::_('SMS_USERNAME') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][userLogin]" id="senderprofile_userLogin" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->userLogin, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_apiKey"><?php echo JText::_('SMS_API_KEY') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][apiKey]" id="senderprofile_apiKey" class="inputbox" style="..." value="<?php echo htmlspecialchars(@$this->apiKey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_smstype"><?php echo JText::_('SMS_TYPE') ?></label>
				</td>
				<td>
					<?php echo $typeOption; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_smsSender"><?php echo JText::_('SMS_SENDER') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][smsSender]" id="senderprofile_smsSender" class="inputbox" style="..." value="<?php echo htmlspecialchars(@$this->smsSender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>

		<?php

		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'Octopush').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=octopush&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}


	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			$this->errors[] = "API didn't answer";
			return false;
		}

		$xml = simplexml_load_string($res);

		if(empty($xml->error_code)){
			$this->errors[] = "Can't find the error code";
			return false;
		}


		$errorCode = (int)$xml->error_code;

		if($errorCode == 000){
			return true;
		}else{
			$errorDetails = $this->getError($errorCode);
			$this->errors[] = $errorDetails;
			return false;
		}
	}

	protected function getError($numError){
		$error = array();
		$error[000] = 'OK (pas d’erreur).';
		$error[100] = 'Paramètres POST manquant.';
		$error[101] = 'Mauvais identifiants.';
		$error[102] = 'Texte trop long.';
		$error[103] = 'Pas de destinataires.';
		$error[104] = 'Pas assez de crédit.';
		$error[105] = 'Pas assez de crédit, mais une commande est en cours de validation';
		$error[106] = 'Valeur du champ « sms_sender » invalide.';
		$error[107] = 'Champ « sms_text » manquant.';
		$error[108] = 'Champ « user_login » manquant.';
		$error[109] = 'Champ « user_password » manquant.';
		$error[110] = 'Champ « sms_recipients » manquant.';
		$error[112] = 'Champ « sms_type » manquant.';
		$error[113] = 'Votre compte Octopush n’est pas activé : vous devez valider votre ligne mobile.';
		$error[114] = 'Vous avez été blacklisté.';
		$error[115] = 'Vous avez renseigné un champ optionnel de publipostage de manière incorrecte.';
		$error[116] = 'Service non disponible.';
		$error[117] = 'Destinataires mal formatés.';
		$error[118] = 'Indiquez si vous souhaitez envoyer de SMS de test ou si vous l\'avez reçu et validé.';
		$error[119] = 'Vous ne pouvez pas envoyer de SMS de plus de 160 caractères.';
		$error[120] = 'Un SMS avec le même request_id a déjà été envoyé.';
		$error[121] = 'En sms Premium, la mention "STOP au XXXXX" (à recopier TELLE QUELLE, sans les guillemets, et avec les 5 X) est obligatoire et doit figurer dans votre texte (respecter les majuscules).';
		$error[122] = 'En SMS Standard, la mention "no PUB rep STOP" (à recopier telle quelle) est obligatoire et doit figurer dans votre texte (respecter les majuscules).';
		$error[123] = 'Le champ request_sha1 est manquant.';
		$error[124] = 'Le champ request_sha1 ne correspond pas. La donnée a été mal renseignée ou la trame contient une erreur. La requête est rejetée.';
		$error[125] = 'Une erreur non définie est survenue. Merci de contacter le support.';
		$error[126] = 'Une campagne SMS est déjà en attente de validation pour envoi. Vous devez la valider ou l\'annuler pour pouvoir en lancer une autre.';
		$error[127] = 'Une campagne SMS est déjà en attente d’estimation. Vous devez attendre que le calcul soit terminé pour en lancer une autre.';
		$error[128] = 'Trop de tentatives effectuées. Vous devez recommencer une nouvelles campagne.';
		$error[150] = 'Le pays correspondant au numéro de mobile transmis n’a pas été trouvé.';
		$error[151] = 'Le pays du destinataire a été trouvé, mais n’est pas couvert par notre plateforme.';
		$error[152] = 'Vous ne pouvez pas envoyer de SMS Standard vers ce pays. Choisissez le SMS Premium.';
		$error[153] = 'La route étant congestionnée, ce type de SMS ne permet pas un envoi immédiat. Si votre envoi est urgent, merci de bien vouloir utiliser un autre type de SMS.';

		$returnError = array_key_exists($numError, $error) ? $error[$numError] : 'Error not found : '.$numError;
		return $returnError;
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	public function getBalance(){
		$fsockParameter = "GET /api/balance?user_login=".$this->userLogin."&api_key=".$this->apiKey." HTTP/1.1\r\n";
		$fsockParameter .= "Host: www.octopush-dm.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			$this->errors[] = "API didn't answer for the balance";
			return false;
		}


		$xml = simplexml_load_string($res);
		if(empty($xml) || !isset($xml->balance[0]) || !isset($xml->balance[1])){
			$this->errors[] = "Can't find the balance information";
			return false;
		}

		$premiumRemainingCredits = (int)$xml->balance[0];
		$lowcostRemainingCredits = (int)$xml->balance[1];

		return array("LowCost" => $lowcostRemainingCredits, "Premium" => $premiumRemainingCredits);
	}

	private function displayBalance(){
		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["LowCost"].' LowCost'), 'message');
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["Premium"].' Premium'), 'message');
	}
}

