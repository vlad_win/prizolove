<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_iphoneps_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $sendername;

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;

	public $name = 'Iphoneps';

	public $domain = "iphone.ps";
	public $port = 80;


	public function openSend($message, $phone){

		$params = array();

		$params['message'] = $message;

		$params['to'] = $this->checkNum($phone);
		$params['comm'] = 'sendsms';
		$params['user'] = $this->username;
		$params['pass'] = $this->password;

		if(!empty($this->sendername)) $params['sender'] = $this->sendername;

		if($this->unicodeChar($message)){
			$params['lang'] = 'ar';
		}else    $params['lang'] = 'en';

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "GET /api.php?".$stringToPost." HTTP/1.1\r\n";
		$fsockParameter .= "Host: www.iphone.ps\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_sendername"><?php echo JText::_('SMS_SENDER'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][sendername]" id="senderprofile_sendername" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->sendername, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))){
			$this->displayBalance();
		}
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(strpos($res, ':') === false){
			$errors = array('-100' => 'Failed sent by SMS provider', '-110' => 'Wrong Username or Password', '-111' => 'The account is not activated', '-112' => 'Blocked account', '-113' => 'Not enough balance', '-114' => 'The service is not available for now', '-115' => 'The sender not available (if user have no opened sender) Note: if the sender opened will allow any sender', '-116' => 'Invalid sender name');

			foreach($errors as $oneError => $errorMsg){
				if($res == $oneError){
					$this->errors[] = $errorMsg;
					return false;
				}
			}
			$this->errors[] = $res;
			return false;
		}else{
			$errors = array('-999' => 'Failed sent by SMS provider', 'u' => 'Unknown Message status', '-2' => 'Invalid destination or not support country');

			foreach($errors as $oneError => $errorMsg){
				if($res == $oneError){
					$this->errors[] = $errorMsg;
					return false;
				}
			}

			$res = explode(':', $res);
			if($res[0] == '1'){
				return true;
			}
		}
	}

	public function getBalance(){
		$fsockParameter = "GET /api.php?comm=chk_balance&user=".$this->username."&pass=".$this->password." HTTP/1.1\r\n";
		$fsockParameter .= "Host: www.iphone.ps\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		return array("default" => $res);
	}

	private function displayBalance(){

		$balance = $this->getBalance();

		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}
}
