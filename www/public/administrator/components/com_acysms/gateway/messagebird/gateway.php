<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_messagebird_gateway extends ACYSMSGateway_default_gateway{

	public $ApiKey;
	public $sender;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = false;

	public $domain = "ssl://rest.messagebird.com";
	public $port = 443;

	public $name = 'MessageBird';
	public $creditsUrl = 'https://www.messagebird.com/en-us/pricing';


	public function openSend($message, $phone){

		$params = array();
		$params['recipients'] = '+'.$this->checkNum($phone);
		$params['username'] = $this->username;
		$params['password'] = $this->password;
		$params['body'] = $this->checkMessage($message);
		$params['reference'] = $this->smsid = 'ACYSMS_'.time().'_'.$this->username.'_'.$params['recipients'];
		$params['originator'] = $this->sender;


		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /messages HTTP/1.1\r\n";
		$fsockParameter .= "Host: rest.messagebird.com\r\n";
		$fsockParameter .= "Authorization: AccessKey ".$this->ApiKey."\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;


		$result = $this->sendRequest($fsockParameter);
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();

		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_sender"><?php echo JText::_('SMS_SENDER'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][sender]" id="senderprofile_sender" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->sender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_ApiKey"><?php echo 'API Key'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][ApiKey]" id="senderprofile_ApiKey" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->ApiKey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'MessageBird').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=messagebird&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	protected function interpretSendResult($result){

		if(!strpos(strtolower($result), '201 created')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$resObject = json_decode($res);
		if(!$resObject){
			$this->errors[] = $res;
			return false;
		}

		if($resObject->recipients->totalCount == 1) return true;

		$this->errors[] = $res;
		return false;
	}


	public function getBalance(){
		$fsockParameter = "GET /api/credits?username=".$this->username."&password=".$this->password." HTTP/1.1\r\n";
		$fsockParameter .= "Host: ".$this->domain." \r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(preg_match('#<credits>(.*)</credits>#Ui', $res, $credits)){
			$nbCredit = $credits[1];
		}
		if(preg_match('#<responseCode>(.*)</responseCode>#Ui', $res, $responseCode)){
			$nbError = $responseCode[1];
		}
		if(!isset($nbError)){
			return array("default" => (int)$nbCredit);
		}else{
			if((string)$nbError != '01') //we have an error
			{
				ACYSMS::enqueueMessage($this->getErrors((string)$nbError), 'error');
			}else //we don't have an error but we don't have credits
			{
				ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', 0), 'message');
			}
			return false;
		}
	}

	private function displayBalance(){
		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	public function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['delivered'] = array(5, "Delivered to mobile");
		$status['not delivered'] = array(-1, "The message could	not	be delivered");
		$status['buffered'] = array(4, "The message is on hold (phone cannot be reached/is off)");


		$messageStatus = strtolower(JRequest::getVar("STATUS", ''));
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';

		if($messageStatus == 'delivered') $apiAnswer->statsdetails_received_date = time();

		$smsId = JRequest::getVar("REFERENCE", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message reference';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}


	public function answer(){
		$apiAnswer = new stdClass();

		$body = JRequest::getString('body', '');
		$message = JRequest::getString('message', '');

		if(!empty($body)){
			$apiAnswer->answer_body = $body;
		}else if(!empty($message)) $apiAnswer->answer_body = $message;

		$receive_datetime = JRequest::getString('receive_datetime', '');
		$createdDatetime = JRequest::getString('createdDatetime', '');

		if(!empty($receive_datetime)){
			$apiAnswer->answer_date = strtotime($receive_datetime);
		}else if(!empty($createdDatetime)) $apiAnswer->answer_date = strtotime($receive_datetime);

		$originator = JRequest::getString('originator', '');

		if(!empty($originator)) $apiAnswer->answer_from = $originator;

		$shortCode = JRequest::getString('shortcode', '');
		$recipient = JRequest::getString('recipient', '');

		if(!empty($shortCode)){
			$apiAnswer->answer_to = $shortCode;
		}else if(!empty($recipient)) $apiAnswer->answer_to = $recipient;

		return $apiAnswer;
	}
}
