<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_textmagic_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $apikey;
	public $senderid;
	public $waittosend = 0;
	public $connectionInformations;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $name = 'TextMagic';
	public $creditsUrl = 'https://www.textmagic.com/sms-pricing/';

	public $domain = 'ssl://rest.textmagic.com';
	public $port = 443;

	public function openSend($message, $phone){

		$params = array();

		$params['phones'] = $this->checkNum($phone);
		$params['text'] = $message;


		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /api/v2/messages HTTP/1.1\r\n";
		$fsockParameter .= "Host: rest.textmagic.com\r\n";
		$fsockParameter .= "X-TM-Username: ".$this->username."\r\n";
		$fsockParameter .= "X-TM-Key: ".$this->apikey."\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SENDER_ID'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]" id="senderprofile_senderid" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_apikey"><?php echo 'API Key'; ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][apikey]" id="senderprofile_apikey" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->apikey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'TextMagic').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=textmagic&pass='.$config->get('pass').'</li>';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'TextMagic').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=textmagic&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	public function afterSaveConfig($senderProfile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	public function getBalance(){
		$fsockParameter = "GET /api/v2/user HTTP/1.1\r\n";
		$fsockParameter .= "Host: rest.textmagic.com\r\n";
		$fsockParameter .= "X-TM-Username: ".$this->username."\r\n";
		$fsockParameter .= "X-TM-Key: ".$this->apikey."\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);
		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		$res = json_decode(trim(substr($result, strpos($result, "\r\n\r\n"))));

		if((!strpos($result, '200 OK'))){
			ACYSMS::enqueueMessage($res->message, 'error');
			return false;
		}
		return array("default" => $res->balance);
	}

	private function displayBalance(){
		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	protected function interpretSendResult($result){

		$res = json_decode(trim(substr($result, strpos($result, "\r\n\r\n"))));

		if(!strpos($result, '200 OK') && !strpos($result, '201 Created')){
			$this->errors[] = $res->message;
			return false;
		}
		$this->smsid = $res->messageId;
		return true;
	}

	public function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['q'] = array("4", "The message is queued on the TextMagic server.");
		$status['r'] = array("3", "The message has been sent to the mobile operator.");
		$status['a'] = array("3", "The mobile operator has acknowledged the message.");
		$status['b'] = array("3", "The mobile operator has queued the message.");
		$status['d'] = array("5", "The message has been successfully delivered to the handset.");
		$status['f'] = array("-1", "An error occurred while delivering message.");
		$status['e'] = array("-1", "An error occurred while sending message.");
		$status['j'] = array("-1", "The mobile operator has rejected the message.");
		$status['u'] = array("-1", "The status is unknown.");
		$status['s'] = array("-1", "This message is scheduled to be sent later.");


		$completed_time = JRequest::getVar("timestamp", '');

		$messageStatus = JRequest::getVar("status", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 11){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = JRequest::getVar("id", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getString("messageTime", '');

		$apiAnswer->answer_body = JRequest::getString("text", '');

		$sender = JRequest::getString("sender", '');
		$answer_to = JRequest::getString("receiver", '');

		if(!empty($sender)) $apiAnswer->answer_from = '+'.$sender;
		if(!empty($answer_to)) $apiAnswer->answer_to = '+'.$answer_to;

		return $apiAnswer;
	}


}
