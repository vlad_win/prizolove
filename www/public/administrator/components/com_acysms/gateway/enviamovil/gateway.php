<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_enviamovil_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;

	public $waittosend = 0;
	public $connectionInformations;

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;

	public $name = 'Envia Movil';

	public $port = 80;
	public $domain = 'envia-movil.com';

	public function openSend($message, $phone){

		$params = array();


		$params['mensaje'] = $message;
		$params['numero'] = $this->checkNum($phone);

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}

		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "GET /api/Envios?".$stringToPost." HTTP/1.1\r\n";
		$fsockParameter .= "Host: envia-movil.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Authorization: Basic ".base64_encode($this->username.":".$this->password)."\r\n\r\n";


		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}


	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo 'Envia Movil ID'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo 'Envia Movil Key'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][password]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
		</table>
		<?php
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(strpos($res, 'enviado') == !false){
			return true;
		}else{
			$this->errors[] = $res;
			return false;
		}
	}
}
