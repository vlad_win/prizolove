<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_apyrtech_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $sender;

	public $waittosend = 0;
	public $connectionInformations;

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;

	public $name = 'Apyrtech';


	public $port = 80;
	public $domain = 'sms.apyrtech.com';

	public function openSend($message, $phone){

		$params = array();

		$params['username'] = $this->username;
		$params['password'] = $this->password;

		if(!empty($this->sender)) $params['sender'] = $this->sender;


		$params['to'] = preg_replace('/91/', '', str_replace('+', '', $phone), 1);
		if($this->unicodeChar($message)){
			$params['message'] = bin2hex(mb_convert_encoding($message, "UTF-16", "UTF-8"));
			$params['unicode'] = '1';
		}else{
			$params['message'] = $message;
		}

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}

		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /api/web2sms.php HTTP/1.1\r\n";
		$fsockParameter .= "Host: sms.apyrtech.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;


		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][password]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_sender"><?php echo JText::_('SMS_SENDER'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][sender]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->sender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
		</table>
		<?php
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(strpos($res, 'ID') == !false){
			return true;
		}else{
			$this->errors[] = $res;
			return false;
		}
	}
}
