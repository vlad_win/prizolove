<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_altiria_gateway extends ACYSMSGateway_default_gateway{

	public $domainID;
	public $login;
	public $password;
	public $waittosend = 0;
	public $sender;


	public $domain = 'www.altiria.net';
	public $port = 80;

	public $name = "Altiria";

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = false;


	public function openSend($message, $phone){

		$phoneWithoutPlus = $this->checkNum($phone);


		$params = array();
		$params['cmd'] = 'sendsms';
		$params['domainId'] = $this->domainID;
		$params['login'] = $this->login;
		$params['passwd'] = $this->password;
		$params['msg'] = $message;
		$params['dest'] = $phoneWithoutPlus;
		$params['senderId'] = $this->sender;
		$params['ack'] = 'true';
		if($this->unicodeChar($message)){
			$params['encoding'] = 'unicode';
		}
		$params['concat'] = 'true';


		$stringToPost = '';

		foreach($params as $aParam => $value){
			$value = urlencode($value);
			$stringToPost .= '&'.$aParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, "&");

		$fSockParameter = "POST /api/http HTTP/1.1\r\n";
		$fSockParameter .= "Host: www.altiria.net\r\n";
		$fSockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fSockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fSockParameter .= $stringToPost;

		return $this->sendRequest($fSockParameter);
	}


	public function displayConfig(){

		$config = ACYSMS::config();

		?>
		<table>
			<tr>
				<td class="key">
					<label for="senderprofile_domainId"><?php echo JText::_('SMS_DOMAIN') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][domainID]" id="senderprofile_domainId" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->domainID, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_login"><?php echo JText::_('SMS_LOGIN') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][login]" id="senderprofile_login" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->login, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input type="password" name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_sender"><?php echo JText::_('SMS_FROM') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][sender]" id="senderprofile_sender" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->sender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>

		</table>

		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '
		<ul id="gateway_addresses">';
			echo '
			<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'Altiria').'<br/>'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=altiria&pass='.$config->get('pass').'</li>
			';
			echo '
		</ul>';
		}
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}


	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(strpos($res, 'OK') !== false){
			$pattern = '#idAck:([a-z0-9]*)#is';
			$matches = array();
			preg_match($pattern, $res, $matches);
			if(!empty($matches)) $this->smsid = $matches[1];
			return true;
		}else{
			$explodedResult = explode(':', $res);
			$this->errors[] = $this->getErrors($explodedResult[count($explodedResult) - 1]);
			return false;
		}
	}


	public function getBalance(){

		$params = array();
		$params['cmd'] = 'getcredit';
		$params['domainId'] = $this->domainID;
		$params['login'] = $this->login;
		$params['passwd'] = $this->password;

		$stringToPost = '';

		foreach($params as $aParam => $value){
			$value = urlencode($value);
			$stringToPost .= '&'.$aParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, "&");


		$fSockParameter = "POST /api/http HTTP/1.1\r\n";
		$fSockParameter .= "Host: www.altiria.net\r\n";
		$fSockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fSockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fSockParameter .= $stringToPost;


		$idConnection = $this->sendRequest($fSockParameter);
		$result = $this->readResult($idConnection);


		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$explodedRes = explode(':', $res);

		if(strpos($explodedRes[0], 'OK') !== false){
			return array('default' => $explodedRes[1]);
		}else{
			$this->errors[] = 'Error '.$this->getErrors($explodedRes[1]);
			return false;
		}
	}

	private function displayBalance(){

		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	private function getErrors($errorCode){
		$errors = array();
		$errors['001'] = 'Internal error. Please contact technical support';
		$errors['010'] = 'Error in the telephone number format';
		$errors['011'] = 'Error in sending of the command parameters or incorrect codification';
		$errors['013'] = 'The length of the message exceeds the maximum allowed length';
		$errors['014'] = 'The HTTP request uses an invalid character codification';
		$errors['015'] = 'There are not valid recipients to send the message';
		$errors['016'] = 'Duplicated recipient';
		$errors['017'] = 'Empty message';
		$errors['020'] = 'Authentication error';
		$errors['022'] = 'The selected originator for the message is not valid';
		$errors['030'] = 'The URL and the message exceed the maximum allowed length';
		$errors['031'] = 'The length of the URL is incorrect';
		$errors['032'] = 'The URL contains not allowed characters';
		$errors['033'] = 'The SMS destination port is incorrect';
		$errors['034'] = 'The SMS source port is incorrect';

		return $errors[$errorCode];
	}


	public function deliveryReport(){
		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['ENTREGADO'] = array(5, 'Delivered');
		$status['NO ENTREGADO'] = array(-1, 'Final state, the message will be not sent ever');
		$status['ERROR_100'] = array(4, 'The message can\'t be sent now, but it will be reattempted several time after,during a limited period of time');
		$status['ERROR_101'] = array(4, 'The message has not been delivered to the recipient yet, because of a problem in the carrier mobile network. Usually, when the carrier will solve the problems the message will be delivered');


		$informations = JRequest::getVar('notification', '');

		if(empty($informations)) $apiAnswer->statsdetails_error[] = 'Empty status received';

		$informationsExploded = explode(',', $informations);

		if($informationsExploded[2] == 'ENTREGADO'){
			$apiAnswer->statsdetails_received_date = time();
		}

		$smsId = $informationsExploded[1];
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can\'t find the message ID';

		if(!isset($status[$informationsExploded[2]])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$informationsExploded[2];
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$informationsExploded[2]][0];
			$apiAnswer->statsdetails_error[] = $status[$informationsExploded[2]][1];
		}

		$apiAnswer->statsdetails_sms_id = $informationsExploded[1];

		return $apiAnswer;
	}
}
