<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_raspisms_gateway extends ACYSMSGateway_default_gateway{

	public $email;
	public $password;
	public $senderid;
	public $waittosend = 0;
	public $connectionInformations;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $port = 80;
	public $domain;

	public $name = 'RaspiSMS';

	public function openSend($message, $phone){

		if(strpos($this->domain, "/") !== false) $this->domain = substr($this->domain, 0, strpos($this->domain, "/"));

		$params = array();

		$params['numbers'] = $this->checkNum($phone);
		$params['email'] = $this->email;
		$params['password'] = $this->password;
		$params['text'] = $message;


		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /RaspiSMS/smsAPI/send/ HTTP/1.1\r\n";
		$fsockParameter .= "Host: ".$this->domain."\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_domain"><?php echo 'Domaine'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][domain]" id="senderprofile_server" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->domain, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_email"><?php echo JText::_('SMS_EMAIL'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][email]" id="senderprofile_email" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->email, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
	}

	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$error = array(1 => 'Identifiants Incrorects', 2 => 'La création du SMS a échoué', 3 => 'Des arguments obligatoires sont manquants');
		$jsonObject = json_decode($res);

		if($jsonObject->error !== 0){
			$this->errors[] = $error[$jsonObject->error];
			return false;
		}
		return true;
	}

	function checkNum($phoneNumber){
		return '+'.$phoneNumber;
	}
}
