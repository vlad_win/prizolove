<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_diafaan_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $senderid;
	public $domain;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;

	public $name = 'Diafaan';
	public $creditsUrl = '';

	public function openSend($message, $phone){
		$params = array();

		$params['to'] = $this->checkNum($phone);
		$params['username'] = $this->username;
		$params['password'] = $this->password;
		$params['message'] = $message;
		$params['message-type'] = 'Unicode';
		$params['from'] = $this->senderid;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /http/send-message HTTP/1.1\r\n";
		$fsockParameter .= "Host: ".$this->domain."\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you chose is blocked on your server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SENDER_ID'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]"
						   id="senderprofile_senderid" class="inputbox" maxlength="11" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]"
						   id="senderprofile_username" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password"
						   class="inputbox" type="password" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_domain"><?php echo JText::_('SMS_DOMAIN') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][domain]"
						   id="senderprofile_domain" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->domain, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_port"><?php echo JText::_('SMS_PORT') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][port]"
						   id="senderprofile_port" class="inputbox" maxlength="11" style="width:200px;"
						   value="<?php echo empty($this->port) ? 80 : intval($this->port); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
	}

	public function beforeSaveConfig(&$senderprofile){
		$senderprofile->senderprofile_params['domain'] = rtrim(str_replace(array('http://','https://'), '', $senderprofile->senderprofile_params['domain']), '/');
	}

	protected function interpretSendResult($result){

		if(strpos($result, '200 OK') === false){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else{
			$result = trim(substr($result, strpos($result, "\r\n\r\n")));
		}

		if(strpos($result, 'ERROR') !== false){
			$this->errors[] = $result;
			return false;
		}

		return true;
	}
}
