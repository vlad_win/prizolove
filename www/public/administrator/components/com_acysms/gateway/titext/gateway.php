<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_titext_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $senderid;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $name = 'Ti-Text';
	public $creditsUrl = 'http://www.ti-text.com/ti-text/portable/tarifs.twp';

	public $domain = "www.titext.com";


	public function openSend($message, $phone){

		$params = array();

		$params['Recipient'] = $this->checkNum($phone);
		$params['Username'] = $this->username;
		$params['Password'] = $this->password;
		$params['Tracking'] = 'true';
		$params['CustomerId'] = str_replace('+', '', substr(microtime().$phone, -20));

		$params['TypeSMS'] = $this->route;

		if(!empty($this->senderid)) $params['Signature'] = $this->senderid;


		$params['Message'] = $message;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /httpsubmit.jsp HTTP/1.1\r\n";
		$fsockParameter .= "Host: titext.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();

		$routeData = array();
		$routeData[] = JHTML::_('select.option', 'Reponse', 'Reponse', 'value', 'text');
		$routeData[] = JHTML::_('select.option', 'Pro', 'Pro', 'value', 'text');
		$routeData[] = JHTML::_('select.option', 'Normal', 'Normal', 'value', 'text');
		$routeData[] = JHTML::_('select.option', 'Int', 'Int', 'value', 'text');

		$routeOptions = JHTML::_('select.genericlist', $routeData, "data[senderprofile][senderprofile_params][route]", 'size="1" class="chzn-done" style="width:110px;"', 'value', 'text', @$this->route);

		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SENDER_ID'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]" id="senderprofile_senderid" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_route"><?php echo 'Type SMS' ?></label>
				</td>
				<td>
					<?php echo $routeOptions; ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'Ti-Text').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=titext&pass='.$config->get('pass').'</li>';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'Ti-Text').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=titext&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$explodedResult = explode(':', $res);
		if(trim($explodedResult[1]) == 0) return true;

		$this->errors[] = $this->getErrors(trim($explodedResult[1]));
		return false;
	}


	protected function getErrors($errNo){
		$errors = array();
		$errors['0'] = 'In progress (a normal message submission, with no error encountered so far).';
		$errors['500'] = 'L’un des paramètres obligatoires « Username », « Password », « Message »ou « Recipient » est manquant.';
		$errors['501'] = 'Le paramètre « TypeSMS » contient une valeur différente de « Normal » ou « Pro »';
		$errors['502'] = 'Le paramètre « Signature » est manquant alors qu’un SMS Pro a été demandé';
		$errors['503'] = 'Le paramètre « DateToSend » ne respecte pas le format YYYY-MM-DD HH :MN';
		$errors['510'] = 'Le compte n’existe pas, ou la valeur de « password » ne correspond pas.';
		$errors['511'] = 'Ce compte a été détruit';
		$errors['512'] = 'Ce compte a été invalidé';
		$errors['520'] = 'Le destinataire n’est pas un numéro français, ou un numéro européen';
		$errors['550'] = 'Le compte ne dispose plus de crédits pour autoriser cet envoi.';
		$errors['560'] = 'Message trop long';
		return isset($errors[$errNo]) ? 'Error '.$errNo.': '.$errors[$errNo] : 'Unknown error : '.$errNo;
	}

	public function getBalance(){
		$fsockParameter = "GET /M2M/httpaccount.twp?Username=".$this->username."&Password=".$this->password." HTTP/1.1\r\n";
		$fsockParameter .= "Host: titext.com \r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(strpos($res, 'Reponse') !== false){
			$finalCreditInformation = array();
			$smsType = explode(',', $res);
			foreach($smsType as $oneType){
				$oneEntry = explode(':', $oneType);
				$finalCreditInformation[$oneEntry[0]] = $oneEntry[1];
			}
			return $finalCreditInformation;
		}else{
			ACYSMS::enqueueMessage('Invalid username / Password', 'error');
			return false;
		}
	}

	private function displayBalance(){

		$balance = $this->getBalance();

		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		foreach($balance as $oneSMSType => $oneBalance){
			ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $oneBalance.' '.$oneSMSType), 'message');
		}
	}

	public function deliveryReport(){

		$typeStatus = JRequest::getVar("typeStatus", '');
		if($typeStatus == 'response'){
			$this->answer();
			return;
		}

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status[0] = array(5, "Message bien envoyé");
		$status[1] = array(-1, "Impossible de se connecter à la gateway du réseau");
		$status[2] = array(-1, "Message en cours de transmission");
		$status[-1] = array(-1, "Refus d'envoi");
		$status[-2] = array(-1, "Problème d'authentification");
		$status[-3] = array(-1, "Problème de protocole");
		$status[-4] = array(-1, "Destinataire invalide");
		$status[-5] = array(-1, "Erreur inconnue");
		$status[-6] = array(-1, "Durée de vie expirée avant l'envoi");


		$messageStatus = JRequest::getVar("ErrorCode", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 0){
			$apiAnswer->statsdetails_received_date = time();
		}

		$smsId = JRequest::getVar("CustomerId", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getString("received_time", time());

		$apiAnswer->answer_body = JRequest::getString("Text", '');

		$apiAnswer->answer_sms_id = JRequest::getString("CustomerId", '');

		return $apiAnswer;
	}
}
