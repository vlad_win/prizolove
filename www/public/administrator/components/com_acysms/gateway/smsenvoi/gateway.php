<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_smsenvoi_gateway extends ACYSMSGateway_default_gateway{

	public $email;
	public $apiKey;
	public $senderName;
	public $subType;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $port = 80;
	public $domain = 'www.smsenvoi.com';

	public $name = 'SMSENVOI';
	public $creditsUrl = 'https://www.smsenvoi.com/forfaits/consulter/sms/1/';

	public function openSend($message, $phone){

		$params = array();

		$params['message[recipients]'] = $this->checkNum($phone);
		$params['email'] = $this->email;
		$params['apikey'] = $this->apiKey;

		$params['message[type]'] = 'sms';
		$params['message[subtype]'] = $this->subType;

		if(!empty($this->senderName)) $params['message[senderlabel]'] = $this->senderName;

		$params['message[content]'] = $message;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /httpapi/sendsms/ HTTP/1.1\r\n";
		$fsockParameter .= "Host: www.smsenvoi.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();

		$subType = array();
		$subType[] = JHTML::_('select.option', 'LOWCOST', 'Low Cost', 'value', 'text');
		$subType[] = JHTML::_('select.option', 'STANDARD', 'Standard', 'value', 'text');
		$subType[] = JHTML::_('select.option', 'PREMIUM', 'Premium', 'value', 'text');
		$subType[] = JHTML::_('select.option', 'CONCATENE', 'Concaténé', 'value', 'text');
		$subType[] = JHTML::_('select.option', 'MONDE', 'Monde', 'value', 'text');
		$subType[] = JHTML::_('select.option', 'STOP', 'Stop', 'value', 'text');

		$subTypeOptions = JHTML::_('select.genericlist', $subType, "data[senderprofile][senderprofile_params][subType]", 'size="1" class="chzn-done" style="width:auto;"', 'value', 'text', @$this->subType);
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderName"><?php echo JText::_('SMS_SENDER'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderName]" id="senderprofile_senderName" class="inputbox" maxlength="11" style="width:200px;" value="<?php echo htmlspecialchars(@$this->senderName, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_email"><?php echo JText::_('SMS_EMAIL'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][email]" id="senderprofile_email" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->email, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_apiKey"><?php echo 'API Key'; ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][apiKey]" id="senderprofile_apiKey" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->apiKey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_route"><?php echo JText::_('SMS_TYPE') ?></label>
				</td>
				<td>
					<?php echo $subTypeOptions; ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'SMSENVOI').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=smsenvoi&pass='.$config->get('pass').'</li>';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'SMSENVOI').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=smsenvoi&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));


		$resObject = json_decode($res);

		if($resObject == false){
			$this->errors[] = 'Unknown error => '.$res;
			return false;
		}

		if($resObject->success == 1){
			$this->smsid = $resObject->message_id;
			return true;
		}else{
			$this->errors[] = $resObject->message;
			return false;
		}
	}
}
