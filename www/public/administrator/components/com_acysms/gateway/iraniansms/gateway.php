<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_iraniansms_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $senderid;
	public $waittosend = 0;
	public $connectionInformations;

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;

	public $port = 80;
	public $domain = 'panel.iraniansms.com';


	public $name = 'IranianSMS';
	public $creditsUrl = 'http://www.iraniansms.net/?setpage=757';

	public function openSend($message, $phone){

		$params = array();

		$params['to'] = $this->checkNum($phone);
		$params['username'] = $this->username;
		$params['password'] = $this->password;

		if(!empty($this->senderid)) $params['from'] = $this->senderid;

		$params['text'] = $message;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /API/SendSms.ashx HTTP/1.1\r\n";
		$fsockParameter .= "Host: panel.iraniansms.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_senderid"><?php echo JText::_('SMS_SENDER_ID'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][senderid]"
						   id="senderprofile_senderid" class="inputbox" maxlength="11" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->senderid, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]"
						   id="senderprofile_username" class="inputbox" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password"
						   class="inputbox" type="password" style="width:200px;"
						   value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'IranianSMS').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=iraniansms&pass='.$config->get('pass').'&to=$TO&from=$FROM&base64text=$Base64Text</li>';
			echo '</ul>';
		}
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$errors = array('0' => 'Username or password is not correct', '1' => 'You don\'t have enough credits ', '2' => 'Your account restrictions send.', '3' => 'Username is missing', '4' => 'Password is missing', '5' => 'Sender number is missing', '6' => 'You didn\’t set audience number', '7' => 'Message body is missing', '8' => 'Flash parameter is not valid', '9' => 'Sender number not set', '10' => 'SMS not send. It is maybe for your sms number is not valid or system not answer', '11' => 'Your audience more than 80');

		if(array_key_exists($res, $errors)){
			$this->errors[] = $errors[$res];
			return false;
		}
		$this->smsid = $res;
		return true;
	}

	public function getBalance(){
		$fsockParameter = "GET /API/GetCredit.ashx?username=".$this->username."&password=".$this->password." HTTP/1.1\r\n";
		$fsockParameter .= "Host: panel.iraniansms.com \r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br/>', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));


		$errors = array(0 => 'Username is missing', 1 => 'Password is missing', 2 => 'Username or Password is not correct');
		if(!in_array($res, $errors)){
			return array("default" => $res);
		}
		ACYSMS::enqueueMessage($errors[$res], 'error');
		return false;
	}

	private function displayBalance(){

		$balance = $this->getBalance();

		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = time();

		$apiAnswer->answer_body = mb_convert_encoding(base64_decode(JRequest::getString("base64text", '')), 'UTF-8', 'UTF-16LE');

		$sender = JRequest::getString("from", '');
		$msisdn = JRequest::getString("to", '');

		if(!empty($sender)) $apiAnswer->answer_from = '+98'.$sender;
		if(!empty($msisdn)) $apiAnswer->answer_to = $msisdn;


		return $apiAnswer;
	}
}
