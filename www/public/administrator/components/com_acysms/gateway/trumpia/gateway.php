<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_trumpia_gateway extends ACYSMSGateway_default_gateway{
	public $username;
	public $password;
	public $senderid;
	public $apikey;

	public $domain = "api.trumpia.com";

	public $name = 'trumpia';

	public function openSend($message, $phone){

		$allValues = ["apikey" => $this->apikey,
					  "message" => $message,
					  "mobile_number" => $phone
		];

		$stringToPost = '';

		foreach($allValues as $oneInputName => $oneInputValue){
			$value = urlencode($oneInputValue);
			$stringToPost .= '&'.$oneInputName.'='.$value;
		}

		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST http://api.trumpia.com HTTP/1.1\r\n";
		$fsockParameter .= "Host: /http/v2/sendverificationsms\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td class="key">
					<label for="senderprofile_apikey"><?php echo JText::_('SMS_APIKEY') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][apikey]" id="senderprofile_apikey" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->apikey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_sender"><?php echo JText::_('SMS_FROM') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][sender]" id="senderprofile_sender" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->sender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>

		</table>

		<?php
	}

	protected function interpretSendResult($result){
		var_dump($result);
		exit;
	}
}
