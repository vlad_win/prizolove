<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_smshosting_gateway extends ACYSMSGateway_default_gateway{

	public $from;
	public $waittosend = 0;
	public $authKey;
	public $authSecret;
	public $quality;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;


	public $domain = 'ssl://api.smshosting.it';
	public $port = 443;

	public $name = 'SMSHosting';
	public $creditsUrl = 'https://www.smshosting.it/en/pricing';


	public function openSend($message, $phone){

		$config = ACYSMS::config();

		if(empty($this->from)){
			$this->errors[] = 'In high quality, a sender name is required.';
			return false;
		}


		$params = array();

		$params['to'] = $this->checkNum($phone);
		$params['text'] = $message;
		$params['from'] = $this->from;
		$params['encoding'] = 'AUTO';
		$params['statusCallback'] = ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=smshosting&pass='.$config->get('pass');

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');


		$fsockParameter = "POST /rest/api/sms/send HTTP/1.1\r\n";
		$fsockParameter .= "Host: api.smshosting.it\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n";
		$fsockParameter .= "Authorization: Basic ".base64_encode($this->authKey.":".$this->authSecret)."\r\n\r\n";
		$fsockParameter .= $stringToPost;


		return $this->sendRequest($fsockParameter);
	}

	public function displayConfig(){

		$config = ACYSMS::config();

		?>
		<table>
			<tr>
				<td class="key">
					<label for="senderprofile_authKey"><?php echo 'AUTH_KEY'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][authKey]" id="senderprofile_authKey" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->authKey, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_authSecret"><?php echo 'AUTH_SECRET'; ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][authSecret]" id="senderprofile_authSecret" class="inputbox" type="text" style="width:200px;" value="<?php echo htmlspecialchars(@$this->authSecret, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_from"><?php echo JText::_('SMS_FROM'); ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][from]" id="senderprofile_from" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->from, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'SMSHosting').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=smshosting&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	protected function interpretSendResult($result){
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));
		if(!strpos($result, '200')){

			$arrayRes = json_decode($res, true);
			$this->errors[] = 'Error 200 KO => '.$arrayRes['errorCode'].' : '.$arrayRes['errorMsg'];
			return false;
		}else{
			$decodedRes = json_decode($res, true);
			if($decodedRes['sms'][0]['status'] == 'NOT_INSERTED'){
				$this->errors[] = $decodedRes['sms'][0]['statusDetail'];
				return false;
			}
		}
		$this->smsid = $decodedRes['sms'][0]['id'];

		return true;
	}


	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	public function getBalance(){

		$fsockParameter = "GET /rest/api/user HTTP/1.1\r\n";
		$fsockParameter .= "Host: api.smshosting.it\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Authorization: Basic ".base64_encode($this->authKey.":".$this->authSecret)."\r\n\r\n";


		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if(!strpos($result, '200')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			$this->errors[] = "We were not able to find the API answer for the balance";
			return false;
		}

		$arrayBalance = json_decode($res, true);

		$balances = array();
		foreach($arrayBalance as $key => $value){
			if(strstr($key, 'sms')){
				$key = ucfirst($key);
				$balances[$key] = $value;
			}
		}

		return $balances;
	}

	private function displayBalance(){
		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		foreach($balance as $key => $value){
			ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $value.' '.$key), 'message');
		}
	}

	public function deliveryReport(){
		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['DELIVERED'] = array(5, "Sms delivered to recipient SIM (final status)");
		$status['SENT'] = array(3, "Sms sent to network operator");
		$status['NOSENT'] = array(0, "Sms not sent to network operator (final status)");
		$status['PENDING'] = array(4, "Sms queued for sending");
		$status['NOTDELIVERED'] = array(-1, "Sms not delivered to recipient SIM (final status)");

		$completed_time = JRequest::getVar("completed_time", '');

		$messageStatus = JRequest::getVar("status", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 'DELIVERED'){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = JRequest::getVar("id", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}


	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_body = JRequest::getString("text", '');

		$apiAnswer->answer_date = time();

		$sender = JRequest::getString("number", '');

		if(!empty($sender)) $apiAnswer->answer_from = $sender;


		return $apiAnswer;
	}
}
