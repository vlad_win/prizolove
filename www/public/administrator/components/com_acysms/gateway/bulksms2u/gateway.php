<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_bulksms2u_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $from;
	public $waittosend = 0;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = false;

	public $domain = "bulksms2u.com";
	public $port = 80;

	public $name = 'Bulksms2u';
	public $creditsUrl = '';

	public function openSend($message, $phone){
		$params = array();
		$params['mobile'] = '+'.$phone;
		$params['username'] = $this->username;
		$params['password'] = $this->password;
		$params['message'] = $message;
		$params['type'] = ($this->unicodeChar($message)) ? 3 : 1;
		$params['sender'] = $this->from;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /websmsapi/ISendSMS.aspx HTTP/1.1\r\n";
		$fsockParameter .= "Host: bulksms2u.com\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		return $this->sendRequest($fsockParameter);
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_from"><?php echo JText::_('SMS_FROM'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][from]" id="senderprofile_from" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->from, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<td colspan="2">
				<label for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
			</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'BulkSMS2u').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=bulksms2u&pass='.$config->get('pass').'</li>';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'BulkSMS2u').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=bulksms2u&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}

	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$explodedResult = explode(':', $res);
		if(empty($explodedResult)){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}

		if($explodedResult[0] == '1701'){
			$this->smsid = $explodedResult[1];
			return true;
		}

		ACYSMS::enqueueMessage($this->getErrors($explodedResult[0]), 'error');
		return false;
	}

	public function getBalance(){
		$fsockParameter = "GET /websmsapi/creditsLeft.aspx?username=".$this->username."&password=".$this->password." HTTP/1.1\r\n";
		$fsockParameter .= "Host: bulksms2u.com \r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if($result === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));


		return array("default" => $res);
	}

	private function displayBalance(){
		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}

	protected function getErrors($errNo){
		$errors = array();
		$errors['1702'] = 'Invalid Username/Password';
		$errors['1703'] = 'Internal Server Error';
		$errors['1704'] = 'Insufficient Credits';
		$errors['1705'] = 'Invalid Mobile Number';
		$errors['1706'] = 'Invalid Message / Invalid SenderID';
		$errors['1707'] = 'Transfer Credits Successful';
		$errors['1708'] = 'Account not existing for Credits Transfer';
		$errors['1709'] = 'Invalid Credits Value for Credits Transfer';
		$errors['1718'] = 'Duplicate record received';


		return isset($errors[$errNo]) ? 'Error '.$errNo.': '.$errors[$errNo] : 'Unknown error : '.$errNo;
	}

	public function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['7'] = array(-99, 'Wrong ID or report has expired');
		$status['8'] = array(-2, 'Messages expired');
		$status['403'] = array(1, 'Message is sent');
		$status['6'] = array(5, 'Delivered');
		$status['9'] = array(-1, 'Unknown Error');
		$status['10'] = array(-1, 'Rejected');


		$completed_time = JRequest::getVar("donedate", '');

		$messageStatus = JRequest::getVar("Status", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 404){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = JRequest::getVar("MsgId", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}

	public function answer(){

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getString("received_time", time());

		$apiAnswer->answer_body = JRequest::getString("Msg", '');

		$sender = JRequest::getString("Sender", '');
		$msisdn = JRequest::getString("ShortCode", '');

		if(!empty($sender)) $apiAnswer->answer_from = '+'.$sender;
		if(!empty($msisdn)) $apiAnswer->answer_to = '+'.$msisdn;


		return $apiAnswer;
	}

	public function closeRequest(){
		header('Content-Type: text/html');
		echo "OK";
	}
}
