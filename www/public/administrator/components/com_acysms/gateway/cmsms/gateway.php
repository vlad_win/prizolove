<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_cmsms_gateway extends ACYSMSGateway_default_gateway{

	public $token;
	public $sender;
	public $waittosend = 0;


	public $domain = 'ssl://sgw01.cm.nl';
	public $port = 443;

	public $name = "CMSMS";

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;


	public function openSend($message, $phone){
		$xml = new SimpleXMLElement('<MESSAGES/>');

		$auth = $xml->addChild('AUTHENTICATION');

		$auth->addChild('PRODUCTTOKEN', $this->token);

		$msg = $xml->addChild('MSG');

		$msg->addChild('FROM', $this->sender);
		$msg->addChild('TO', $phone);
		if($this->unicodeChar($message)){
			$msg->addChild('DCS', '8');
		}
		$msg->addChild('BODY', $message);


		$msgClass = ACYSMS::get('class.message');
		$numberOfParts = $msgClass->countMessageParts($message);

		$msg->addChild('MAXIMUMNUMBEROFMESSAGEPARTS', $numberOfParts->nbParts);

		$stringToPost = $xml->asXML();

		$fSockParameter = "POST /gateway.ashx HTTP/1.1\r\n";
		$fSockParameter .= "Host: sgw01.cm.nl\r\n";
		$fSockParameter .= "Content-type: application/xml\r\n";
		$fSockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fSockParameter .= $stringToPost;

		return $this->sendRequest($fSockParameter);
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td class="key">
					<label for="senderprofile_token"><?php echo JText::_('SMS_TOKEN') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][token]" id="senderprofile_token" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->token, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_sender"><?php echo JText::_('SMS_FROM') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][sender]" id="senderprofile_sender" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->sender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>

		</table>

		<?php
	}


	protected function interpretSendResult($result){

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			return true;
		}

		if(strpos($res, 'Error') !== false){
			$this->errors[] = $res;
			return false;
		}
	}
}
