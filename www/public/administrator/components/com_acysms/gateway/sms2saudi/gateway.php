<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class ACYSMSGateway_sms2saudi_gateway extends ACYSMSGateway_default_gateway{

	public $username;
	public $password;
	public $language;
	public $sender;
	public $waittosend = 0;


	public $domain;
	public $port = "80";

	public $name = "sms2saudi";
	public $creditsUrl = 'http://www.sms2saudi.com/en-pricing.htm';

	public $sendMessage = true;
	public $deliveryReport = false;
	public $answerManagement = false;


	public function openSend($message, $phone){


		$phoneWithoutPlus = $this->checkNum($phone);

		$this->language = '1';
		if($this->unicodeChar($message)){
			$this->language = '3';
			$message = bin2hex(mb_convert_encoding($message, "UTF-16", "UTF-8"));
		}

		$params = array();
		$params['username'] = $this->username;
		$params['password'] = $this->password;
		$params['language'] = $this->language;
		$params['sender'] = $this->sender;
		$params['mobile'] = $phoneWithoutPlus;
		$params['message'] = $message;

		$stringToPost = '';

		foreach($params as $aParam => $value){
			$value = urlencode($value);
			$stringToPost .= '&'.$aParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, "&");

		$fSockParameter = "POST /api/send.aspx HTTP/1.1\r\n";
		$fSockParameter .= "Host:".$this->domain."\r\n";
		$fSockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fSockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fSockParameter .= $stringToPost;


		return $this->sendRequest($fSockParameter);
	}

	public function displayConfig(){
		?>
		<table>
			<tr>
				<td class="key">
					<label for="senderprofile_username"><?php echo JText::_('SMS_USERNAME') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][username]" id="senderprofile_username" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->username, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input type="password" name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_domain"><?php echo JText::_('SMS_DOMAIN') ?></label>
				</td>
				<td>
					<input typ="text" name="data[senderprofile][senderprofile_params][domain]" id="senderprofile_domain" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->domain, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td class="key">
					<label for="senderprofile_sender"><?php echo JText::_('SMS_FROM') ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][sender]" id="senderprofile_sender" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->sender, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>

		</table>


		<?php
	}


	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			$this->errors[] = "We were not able to find the API answer";
			return false;
		}

		if(strpos($res, 'error') !== false){
			$arrayRes = explode(',', $res);
			$this->errors[] = 'Error : '.$arrayRes[1];
			return false;
		}
		return true;
	}

	public function afterSaveConfig($senderprofile){
		if(in_array(JRequest::getCmd('task'), array('save', 'apply'))) $this->displayBalance();
	}


	public function getBalance(){

		$fsockParameter = "GET /api/balance.aspx?username=".$this->username."&password=".$this->password." HTTP/1.1\r\n";
		$fsockParameter .= "Host: ".$this->domain."\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n\r\n";

		$idConnection = $this->sendRequest($fsockParameter);
		$result = $this->readResult($idConnection);

		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}
		$res = trim(substr($result, strpos($result, "\r\n\r\n")));

		if(empty($res)){
			$this->errors[] = "We were not able to find the API answer for the balance";
			return false;
		}

		$creditLeft = (int)$res;
		return array("default" => $creditLeft);
	}

	private function displayBalance(){

		$balance = $this->getBalance();
		if($balance === false){
			ACYSMS::enqueueMessage(implode('<br />', $this->errors), 'error');
			return false;
		}
		ACYSMS::enqueueMessage(JText::sprintf('SMS_CREDIT_LEFT_ACCOUNT', $balance["default"]), 'message');
	}


	protected function checkNum($phone){
		$internationalPhone = str_replace('+', '', $phone);
		return $internationalPhone;
	}
}
