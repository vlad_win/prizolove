<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php


class ACYSMSGateway_smsgatewayme_gateway extends ACYSMSGateway_default_gateway{

	public $email;
	public $password;
	public $device;
	public $waittosend = 0;
	public $connectionInformations;

	public $sendMessage = true;
	public $deliveryReport = true;
	public $answerManagement = true;

	public $port = 443;
	public $domain = 'ssl://smsgateway.me';

	public $name = 'SMSGateway.me';
	public $creditsUrl = 'https://smsgateway.me/pricing';

	public function openSend($message, $phone){

		$params = array();
		$params['number'] = $this->checkNum($phone);
		$params['device'] = $this->device;
		$params['email'] = $this->email;
		$params['password'] = $this->password;
		$params['message'] = $message;

		$stringToPost = '';
		foreach($params as $oneParam => $value){
			$value = urlencode(($value));
			$stringToPost .= '&'.$oneParam.'='.$value;
		}
		$stringToPost = ltrim($stringToPost, '&');

		$fsockParameter = "POST /api/v3/messages/send HTTP/1.1\r\n";
		$fsockParameter .= "Host: smsgateway.me\r\n";
		$fsockParameter .= "Content-type: application/x-www-form-urlencoded\r\n";
		$fsockParameter .= "Content-length: ".strlen($stringToPost)."\r\n\r\n";
		$fsockParameter .= $stringToPost;

		$result = $this->sendRequest($fsockParameter);
		if(!$result && strpos(implode(',', $this->errors), 'Connection timed out') !== false && $this->port != '80'){
			$this->errors[] = 'It seems that the port you choose is blocked on you server. You should try to select the port 80';
		}
		return $result;
	}

	public function displayConfig(){
		$config = ACYSMS::config();
		?>
		<table>
			<tr>
				<td>
					<label for="senderprofile_email"><?php echo JText::_('SMS_EMAIL'); ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][email]" id="senderprofile_email" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->email, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_password"><?php echo JText::_('SMS_PASSWORD') ?></label>
				</td>
				<td>
					<input name="data[senderprofile][senderprofile_params][password]" id="senderprofile_password" class="inputbox" type="password" style="width:200px;" value="<?php echo htmlspecialchars(@$this->password, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="senderprofile_device"><?php echo 'Device ID'; ?></label>
				</td>
				<td>
					<input type="text" name="data[senderprofile][senderprofile_params][device]" id="senderprofile_device" class="inputbox" style="width:200px;" value="<?php echo htmlspecialchars(@$this->device, ENT_COMPAT, 'UTF-8'); ?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label
						for="senderprofile_waittosend"><?php echo JText::sprintf('SMS_WAIT_TO_SEND', '<input type="text" style="width:20px;" name="data[senderprofile][senderprofile_params][waittosend]" id="senderprofile_waittosend" class="inputbox" value="'.intval($this->waittosend).'" />'); ?></label>
				</td>
			</tr>
		</table>
		<?php
		if(strpos(ACYSMS_LIVE, 'localhost') !== false){
			echo JText::_('SMS_LOCALHOST_PROBLEM');
		}else{
			echo '<ul id="gateway_addresses">';
			echo '<li>'.JText::sprintf('SMS_DELIVERY_ADDRESS', 'SMS Gateway').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=deliveryreport&gateway=smsgatewayme&pass='.$config->get('pass').'</li>';
			echo '<li>'.JText::sprintf('SMS_ANSWER_ADDRESS', 'SMS Gateway').'<br />'.ACYSMS_LIVE.'index.php?option=com_acysms&ctrl=answer&gateway=smsgatewayme&pass='.$config->get('pass').'</li>';
			echo '</ul>';
		}
	}

	protected function interpretSendResult($result){
		if(!strpos($result, '200 OK')){
			$this->errors[] = 'Error 200 KO => '.$result;
			return false;
		}else $res = trim(substr($result, strpos($result, "\r\n\r\n")));

		$resObject = json_decode($res);

		if(!empty($resObject->result->success[0]->id)){
			$this->smsid = $resObject->result->success[0]->id;
			return true;
		}else{
			if(!empty($resObject->result->fails[0]->errors)) $this->errors[] = print_r($resObject->result->fails[0]->errors, true);
			return false;
		}
	}

	public function deliveryReport(){

		$status = array();
		$apiAnswer = new stdClass();
		$apiAnswer->statsdetails_error = array();



		$status['delivered'] = array(5, "Delivered");
		$status['queued'] = array(4, "Queued");
		$status['pending'] = array(4, "Pending");
		$status['sent'] = array(1, "Sent");
		$status['failed'] = array(-1, "Failed");

		$event = JRequest::getVar("event", '');
		if($event != 'Updated') return;

		$completed_time = JRequest::getVar("delivered_at", '');

		$messageStatus = JRequest::getVar("status", '');
		if(empty($messageStatus)) $apiAnswer->statsdetails_error[] = 'Empty status received';
		if($messageStatus == 'delivered'){
			if(empty($completed_time)){
				$apiAnswer->statsdetails_received_date = time();
			}else $apiAnswer->statsdetails_received_date = $completed_time;
		}

		$smsId = JRequest::getVar("id", '');
		if(empty($smsId)) $apiAnswer->statsdetails_error[] = 'Can t find the message_id';

		if(!isset($status[$messageStatus])){
			$apiAnswer->statsdetails_error[] = 'Unknow status : '.$messageStatus;
			$apiAnswer->statsdetails_status = -99;
		}else{
			$apiAnswer->statsdetails_status = $status[$messageStatus][0];
			$apiAnswer->statsdetails_error[] = $status[$messageStatus][1];
		}

		$apiAnswer->statsdetails_sms_id = $smsId;

		return $apiAnswer;
	}

	public function answer(){

		$event = JRequest::getString("event", '');
		if(empty($event) || $event != 'Received') return;

		$apiAnswer = new stdClass();
		$apiAnswer->answer_date = JRequest::getString("received_time", '');

		$apiAnswer->answer_body = JRequest::getString("message", '');

		$contact = JRequest::getVar("contact", '');
		if(!empty($contact['number'])) $apiAnswer->answer_from = $contact['number'];

		return $apiAnswer;
	}
}
