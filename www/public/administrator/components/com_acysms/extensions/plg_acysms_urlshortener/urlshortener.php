<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class plgAcysmsUrlShortener extends JPlugin{

	function onACYSMSReplaceUrl(&$message, $send = true){
		$config = ACYSMS::config();
		$apiKeyShortUrl = $config->get('api_key_short_url', '');

		if(empty($apiKeyShortUrl)){
			return;
		}

		$allUrlShort = array();

		preg_match_all('!https?://\S+!', $message->message_body, $match);
		if(empty($match[0])){
			return;
		}
		foreach($match[0] as $url){
			array_push($allUrlShort, $this->_shortenURL($url, $apiKeyShortUrl));
		}
		$message->message_body = str_replace($match[0], $allUrlShort, $message->message_body);
	}

	private function _shortenURL($url, $apiKey){

		$ch = curl_init();

		if($ch === false){
			ACYSMS::enqueueMessage("curl_init() function has failed", "error");
			return false;
		}

		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/urlshortener/v1/url?key=".$apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '{"longUrl": "'.$url.'"}');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$res = curl_exec($ch);

		if(empty($res)){
			ACYSMS::enqueueMessage("An error appeared with the connection to the Google API", "error");
			return false;
		}

		curl_close($ch);

		$arrayJson = json_decode($res, true);

		if(array_key_exists("error", $arrayJson)){
			$errorCode = $arrayJson['error']['code'];
			$errorMessage = $arrayJson['error']['message'];
			if($errorCode == 400){
				$error = JText::_('SMS_WRONG_API_KEY');
				$error .= ' <a href="http://www.acyba.com/acysms/528-how-to-configure-the-shortened-urls-option.html" target="_blank">'.JText::_('SMS_CLICK_HERE').'</a>';
			}else $error = "Error ".$errorCode." : ".$errorMessage;
			ACYSMS::enqueueMessage($error, 'error');
			return false;
		}

		$shortUrl = $arrayJson['id'];

		return $shortUrl;
	}

}
