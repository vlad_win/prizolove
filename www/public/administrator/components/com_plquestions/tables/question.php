<?php

defined('_JEXEC') or die('Restricted access');


class PlQuestionsTableQuestion extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__pl_questions', 'id', $db);
	}
}
