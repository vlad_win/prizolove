<?php

defined('_JEXEC') or die('Restricted access');


class PlQuestionsTableCampaign extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__pl_campaigns', 'id', $db);
	}
}
