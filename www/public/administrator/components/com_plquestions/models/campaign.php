<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
class PlQuestionsModelCampaign extends JModelAdmin
{
	
	public function getTable($type = 'Campaign', $prefix = 'PlQuestionsTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
 
	
	public function getForm($data = array(), $loadData = true)
	{		
		// Get the form.
		$form = $this->loadForm(
			'com_plquestions.campaign',
			'campaign',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);
 
		if (empty($form))
		{
			return false;
		}
 
		return $form;
	}
 
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{		
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_plquestions.edit.campaign.data',
			array()
		);
 
		if (empty($data))
		{
			$data = $this->getItem();
		}
 
		return $data;
	}

	protected function prepareTable($table)
	{
		$date = JFactory::getDate();
//		$table->date_modified    = $date->toSql();
	}
	
	/*
	public function save($data)
	{		
		$db    = JFactory::getDbo();
		$input = JFactory::getApplication()->input;		
		
		$query = "
			SELECT u.subid customer_id, u.phone, u.name
			FROM #__acymailing_subscriber u					
			WHERE u.subid =".$data['customer_id'];
	
		$db->setQuery($query);
		
		$customer = $db->loadObject();

		
		if ($customer && preg_match('/0[\d]{9}/',$customer->phone) && !preg_match('/[test|тест]/',$customer->phone)) {
			$finalQuery = '';
			switch($data['sent']) {
					case 1: 
						$finalQuery = 'INSERT IGNORE INTO `#__acysms_queue` (`queue_message_id`,`queue_receiver_id`,`queue_receiver_table`,`queue_senddate`,`queue_try`,`queue_priority`, `queue_paramqueue`) 
						VALUES ("5","'.$customer->customer_id.'","acymailing",'.(time()+100).',"1","1","")';				
						break;
					case 2:	
						$finalQuery = 'INSERT IGNORE INTO `#__acysms_queue` (`queue_message_id`,`queue_receiver_id`,`queue_receiver_table`,`queue_senddate`,`queue_try`,`queue_priority`, `queue_paramqueue`) 
						VALUES ("3","'.$customer->customer_id.'","acymailing",'.(time()+100).',"1","1","")';
						break;
					case 5:	
						$finalQuery = 'INSERT IGNORE INTO `#__acysms_queue` (`queue_message_id`,`queue_receiver_id`,`queue_receiver_table`,`queue_senddate`,`queue_try`,`queue_priority`, `queue_paramqueue`) 
						VALUES ("6","'.$customer->customer_id.'","acymailing",'.(time()+100).',"1","1","")';
						break;
			}
			if ($finalQuery) {
				$db->setQuery($finalQuery);
				$db->query();
			}	

		}
	
		return parent::save($data);
	} */

}