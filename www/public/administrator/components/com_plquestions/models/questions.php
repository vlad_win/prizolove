<?php
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorldList Model
 *
 * @since  0.0.1
 */
class PlQuestionsModelQuestions extends JModelList
{
    public function __construct($config = array())
    {
        

        parent::__construct($config);
    }

	protected function getListQuery()
	{
		// Initialize variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

	        // Filter by search in title
        	$search = $this->getState('filter.search');
	


		// Create the base select statement.
		$query->select('a.*')
		  ->from($db->quoteName('#__pl_questions', 'a'))
        ;
       

        if (!empty($search))
        {
            //$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
            //$query->where('(c.customer_email LIKE ' . $search . ')');
        }

        $listOrdering  = $this->state->get('list.ordering', 'id');
        $orderDirn     = $this->state->get('list.direction', 'DESC');

        $query->order($db->escape($listOrdering) . ' ' . $db->escape($orderDirn));

		return $query;
	}


}
