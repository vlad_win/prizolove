<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_CREDITFORM
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

JHtml::_('formbehavior.chosen', 'select');


$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.id';

?>

<form action="index.php?option=com_plquestions&view=questions" method="post" id="adminForm" name="adminForm">
    <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
                <?php //echo JText::_('COM_PRIZOLOVE_ORDERS_NUM'); ?>
                <?php echo JHtml::_('searchtools.sort', 'COM_PLQUESTIONS_NUM', 'a.id', $listDirn, $listOrder, null, 'desc'); ?>
            </th>			
			
			<th width="3%">
				<?php echo JText::_('COM_PLQUESTIONS_TITLE') ;?>
			</th>			
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
          $link = JRoute::_('index.php?option=com_plquestions&task=question.edit&id=' . $row->id);
        ?>
    
					<tr>
						<td><?php echo $row->id ?></td>
						<td>
            <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_PLQUESTIONS_TITLE'); ?>">
								<?php echo $row->name; ?>
							</a>								
						</td>                        
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>

    
	<?php echo JHtml::_('form.token'); ?>
</form>

