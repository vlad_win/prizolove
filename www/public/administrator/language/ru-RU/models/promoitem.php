<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


class PrizoloveModelPromoitem extends JModelAdmin
{
    /**
     * Method to get a table object, load it if necessary.
     *
     * @param   string  $type    The table name. Optional.
     * @param   string  $prefix  The class prefix. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return  JTable  A JTable object
     *
     * @since   1.6
     */
    public function getTable($type = 'Promoitem', $prefix = 'PrizoloveTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the record form.
     *
     * @param   array    $data      Data for the form.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  mixed    A JForm object on success, false on failure
     *
     * @since   1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm(
            'com_prizolove.promoitem',
            'promoitem',
            array(
                'control' => 'jform',
                'load_data' => $loadData
            )
        );

        if (empty($form))
        {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed  The data for the form.
     *
     * @since   1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState(
            'com_prizolove.edit.promoitem.data',
            array()
        );

        if (empty($data))
        {
            $data = $this->getItem();
        }

        if (!empty($data->params) && !is_array($data->params)) {
            $data->params = json_decode($data->params,true);
        }

        return $data;
    }

    protected function prepareTable($table)
    {
        /* $date = JFactory::getDate();
        $table->modified    = $date->toSql();

        if (empty($table->id)) {
            $table->created    = $date->toSql();
        } */
    }

    public function save($data)
    {

        $input = JFactory::getApplication()->input;

        $jformData = $input->get('jform', array(), 'array');

        if (!empty($jformData['params'])) $data['params'] = json_encode($jformData['params']);

        return parent::save($data);
    }



}