<?php
// No direct access
defined('_JEXEC') or die('Restricted access');


class PrizoloveUsersTableOrder extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__acymailing_subscriber', 'order_id', $db);
	}
}
