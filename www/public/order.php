<?php
	include('LiqPay.php');
	$liqpay = new LiqPay('i8151250366', '2YVzRXHWMSIxOo3xkovdvvX5H28t0Tz');
	$data = base64_encode(json_encode(array(
		'public_key'     => 'i8151250366',
		'action'         => 'pay',
		'amount'         => '387',
		'currency'       => 'UAH',
		'description'    => '',
		'order_id'       => '101',
		'sandbox' 	 => 1,
		'version'        => '3'
	)));
	$signature = $liqpay->cnb_signature(array(
		'public_key'     => 'i8151250366',
		'action'         => 'pay',
		'amount'         => '387',
		'currency'       => 'UAH',
		'description'    => '',
		'order_id'       => '101',
		'sandbox' 	 => 1,
		'version'        => '3'
	));
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Test Order</title>
</head>

<body>
<div id="liqpay_checkout"></div>
<script>
  window.LiqPayCheckoutCallback = function() {
    LiqPayCheckout.init({
      data:"<?=$data;?>",
      signature: "<?=$signature;?>",
      embedTo: "#liqpay_checkout",
      mode: "embed" // embed || popup,
       }).on("liqpay.callback", function(data){
			console.log(data.status);
			console.log(data);
			}).on("liqpay.ready", function(data){
				// ready
			}).on("liqpay.close", function(data){
				// close
		});
  };
</script>
<script src="//static.liqpay.ua/libjs/checkout.js" async></script>
</body>
</html>