<?php

/*------------------------------------------------------------------------
# plg_contentstats_com_login - Content Statistics Joomla Users extension plugin
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2011 JoomlaContentStatistics.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlacontentstatistics.com
# Technical Support:	Forum - http://www.joomlacontentstatistics.com/forum
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class  PlgContentstatsCom_login extends JPlugin
{
	var $component = 'com_login';
	var $component_name = 'Joomla Login (backend)';

	function registerStatistic()
	{
		//in this plugin this function is special. DON'T use as a reference. in this plugin we call the register functions using the user's plugin triggers, onLoginUser and onAfterStoreUser, see below
		
		$data = array();
		
		// we have to set this to TRUE if we want the statistic to be recorded.
		$data['register'] = false ;
		
		//do not change this
		$data['component'] = $this->component ;
		
		return $data;
		
	}
	
	//this function is triggered by Joomla on OK user login
	function onUserLogin($response, $options)
	{
		$db = JFactory::getDBO();
		$query = ' SELECT id FROM #__users WHERE email = "'.$response["email"].'" AND username = "'.$response["username"].'" ';
		$db->setQuery($query);
		
		$data = array();
		
		// we have to set this to TRUE if we want the statistic to be recorded.
		$data['register'] = false ;
		
		$register = true ;
		
		if($this->params->get('userlogin') && $register){
			$data['type'] = 1 ;
			$data['reference_id'] = $db->loadResult();
			$data['register'] = true ;
		}

		//do not change this
		$data['component'] = $this->component ;
		
		$this->saveData($data);
		
		return true;
		
	}

	//this function is triggered by Joomla on OK user logout
	function onUserLogout($parameters, $options)
	{
		
		$data = array();
		
		// we have to set this to TRUE if we want the statistic to be recorded.
		$data['register'] = false ;
		
		$register = true ;
		
		if($this->params->get('userlogout') && $register){
			$data['type'] = 2 ;
			$data['reference_id'] = $parameters['id'];
			$data['register'] = true ;
		}

		//do not change this
		$data['component'] = $this->component ;
		
		$this->saveData($data);
		
		return true;
		
	}
	
	function saveData($data){
		
		$plugin =JPluginHelper::getPlugin('system', 'contentstats');
		$pluginParams = new JRegistry($plugin->params);

		$mainframe = JFactory::getApplication();
		$db = JFactory::getDBO();
		$row = new TableContentStatistic($db);
		
		//new for version 1.6.0
		if($pluginParams->get('hours')) $row->hours = $pluginParams->get('hours');
		
		$blockips = $pluginParams->get('blockips');
		$blockips = explode(",", $blockips);
		
		$ip = $_SERVER['REMOTE_ADDR'] ;
		$user = JFactory::getUser();
		
		$blockusers = $pluginParams->get('blockusers');
		$blockusers = explode(",", $blockusers);
		
		if($user->id) $user_block = in_array($user->id, $blockusers) ;
		else $user_block = false ;
		
		$ip_block = in_array($ip, $blockips) ;
		$robot_block = $this->is_robot($pluginParams) ;
		
		$track = true ;
		
		if(!$ip_block && !$robot_block && !$user_block && $track){
			
			$dispatcher   = JDispatcher::getInstance();
			
			if($pluginParams->get('ip_geolocation') && $pluginParams->get('api_key')){
				$results = $dispatcher->trigger('set_cookie', array($ip));
				
				$geolocation = $results[0];
				$data['country'] = $geolocation['countryCode'] ;
				$data['state'] = $geolocation['regionName'] ;
				$data['city'] = $geolocation['cityName'] ;
			}
			
			// Bind the form fields to the statistics table
			if (!$row->bind($data)) {
				$mainframe->enqueueMessage($db->getErrorMsg());
				return false;
			}

			if (!$row->check()) {
				$mainframe->enqueueMessage($db->getErrorMsg());
				return false;
			}
			
			//special for this case
			if(!$row->user_id) $row->user_id = $data['reference_id'] ;
			
			if (!$row->store()) {
				$mainframe->enqueueMessage($db->getErrorMsg());
				return false;
			}

		}
		
	}
	
	function is_robot($pluginParams){
		
		$user_agent = $_SERVER['HTTP_USER_AGENT'] ;
		
		$robot = false ;
		
		//you can add more robots to block: http://www.searchenginedictionary.com/spider-names.shtml
		
		$blockuseragents = $pluginParams->get('blockuseragents');
		$blockuseragents = explode(",", $blockuseragents);
		
		foreach($blockuseragents as $current_agent){

			if(preg_match("/".preg_quote($current_agent)."/i",$user_agent)) $robot = true ;

		}
		
		return $robot ;
		
	}
	
	function getRankingCompatibility()
	{
		$return->value = $this->component ;
		$return->text = $this->component_name . " (" . $this->component .")" ;
		
		return $return;
	}
	
	function getEvolutionCompatibility()
	{
		$return->value = $this->component ;
		$return->text = $this->component_name . " (" . $this->component .")" ;
		
		return $return;
	}
	
	function getCriteriaRanking()
	{
		//create an array with the diferent options you want to ofer for the RANKING module. It should be consistent with the cases you considered on the registerStatistic() function. The field "value" stands for the TYPE of action.
		$criteria = array();
		
		$criteria[0] = new stdClass();		
		$criteria[0]->value = 1 ;
		$criteria[0]->text = "Most admin logins" ;

		$criteria[1] = new stdClass();	
		$criteria[1]->value = 2 ;
		$criteria[1]->text = "Most admin logouts" ;
		
		//do not change this
		$return = new stdClass();
		$return->options = $criteria ;
		$return->component = $this->component ;
		
		return $return ;
		
	}
	
	function getCriteriaEvolution()
	{
		
		//create an array with the diferent options you want to ofer for the EVOLUTION module. It can be consistent with the cases you considered on the registerStatistic() function, but you can also add new options (do not identify with a number in that case, but a string). The "new" options, which are more elaborated, are usually used to display data that "belongs" to some upper hierachy. For instance, in com_content, if we want to display the ARTICLE VIEWS belonging to an specific CATEGORY, we must create a new option, which we call "category_articles"
		$criteria = array();
		
		$criteria[0] = new stdClass();		
		$criteria[0]->value = 1 ;
		$criteria[0]->text = "1) User logins" ;

		$criteria[1] = new stdClass();	
		$criteria[1]->value = 2 ;
		$criteria[1]->text = "2) User logouts" ;

				//do not change this
		$return = new stdClass();
		$return->options = $criteria ;
		$return->component = $this->component ;

		return $return ;

	}


	function getSelectorsRanking()
	{
			//define your selectors for the RAKING module
		$selectors = array();

		$selectors[0] = new stdClass();
		$selectors[0]->value = "all" ;
		$selectors[0]->text = "ALL users (equivalent to NO FILTER)" ;

			//do not change this
		$return = new stdClass();
		$return->options = $selectors ;
		$return->component = $this->component ;

		return $return ;

	}

	function getSelectorsEvolution()
	{

		//define your selectors for the RAKING module
		$selectors = array();

		$selectors[0] = new stdClass();
		$selectors[0]->value = "all" ;
		$selectors[0]->text = "ALL users (equivalent to NO FILTER)" ;

		//do not change this
		$return = new stdClass();
		$return->options = $selectors ;
		$return->component = $this->component ;

		return $return ;

	}

	function getQueryRanking($criteria, $selector, $specific_id, $where_clause, $params){

		/* 
		
		you must return your own query in this function for the RANKING module, and you MUST include the $where_clause inside your WHERE clause, on your query.
		your WHERE clause will have at least this form:
		
		' WHERE st.type = ' . $criteria . 
		$where_clause .
		
		after that, you can add your own additional clauses, starting with an "AND" sentence.
		
		*/
		
		$query = '' ;
		
		$db = JFactory::getDBO();
		
		$view = JRequest::getVar('view');
		$task = JRequest::getVar('task');
		$id = JRequest::getInt('id');
		
		// artist_id is for everything, actually: artist, album, song
		if($specific_id){
			$reference_id = $specific_id;
			$category_id = $reference_id ;
			$admin_id = $reference_id ;
		}
		else{
			
			//make your necessary querys to get parent categories IDs and such, if you're going to need these filters later.

			switch($criteria){
				//your querys for the RAKING module. must be consisten with the options you defined on the getCriteriaRanking() function.
				case 1:case 2:// user profile views
				
				$query = 	' SELECT COUNT(st.id) AS howmuch, u.username as item_name, '.
				' CONCAT("index.php?option=com_user&view=user&id=", u.id) as item_link ' .
				' FROM #__content_statistics as st '.
				' LEFT JOIN #__users AS u ON u.id = st.reference_id ' .
				' WHERE st.type = ' . $criteria . 
				$where_clause .
				$where_selector .
				' AND u.block = 0 ' .
				' GROUP BY u.id ' .
				' ORDER BY howmuch DESC '
				;
				
				break;
			}
			
		}
		//print_r(str_replace("#_","jos",$query));die;
		
		//do not change this
		$return = new stdClass();
		$return->query = $query ;
		$return->component = $this->component ;
		
		return $return ;
		
	}
	
	function getQueryEvolution($criteria, $selector, $specific_id, $where_clause, $params){
		
		$query = '' ;
		
		$db = JFactory::getDBO();
		
		$view = JRequest::getVar('view');
		$task = JRequest::getVar('task');
		$id = JRequest::getInt('id');
		
		$user = JFactory::getUser();
		
		if(!$specific_id){
			
			$user_id = $user->id ;	
			
		}
		
		else{
			
			$user_id = $specific_id ;	
			
		}
		
		$query = '' ;
		
		switch($criteria){
			case 1: case 2:
			
				//selectors may apply.
			switch($selector){

				case 'current': case 'specific':

				$where_selector = ' AND st.reference_id = ' . $user_id ;

				break;

				default:

				$where_selector = "";

				break;

			}

			$query = 	' SELECT COUNT(st.id) AS howmuch, st.date_event FROM #__content_statistics as st '.
			' LEFT JOIN #__users AS u ON u.id = st.reference_id ' .
			' WHERE st.type = ' . $criteria .
			$where_selector .
			$where_clause .
			' AND u.block = 0 ' 
			;

				//print_r(str_replace("#_","jos",$query));echo "\n";

			break;
			
			
		} // end switch
		
		//do not change this
		$return = new stdClass();
		$return->query = $query ;
		$return->component = $this->component ;
		
		return $return ;
		
	}
	
	//new in version 1.3
	function registerStatistic_com_login(){
		return $this->registerStatistic() ;
	}
	
	function getQueryRanking_com_login($criteria, $selector, $specific_id, $where_clause, $params){
		return $this->getQueryRanking($criteria, $selector, $specific_id, $where_clause, $params) ;
	}
	
	function getQueryEvolution_com_login($criteria, $selector, $specific_id, $where_clause, $params){
		return $this->getQueryEvolution($criteria, $selector, $specific_id, $where_clause, $params) ;
	}
	
	//new for version 1.4
	function getTypes($stream = false)
	{
		$suffix = "";
		if($stream) $suffix = "_STREAM";

		//load the translation
		$this->loadLanguage( );

		$criteria = array();
		
		$criteria[1] = JText::_('ADMIN_LOGIN'.$suffix) ;
		$criteria[2] = JText::_('ADMIN_LOGOUT'.$suffix) ;
		
		$icons[1] = 'log-in' ;
		$icons[2] = 'log-out' ;
		
		//do not change this
		$return = new stdClass();
		$return->options = $criteria ;
		$return->icons = $icons ;
		$return->component = $this->component ;
		
		return $return ;
		
	}
	
	function getItemName_com_login($reference_id, $type, $entry_id)
	{
		switch($type){
			case 1: case 2: default:
			if(!$reference_id) $reference_id = 0 ;
			$query = ' SELECT u.username as item_name '.
			' FROM #__users as u '.
			' WHERE u.id = ' . $reference_id ;
			break;
			
		}
		
		$db = JFactory::getDBO();
		$db->setQuery($query);
		$return = $db->loadResult();
		
		return $return ;
		
	}

}