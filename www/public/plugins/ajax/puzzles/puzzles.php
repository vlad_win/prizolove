<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxPuzzles extends JPlugin
{

	function onAjaxPuzzles()
	{
		$inputCookie  = JFactory::getApplication()->input->cookie;

        $user = JFactory::getUser();

		$input = JFactory::getApplication()->input;
		$session = JFactory::getSession();

		$request = $input->getArray();
		$correct = true;
		$puzzles = array();
        $bonusAccount = 0;
        if ($user->id) {
            $userAccount = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
            $bonusAccount = $userAccount->value;
        }
        $inputCookie  = JFactory::getApplication()->input->cookie;
        if (!empty($request['puzzle'])) {
            foreach($request['puzzle'] as $eventId => $question) {
                $puzzles[$eventId] = PlLibHelperEvents::getInstance()->getEvent($eventId, $user->id);

                switch($puzzles[$eventId]->puzzle_type) {
                    case 'missing_letters':
                        $chrArray = preg_split('//u', $puzzles[$eventId]->params['template'], -1, PREG_SPLIT_NO_EMPTY);
                        $strAnswer = '';
                        foreach($chrArray as $key=>$letter) {
                            if (isset($question[$key])) {
                                $strAnswer .= $question[$key];
                            } else {
                                $strAnswer .= $letter;
                            }
                        }
                        $answer = $strAnswer;
                        
                        if (!empty($puzzles[$eventId]->check_answer)) {
                            if (mb_strtolower($puzzles[$eventId]->params['answer']) != mb_strtolower($answer)) $correct = false;
                        }
                        if ($correct) {
                            $nextLink = PlLibHelperEvents::getInstance()->setCompletedEvent($eventId, $user->id);
                            if ($user->id) {
                                $bonusInfo = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
                                $total = $bonusInfo->value;
                            } else {
                                $inputCookie->set('eventResult', base64_encode(json_encode(['eventId'=>$eventId])));
                                $total = 0;
                            }
                            return [
                                'nextLink' => $nextLink,
                                'bonuses' => $puzzles[$eventId]->bonuses,
                                'correct' => $correct,
                                'total' => $total

                            ];
                        } else {
                            if ($user->id && $bonusAccount > $puzzles[$eventId]->penalty) {
                                $penalty = PlLibHelperEvents::getInstance()->setFailedEvent($eventId, $user->id);
                                $bonusAccount -= $puzzles[$eventId]->penalty;
                            } else {
                                $penalty = 0;
                            }
                            return [
                                'total' => $bonusAccount,
                                'penalty' => $penalty,
                                'correct' => $correct
                            ];
                        }

                        break;
                    case 'images':
                        foreach($puzzles[$eventId]->params['images'] as $key=>$image) {
                            if ($key == $question && $image['answer']) {
                                $nextLink = PlLibHelperEvents::getInstance()->setCompletedEvent($eventId, $user->id);
                                if ($user->id) {
                                    $bonusInfo = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
                                    $total = $bonusInfo->value;
                                } else {
                                    $inputCookie->set('eventResult', base64_encode(json_encode(['eventId'=>$eventId])));
                                    $total = 0;
                                }
                                return [
                                    'nextLink' => $nextLink,
                                    'bonuses' => $puzzles[$eventId]->bonuses,
                                    'correct' => $correct,
                                    'total' => $total
                                ];
                            }
                        }
			            $correct = false;
                        if ($user->id && $bonusAccount > $puzzles[$eventId]->penalty) {
                            $penalty = PlLibHelperEvents::getInstance()->setFailedEvent($eventId, $user->id);
                            $bonusAccount -= $puzzles[$eventId]->penalty;
                        } else {
                            $penalty = 0;
                        }
                        return [
                            'penalty' => $penalty,
                            'correct' => $correct,
                            'total' => $bonusAccount
                        ];
                        break;
                    case 'millionaire':
                        if (is_numeric($question['questionId'])) {
                            $lastQuestion = count($puzzles[$eventId]->params['questions']) - 1;
                            if ($puzzles[$eventId]->params['questions'][$question['questionId']]['right_answer'] == $question['answer']
                                && $lastQuestion == $question['questionId']
                            ) {
				                $nextLink = PlLibHelperEvents::getInstance()->setCompletedEvent($eventId, $user->id);
                                if ($user->id) {
                                    $bonusInfo = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
                                    $total = $bonusInfo->value;
                                } else {
                                    $inputCookie->set('eventResult', base64_encode(json_encode(['eventId'=>$eventId])));
                                    $total = 0;
                                }
                                return [
                                    'nextLink' => $nextLink,
                                    'bonuses'=> $puzzles[$eventId]->bonuses,
                                    'activate' => true,
                                    'total' => $total
                                ];
                            } else if ($puzzles[$eventId]->params['questions'][$question['questionId']]['right_answer'] == $question['answer'] ) {
                                return [
                                    'nextQuestion' => true
                                ];
                            } else {
                                if ($user->id && $bonusAccount > $puzzles[$eventId]->penalty) {
                                    $penalty = PlLibHelperEvents::getInstance()->setFailedEvent($eventId, $user->id);
                                    $bonusAccount -= $puzzles[$eventId]->penalty;
                                } else {
                                    $penalty = 0;
                                }
                            	return [
                                	'penalty' => $penalty,
	                                'correct' => false,
                                    'total' => $bonusAccount
        	                    ];
                            }
                        }
                        break;
                }
                /* if ($questions[$campaignId][$questionId]['type'] == 'pattern') {
                    $chrArray = preg_split('//u', $questions[$campaignId][$questionId]['pattern'], -1, PREG_SPLIT_NO_EMPTY);
                    $strAnswer = '';
                    foreach($chrArray as $key=>$letter) {
                        if (!empty($answer[$key])) {
                            $strAnswer .= $answer[$key];
                        } else {
                            $strAnswer .= $letter;
                        }
                    }
                    $answer = $strAnswer;
                } */
            }

        }

		/*
		if (!empty($request['questions'])) {
			$questions = json_decode($session->get('questions', json_encode(array())), true);		
			
			foreach($request['questions'] as $campaignId => $campaign) {
				foreach($campaign as $questionId=>$answer) {
					if (empty($questions[$campaignId][$questionId])) continue;
					$eventId = intval($questions[$campaignId][$questionId]['event_id']);
					
					if ($questions[$campaignId][$questionId]['type'] == 'letter') {
						$strAnswer = '';
						foreach($answer as $letter) {
							$strAnswer .= $letter;
						}
						$answer = $strAnswer;	
					}
					if ($questions[$campaignId][$questionId]['type'] == 'pattern') {
						$chrArray = preg_split('//u', $questions[$campaignId][$questionId]['pattern'], -1, PREG_SPLIT_NO_EMPTY);
						$strAnswer = '';						
						foreach($chrArray as $key=>$letter) {
							if (!empty($answer[$key])) {
								$strAnswer .= $answer[$key];
							} else {
								$strAnswer .= $letter;
							}	 							
						}
						$answer = $strAnswer;		
					}
					file_put_contents('logs/questions'.date('Y.m.d').'.log', date('Y-m-d H:i:s').' "'.$_SERVER['REMOTE_ADDR'].'" "'.$answer.'" "'.$_SERVER['HTTP_REFERER'].'"'."\r\n" , FILE_APPEND);
					if (empty($questions[$campaignId][$questionId]['settings']['always_right'])) {
						if (!empty($questions[$campaignId][$questionId]['settings']['convert_letters'])) {					
							if ($questions[$campaignId][$questionId]['answer'] != $this->checkLetters($answer)) $correct = false;
						} else {
							if ($questions[$campaignId][$questionId]['answer'] != mb_strtolower($answer)) $correct = false;
						}
					}

				}	
			}
			
			if ($correct) {
                if (!empty($eventId) && $user->id) PlLibHelperEvents::getInstance()->setCompletedEvent($eventId, $user->id);
				return $questions[$campaignId][$questionId]['rightLink'];
			} else {
		        $value = $inputCookie->get($name = 'attempts'.$campaignId, $defaultValue = 3);                
				
				$value = ($value > 1)?($value - 1) : 1;      				                
				
				$inputCookie->set('attempts'.$campaignId, $value,  $expire = 0);
				return $questions[$campaignId][$questionId]['wrongLink'];
			}
		}
		
		if (!empty($request['squestions'])) {
			$questions = json_decode($session->get('questions', json_encode(array())), true);		
			
			foreach($request['squestions'] as $campaignId => $campaign) {				
				foreach($campaign as $questionId=>$answer) {
					if (empty($questions[$campaignId][$questionId])) continue;

                    $eventId = intval($questions[$campaignId][$questionId]['event_id']);
					$response = array(
						'hide'=> $questions[$campaignId][$questionId]['settings']['hide_after'],
						'show'=> $questions[$campaignId][$questionId]['settings']['show_after'],
						'click_after' => $questions[$campaignId][$questionId]['settings']['show_after_click']
						);
					file_put_contents('logs/questions'.date('Y.m.d').'.log', date('Y-m-d H:i:s').' "'.$_SERVER['REMOTE_ADDR'].'" "'.$answer.'" "'.$_SERVER['HTTP_REFERER'].'"'."\r\n" , FILE_APPEND);
					if (empty($questions[$campaignId][$questionId]['settings']['always_right'])) {						
						if (!empty($questions[$campaignId][$questionId]['settings']['convert_letters'])) {					
							if ($questions[$campaignId][$questionId]['answer'] != $this->checkLetters($answer)) $correct = false;
						} else {
							if ($questions[$campaignId][$questionId]['answer'] != mb_strtolower($answer)) $correct = false;
						}
					}

				}	
			}
			
			if ($correct) {
			    if (!empty($eventId) && $user->id) PlLibHelperEvents::getInstance()->setCompletedEvent($eventId, $user->id);
				return $response;
			} else {
		        $value = $inputCookie->get($name = 'attempts'.$campaignId, $defaultValue = 3);                
				
				$value = ($value > 1)?($value - 1) : 1;      				                
				
				$inputCookie->set('attempts'.$campaignId, $value,  $expire = 0);
				return $questions[$campaignId][$questionId]['wrongLink'];
			}
		}
		*/
		return false;
	}
	
	private function checkLetters($str)
	{		
		$replace=array(
			"a" => "а",
			"p" => "р",
			"e" => "е",
			"y" => "у",
			"i" => "і",
			"o" => "о",
			"c" => "с",						
			"A" => "А",
			"H" => "Н",
			"K" => "К",
			"M" => "М",
			"B" => "В",
			"C" => "С",
			"E" => "Е",
			"T" => "Т",
			"Y" => "У",
			"I" => "І",
			"O" => "О",
			"P" => "Р",
			"X" => "Х",
			
		);		
		
		$str=iconv("UTF-8","UTF-8//IGNORE",strtr($str,$replace));			
		
		$str = mb_strtolower($str);
	
		return $str;
	}
}	