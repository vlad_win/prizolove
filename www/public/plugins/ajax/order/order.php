<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxOrder extends JPlugin
{

    protected $autoloadLanguage = true;

    public function __construct( &$subject, $config )
    {
        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
    }

    function onAjaxOrder()
    {
        $userData = $this->input->post->getArray();


        $subid = PlLibHelperAcy::checkAcyUser($userData);

        if (!$subid) return false;


        if (!PlLibHelperAcy::validateEmail($userData['email'])) return false;

        $user = JFactory::getUser();
        $session = JFactory::getSession();
        $cart = json_decode($session->get('plCart', json_encode(array('items' => array()))), true);

        $orderUrl = $session->get('plOrderReferer', $_SERVER['HTTP_REFERER']);

        $utmTags = PlLibHelperUtms::getCookieUtms('orderUtms');

        $orderUrl = PlLibHelperUtms::getUtmLink($orderUrl);

        $bonuses = PlLibHelperBonus::getBonusArray();


        PlLibHelperOrders::getInstance()->addCustomer($subid, $userData['fullname'], $userData['phone'], $userData['email']);

        $cost = 0;
        $discount = 0;

        $productBusiness = false;
        $productProduct = false;
        foreach ($cart['items'] as $id => $item) {
            $bonusText = '';
            if ($item['discount'] && $item['type'] == 'product' && $user->id > 0) {
                $productProduct = true;
                $bonusAccount = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
                $discount = PlLibHelperRates::convertUSD($bonusAccount->value);
                if ($item['discount_percent']) {
                    $maxDiscount = ($item['price'] / 100) * $item['discount_percent'];
                    if ($discount < $maxDiscount) $maxDiscount = $discount;
                    $bonusText = (!empty($userData['bonus'])?'Списано бонусов: '.$userData['bonus']:'');
                    if ($userData['bonus'] > $maxDiscount) {
                        $session->set('orderUserData', json_encode($userData));
                        //$app->enqueueMessage('Некорректная сумма списания бонуса', 'warning');
                        //$app->redirect($_SERVER['HTTP_REFERER']);
                        //exit();
                    }
                }
            }
            if ($item['discount'] && $item['type'] == 'business' && $user->id > 0) {
                $productBusiness = true;
                $comission = PlLibHelperBalances::getInstance()->getBalance($user->id);
                $discount = PlLibHelperRates::convertUSD($comission);

                if ($item['discount_percent']) {
                    $maxDiscount = ($item['price'] / 100) * $item['discount_percent'];
                    if ($discount < $maxDiscount) $maxDiscount = $discount;
                    if ($userData['bonus'] > $maxDiscount) {
                        $session->set('orderUserData', json_encode($userData));
                        //$app->enqueueMessage('Некорректная сумма списания бонуса', 'warning');
                        //$app->redirect($_SERVER['HTTP_REFERER']);
                        //exit();
                    }
                }
                $cost += ($item['price'] * $item['quantity']);
                $items[] = $item['name'] . ' *' . $item['code'] . '* Цена: ' . number_format(($cost - $maxDiscount),2) . ' грн. ' . $item['quantity'] . ' шт. ' . implode(' ', $bonuses).(!empty($userData['bonus'])?'Списано бонусов: '.$userData['bonus']:'');
                break;
            }
            $cost += ($item['price'] * $item['quantity']);
            $items[] = $item['name'] . ' *' . $item['code'] . '* Цена: ' . number_format(($item['price'] - $maxDiscount),2) . ' грн. ' . $item['quantity'] . ' шт. ' . implode(' ', $bonuses).$bonusText;
        }

        if (!$cost || !count($items)) {
            //JFactory::getApplication()->enqueueMessage('Произошла ошибка при заказе, повторите пожалуйста Ваш заказ', 'warning');            ;
        }

        if ($discount && $productBusiness && ($cost - $maxDiscount) > 0) {
            $cost = $cost - $maxDiscount;
        } else if ($discount && ($cost - $maxDiscount) >= 0) {
            $cost = $cost - $maxDiscount;
        }
        $orderInfo['cost'] = $cost;

        $orderInfo['orderId'] = PlLibHelperOrders::getInstance()->createOrder($userData, $items, $subid, $orderUrl, $bonuses, $cart, $maxDiscount);
        $orderInfo['desc'] = 'Оплата согласно заказа №'. $orderInfo['orderId'];
        $ikOrder = new StdClass();
        $ikOrder->order_id = $orderInfo['orderId'];
        $ikOrder->created = $ikOrder->modified = date('Y-m-d H:i:s');

        $db    = JFactory::getDbo();

        $db->insertObject('#__interkassa_orders', $ikOrder);

        return $orderInfo;

    }


}