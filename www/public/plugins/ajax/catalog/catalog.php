<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxCatalog extends JPlugin
{

    protected $autoloadLanguage = true;

    public function __construct( &$subject, $config )
    {
        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
    }

    function onAjaxCatalog()
    {
        $num = $this->input->post->get('num', '', 'INTEGER');

        $products = PlLibHelperCatalog::getInstance()->getProducts(16, $num);

        $parentId = PlLibHelperAffiliate::getParent();
        $affiliateLink = '';

        if ($parentId) $affiliateLink = '&atid='.$parentId;

        $output = '';

        foreach($products as $product) {
            $output .= '<div class="col-12 col-md-3 col-sm-12 col-lg-3">';
            $output .= '   <div class="product">';
            $output .= '        <a href="/catalog?pid='.$product->id.$affiliateLink.'" class="overlay-link"></a>';
            $output .= '        <div class="diamond-icon"></div>';

            $output .= '        <div class="wrapper-left">';
            $output .= '            <div class="image-wrapper">'.(($product->image)?'<img src="'.$product->image.'">':'').'</div>';
            $output .= '        </div>';
            $output .= '        <div class="wrapper-right">';
            $output .= '            <div class="name-wrapper">'.$product->name.'</div>';
            $output .= '            <div class="price-wrapper">'.number_format(PlLibHelperRates::recalc(PlLibHelperRates::convertUSD($product->price_usd)),2).' <span class="valute">'.PlLibHelperRates::currencyLabel().'</span></div>';
            $output .= '      </div>';
            $output .= '   </div>';
            $output .= '</div>';
        }
        return $output;
    }


}