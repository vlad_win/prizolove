<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxEvents extends JPlugin
{

    protected $autoloadLanguage = true;

    public function __construct( &$subject, $config )
    {
        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
    }

    function onAjaxEvents()
    {
        $eid = $this->input->post->get('eid', '', 'INTEGER');
        $task = $this->input->get('task', '', 'STRING');

        $user = JFactory::getUser();

        if (!$eid) return false;
        $event = PlLibHelperEvents::getInstance()->getEvent($eid, $user->id);
        $inputCookie  = JFactory::getApplication()->input->cookie;
        switch($task) {
            case 'pass':
                PlLibHelperUsers::getInstance()->changeBonuses($user->id, 0.05, 'cr','Пропуск загадки');
                PlLibHelperEvents::getInstance()->setCompletedEvent($eid, $user->id, true);
                $link = PlLibHelperEvents::getInstance()->getNewUserEvent($eid, $user->id);
                return [
                    'link' => $link
                ];
            case 'fail':
                $penalty = PlLibHelperEvents::getInstance()->setFailedEvent($eid,$user->id);
                return [
                    'penalty' => $penalty
                ];
            default:
                if ($user->id) {
                    $bonusInfo = PlLibHelperUsers::getInstance()->getBonusAccount($user->id);
                    $total = $bonusInfo->value;
                } else {
                    $total = 0;
                    $inputCookie->set('eventResult', base64_encode(json_encode(['eventId'=>$eid])));
                }
                return [
                    'bonuses' => $event->bonuses,
                    'total' => $total,
                    'nextLink' => PlLibHelperEvents::getInstance()->setCompletedEvent($eid, $user->id)
                ];
                break;
        }
    }


}