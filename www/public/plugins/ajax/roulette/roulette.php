<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxRoulette extends JPlugin
{

    public function __construct( &$subject, $config )
    {
        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
        $this->session = JFactory::getSession();
        $this->db = JFactory::getDbo();
    }

    function onAjaxRoulette()
    {
	$unlim = [2,3];
        $ref = substr(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH),1);

        $currentSession = JFactory::getSession();

        $promos = $currentSession->get('plPromos');

        $catId = intval($promos[$ref]);

        if (empty($promos[$ref])) return true;

        $plPrm = $this->input->cookie->get($name = '_plprm_'.$catId, $defaultValue = 0);

        if ($plPrm >= 3 && !in_array($catId, $unlim)) return array('removeSpin'=>1);

        $query = "SELECT `*` FROM #__prizolove_promo_items WHERE `cat_id` = ".$catId;

        $this->db->setQuery($query);

        $items = $this->db->loadObjectList();
        $prizeArray = array();

        foreach($items as $item) {
            if (empty($item->percent)) continue;

            $prize = array();
            $prize['name'] = $item->name;
            $prize['type'] = $item->type;
            $prize['id'] = $item->id;
            $prize['desc'] = $item->custom_prize;
            for($i=0; $i<intval($item->percent); $i++) {
                $prizeArray[] =  $prize;
            }
        }

        shuffle($prizeArray);

        $plPrm++;

        if ($catId != 2) {
            $this->input->cookie->set($name = '_plprm_' . $catId, $value = $plPrm, $expire = (time() + (30*60)));
            $this->input->cookie->set($name = '_plprmp_' . $catId, $value = end($prizeArray)['id'], $expire = (time() + (30*60)));
        }


        $userPrize = end($prizeArray);

        $prizeName = $userPrize['name'];
        $prizeDesc = '';

        switch($userPrize['type']) {
            case 'custom':
                    $prizeDesc = $userPrize['desc'];
                break;
        }

        $returnArray = array(
            'prize' => $prizeName,
            'desc' => $prizeDesc
        );

        if ($plPrm >= 3 && !in_array($catId, $unlim)) $returnArray['removeSpin'] = 1;

        return $returnArray;


    }

}