<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxAjaxlogin extends JPlugin
{

    protected $autoloadLanguage = true;

    public function __construct( &$subject, $config )
    {

        $lang = JFactory::getLanguage();

        $lang->load('com_users', JPATH_SITE, 'ru-RU', true);

        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
    }

    function onAjaxAjaxlogin()
    {
        $action = $this->input->post->get('action', '', 'STRING');

        switch($action) {
            case 'auth':
                    return $this->authUser();
                break;
            case 'reg':
                return $this->regUser();
                break;
            case 'remind':
                return $this->remind();
                break;
        }
    }

    private function authUser()
    {
        $result = [
            'userId' => 0,
            'error' => '',
            'location' => '',
	        'action' => ''
        ];

        $credentials = array();
        $credentials['username'] = $this->input->post->get('username', '', 'STRING');
        $credentials['password'] = $this->input->post->get('password', '', 'STRING');

        $this->app->login($credentials);

        $user = JFactory::getUser();

        if ($user->id) {
            $this->app->setUserState('rememberLogin', true);		 
            $result['userId'] = $user->id;
            $registry = JFactory::getSession()->get('registry');
            $returnLink = '';

            $returnLink = $this->app->input->get('return', 'false');
            if ($returnLink) $returnLink = base64_decode($returnLink);

            if (is_object($registry) && !$returnLink) {
                $users = $registry->get('users');
				if (is_object($users)) {
					$returnLink = $users->login->form->data['return'];
				}
            }
            $inputCookie  = JFactory::getApplication()->input->cookie;
            $eventResult = $inputCookie->get('eventResult');
            if ($eventResult) {
                $eventResult = json_decode(base64_decode($eventResult),true);
                if (!empty($eventResult['eventId'])) {
                    $returnLink = PlLibHelperEvents::getInstance()->setCompletedEvent($eventResult['eventId'],$user->id);
                }
            }


            if ($returnLink) {
                $result['location'] = $returnLink;
            } else {
                $result['location'] = '/personal';
            }
        } else {
	        $result['error'] = 'Вы ввели неверные данные для входа или такой учетной записи не существует.';
            $result['action'] = 'auth-popup';
        }

        return $result;
    }

    private function regUser()
    {

        $result = [
            'error' => '',
            'location' => '',
            'action' => ''
        ];

	    JPluginHelper::importPlugin('captcha', 'recaptcha_invisible_cf');
	    $dispathcer = JDispatcher::getInstance();






        jimport('joomla.user.helper');

        $name = $this->input->post->get('name', '', 'STRING');
        $email = $this->input->post->get('email', '', 'STRING');
        $phone = $this->input->post->get('phone', '', 'STRING');
        $password1 = $this->input->post->get('password1', '', 'STRING');
        $password2 = $this->input->post->get('password2', '', 'STRING');

        if ($password1 != $password2) {
	        throw new Exception('Пароли не совпадают, проверьте пожалуйста');
            return false;
        }
	    $captcha_valiadte = $dispathcer->trigger('onCheckAnswerCf');
	    if(!$captcha_valiadte[0] ){
		    throw new Exception('Не правильная капча.');
		    return false;
	    }

        $udata = array(
            "name"=>$name,
            "username"=>$email,
            "password"=>$password1,
            "password2"=>$password1,
            "email"=>$email,
            "phone"=>$phone,
            "block"=>0,
            "requireReset"=>0,
            "groups"=>array("1","2")
        );

        $user = new JUser;

        //Write to database
        if(!$user->bind($udata)) {
	        throw new Exception( $user->getError() );
			return false;
        }
        if (!$user->save()) {
	        throw new Exception( $user->getError() );
			return false;
        }

        if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
	        throw new Exception( 'This code can not work without the AcyMailing Component');
            return false;
        }

        $subscriberClass = acymailing_get('class.subscriber');

        $userClass = acymailing_get('class.subscriber');

        $subid = $userClass->subid($email);


        if ($subid) {

            $db = JFactory::getDbo();

            $db->setQuery('INSERT INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$db->Quote(22).','.$db->Quote($subid).','.$db->Quote(time()).',2) ON DUPLICATE KEY UPDATE `subdate` = '.(time()));
            $db->query();

            $mailer = acymailing_get('helper.mailer');

            $mailer->report = true;
            $mailer->trackEmail = true;
            $mailer->autoAddUser = false;
            $mailer->forceVersion = 1;
//TODO: @MAX - отправляет 2 емейла при регистрации
	        $app = JFactory::getApplication();
	        $credentials = array();
	        $credentials['username'] = $email;
	        $credentials['password'] = $password1;
	        $login = $app->login($credentials);
//	        if($login === true){
//		        $app->redirect('/personal');
//		        exit(0);
//	        }
//            $mailer->sendOne(8, $email);
        }


		if (!$result['error']){
			$result['action'] = 'thankyou-popup';
			$result['location'] = '/personal';
		}

        return $result;
    }

    private function remind()
    {
        $error = '';

        JModelLegacy::addIncludePath(JPATH_ROOT . '/components/com_users/models');

        $model = JModelLegacy::getInstance('Reset', 'UsersModel');

        $data  = $this->input->post->get('jform', array(), 'array');


        $model->getForm($data);

        $db = JFactory::getDbo();

        $query = $db->getQuery(true)
            ->select('id')
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('email') . ' = ' . $db->quote($data['email']));

        // Get the user object.
        $db->setQuery($query);

        try
        {
            $userId = $db->loadResult();
        }
        catch (RuntimeException $e)
        {
            $error = $e->getMessage();
        }

        if (empty($userId))
        {
            $error = JText::_('COM_USERS_INVALID_EMAIL');
        }

        $user = JUser::getInstance($userId);

        if ($user->block)
        {
            $error = JText::_('COM_USERS_USER_BLOCKED');


        }

        // Make sure the user isn't a Super Admin.
        if ($user->authorise('core.admin'))
        {
            $error = JText::_('COM_USERS_REMIND_SUPERADMIN_ERROR');
        }


        // Set the confirmation token.
        $token = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
        $hashedToken = JUserHelper::hashPassword($token);

        $user->activation = $hashedToken;

        // Save the user to the database.
        if (!$user->save(true))
        {
            return new JException(JText::sprintf('COM_USERS_USER_SAVE_FAILED', $user->getError()), 500);
        }

        $config = JFactory::getConfig();

        // Assemble the password reset confirmation link.
        $mode = $config->get('force_ssl', 0) == 2 ? 1 : (-1);
        $link = 'index.php?option=com_users&view=reset&layout=confirm&token=' . $token;

        // Put together the email template data.
        $data = $user->getProperties();
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');
        $data['link_text'] = JRoute::_($link, false, $mode);
        $data['link_html'] = JRoute::_($link, true, $mode);
        $data['token'] = $token;

        $subject = JText::sprintf(
            'COM_USERS_EMAIL_PASSWORD_RESET_SUBJECT',
            $data['sitename']
        );



        $body = JText::sprintf(
            'COM_USERS_EMAIL_PASSWORD_RESET_BODY',
            $data['sitename'],
            $data['token'],
            $data['link_text']
        );

        // Send the password reset request email.
        $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $user->email, $subject, $body);

        // Check for an error.
        if ($return !== true)
        {
            return new JException(JText::_('COM_USERS_MAIL_FAILED'), 500);
        }


        $result = [
            'error' => $error,
            'location' => '',
            'action' => 'remind-manual-popup'
        ];

        return $result;

        //$return	= $userModelInstance->processRemindRequest($data);
    }
}