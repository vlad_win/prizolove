<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxCart extends JPlugin
{

    public function __construct( &$subject, $config )
    {
        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
        $this->session = JFactory::getSession();
        $this->db = JFactory::getDbo();
        $this->cart = json_decode($this->session->get('plCart', json_encode(array())), true);

    }

    function onAjaxCart()
    {
        $output = '';

        $productId = $this->input->post->get('pid',false,'INT');
        $act = $this->input->post->get('act','change','STRING');
        $num = $this->input->post->get('num',false,'INT');

        if (!$productId) {
            return array('error'=>'нет ID товара');
        }

        switch($act) {
            case 'plus':
                $this->cartPlus($productId);
                break;
            case 'minus':
                $this->cartMinus($productId);
                break;
            case 'change':
                $this->cartChange($productId, $num);
                break;
            case 'delete':
                $this->cartDelete($productId);
                break;
            case 'add':
                $output = $this->cartAdd($productId);
                break;
        }

        $this->session->set('plCart', json_encode($this->cart));

        if ($output) return array('table' => $output);

        return true;

    }

    private function cartDelete($productId)
    {
        if (isset($this->cart['items']) && array_key_exists($productId, $this->cart['items'])) {
            unset($this->cart['items'][$productId]);
        }
    }

    private function cartChange($productId, $num)
    {
        if (isset($this->cart['items']) && array_key_exists($productId, $this->cart['items'])) {
            if ($num)
                $this->cart['items'][$productId]['quantity'] = $num;
            else
                unset($this->cart['items'][$productId]);
        }
    }

    private function cartPlus($productId)
    {
        if (isset($this->cart['items']) && array_key_exists($productId, $this->cart['items'])) {
            $this->cart['items'][$productId]['quantity']++;
        }
    }

    private function cartMinus($productId)
    {
        if (isset($this->cart['items']) && array_key_exists($productId, $this->cart['items'])) {
            $this->cart['items'][$productId]['quantity']--;
            if ($this->cart['items'][$productId]['quantity'] < 1) unset($this->cart['items'][$productId]);
        }
    }

    private function cartAdd($productId)
    {
        if (isset($this->cart['items']) && array_key_exists($productId, $this->cart['items'])) {
            $this->cart['items'][$productId]['quantity']++;
        } else {
            $query = "
			  SELECT p.id, p.name, p.price, p.image, p.id_1c 
			  FROM #__prizolove_products p
			  WHERE p.id =".$productId." AND p.status = 1";

            $this->db->setQuery($query);

            $product = $this->db->loadObject();

            if (!$product) {
                return array('error'=>'Товара нет в наличии');
            }
            $this->cart['items'][$product->id] = array(
                'name' => $product->name,
                'code' => $product->id_1c,
                'image' => $product->image,
                'price' => $product->price,
                'quantity' => 1
            );

        }




        $cost = 0;
        $num = 0;

        $output = '<tr>';
        $output .= '<td>Фото</td>';
        $output .= '<td>Название</td>';
        $output .= '<td>Количество</td>';
        $output .= '<td>Цена</td>';
        $output .= '<td>Итого</td>';
        $output .= '<td>Удалить</td>';
        $output .= '</tr>';
        foreach($this->cart['items'] as $id=>$item) {
            $cost += $item['quantity'] * $item['price'];
            $num += $item['quantity'];
            $output .= '<tr class="cart-row" data-id="'.$id.'">';
            $output .= '<td><img src="'.$item['image'].'" alt="'.$item['name'].'" ></td>';
            $output .= '<td>'.$item['name'].'</td>';
            $output .= '<td><a href="#" class="cart__plus" data-act="plus">+</a><input class="cart__count" value="'.$item['quantity'].'" maxlength="3"><a href="#" class="cart__minus" data-act="minus">-</a></div></td>';
            $output .= '<td class="cart__col__price">'.$item['price'].'</td>';
            $output .= '<td class="cart__col__quantity">'.$item['quantity'] * $item['price'].'</td>';
            $output .= '<td><a data-act="delete" class="cart__delete" href="javascript:void(0)"><img src="/images/cart1.png" alt=""></a></td>';
            $output .= '</tr>';
        }
        $output .= '<tr>';
        $output .= '<td>В корзине:</td>';
        $output .= '<td colspan="5" class="cart__total-num">'.$this->plural_form($num).'</td>';
        $output .= '</tr>';
        $output .= '<tr>';
        $output .= '<td colspan="5">Итого к оплате</td>';
        $output .= '<td class="cart__total-cost">'.$cost.'</td>';
        $output .= '</tr>';
        $output .= '</table>';
        return $output;
    }

    private function plural_form($number) {
        $after = array('товар','товара','товаров');
        $cases = array(2,0,1,1,1,2);
        return $number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
    }

}