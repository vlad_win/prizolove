<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxOneclick extends JPlugin
{

    public function __construct( &$subject, $config )
    {
        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
        $this->session = JFactory::getSession();
        $this->db = JFactory::getDbo();
        $this->cart = json_decode($this->session->get('plCart', json_encode(array())), true);

    }

    function onAjaxOneclick()
    {
        $productId = $this->input->post->get('pid', false, 'INT');

        if (!$productId) {
            return array('error'=>'нет ID товара');
        }

        $this->cartAdd($productId);


        $this->session->set('plOrderReferer', $_SERVER['HTTP_REFERER']);
        $this->session->set('plCart', json_encode($this->cart));

        return true;

    }

    private function cartAdd($productId)
    {
        if (isset($this->cart['items']) && array_key_exists($productId, $this->cart['items'])) {
            $this->cart['items'][$productId]['quantity']++;
        } else {
            $query = "
			  SELECT p.id, p.name, p.price, p.image, p.id_1c 
			  FROM #__prizolove_products p
			  WHERE p.id =".$productId." AND p.status = 1";

            $this->db->setQuery($query);

            $product = $this->db->loadObject();

            if (!$product) {
                return array('error'=>'Товара нет в наличии');
            }
            $this->cart['items'][$product->id] = array(
                'name' => $product->name,
                'code' => $product->id_1c,
                'image' => $product->image,
                'price' => $product->price,
                'quantity' => 1
            );

        }
    }

}