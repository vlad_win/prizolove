<?php defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgAjaxMessages extends JPlugin
{

    protected $autoloadLanguage = true;

    public function __construct( &$subject, $config )
    {

        $lang = JFactory::getLanguage();

        $lang->load('com_users', JPATH_SITE, 'ru-RU', true);

        parent::__construct( $subject, $config );

        $this->app = JFactory::getApplication();
        $this->input = $this->app->input;
    }

    function onAjaxMessages()
    {
        $id = $this->input->post->get('id', 0, 'STRING');
        $user = JFactory::getUser();
        if ($id && $user->id) {
            PlLibHelperMessages::getInstance()->removeMessage($id, $user->id);
        }
        return true;

    }

}