<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

//new for Joomla 3.0
if(!defined('DS')){
    define('DS',DIRECTORY_SEPARATOR);
}

class  plgJ2StoreAffiliatetracker extends JPlugin
{


    function plgJ2StoreAffiliatetracker(& $subject, $config)
    {
        $lang = JFactory::getLanguage();
        $lang->load('com_affiliatetracker', JPATH_SITE);

        parent::__construct($subject, $config);

    }

    /**
     * Creates a conversion when the order is placed. Even if the status is pending
     *
     * @param $payment: Information about the order placed
     */
    function onJ2StoreAfterPayment($payment)
    {
        if ($this->params->get('create_when') != 'confirmed') {
            require_once(JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');

            $exists = $this->checkOrderTrackedExists($payment->j2store_order_id);

            //if it didn't exist, we attemp to create the conversion
            if(!$exists){
                $conversion_data = array(
                    "name" => "J2Store Order",
                    "component" => "com_j2store",
                    "extended_name" => $payment->orderpayment_type,
                    "type" => 1,
                    "value" => (float)$payment->order_total ,
                    "reference_id" => $payment->j2store_order_id,
                    "approved" => $this->params->get('activation'),
                    "atid" => 0
                );
                AffiliateHelper::create_conversion($conversion_data, $payment->user_id);
            }
        }
    }

    /**
     * Creates a conversion only when the order is confirmed
     *
     * @param $order_obj: The order
     * @param $status_id: Its value is 1 when the order is confirmed
     */
    function onJ2StoreAfterOrderstatusUpdate($order_obj, $status_id)
    {
        if ($this->params->get('create_when') != 'placed' && $status_id == 1) {
            require_once(JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');

            $order_info = $order_obj->getOrderInformation();

            $exists = $this->checkOrderTrackedExists($order_info->j2store_orderinfo_id);

            //if it didn't exist, we attemp to create the conversion
            if(!$exists){
                $conversion_data = array(
                    "name" => "J2Store Order",
                    "component" => "com_j2store",
                    "extended_name" => $order_obj->orderpayment_type,
                    "type" => 1,
                    "value" => (float)$order_obj->order_total ,
                    "reference_id" => $order_obj->j2store_order_id,
                    "approved" => $this->params->get('activation'),
                    "atid" => 0
                );
                AffiliateHelper::create_conversion($conversion_data, $order_obj->user_id);
            }
        }
    }

    private function checkOrderTrackedExists($order_id)
    {
        $db = JFactory::getDBO();

        //we check if this particular order had already been tracked
        $query = " SELECT id FROM #__affiliate_tracker_conversions WHERE component = 'com_j2store' AND type = 1 AND reference_id = ".$order_id;
        $db->setQuery($query);
        return $db->loadResult();
    }

}
