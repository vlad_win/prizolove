<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class  plgUserAffiliateUsers extends JPlugin
{

	function __construct(&$subject, $config)
	{

		$lang = JFactory::getLanguage();
		$lang->load('com_affiliatetracker', JPATH_SITE);

		parent::__construct($subject, $config);

	}

	/**
	 * Trigger activated when a Joomla user is saved to the database
	 */
	function onUserAfterSave($user, $isnew, $success, $msg)
	{
		if ($isnew && $success) {
			$this->create_affiliate($user);
		}

		return true;
	}

	/**
	 * Cretates an affiliate account from the Joomla user data
	 *
	 * @param $data. User info
	 */
	function create_affiliate($data){
		//if he has an affiliate account already, do nothing.

		require_once( JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_affiliatetracker'.DS.'tables'.DS.'account.php' );
		require_once( JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php' );

		$compparams = JComponentHelper::getParams( 'com_affiliatetracker' );

		$exists = AffiliateHelper::hasAccounts($data['id']);

		if (!empty($exists)) return;

		$userData = array();

		// Assign affiliate_parent if necessary
		$parent_atid = AffiliateHelper::get_atid_from_userid($data['id']);
		if (!empty($parent_atid)) {
			$userData['parent_id'] = $parent_atid;
		}
		elseif (!empty($_COOKIE["atid"])) {
			$cookiecontent = unserialize(base64_decode($_COOKIE["atid"]));
			$parent_atid = (int)$cookiecontent["atid"] ;

			if($parent_atid) $userData['parent_id'] = $parent_atid;
		}

		$userData['account_name'] = $data['username'];
		$userData['name'] = $data['name'];
		$userData['email'] = $data['email'];
		$userData['user_id'] = $data['id'];
		if ($this->params->get('account_activation', '0') == 1) {
			$userData['publish'] = true;
		}

		$userData['type'] = $compparams->get('default_type', 'percent');
		$userData['comission'] = $compparams->get('default_amount', '10');

		$model = $this->getAccountModel();

		$model->store($userData);

		return;
	}

	static private function getAccountModel()
	{
		if (!class_exists( 'AffiliateModelAccount' ))
		{
			// Build the path to the model based upon a supplied base path
			$path = JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'models'.DS.'account.php';
			$false = false;

			// If the model file exists include it and try to instantiate the object
			if (file_exists( $path )) {
				require_once( $path );
				if (!class_exists( 'AffiliateModelAccount' )) {
					JError::raiseWarning( 0, 'Model class AffiliateModelAccount not found in file.' );
					return $false;
				}
			} else {
				JError::raiseWarning( 0, 'Model AffiliateModelAccount not supported. File not found.' );
				return $false;
			}
		}

		$model = new AffiliateModelAccount();
		return $model;
	}

}
