<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class  plgUserNotifications extends JPlugin
{

	function __construct(&$subject, $config)
	{

		$lang = JFactory::getLanguage();
		$lang->load('com_affiliatetracker', JPATH_SITE);

		parent::__construct($subject, $config);

	}

	/**
	 * Trigger activated when a Joomla user is saved to the database
	 */
	function onUserAfterSave($user, $isnew, $success, $msg)
	{
		if ($isnew && $success) {
			$this->checkAffiliate($user);
		}

		return true;
	}

	/**
	 * Cretates an affiliate account from the Joomla user data
	 *
	 * @param $data. User info
	 */
	function checkAffiliate($data){
		//if he has an affiliate account already, do nothing.

		require_once( JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_affiliatetracker'.DS.'tables'.DS.'account.php' );
		require_once( JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php' );

		$compparams = JComponentHelper::getParams( 'com_affiliatetracker' );

		$exists = AffiliateHelper::hasAccounts($data['id']);

		if (!empty($exists)) return;

		$userData = array();

		// Assign affiliate_parent if necessary
		$parent_atid = AffiliateHelper::get_atid_from_userid($data['id']);
		if (!empty($parent_atid)) {
			$userData['parent_id'] = $parent_atid;
		}
		elseif (!empty($_COOKIE["atid"])) {
			$cookiecontent = unserialize(base64_decode($_COOKIE["atid"]));
			$parent_atid = (int)$cookiecontent["atid"] ;

			if($parent_atid) $userData['parent_id'] = $parent_atid;
		}

		$userData['account_name'] = $data['username'];
		$userData['email'] = $data['email'];
		$userData['user_id'] = $data['id'];

		if (empty($userData['parent_id'])) return;

		$userId = PlLibHelperAffiliate::getUserIdByAtid($userData['parent_id']);

        if ($userId) {
            PlLibHelperDemo::getInstance()->sendEmailNotification($userId,360, true);
            PlLibHelperUsers::getInstance()->changeBonuses($userId, 7, 'deb', 'Бонус за регистрацию пользователя');
        }




		return;
	}


}
