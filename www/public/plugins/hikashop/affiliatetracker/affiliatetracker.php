<?php

/*------------------------------------------------------------------------
# com_affiliatetracker - Affiliate Tracker for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2016 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

//new for Joomla 3.0
if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}

class  plgHikashopAffiliatetracker extends JPlugin
{

	function __construct(& $subject, $config)
	{
		$lang = JFactory::getLanguage();
		$lang->load('com_affiliatetracker', JPATH_SITE);

		parent::__construct($subject, $config);

	}

	function onAfterOrderCreate(&$order,&$send_email) {

		$this->onAfterOrderUpdate($order, $send_email);

	}

	function onAfterOrderUpdate(&$order,&$send_email) {

		//only attempt to create the conversion if the order status is CONFIRMED
		if($order->order_status == "confirmed"){

			$db = JFactory::getDBO();

			if(isset($order->order_id)) $order_id = $order->order_id ;
			else $order_id = $order->old->order_id ;

			//we check if this particular order had already been tracked
			$query = " SELECT id FROM #__affiliate_tracker_conversions WHERE component = 'com_hikashop' AND type = 1 AND reference_id = ".$order_id;
			$db->setQuery($query);
			$exists = $db->loadResult();

			//if it didn't exist, we attemp to create the conversion
			if(!$exists){

				if(isset($order->order_user_id) && !empty($order->order_user_id)) $order_user_id = $order->order_user_id ;
				else $order_user_id = $order->old->order_user_id ;

				$query = " SELECT user_cms_id FROM #__hikashop_user WHERE user_id = ".$order_user_id;
				$db->setQuery($query);
				$user_id = $db->loadResult();

				if(isset($order->order_full_price) && !empty($order->order_full_price)) $order_full_price = $order->order_full_price ;
				else $order_full_price = $order->old->order_full_price ;

				if(isset($order->order_type) && !empty($order->order_type)) $order_type = $order->order_type ;
				else $order_type = $order->old->order_type ;

				if(isset($order->order_shipping_price) && !empty($order->order_shipping_price)) $order_shipping_price = $order->order_shipping_price ;
				else $order_shipping_price = $order->old->order_shipping_price ;

				$query = " SELECT atid FROM #__affiliate_tracker_logs WHERE ip = '".$order->order_ip . "' ";
				$db->setQuery($query);
				$atid = $db->loadResult();

				$conversion_data = array(
										"name" => "HikaShop Order",
										"component" => "com_hikashop",
										"extended_name" => $order_type,
										"type" => 1,
										"value" => (float)$order_full_price - $order_shipping_price ,
										"reference_id" => $order->order_id,
										"approved" => $this->params->get('activation'),
										"atid" => $atid
									);

				require_once(JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');
				AffiliateHelper::create_conversion($conversion_data, $user_id);
			}

		}
	}

}
