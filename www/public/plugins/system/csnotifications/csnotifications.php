<?php

/*------------------------------------------------------------------------
# com_contentstats - Content Statistics for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2017 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

//new for Joomla 3.0
if(!defined('DS')){
	define('DS',DIRECTORY_SEPARATOR);
}

class  plgSystemCsnotifications extends JPlugin
{
	
	function onAfterRoute()
	{
		if($this->params->get('delayed', 1)){
			if(file_exists(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'helpers'.DS.'helpers.php')){
				require_once(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'helpers'.DS.'helpers.php');

				require_once(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'models'.DS.'notification.php');

				$model = new NotificationsModelNotification();
				$model->getDelayedNotifications();
			}
		}	

		return true ;
		
	}

	function CSInstantNotification($entry){
		if($this->params->get('instant', 1)){
			if(file_exists(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'helpers'.DS.'helpers.php')){
				require_once(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'helpers'.DS.'helpers.php');

				require_once(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_contentstats'.DS.'models'.DS.'notification.php');

				$model = new NotificationsModelNotification();
				$model->getMatchingInstantNotifications($entry);
			}
		}
	}
	
}