<?php

/*------------------------------------------------------------------------
# com_affiliatetracker - Affiliate Tracker for Joomla
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2014 JoomlaThat.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.JoomlaThat.com
# Technical Support:	Forum - http://www.JoomlaThat.com/support
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class  plgSystemAffiliates_virtuemart extends JPlugin
{

	function __construct(& $subject, $config)
	{

		$lang = JFactory::getLanguage();
		$lang->load('com_affiliatetracker', JPATH_SITE);

		parent::__construct($subject, $config);

	}

	function plgVmConfirmedOrder($cart, $order) {
		//print_r($order);die;

		$data = $order['details']['BT'] ;

	  if(in_array($data->order_status, $this->params->get('activate_on', array('C')))){
			$this->create_conversion($data);
	  }

		return true;
	}

	function plgVmOnUpdateOrderPayment($data,$old_order_status){
		if(in_array($data->order_status, $this->params->get('activate_on', array('C')))){
			$this->create_conversion($data);
	  }

	}

	function create_conversion($data){
		$db = JFactory::getDBO();

		//we check if this particular order had already been tracked
		$query = " SELECT id FROM #__affiliate_tracker_conversions WHERE component = 'com_virtuemart' AND type = 1 AND reference_id = ".$data->virtuemart_order_id;
		$db->setQuery($query);
		$exists = $db->loadResult();

		//if it didn't exist, we attemp to create the conversion
		if(!$exists){

			$user_id = $data->virtuemart_user_id;

			$ip_address = explode(".", $data->ip_address);
			$ip_address = $ip_address[0].".".$ip_address[1].".".$ip_address[2];

			$query = " SELECT atid FROM #__affiliate_tracker_logs WHERE ip LIKE '".$ip_address . "%' ";
			$db->setQuery($query);
			$atid = $db->loadResult();

			if($this->params->get('comissionvalue')) $value = $data->order_subtotal ;
			else $value = $data->order_total ;

			if($this->params->get('elegibleproducts')){
				$query = " SELECT SUM(voi.product_subtotal_with_tax) FROM #__virtuemart_order_items AS voi
										WHERE voi.virtuemart_order_id = ". $data->virtuemart_order_id . "
										AND voi.virtuemart_product_id IN (".$this->params->get('elegibleproducts').")";
				$db->setQuery($query);
				$value = $db->loadResult();
			}

			$conversion_data = array(
									"name" => "VirtueMart Order",
									"component" => "com_virtuemart",
									"extended_name" => "Order",
									"type" => 1,
									"value" => (float)$value ,
									"reference_id" => $data->virtuemart_order_id,
									"approved" => $this->params->get('activation'),
									"atid" => $atid
								);

			require_once(JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');
			AffiliateHelper::create_conversion($conversion_data, $user_id);
		}
	}

function _logData($data)
	{
		$f = fopen(JPATH_CACHE . '/vm-affiliates.txt', 'a');
		fwrite($f, "\n" . date('F j, Y, g:i a') . "\n");
		fwrite($f, print_r($data, true));
		fclose($f);
	}

}
