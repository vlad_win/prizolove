<?php
defined('_JEXEC') or die('Restricted access');

class PlgSystemAutologin extends JPlugin
{
    var $username;
    var $password;

    function __construct(& $subject, $config){
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    function onAfterInitialise(){
        $this->username = JRequest::getVar('user', null);
        $this->password = JRequest::getVar('passw', null);

        if(empty($this->username) || empty($this->password)) return true;

        $result = $this->params->get('authmethod','1') === '0' ? $this->plainLogin() : $this->encryptLogin();

        $redirectURL = $this->params->get('urlredirect', null);
        if(!empty($redirectURL)){
            if(!JError::isError($result)){
                $app = JFactory::getApplication();
                $app->redirect($redirectURL);
            }
        }
    }

    function plainLogin(){
        $app = JFactory::getApplication();

        $credentials = array();
        $credentials['username'] = $this->username;
        $credentials['password'] = $this->password;

        $options = array();
        $result = $app->login($credentials, $options);
        if(!$result){
            $credentials['username'] = urldecode($this->username);
            $credentials['password'] = urldecode($this->password);
            $result = $app->login($credentials, $options);
        }

        return $result;
    }

    function encryptLogin(){
        $app = JFactory::getApplication();
        $db = JFactory::getDBO();
      
        $db->setQuery('SELECT `id`, `username`, `password` FROM `#__users` WHERE username = '.$db->Quote($this->username).' AND password='.$db->Quote($this->password));
        $result = $db->loadObject();
        if(!$result) {
            $db->setQuery('SELECT `id`, `username`, `password` FROM `#__users` WHERE username = '.$db->Quote(urldecode($this->username)).' AND password='.$db->Quote(urldecode($this->password)));
            $result = $db->loadObject();
        }

        if($result) {
            JPluginHelper::importPlugin('user');

            $options = array();
            $options['action'] = 'core.login.site';

            $response['username'] = $result->username;
            $result = $app->triggerEvent('onUserLogin', array((array)$response, $options));
        }
    }
}