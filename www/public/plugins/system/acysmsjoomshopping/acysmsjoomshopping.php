<?php
/**
 * @package	AcySMS for Joomla!
 * @version	3.5.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

class plgSystemAcysmsJoomShopping extends JPlugin{
	var $version = false;

	function __construct(&$subject, $config){
		if(!file_exists(rtrim(JPATH_ADMINISTRATOR, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_jshopping')) return;
		parent::__construct($subject, $config);
	}


	function onACYSMSGetMessageType(&$types, $integration){
		$newType = new stdClass();
		$newType->name = JText::sprintf('SMS_AUTO_ORDER_STATUS', 'JoomShopping');
		$types['joomshoppingorder'] = $newType;
	}






	function onACYSMSdisplayParamsAutoMessage_joomshoppingorder($message){

		$lang = JFactory::getLanguage();

		$db = JFactory::getDBO();
		$timevalue = array();
		$timevalue[] = JHTML::_('select.option', 'hours', JText::_('SMS_HOURS'));
		$timevalue[] = JHTML::_('select.option', 'days', JText::_('SMS_DAYS'));
		$timevalue[] = JHTML::_('select.option', 'weeks', JText::_('SMS_WEEKS'));
		$timevalue[] = JHTML::_('select.option', 'months', JText::_('SMS_MONTHS'));

		$orderStatus[] = JHTML::_('select.option', '', JText::_(' - - - '));
		$query = 'SELECT `status_id` AS value, `name_'.$lang->getTag().'` AS text
				 FROM `#__jshopping_order_status`
				 ORDER BY `status_id` ASC';
		$db->setQuery($query);
		$orders = $db->loadObjectList();

		foreach($orders as $oneOrder){
			$orderStatus[] = JHTML::_('select.option', $oneOrder->value, $oneOrder->text);
		}

		$delay = JHTML::_('select.genericlist', $timevalue, "data[message][message_receiver][auto][joomshoppingorder][delay][timevalue]", 'size="1" style="width:auto"', 'value', 'text', '0');
		$status1 = JHTML::_('select.genericlist', $orderStatus, "data[message][message_receiver][auto][joomshoppingorder][status][status1]", 'size="1"  style="width:auto;"', 'value', 'text', '0');
		$status2 = JHTML::_('select.genericlist', $orderStatus, "data[message][message_receiver][auto][joomshoppingorder][status][status2]", 'size="1"  style="width:auto;"', 'value', 'text', '0');


		$timeNumber = '<input type="text" name="data[message][message_receiver][auto][joomshoppingorder][delay][duration]" class="inputbox" style="width:30px" value="0">';
		echo JText::sprintf('SMS_AFTER_ORDER_MODIF', $timeNumber.' '.$delay).'<br />';

		$db->setQuery('SELECT category_id, `name_'.$lang->getTag().'` as name
						FROM `#__jshopping_categories`
						ORDER BY `category_id`');
		$jshoppingCategories = $db->loadObjectList();

		if(!empty($jshoppingCategories)){
			$jshoppingCategoriesOptions = array();
			$jshoppingCategoriesOptions[] = JHTML::_('select.option', '', JText::_('SMS_ANY_CATEGORIES'));
			foreach($jshoppingCategories as $oneMijoCategory){
				$jshoppingCategoriesOptions[] = JHTML::_('select.option', $oneMijoCategory->category_id, $oneMijoCategory->name);
			}
			$jShoppingCategoryDropDown = JHTML::_('select.genericlist', $jshoppingCategoriesOptions, "data[message][message_receiver][auto][joomshoppingorder][other][category]", 'size="1" style="width:auto"', 'value', 'text', '0');
		}

		$productName = '';
		if(!empty($message->message_receiver['auto']['joomshoppingorder']['productName'])) $productName = $message->message_receiver['auto']['joomshoppingorder']['productName'];

		echo str_replace(array('%s', '%t'), array($status1, $status2), JText::_('SMS_STATUS_CHANGES')).'<br />';
		echo JText::_('SMS_ORDER_CONTAINS_PRODUCT').' : <span id="displayedjShoppingProduct"/>'.$productName.'</span><a class="modal"  onclick="window.acysms_js.openBox(this,\'index.php?option=com_acysms&tmpl=component&ctrl=cpanel&task=plgtrigger&plgtype=system&plg=acysmsjoomshopping&fctName=displayjShoppingArticles\');return false;" rel="{handler: \'iframe\', size: {x: 800, y: 500}}"><i class="smsicon-edit"></i></a>';
		echo '<input type="hidden" name="data[message][message_receiver][auto][joomshoppingorder][other][product]" id="selectedjShoppingProduct"></input><br />';
		echo '<input type="hidden" name="data[message][message_receiver][auto][joomshoppingorder][productName]" id="hiddenjShoppingProduct"/><br />';
		if(!empty($jShoppingCategoryDropDown)) echo JText::_('SMS_ONLY_ORDER_CONTAINS_PRODUCT_FROM_CATEGORY').' : '.$jShoppingCategoryDropDown;
	}



	function onAcySMSdisplayjShoppingArticles(){
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		$lang = JFactory::getLanguage();

		$pageInfo = new stdClass();
		$pageInfo->filter = new stdClass();
		$pageInfo->filter->order = new stdClass();
		$pageInfo->limit = new stdClass();
		$pageInfo->elements = new stdClass();

		$paramBase = ACYSMS_COMPONENT.'content';
		$pageInfo->filter->order->value = $app->getUserStateFromRequest($paramBase.".filter_order", 'filter_order', 'product_id', 'cmd');
		$pageInfo->filter->order->dir = $app->getUserStateFromRequest($paramBase.".filter_order_Dir", 'filter_order_Dir', 'desc', 'word');
		$pageInfo->search = $app->getUserStateFromRequest($paramBase.".search", 'search', '', 'string');
		$pageInfo->search = JString::strtolower(trim($pageInfo->search));

		$pageInfo->limit->value = $app->getUserStateFromRequest($paramBase.'.list_limit', 'limit', $app->getCfg('list_limit'), 'int');
		$pageInfo->limit->start = $app->getUserStateFromRequest($paramBase.'.limitstart', 'limitstart', 0, 'int');

		$query = ('SELECT jShoppingProduct.product_id, jShoppingProduct.`name_'.$lang->getTag().'` AS name, jShoppingProduct.`short_description_'.$lang->getTag().'` as description, jShoppingCategory.`name_'.$lang->getTag().'` AS catname
					FROM #__jshopping_products AS jShoppingProduct
					LEFT JOIN #__jshopping_products_to_categories AS jShoppingProductToCategory ON jShoppingProduct.product_id = jShoppingProductToCategory.product_id
					LEFT JOIN #__jshopping_categories AS jShoppingCategory ON jShoppingProductToCategory.category_id = jShoppingCategory.category_id');

		$searchMap = array('jShoppingProduct.`name_'.$lang->getTag().'`', 'jShoppingProduct.`short_description_'.$lang->getTag().'`', 'jShoppingCategory.`name_'.$lang->getTag().'`');
		if(!empty($pageInfo->search)){
			$searchVal = '\'%'.acysms_getEscaped($pageInfo->search, true).'%\'';
			$filters[] = implode(" LIKE $searchVal OR ", $searchMap)." LIKE $searchVal";
		}
		if(!empty($filters)) $query .= ' WHERE ('.implode(') AND (', $filters).')';

		$query .= ' GROUP BY jShoppingProduct.product_id';

		if(!empty ($pageInfo->filter->order->value)){
			$query .= ' ORDER BY '.$pageInfo->filter->order->value.' '.$pageInfo->filter->order->dir;

			$db->setQuery($query);
			$rows = $db->loadObjectList();

			$pageInfo->elements->total = count($rows);

			jimport('joomla.html.pagination');
			$pagination = new JPagination($pageInfo->elements->total, $pageInfo->limit->start, $pageInfo->limit->value);
			?>
			<script language="javascript" type="text/javascript">
				var selectedContents = new Array();
				var selectedContentsName = new Array();
				function addProduct(){
					var selectedProduct = "";
					var selectedProductId = "";
					var form = document.adminForm;
					for(i = 0; i <= form.length - 1; i++){
						if(form[i].type == 'checkbox'){

							if(!document.getElementById("productId" + form[i].id)) continue;
							if(document.getElementById("productId" + form[i].id).innerHTML.lentgth == 0) continue;
							oneProductId = document.getElementById("productId" + form[i].id).innerHTML.trim();

							productId = "productId" + form[i].id
							if(!document.getElementById("productName" + form[i].id)) continue;
							if(document.getElementById("productName" + form[i].id).innerHTML.lentgth == 0) continue;
							oneProduct = document.getElementById("productName" + form[i].id).innerHTML;

							var tmp = selectedContents.indexOf(oneProductId);
							if(tmp != -1 && form[i].checked == false){
								delete selectedContents[tmp];
								delete selectedContentsName[tmp];
							}else if(tmp == -1 && form[i].checked == true){
								selectedContents.push(oneProductId);
								selectedContentsName.push(oneProduct);
							}
						}
					}

					for(var i in selectedContents){
						if(selectedContents[i] && !isNaN(i))    selectedProductId += selectedContents[i].trim() + ",";
						if(selectedContentsName[i] && !isNaN(i))    selectedProduct += " " + selectedContentsName[i].trim() + " , ";
					}

					window.document.getElementById("productSelected").value = selectedProductId;
					window.document.getElementById("productDisplayed").value = selectedProduct;
				}

				function confirmProductSelection(){
					selected = window.document.getElementById("productSelected").value;
					displayed = window.document.getElementById("productDisplayed").value;

					parent.window.document.getElementById("selectedjShoppingProduct").value = selected.substring(0, selected.length - 1);

					parent.window.document.getElementById("displayedjShoppingProduct").innerHTML = displayed.substring(1, displayed.length - 3);
					parent.window.document.getElementById("hiddenjShoppingProduct").value = displayed.substring(1, displayed.length - 3);


					acysms_js.closeBox(true);
				}

			</script>
			<form action="#" method="post" name="adminForm" id="adminForm" autocomplete="off">
				<table class="acysms_table_options">
					<tr>
						<td>
							<input type="hidden" id="productSelected"/>
							<input type="textbox" size="30" id="productDisplayed" readonly value=""/>
							<input type="button" onclick="confirmProductSelection()" value="<?php echo JText::_('SMS_VALIDATE') ?>"/>
						</td>
					</tr>
					<tr>
						<td>
							<?php ACYSMS::listingSearch($pageInfo->search); ?>
						</td>
					</tr>
				</table>
				<table class="acysms_table">
					<thead>
					<th class="title titlebox">
						<input type="checkbox" name="toggle" value="" onclick="acysms_js.checkAll(this); addProduct();"/>
					</th>
					<th class="title titlename">
						<?php echo JHTML::_('grid.sort', JText::_('SMS_NAME'), 'jShoppingProduct.`name_'.$lang->getTag().'`', $pageInfo->filter->order->dir, $pageInfo->filter->order->value); ?>
					</th>
					<th class="title titledesc">
						<?php echo JHTML::_('grid.sort', JText::_('SMS_DESCRIPTION'), 'jShoppingProduct.`short_description_'.$lang->getTag().'`', $pageInfo->filter->order->dir, $pageInfo->filter->order->value); ?>
					</th>
					<th class="title titlecode">
						<?php echo JHTML::_('grid.sort', JText::_('SMS_CATEGORY'), 'catname', $pageInfo->filter->order->dir, $pageInfo->filter->order->value); ?>
					</th>
					<th class="title titleid">
						<?php echo JHTML::_('grid.sort', JText::_('SMS_ID'), 'jShoppingProduct.product_id', $pageInfo->filter->order->dir, $pageInfo->filter->order->value); ?>
					</th>
					</tr>
					</thead>
					<tfoot>
					<tr>
						<td colspan="5">
							<?php echo $pagination->getListFooter(); ?>
							<?php echo $pagination->getResultsCounter(); ?>
						</td>
					</tr>
					</tfoot>
					<tbody>

					<?php
					$k = 0;
					for($i = 0, $a = count($rows); $i < $a; $i++){
						$row = $rows[$i];
						?>
						<tr class="<?php echo "row$k"; ?>">
							<td align="center">
								<input type="checkbox" value="<?php echo $row->product_id ?>" id="cb<?php echo $i; ?>" onclick="addProduct();">
							</td>
							<td align="center" id="productNamecb<?php echo $i; ?>">
								<?php
								echo $row->name;
								?>
							</td>
							<td align="center">
								<?php
								if(!empty($row->description)) echo substr(strip_tags(html_entity_decode($row->description), '<br>'), 0, 200).'...';
								?>
							</td>
							<td align="center">
								<?php
								echo $row->catname;
								?>
							</td>
							<td align="center" id="productIdcb<?php echo $i; ?>">
								<?php
								echo $row->product_id;
								?>
							</td>
						</tr>
						<?php
						$k = 1 - $k;
					}
					?>
					</tbody>
				</table>
				<input type="hidden" name="boxchecked" value="0"/>
				<input type="hidden" name="filter_order" value="<?php echo $pageInfo->filter->order->value; ?>"/>
				<input type="hidden" name="filter_order_Dir" value="<?php echo $pageInfo->filter->order->dir; ?>"/>
			</form>
			<?php
		}
	}

	public function onBeforeChangeOrderStatus(&$order_id, &$status, &$sendmessage, &$resttext){
		if(empty($order_id)) return;
		if(!$this->init()) return;

		$this->manageStatus($order_id, $status);
	}

	public function onBeforeChangeOrderStatusAdmin(&$order_id, &$order_status, &$notify, &$comments, &$include, &$view_order){
		if(empty($order_id)) return;
		if(!$this->init()) return;

		$this->manageStatus($order_id, $order_status);
	}


	private function init(){
		if(defined('ACYSMS_COMPONENT')) return true;
		$acySmsHelper = rtrim(JPATH_ROOT, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acysms'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php';
		if(file_exists($acySmsHelper)){
			include_once $acySmsHelper;
		}else return false;
		return defined('ACYSMS_COMPONENT');
	}




	public function manageStatus($order_id, $status){

		$db = JFactory::getDBO();
		$messageClass = ACYSMS::get('class.message');
		$allMessages = $messageClass->getAutoMessage('joomshoppingorder');
		if(empty($allMessages)) return;

		$query = 'SELECT order_status FROM #__jshopping_orders WHERE order_id = '.intval($order_id);
		$db->setQuery($query);
		$old_order_status = $db->loadResult();

		$newStatus = $status;

		foreach($allMessages as $messageID => $oneMessage){
			if(empty($oneMessage->message_receiver['auto']['joomshoppingorder']['status'])) continue;

			if(empty($old_order_status) && !empty($oneMessage->message_receiver['auto']['joomshoppingorder']['status']['status1'])) continue;

			if(!empty($oneMessage->message_receiver['auto']['joomshoppingorder']['status']['status1']) && $old_order_status != $oneMessage->message_receiver['auto']['joomshoppingorder']['status']['status1']) continue;
			if(!empty($oneMessage->message_receiver['auto']['joomshoppingorder']['status']['status2']) && $newStatus != $oneMessage->message_receiver['auto']['joomshoppingorder']['status']['status2']) continue;

			if(!empty($oneMessage->message_receiver['auto']['joomshoppingorder']['other']['product'])){
				$query = 'SELECT product_id FROM #__jshopping_order_item WHERE order_id = '.intval($order_id);
				$db->setQuery($query);
				$productIds = acysms_loadResultArray($db);
				if(!in_array($oneMessage->message_receiver['auto']['joomshoppingorder']['other']['product'], $productIds)) continue;
			}
			if(!empty($oneMessage->message_receiver['auto']['joomshoppingorder']['other']['category'])){
				$query = 'SELECT jShoppingProductToCategory.category_id
					FROM #__jshopping_order_item AS jShoppingOrderItem
					JOIN #__jshopping_products_to_categories AS jShoppingProductToCategory
					ON jShoppingOrderItem.product_id = jShoppingProductToCategory.product_id
					WHERE jShoppingOrderItem.order_id = '.intval($order_id);
				$db->setQuery($query);
				$categoryIds = acysms_loadResultArray($db);
				if(!in_array($oneMessage->message_receiver['auto']['joomshoppingorder']['other']['category'], $categoryIds)) continue;
			}
			$senddate = strtotime('+'.intval($oneMessage->message_receiver['auto']['joomshoppingorder']['delay']['duration']).' '.$oneMessage->message_receiver['auto']['joomshoppingorder']['delay']['timevalue'], time());

			$queryUser = 'SELECT jShoppingUser.user_id
			FROM #__jshopping_users AS jShoppingUser
			INNER JOIN #__jshopping_orders AS jShoppingOrder ON jShoppingOrder.user_id = jShoppingUser.user_id
			WHERE jShoppingOrder.order_id  = '.intval($order_id);
			$db->setQuery($queryUser);
			$receiver_id = $db->loadResult();

			$params = new stdClass();
			$params->joomshopping_order_id = $order_id;
			$paramqueue = serialize($params);

			$acyquery = ACYSMS::get('class.acyquery');
			$integrationTo = $oneMessage->message_receiver_table;
			$integration = ACYSMS::getIntegration($oneMessage->message_receiver_table);
			$integration->initQuery($acyquery);
			$acyquery->addMessageFilters($oneMessage);

			$integrationFrom = ACYSMS::getIntegration();

			if(!empty($receiver_id)) $acyquery->addUserFilters(array($receiver_id), $integrationFrom->componentName, $integrationTo);

			$querySelect = $acyquery->getQuery(array($oneMessage->message_id.','.$integration->tableAlias.'.'.$integration->primaryField.','.$db->Quote($oneMessage->message_receiver_table).','.$senddate.',0,2,'.$db->Quote($paramqueue)));


			$finalQuery = 'INSERT IGNORE INTO `#__acysms_queue` (`queue_message_id`,`queue_receiver_id`,`queue_receiver_table`,`queue_senddate`,`queue_try`,`queue_priority`,`queue_paramqueue`) '.$querySelect;
			$db->setQuery($finalQuery);
			$db->query();

			if(empty($oneMessage->message_receiver['auto']['joomshoppingorder']['delay']['duration'])){
				$queueHelper = ACYSMS::get('helper.queue');
				$queueHelper->report = false;
				$queueHelper->message_id = $oneMessage->message_id;
				$queueHelper->process();
			}
		}
	}


	function onACYSMSGetTags(&$tags){
		$tags['ecommerceTags']['joomshoppinguser'] = new stdClass();
		$tags['ecommerceTags']['joomshoppinguser']->name = JText::sprintf('SMS_X_USER_INFO', 'JoomShopping');

		$tableFields = array();
		$tableFields += acysms_getColumns('#__jshopping_users');

		$tags['ecommerceTags']['joomshoppinguser']->content = '<table class="acysms_table"><tbody>';
		$k = 0;
		foreach($tableFields as $oneField => $fieldType){
			$tags['ecommerceTags']['joomshoppinguser']->content .= '<tr style="cursor:pointer" onclick="insertTag(\'{joomshoppinguser:'.$oneField.'}\')" class="row'.$k.'"><td>'.$oneField.'</td></tr>';
			$k = 1 - $k;
		}
		$tags['ecommerceTags']['joomshoppinguser']->content .= '</tbody></table>';

		$tags['ecommerceTags']['joomshoppingorder'] = new stdClass();
		$tags['ecommerceTags']['joomshoppingorder']->name = JText::sprintf('SMS_X_ORDER_INFO', 'JoomShopping');

		$tableFields = array();
		$tableFields['order_id'] = 'char';
		$tableFields['order_number'] = 'varchar';
		$tableFields['order_total'] = 'decimal(14,4)';
		$tableFields['order_subtotal'] = 'decimal(14,4)';
		$tableFields['order_tax'] = 'decimal(14,4)';

		$tags['ecommerceTags']['joomshoppingorder']->content = '<table class="acysms_table"><tbody>';
		$k = 0;
		foreach($tableFields as $oneField => $fieldType){
			$tags['ecommerceTags']['joomshoppingorder']->content .= '<tr style="cursor:pointer" onclick="insertTag(\'{joomshoppinguser:'.$oneField.'}\')" class="row'.$k.'"><td>'.$oneField.'</td></tr>';
			$k = 1 - $k;
		}
		$tags['ecommerceTags']['joomshoppingorder']->content .= '</tbody></table>';
	}

	function onACYSMSReplaceUserTags(&$message, &$user, $send = true){
		$db = JFactory::getDBO();
		$helperPlugin = ACYSMS::get('helper.plugins');

		$match = '#(?:{|%7B)joomshoppinguser:(.*)(?:}|%7D)#Ui';
		$match2 = '#(?:{|%7B)joomshoppingorder:(.*)(?:}|%7D)#Ui';

		if(empty($message->message_body)) return;

		if(!preg_match_all($match, $message->message_body, $results) && !preg_match_all($match2, $message->message_body, $results)) return;

		if(!isset($user->joomshoppinguser)){
			$query = 'SELECT *
					FROM #__jshopping_users AS orders
					WHERE user_id = '.intval($user->joomla->id).'
					LIMIT 1';
			$db->setQuery($query);
			$user->joomshoppinguser = $db->loadObject();
		}
		if(!isset($user->joomshoppingorder) && (!empty($user->queue_paramqueue->joomshopping_order_id))){
			$query = 'SELECT *
					FROM #__jshopping_orders AS orders
					WHERE order_id = '.intval($user->queue_paramqueue->joomshopping_order_id).'
					LIMIT 1';
			$db->setQuery($query);
			$user->joomshoppingorder = $db->loadObject();
		}

		$tags = array();
		foreach($results[0] as $i => $oneTag){
			if(isset($tags[$oneTag])) continue;
			$arguments = explode('|', strip_tags($results[1][$i]));
			$field = $arguments[0];
			unset($arguments[0]);
			$mytag = new stdClass();
			$mytag->default = '';
			if(!empty($arguments)){
				foreach($arguments as $onearg){
					$args = explode(':', $onearg);
					if(isset($args[1])){
						$mytag->$args[0] = $args[1];
					}else{
						$mytag->$args[0] = 1;
					}
				}
			}
			$tags[$oneTag] = (isset($user->joomshoppinguser->$field) && strlen($user->joomshoppinguser->$field) > 0) ? $user->joomshoppinguser->$field : $mytag->default;
			$tags[$oneTag] = (isset($user->joomshoppingorder->$field) && strlen($user->joomshoppingorder->$field) > 0) ? $user->joomshoppingorder->$field : $mytag->default;
			$helperPlugin->formatString($tags[$oneTag], $mytag);
		}
		$message->message_body = str_replace(array_keys($tags), $tags, $message->message_body);
	}
}
