<?php

defined('_JEXEC') or die;

JLoader::import('joomla.plugin.plugin');

class PlgSystemGeoRedirect extends JPlugin
{
    // Plugin info constants
    const TYPE = 'system';
    const NAME = 'georedirect';

    private $pathPlugin = null;

    private $plugin;

    public $params;

    private $componentsEnabled = array('*');

    private $viewsEnabled 		= array('*');
    
    private $frontendEnabled 	= true;

    private $backendEnabled 	= false;

    function __construct( &$subject )
    {
        parent::__construct($subject);

        // Load plugin parameters
        $this->plugin = JPluginHelper::getPlugin(self::TYPE, self::NAME);
        $this->params = new JRegistry($this->plugin->params);

        // Init folder structure
        $this->initFolders();

    }

    public function onContentPrepareForm($form, $data)
    {
        // Check we have a form
        if (!($form instanceof JForm)) {
            $this->_subject->setError('JERROR_NOT_A_FORM');

            return false;
        }

        // Extra parameters for menu edit
        if ($form->getName() == 'com_menus.item') {
            $form->loadFile($this->pathPlugin . '/forms/menuitem.xml');
        }

        return true;
    }

    private function initFolders()
    {
        // Path
        $this->pathPlugin = JPATH_PLUGINS . '/' . self::TYPE . '/' . self::NAME;

        // Url
        $this->urlPlugin = JURI::root(true) . "/plugins/" . self::TYPE . "/" . self::NAME;
    }

    function onBeforeCompileHead()
    {
        if (!$this->validateUrl())
        {
            return true;
        }

        $app        = JFactory::getApplication();

        $pageParams = $app->getParams();

        $redirect = $pageParams->get('redirect', 0);
        $countryCode = 'ua';
        if (function_exists('geoip_country_code_by_name')) {
            $countryCode = strtolower(geoip_country_code_by_name($_SERVER['REMOTE_ADDR']));
        }
        if ($redirect && $countryCode == 'us') {
            $uri = JUri::getInstance();
            $url = parse_url($uri->toString());
            if (substr($url['path'], -1, 1) == '/') $url['path'] = substr($url['path'], 0, -1);

            $redirectUrl = $url['scheme'].'://'.$url['host'].$url['path'].$this->params->get('defaultSuffix').(isset($url['query'])?'/?'.$url['query']:'');
            $app->redirect($redirectUrl);
        }
    }

    /**
     * validate if the plugin is enabled for current application (frontend / backend)
     *
     * @return boolean
     */
    private function validateApplication()
    {
        $app = JFactory::getApplication();

        if ( ($app->isSite() && $this->frontendEnabled) || ($app->isAdmin() && $this->backendEnabled) )
        {
            return true;
        }

        return false;
    }

    /**
     * Validate option in url
     *
     * @return boolean
     */
    private function validateComponent()
    {
        $option = JFactory::getApplication()->input->get('option');

        if ( in_array('*', $this->componentsEnabled) || in_array($option, $this->componentsEnabled) )
        {
            return true;
        }

        return false;
    }

    /**
     * Custom method for extra validations
     *
     * @return true
     */
    private function validateExtra()
    {
        return $this->validateApplication();
    }

    /**
     * Is the plugin enabled for this url?
     *
     * @return boolean
     */
    private function validateUrl()
    {
        if ( $this->validateComponent() && $this->validateView())
        {
            if (method_exists($this, 'validateExtra'))
            {
                return $this->validateExtra();
            }
            else
            {
                return true;
            }
        }

        return false;
    }

    /**
     * validate view parameter in url
     *
     * @return boolean
     */
    private function validateView()
    {
        $view = JFactory::getApplication()->input->get('view');

        if ( in_array('*', $this->viewsEnabled) || in_array($view, $this->viewsEnabled))
        {
            return true;
        }

        return false;
    }
}