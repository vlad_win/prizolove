<?php
// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.plugin.plugin');

class plgSystemExchangeRate extends JPlugin
{
    public function __construct(&$subject, $config)
    {
        parent::__construct($subject, $config);

        if (!isset($this->params)) {
            $plugin = JPluginHelper::getPlugin('system', 'exchangerate');
            $this->params = new JRegistry($plugin->params);
        }
    }

    function onAfterInitialise()
    {
        $app = JFactory::getApplication();

        $rates = $this->params->get('rates', array());



        $app->set('plExchangeRates', $rates);


        return true;
    }
}