CREATE TABLE `fu3f5_prizolove_customers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `customer_id` INT NULL,
  `customer_name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `customer_phone` VARCHAR(13) NULL,
  `customer_email` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,  
  PRIMARY KEY (`id`));


CREATE TABLE `fu3f5_prizolove_orders` (
  `order_id` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NULL,
  `customer_id` INT NULL,
  `product_id` INT NULL,
  `product_name` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `click_id` VARCHAR(200) NULL,
  `address_delivery` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`order_id`));

CREATE TABLE `fu3f5_prizolove_status_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `order_id` INT NULL,
  `date` DATETIME NULL,
  `status` VARCHAR(10) NULL,
  `comment` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`));