DROP TABLE IF EXISTS `#__affise`;

CREATE TABLE `#__affise` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`click_id` tinytext NOT NULL,
	`pid` INT(11) NOT NULL,
	`category_id` INT(11) NOT NULL,
	`offer_id` INT(11) NOT NULL,
	`status` tinyint(1) NOT NULL,
	`date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
)
	ENGINE =MyISAM
	AUTO_INCREMENT =0
	DEFAULT CHARSET =utf8;
