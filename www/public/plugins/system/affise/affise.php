<?php
// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.plugin.plugin');
JLoader::import('pl_lib.library');

class plgSystemAffise extends JPlugin
{
    private $apiKey;
    private $apiUrl;
    private $logging;
    private $defaultCategory;
    private $cpaClickId;
    private $cpaClickTypes;
    private $clickId;
    private $pid;
    private $referer;
    private $userInfo;
    private $landId;
    private $landList;
    private $landUp;
    private $landDown;
    private $things;
    private $task;
    private $logLine;
    private $clickType;
    private $customerComment;
    private $enableCrm;



    /**
     * Load the language file on instantiation. Note this is only available in Joomla 3.1 and higher.
     * If you want to support 3.0 series you must override the constructor
     *
     * @var    boolean
     * @since  3.1
     */
    protected $autoloadLanguage = true;

    public function __construct(&$subject, $config)
    {
        parent::__construct($subject, $config);

        if(!isset($this->params)){
            $plugin = JPluginHelper::getPlugin('system', 'affise');
            $this->params = new JRegistry($plugin->params);
        }


        $landList = $this->params->get('land_list', '');

        $landUp = $this->params->get('land_up', '');
        $landDown = $this->params->get('land_down', '');

        if ($landList) {
            $pair = explode(',', $landList);
            foreach($pair as $item) {
                $value = explode(':',$item);
                $this->landList[$value[0]] = $value[1];
            }
        }

        if (!empty($landUp)) {
            $this->landUp = [];
            $line = explode(',', $landUp);
            foreach($line as $row) {
                $value = explode(':', $row);
                $this->landUp[$value[0]] = (int) $value[1];
            }
        }

        if (!empty($landDown)) {
            $this->landDown = [];
            $line = explode(',', $landDown);
            foreach($line as $row) {
                $value = explode(':', $row);
                $this->landDown[$value[0]] = (int) $value[1];
            }
        }

        $categoryGoal = $this->params->get('category_goal', '');

        if ($categoryGoal) {
            $pair = explode(',', $categoryGoal);
            foreach($pair as $item) {
                $value = explode(':',$item);
                $this->categoryGoal[$value[0]] = $value[1];
            }
        }

        $this->categoriesList = $this->params->get('categories_list', '');
        if ($this->categoriesList) $this->categoriesList = explode(',', $this->categoriesList);
        $this->defaultCategory = $this->params->get('default_category_id', 2);
        $this->apiKey = $this->params->get('api_key', '');
        $this->apiUrl = $this->params->get('send_api_url', '');
        $this->logging = $this->params->get('logging', '');
        $this->crmToken = $this->params->get('send_crm_token', '');
        $this->crmUrl = $this->params->get('send_crm_url', '');
		$this->enableCrm = $this->params->get('enable_crm', false);
        $this->greetingText = $this->params->get('greeting_text', '');

    }


    function onAcyUserCreate($subscriber)
    {
	PlLibHelperUsers::getInstance()->checkUser($subscriber);
        PlLibHelperAcy::setUnconfirmed($subscriber->subid);
		
        //$mailer = acymailing_get('helper.mailer');
        //$mailer->sendOne(8, $subscriber->email);

        $currentSession = JFactory::getSession();

        $inputCookie  = JFactory::getApplication()->input->cookie;

        $inputCookie->set($name = 'subId', $value = $subscriber->subid, $expire = (time() + 31536000));

        $this->cpaClickId = $currentSession->get('cpaClickId', false);
        $this->cpaClickType = $currentSession->get('cpaClickType', false);
        $this->clickId = $currentSession->get('penguinClickId', false);
        $this->pid = $currentSession->get('penguinPid', false);
        $this->referer = $currentSession->get('penguinReferer', false);
        $this->landId = $currentSession->get('penguinLandId', false);
        $this->things = $currentSession->get('penguinThings', false);
        $this->customerComment = $currentSession->get('penguinCustomerComment', false);
        $this->task = $currentSession->get('penguinTask', false);

        if ($this->things) {
            $this->processOrder($subscriber);
        }


        if (!empty($this->cpaClickType) && !empty($this->cpaClickId)) {
            PlLibHelperCpa::processRegistration($subscriber->subid, $this->cpaClickId, $this->cpaClickType);
        }

        if (!empty($this->clickId) && !empty(intval($this->pid))) {

            if ($this->things) {
                $things = implode(',',json_decode($this->things));
            } else {
                $things = '';
            }

            $lastRowId = PlLibHelperAffise::addRecord($subscriber->subid, $this->clickId, $this->pid, $things);

            PlLibHelperAffise::sendRegistration($subscriber, 2, $lastRowId, $this->clickId, $things, $this->landId, $this->task, 1);

            $this->clearCookies();

        }

        return true;
    }

    private function clearCookies()
    {
        $currentSession = JFactory::getSession();
        $currentSession->clear('сpaClickType');
        $currentSession->clear('cpaClickId');
        $currentSession->clear('penguinClickId');
        $currentSession->clear('penguinPid');
        $currentSession->clear('penguinThings');
        $currentSession->clear('penguinTask');
    }

    function onAcySubscribe($subid,$listids)
    {
	PlLibHelperAcy::setConfirmed($subid);
        if ($listids[0] == 46) {
            PlLibHelperAcy::subscribePartner($subid);
        } else
        if (in_array(46, $listids)) {
            PlLibHelperAcy::subscribePartner($subid);
        }

        return true;
    }

    function onAcyUserModify($subscriber)
    {
	if (!empty($subscriber->email)) PlLibHelperUsers::getInstance()->checkUser($subscriber);
        $inputCookie  = JFactory::getApplication()->input->cookie;

        $inputCookie->set($name = 'subId', $value = $subscriber->subid, $expire = (time() + 31536000));

        $currentSession = JFactory::getSession();
        $this->clickId = $currentSession->get('penguinClickId', false);
        $this->pid = $currentSession->get('penguinPid', false);
        $this->referer = $currentSession->get('penguinReferer', false);
        $this->landId = $currentSession->get('penguinLandId', false);
        $this->things = $currentSession->get('penguinThings', false);
        $this->customerComment = $currentSession->get('penguinCustomerComment', false);
        $this->task = $currentSession->get('penguinTask', false);

        if ($this->things) {
            $this->processOrder($subscriber);
        }


        return true;
    }

    function onBeforeRender()
    {
        $jinput = JFactory::getApplication()->input;

        $currentSession = JFactory::getSession();

        $clickId = $jinput->get($this->params->get('click_id_var'), false, 'STRING');
        $pid = $jinput->get($this->params->get('pid_var'), false, 'INT');
        $landId = $jinput->get('id', false);
	$app = JFactory::getApplication();
        $option = $jinput->get('option', false);

        if ( $option == 'com_sppagebuilder' && !empty($landId) && $landId == 366 ) {
		$countryCode = 'ua';
			
		if (function_exists('geoip_country_code_by_name')) {
            		$countryCode = strtolower(geoip_country_code_by_name($_SERVER['REMOTE_ADDR']));
		}		
		$app = JFactory::getApplication();
		switch($countryCode) {
			case 'kz':
				$app->redirect('/376');		
				break;
			case 'ru':
				$app->redirect('/369');		
				break;
			}
			
	}
//	 383, 384, 385, 379, 380, 366
        if ( $option == 'com_sppagebuilder' && !empty($landId) && in_array($landId, [383, 384, 385, 379, 380, 366])) {
		$app->redirect('/catalog');		
        }
	if ( $option == 'com_sppagebuilder' && !empty($landId) && $landId == 383 ) {
		$countryCode = 'ua';
			
		if (function_exists('geoip_country_code_by_name')) {
            		$countryCode = strtolower(geoip_country_code_by_name($_SERVER['REMOTE_ADDR']));
		}		

		switch($countryCode) {
			case 'kz':
				$app->redirect('/shop-window-kz');		
				break;
			case 'ru':
				$app->redirect('/shop-window-ru');		
				break;
			}
			
	}
        /* if ( $option == 'com_sppagebuilder' && !empty($landId) && $landId == 186 ) {
		$countryCode = 'ua';
			
		if (function_exists('geoip_country_code_by_name')) {
            		$countryCode = strtolower(geoip_country_code_by_name($_SERVER['REMOTE_ADDR']));
		}		
		$app = JFactory::getApplication();
		switch($countryCode) {
			case 'kz':
				$app->redirect('/380');		
				break;
			case 'ru':
				$app->redirect('/379');		
				break;
			}
			
	}*/ 


        if ( $clickId && $pid && $option == 'com_sppagebuilder' && !empty($landId) ) {
            $currentSession->set('penguinLandId', $landId);
        }

    }

    function onAfterInitialise()
    {
	PlLibHelperUtms::utmToCookie();

        $currentSession = JFactory::getSession();

        $jinput = JFactory::getApplication()->input;

        $option = $jinput->get('option', false);
        $task = $jinput->get('task', false);

        if ($task == 'confirm' && $option != 'com_acymailing') {
            $subid = $jinput->get('subid', false, 'INT');
            $ctrl = $jinput->get('ctrl', false, 'WORD');
            $key = $jinput->get('key', false, 'STRING');

            if ($subid && $ctrl && $key && $ctrl == 'user') {
                PlLibHelperUsers::getInstance()->confirmAcyMailingUser();
            }
        }

        if ($option == 'com_acymailing') {
            $subid = $jinput->get('subid', false, 'INT');
            $ctrl = $jinput->get('ctrl', false, 'WORD');
            $task = $jinput->get('task', false, 'WORD');
            $key = $jinput->get('key', false, 'STRING');

            if ($subid && $ctrl && $task && $key && $task == 'confirm' && $ctrl == 'user') {

                $helperFile = rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php';

                if(!file_exists($helperFile) || !include_once($helperFile)) return true;

                $listsubClass = acymailing_get('class.listsub');
                $userSubscriptions = $listsubClass->getSubscription($subid);

                if (!$userSubscriptions) return true;

                $db = JFactory::getDbo();

                $newSubscription = array();

                foreach($userSubscriptions as $listId=>$list) {

                    $listName = '1.'.$list->name;

                    $newList = array();
                    $newList['status'] = 1;
                    $newSubscription[$listId] = $newList;

                    $query = "SELECT `listid` FROM #__acymailing_list WHERE `name` = '".$listName."' LIMIT 1";

                    $db->setQuery($query);

                    $list = $db->loadObject();

                    if ($list) {
                        $newSubscription[$list->listid] = $newList;
                    }
                }
                $userClass = acymailing_get('class.subscriber');
                $userClass->saveSubscription($subid,$newSubscription);

                $userInfo = $this->findAffiseUser($subid, 5);

                if ($userInfo) $this->changeAffiseUserStatus($userInfo->id, 2);

                if ($userInfo) PlLibHelperAffise::sendConfirmation($userInfo, 1, 2);

                $db = JFactory::getDbo();

                $query = "SELECT `*` FROM #__acymailing_subscriber WHERE `subid` = ".$subid.' LIMIT 1';

                $db->setQuery($query);

                $user = $db->loadObject();

                if (empty($user->confirmed_date) && $user->key == $key) {
                    $query = "UPDATE #__acymailing_subscriber 
							  SET `confirmed_date` = '".(time())."',
								  `confirmed` = 1,
								  `confirmed_ip` = '".$_SERVER['REMOTE_ADDR']."'  		
							  WHERE `subid` = ".$user->subid;
                    $db->setQuery($query);
                    $db->execute();
                }

                PlLibHelperCpa::processConfirmation($user);
            }

        }

        $landId = $jinput->get('id', false);
        $clickId = $jinput->get($this->params->get('click_id_var'), false, 'STRING');
        $pid = $jinput->get($this->params->get('pid_var'), false, 'INT');

        if ( $clickId && $pid && $option == 'com_sppagebuilder' && !empty($landId) ) {
            $currentSession->set('penguinLandId', $landId);
        }

        if ($option == 'com_ajax' && $cf = $jinput->getVar('cf')) {
			
			if (!empty($cf['form_id'])) {
				PlLibHelperCForms::processForm($cf);
			}
			
			if (isset($cf['order_process']) && intval($cf['order_process']) == 1 && empty($cf['things'])) {
                $cart = json_decode($currentSession->get('plCart', json_encode(array())), true);

                if (!empty($cart['items'])) {
                    $items = array();
                    foreach($cart['items'] as $item) {
                        $items[] = $item['name'].' *'.$item['code'].'* Цена: '.$item['price'].' грн. '.$item['quantity'].' шт. ';
                    }
                    $currentSession->set('penguinThings', json_encode($items));
                    $currentSession->clear('plCart');
                }
            }
            if (!empty($cf['customer_comment'])) {
                $currentSession->set('penguinCustomerComment', $cf['customer_comment']);
                $this->customerComment = $cf['customer_comment'];
            }
            if (!empty($cf['things'])) {
                if (is_array($cf['things'])) {
                    $currentSession->set('penguinThings', json_encode($cf['things']));
                } else {
                    $currentSession->set('penguinThings', json_encode([$cf['things']]));
                }
            }
            if (!empty($cf['task'])) {
                $currentSession->set('penguinTask', $cf['task']);
            }
        }


        $cpaClickId = $jinput->get('subid', false, 'STRING');
        $cpaClickType = $jinput->get('utm_source', false, 'STRING');

        if ($cpaClickId && $cpaClickType) {
            $currentSession->set('cpaClickId', $cpaClickId);
            $currentSession->set('cpaClickType', $cpaClickType);
        }


        if ($clickId && $pid) {
            $currentSession->set('penguinClickId', $clickId);
            $currentSession->set('penguinPid', $pid);
        }

        $inputCookie  = JFactory::getApplication()->input->cookie;
        $subId        = $inputCookie->get($name = 'subId', $defaultValue = false);

        if (empty($subId)) return true;

        $db = JFactory::getDbo();

        $query = "SELECT `name`,`email`,`phone` FROM #__acymailing_subscriber WHERE `subid` = ".$subId.' LIMIT 1';

        $db->setQuery($query);

        $userInfo = $db->loadObject();

        if (empty($userInfo)) return true;

        JFactory::getDocument()->addScriptDeclaration('
            var kName = "'.$userInfo->name.'";
            var kPhone = "'.$userInfo->phone.'";
            var kEmail = "'.$userInfo->email.'";
        ');
    }

    private function logging()
    {
        if (!$this->logLine) return;

        $fp = fopen(JPATH_ROOT.'/logs/log_affise.log', 'a+');

        fwrite($fp, $this->logLine."\n");
    }

    private function sendAffiseConfirmation($subscriber, $status, $goal = 2)
    {
        $apiUrl = $this->apiUrl.'?clickid='.$subscriber->click_id
            .'&action_id='.(($subscriber->action_id)?$subscriber->action_id:'pl'.$subscriber->id)
            .'&status='.$status
            .'&custom_field1='.$subscriber->name
            ."&custom_field2=".$subscriber->email;

        //if (!empty($customFieldStatus)) $apiUrl .= "&custom_field7=".$customFieldStatus.'&goal=1';
        if (!empty($customFieldStatus)) $apiUrl .= "&goal=".$goal;

        if (!empty($subscriber->phone)) $apiUrl .= "&custom_field3=".$subscriber->phone;
        if ($subscriber->things) $apiUrl .= "&custom_field4=".$subscriber->things;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: multipart/form-data',
            'Api-Key: ' . $this->apiKey
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($ch);

        $this->logLine = date('Y-m-d H:i:s').' "UserId:'.$subscriber->user_id.'" "SendToAffise:'.$apiUrl.'" "AffiseResponse:'.$resp.'"';

        if ($this->logging) {
            $this->logging($resp);
        }

        if ($subscriber->pid == 4256) {
            $refId = $this->getRefId($subscriber->click_id);
            if ($refId) $this->sendToMobytize($refId);
        }

        $currentSession = JFactory::getSession();
        $currentSession->clear('penguinClickId');
        $currentSession->clear('penguinPid');
        $currentSession->clear('penguinThings');
        $currentSession->clear('penguinTask');

        curl_close($ch);
    }

    private function sendSms($subscriber, $smsId)
    {
        $finalQuery = 'INSERT IGNORE INTO `#__acysms_queue` (`queue_message_id`,`queue_receiver_id`,`queue_receiver_table`,`queue_senddate`,`queue_try`,`queue_priority`, `queue_paramqueue`) 
							VALUES ("'.$smsId.'","'.$subscriber->subid.'","acymailing",'.(time()+100).',"1","1","")';

        $db = JFactory::getDbo();
        $db->setQuery($finalQuery);
        $db->execute();
        return true;
    }

    private function processOrder($subscriber)
    {

        PlLibHelperOrders::getInstance()->addCustomer(
                $subscriber->subid,
                $subscriber->name,
                $subscriber->email,
                $subscriber->phone
        );

        $orderId = $this->insertOrder($subscriber);
		
        $currentSession = JFactory::getSession();
        $currentSession->set('plOrderId', $orderId);

        $userInfo = $this->findAffiseUser($subscriber->subid);

        if ($userInfo) $this->sendAffiseConfirmation($userInfo, 1);

        $this->changeAffiseUserStatus($userInfo->id, 1);

        if ($orderId) $this->sendSms($subscriber, 9);

        $currentSession = JFactory::getSession();
        $currentSession->clear('penguinClickId');
        $currentSession->clear('penguinPid');
        $currentSession->clear('penguinThings');
        $currentSession->clear('penguinTask');

        return true;
    }

    private function checkJoomlaUser($subscriber)
    {
        jimport('joomla.user.helper');
        $userId = JUserHelper::getUserId($subscriber->email);

        if (!$userId) {
            $pwd = bin2hex(openssl_random_pseudo_bytes(4));
            $udata = array(
                "name"=>$subscriber->name,
                "username"=>$subscriber->email,
                "password"=>$pwd,
                "password2"=>$pwd,
                "email"=>$subscriber->email,
                "block"=>0,
                "groups"=>array("1","2")
            );
            $user = new JUser;

            //Write to database
            if(!$user->bind($udata)) {
                throw new Exception("Could not bind data. Error: " . $user->getError());
            }
            if (!$user->save()) {
                throw new Exception("Could not save user. Error: " . $user->getError());
            }
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $columns = array('user_id', 'psw');

        $values = array(
            $subscriber->subid,
            $db->qoute($pwd)
        );

        $query
            ->insert($db->quoteName('#__plprofile_psws'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();


    }

    private function insertOrder($subscriber)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $utmTags = array(
            'utm_source' => '',
            'utm_medium' => '',
            'utm_campaign' => '',
            'utm_term' => '',
            'utm_content' => '',
        );


        $columns = array(
            'date',
            'customer_id',
            'product_id',
            'product_name',
            'address_delivery',
            'click_id',
            'order_url',
            'customer_comment'
        );

        if ($this->things) {
            $things = implode(',',json_decode($this->things));
        } else {
            $things = '';
        }

        $currentSession = JFactory::getSession();
        $welcomeBonus = json_decode($currentSession->get('welcomeBonus', false));
        $welcomeBonus2 = json_decode($currentSession->get('welcomeBonus2', false));

        if ($welcomeBonus) {
            $things = $things.' Бонус '.$welcomeBonus->text.' грн.';
        }

		if ($welcomeBonus2) {
            $things = $things.' Бонус '.$welcomeBonus2->text;
        }
		
		$mix = JFactory::getApplication()->input->cookie->getArray();
        foreach($mix as $key=>$el) {
            preg_match('/(plprmp_+)(\d)+/', $key, $match);
            if (!empty($match[2])) $things .= ' Бонус: '.$this->getBonusText($el);
        }

		$utmTags = json_decode($currentSession->get('utmTags', $utmTags));

        $ref = $_SERVER['HTTP_REFERER'];

        if (strpos($ref, 'utm_') === false) {
            foreach($utmTags as $key=>$tag) {

                if (empty($tag)) continue;

                if (strpos($ref, '?' )) {
                    $ref .= '&'.$key.'='.$tag;
                } else {
                    $ref .= '?'.$key.'='.$tag;
                }
            }
        }

        $values = array(
            $db->quote(date('Y-m-d H:i:s')),
            $subscriber->subid,
            $db->quote(''),
            $db->quote($things),
            $db->quote(''),
            $db->quote($this->clickId),
            $db->quote($ref),
            $db->quote($this->customerComment),
        );

        $query
            ->insert($db->quoteName('#__prizolove_orders'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();

        $orderId = $db->insertid();

        if ($this->enableCrm) {
            $this->sendOrderToCrm($subscriber, $orderId, $ref, $welcomeBonus, $welcomeBonus2);
        }


        return $orderId;
    }
	
	private function sendOrderToCrm($subscriber, $orderId, $ref, $welcomeBonus = '', $welcomeBonus2 = '')
    {

        if ($this->things) {
            $things = implode(',',json_decode($this->things));
        } else {
            $things = '';
        }

        $order = new \StdClass();

        $order->id = (string) $orderId;
        $order->name = $subscriber->name;
        $order->phone = (string) $subscriber->phone;
        $order->email = $subscriber->email;
        $order->url = (string) $ref;
        $order->orderId = (string) $orderId;
        $order->paymentType = 'Наложенный платеж';
        $order->login = '';
        $order->password = '';

	$uri = parse_url($ref, PHP_URL_PATH);
	if ($uri[0] == '/') $uri = substr($uri, 1);

	$order->landId = $uri;

        $currentSession = JFactory::getSession();

        $utmTags = array(
            'utm_source' => '',
            'utm_medium' => '',
            'utm_campaign' => '',
            'utm_term' => '',
            'utm_content' => '',
        );

        $utmTags = json_decode($currentSession->get('utmTags', json_encode($utmTags)));
		
        $order->utm = new \StdClass();

        $order->utm = $utmTags;

        $regex = '/\*(\d*?)\*/i';

        $db = JFactory::getDbo();

        if (is_array($things)) {
            $productIds = array();

            foreach($things as $thing) {
                preg_match($regex, $thing, $match);
                if (!empty($match[1])) {
                    $productIds[] = $match[1];
                    $productNames[$match[1]] = $thing;
                }

            }

            $query = $db->getQuery(true);

            $query->select('* FROM #__prizolove_products')
                ->where('id_1c IN ('.implode(',',$productIds).')');

            $db->setQuery($query);

            $products = $db->loadResult();

            $items = array();

            foreach($products as $product) {
                if (!empty($productNames[$products->id_1c])) {
                    $newProduct = new \StdClass();
                    $newProduct->id = $product->id_1c;
                    $newProduct->name = trim($productNames[$products->id_1c].' '.$welcomeBonus);
                    $newProduct->price = $product->price;
                    $newProduct->status = ($product->quantity)?1:0;
                    $newProduct->quantity = 1;
                    $newProduct->store = $product->quantity;
                    $items[] = $newProduct;
                }
            }
        } else {
            preg_match($regex, $things, $match);
            if (!empty($match[1])) {
                $productId = $match[1];
            } else {
                return;
            }

            $query = $db->getQuery(true);

            $query->select('* FROM #__prizolove_products p')
                ->where('p.id_1c = '.$db->quote($productId));

            $db->setQuery($query);

            $product = $db->loadObject();

            $items = array();

            $newProduct = new \StdClass();
            $newProduct->id = $productId;
            $newProduct->name = trim($things . ' ' . $welcomeBonus);
            $newProduct->price = $product->price;
            $newProduct->status = ($product->quantity) ? 1 : 0;
            $newProduct->quantity = 1;
            $newProduct->store = $product->quantity;
            $items[] = $newProduct;
        }

        $order->items = new \StdClass();

        $order->items = $items;

        $dataString = json_encode($order);

        $ch = curl_init($this->crmUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($dataString))
        );

        $result = curl_exec($ch);

        $log = date('Y-m-d H:i:s').' "'.json_encode($order, JSON_UNESCAPED_UNICODE).'" Result:"'.$result.'""';

        $fp = fopen(JPATH_ROOT.'/logs/orders.log', 'a+');

        fwrite($fp, $log."\n");

        //file_put_contents('order.log', json_encode($order, JSON_UNESCAPED_UNICODE), FILE_APPEND);

    }


    public function onRenderModule(&$module, &$attribs)
    {
        if ($module->module != 'mod_custom') return true;

        $regex = '/{username}/i';

        preg_match($regex, $module->content, $match);

        if ($match) {
            $this->_username($module, $match);
        }

        $regex = '/{welcomeBonus}/i';

        preg_match($regex, $module->content, $match);

        if ($match) {
            $this->_welcomeBonus($module, $match);
        }

        $regex = '/{welcomeBonus2}/i';

        preg_match($regex, $module->content, $match);

        if ($match) {
            $this->_welcomeBonus2($module, $match);
        }

        $regex = '/{attempts(.*?)}/i';

        preg_match($regex, $module->content, $match);


        if ($match) {
            $this->_attempts($module, $match);
        }          

        $regex = '/{_plprm_(.*?)}/i';

        preg_match($regex, $module->content, $match);

        if ($match) {
            $this->_plprm($module, $match);
        }

        return true;
    }

    private function _plprm($module, $match)
    {
        $db = JFactory::getDbo();

        $query = "SELECT `name` FROM #__prizolove_promo_items WHERE `id` = ".intval($match[1]).' LIMIT 1';

        $db->setQuery($query);

        $promoItem = $db->loadObject();

        $text = 'Ваш бонус: '.$promoItem->name.' будет вместе с заказом.';

        $module->content = preg_replace("|$match[0]|",$text , $module->content, 1);

        return true;

    }

    private function _welcomeBonus($module, $match)
    {
        $currentSession = JFactory::getSession();
        $welcomeBonus = json_decode($currentSession->get('welcomeBonus', false));
        $text = '';
        if ($welcomeBonus) {
            $text = '<strong>Ваш бонус на покупку - <span style="color: #f94e0b;">'.$welcomeBonus->text.'</span> грн. Отнимайте его от любой цены.</strong>';
        }

        $module->content = preg_replace("|$match[0]|",$text , $module->content, 1);
        return true;
    }

    private function _welcomeBonus2($module, $match)
    {
        $currentSession = JFactory::getSession();
        $welcomeBonus2 = json_decode($currentSession->get('welcomeBonus2', false));
        $text = '';
        if ($welcomeBonus2) {
            $text = '<strong>В заказе будет Ваш бонус: <span class="prize">'.$welcomeBonus2->text.'</span>.</strong>';
        }

        $module->content = preg_replace("|$match[0]|",$text , $module->content, 1);

        return true;
    }	

    private function _attempts($module, $match)
    {
        $inputCookie  = JFactory::getApplication()->input->cookie;
        $attempts        = $inputCookie->get($name = 'attempts'.$match[1], $defaultValue = 3);
        $module->content = preg_replace("|$match[0]|",$this->_pluralAttempts($attempts), $module->content, 1);
        return true;
    }

    private function _username($module, $match)
    {
        $greetingText = '';
        $inputCookie  = JFactory::getApplication()->input->cookie;
        $subId        = $inputCookie->get($name = 'subId', $defaultValue = false);

        if (!$subId) {
            if ($match) {
                $module->content = preg_replace("|$match[0]|", $greetingText, $module->content, 1);
            }
            return true;
        }

        $db = JFactory::getDbo();

        $query = "SELECT `name`,`email`,`phone` FROM #__acymailing_subscriber WHERE `subid` = ".$subId.' LIMIT 1';

        $db->setQuery($query);

        $userInfo = $db->loadObject();
        $greetingText = sprintf($this->greetingText, $userInfo->name);
        //$greetingText = $this->greetingText.$userInfo->name;

        $regex = '/{username}/i';

        preg_match($regex, $module->content, $match);

        if ($match) {
            $module->content = preg_replace("|$match[0]|", $greetingText, $module->content, 1);
        }

        return true;
    }

    private function _pluralAttempts($number)
    {
        $after = array('бесплатная попытка','бесплатные попытки','бесплатных попыток');
        $cases = array(2, 0, 1, 1, 1, 2);
        return $number.' '.$after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
    }

    private function findAffiseUser($subid, $status = 2)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select($db->quoteName(array('a.user_id','a.things', 'a.click_id', 'a.pid', 'a.action_id', 'a.id', 'a.status','s.phone','s.name', 's.email')));
        $query->from($db->quoteName('#__affise','a'));
        $query->join('INNER', $db->quoteName('#__acymailing_subscriber', 's') . ' ON (' . $db->quoteName('a.user_id') . ' = ' . $db->quoteName('s.subid') . ')');

        $query->where($db->quoteName('a.user_id') . ' = '. $subid.' AND '.$db->quoteName('a.status'). ' = '.$status);

        $query->limit(1);

        $db->setQuery($query);

        $userInfo = $db->loadObject();

        return $userInfo;
    }

    private function changeAffiseUserStatus($id, $status)
    {
        $db = JFactory::getDbo();

        $query = "UPDATE #__affise SET `status` = ".intval($status)." WHERE `id` = ".intval($id);
        $db->setQuery($query);
        $db->execute();
    }
	
	public function onConvertFormsResponseValue(&$value, $formId)
    {
        if ($formId != 324) return;

       
       $currentSession = JFactory::getSession();       
       $orderId = $currentSession->get('plOrderId', '');
       $currentSession->clear('plOrderId');

       $value = str_replace('{orderId}', '<p>Номер Вашего заказа: '.$orderId.'</p>', $value);


    }
	
	private function getBonusText($id)
    {
        $db = JFactory::getDbo();

        $query = "SELECT name FROM #__prizolove_promo_items WHERE `id` = ".intval($id);

        $db->setQuery($query);

        $result =  $db->loadObject();

        if ($result && !empty($result->name)) {
            return $result->name;
        }

        return;
    }
/*    function onUserLogin($user, $options)
    {
        $registry = JFactory::getSession()->get('registry');
        $returnLink = '';
        file_put_contents('user.log',print_r(JFactory::getSession(), true), FILE_APPEND);
        if (is_object($registry)) {
            $users = $registry->get('users');
            $returnLink = $users->login->form->data['return'];
        }

        file_put_contents('user.log',print_r($options, true), FILE_APPEND);
        return true;
    } */

}
