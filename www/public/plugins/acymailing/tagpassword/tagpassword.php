<?php

defined('_JEXEC') or die('Restricted access');
?><?php

class plgAcymailingTagpassword extends JPlugin{

	function __construct(&$subject, $config){		
		parent::__construct($subject, $config);
		if(!isset($this->params)){
			$plugin = JPluginHelper::getPlugin('acymailing', 'tagpassword');
			$this->params = new acyParameter($plugin->params);
		}
	}


	function acymailing_getPluginType(){		
		$onePlugin = new stdClass();
		$onePlugin->name = acymailing_translation('ACY_PASSWORD');
		$onePlugin->function = 'acymailingtagpassword_show';
		$onePlugin->help = 'plugin-tagpassword';

		return $onePlugin;
	}

	function acymailingtagpassword_show(){

		$text = '<br style="clear:both;"/><div class="onelineblockoptions"><table class="acymailing_table" cellpadding="1">';

		$others = array();
		$others['{password}'] = 'password';
		$others['{email}'] = 'email';


		$k = 0;
		foreach($others as $tagname => $tag){
			$text .= '<tr style="cursor:pointer" class="row'.$k.'" onclick="setTag(\''.$tagname.'\');insertTag();" ><td class="acytdcheckbox"></td><td>'.$tag.'</td><td>test</td></tr>';
			$k = 1 - $k;
		}

		$text .= '</table></div>';

		echo $text;
	}

    function acymailing_replaceusertags(&$email, &$user, $send = true)
	{		
		$match = '#{password:?([^:].*)?}#Ui';
		$variables = array('subject', 'body', 'altbody');

		foreach($variables as $var){
			$email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
		}
		$email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



		$found = false;
		foreach($variables as $var){
			if(empty($email->$var)) continue;
			$found = preg_match_all($match, $email->$var, $results[$var]) || $found;
			if(empty($results[$var][0])) unset($results[$var]);
		}		

		if(!$found) return;
		
        
        
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select($db->quoteName('psw'))
            ->from($db->quoteName('#__plprofile_psws'))
            ->where($db->quoteName('user_id') . ' = ' . $db->quote($user->subid));

        $db->setQuery($query, 0, 1);

        $psw = $db->loadResult();
			

        if (!$psw) return;

		$tags = array();
		foreach($results as $var => $allresults){
			foreach($allresults[0] as $i => $oneTag){

				if(isset($tags[$oneTag])) continue;

				$tags[$oneTag] = $psw;

			}
		}

		foreach(array_keys($results) as $var){
			$email->$var = str_replace(array_keys($tags), $tags, $email->$var);
		}
	}
}//endclass
