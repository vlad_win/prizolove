<?php

defined('_JEXEC') or die('Restricted access');

JLoader::import('pl_lib.library');

class plgAcymailingTaginvestors extends JPlugin{

	function __construct(&$subject, $config){		
		parent::__construct($subject, $config);
		if(!isset($this->params)){
			$plugin = JPluginHelper::getPlugin('acymailing', 'taginvestors');
			$this->params = new acyParameter($plugin->params);
		}
	}


	function acymailing_getPluginType(){		
		$onePlugin = new stdClass();
		$onePlugin->name = acymailing_translation('ACY_INVESTORS');
		$onePlugin->function = 'acymailingtaginverstors_show';
		$onePlugin->help = 'plugin-taginvestors';

		return $onePlugin;
	}

	function acymailingtagpassword_show(){

		$text = '<br style="clear:both;"/><div class="onelineblockoptions"><table class="acymailing_table" cellpadding="1">';

		$others = array();
		$others['{partner_name}'] = 'partner_name';
		$others['{needed_partners}'] = 'needed_partners';
		$others['{total_partners}'] = 'total_partners';
		$others['{expected_income}'] = 'expected_income';
		$others['{lead_name}'] = 'lead_name';
		$others['{product_name}'] = 'product_name';


		$k = 0;
		foreach($others as $tagname => $tag){
			$text .= '<tr style="cursor:pointer" class="row'.$k.'" onclick="setTag(\''.$tagname.'\');insertTag();" ><td class="acytdcheckbox"></td><td>'.$tag.'</td><td>test</td></tr>';
			$k = 1 - $k;
		}

		$text .= '</table></div>';

		echo $text;
	}

    function acymailing_replaceusertags(&$email, &$user, $send = true)
	{
	    $paramqueue = json_decode($user->paramqueue, true);

        $match = '#{business_list:?([^:].*)?}#Ui';

        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {


            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {
                    if (isset($tags[$oneTag])) continue;
                    $tags[$oneTag] = PlLibHelperAffiliate::getActiveBusinessProducts($user->userid);
                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{customvar1:?([^:].*)?}#Ui';

        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {


            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {
                    if (isset($tags[$oneTag])) continue;
                    $tags[$oneTag] = $paramqueue['customvar1'];
                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{customvar2:?([^:].*)?}#Ui';

        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {


            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {
                    if (isset($tags[$oneTag])) continue;
                    $tags[$oneTag] = $paramqueue['customvar2'];
                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }


        $match = '#{bonusAccount:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {

            $bonusAccount = PlLibHelperUsers::getInstance()->getBonusAccount($user->userid);


            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {
                    if (isset($tags[$oneTag])) continue;
                    $tags[$oneTag] = $bonusAccount->value;
                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{leads2_new:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {

            if (!isset($accountName)) $accountName = PlLibHelperDemo::getInstance()->getAccountInfo($user->userid);
            $newLeads = 0;
            foreach($accountName->users[2] as $lead) {
                if ($lead['date'] == date('Y-m-d')) $newLeads++;
            }

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {
                    if (isset($tags[$oneTag])) continue;
                    $tags[$oneTag] = $newLeads;
                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{total_leads:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {
            if (!isset($parentId)) $parentId = PlLibHelperAffiliate::getParent($user->userid);
            $totalLevelOne = PlLibHelperAffiliate::getLevelOneTotal($parentId, $user->userid);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {
                    if (isset($tags[$oneTag])) continue;
                    $tags[$oneTag] = $totalLevelOne;
                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{total2_leads:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {
            if (!isset($parentId)) $parentId = PlLibHelperAffiliate::getParent($user->userid);
            $totalLevelTwo = PlLibHelperAffiliate::getLevelTwoLeads($user->userid, $user->userid);


            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {
                    if (isset($tags[$oneTag])) continue;
                    $tags[$oneTag] = $totalLevelTwo;
                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{needed_leads:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }

        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            if (!isset($parentId)) $parentId = PlLibHelperAffiliate::getParent($user->userid);
            $totalLevelOne = PlLibHelperAffiliate::getLevelOneTotalSystem($parentId, $user->userid);
            $totalLeads = $this->getAccountTotalLeads($user);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = (($totalLeads - $totalLevelOne)>0)?($totalLeads - $totalLevelOne):0;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }


        $match = '#{renew_link:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {

            $total = PlLibHelperAffiliate::getTotalComission($user);

            if ($total > 5) {
                $text = 'Продлите действие <a href="https://prizolove.com/orderpage?pid=254">PROBA</a> на 6 месяцев за 12$. ';
            } else {
                $text = 'Продлите действие <a href="https://prizolove.com/orderpage?pid=324">PROBA</a> на 2 месяця за 6$. ';
            }

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $text;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{comission:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {

            $total = PlLibHelperAffiliate::getTotalComission($user);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $total;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{lead_num:?([^:].*)?}#Ui';

        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {

            $num = PlLibHelperAffiliate::getLevelOneNewLeads($user->userid);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $num;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

		$match = '#{partner_name:?([^:].*)?}#Ui';
		$variables = array('subject', 'body', 'altbody');

		foreach($variables as $var){
			$email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
		}
		$email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



		$found = false;
		foreach($variables as $var){
			if(empty($email->$var)) continue;
			$found = preg_match_all($match, $email->$var, $results[$var]) || $found;
			if(empty($results[$var][0])) unset($results[$var]);
		}		

		if($found) {

            $name = $this->getPartnerName($user);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $name;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{lead2_name:?([^:].*)?}#Ui';

        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {

            $name = PlLibHelperAffiliate::getLevelTwoSale($user->userid);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $name['name'];

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }


        $match = '#{partner2_new:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if($found) {

            $num = PlLibHelperAffiliate::getLevelTwoNewLeads($user->userid);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $num;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }
        }

        $match = '#{total_partners:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $num = PlLibHelperAffiliate::getLevelOneLeads($user->userid);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $num;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{total2_partners:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $level2num = PlLibHelperAffiliate::getLevelTwoLeads($user->userid);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $level2num;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{needed_partners:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $num = $this->getAddedPartners($user);
            $totalLeads = $this->getAccountTotalLeads($user);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = (($totalLeads - $num)>0)?($totalLeads - $num):0;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }
		
		
		$match = '#{expected_income:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            if (!isset($parentId)) $parentId = PlLibHelperAffiliate::getParent($user->userid);
            $num = PlLibHelperAffiliate::getLevelOneTotal($parentId, $user->userid);
            $level2expected = 0;
            if (empty($level2num)) {
                $level2num = PlLibHelperAffiliate::getLevelTwoLeads($user->userid);
            }
            $level2expected = $level2num * 2 * (15 * 0.05);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = ($num * 2 * 3) + $level2expected;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }
        $match = '#{expected2_income:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $num = PlLibHelperAffiliate::getTotalComission($user);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = ($num * 12 * 2)/1.5;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{lead_name:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $lastLead = PlLibHelperAffiliate::getLastLevelOneLeads($user->userid);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $lastLead['name'];

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{productnew_name:?([^:].*)?}#Ui';

        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {

            $lastLeads = PlLibHelperAffiliate::getLastLevelOneLeads($user->userid);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $lastLeads['product_name'];

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{leadnew_name:?([^:].*)?}#Ui';

        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);

        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {

            $lastLeads = PlLibHelperAffiliate::getLastLevelOneLeads($user->userid);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $lastLeads['name'];

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{product_name:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $lastLead = $this->getLastLead($user);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $lastLead->product_name;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{product2_name:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $lastLead = PlLibHelperAffiliate::getLevelTwoSale($user->userid);
            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = $lastLead['product_name'];

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }

        $match = '#{referal:?([^:].*)?}#Ui';
        $variables = array('subject', 'body', 'altbody');

        foreach($variables as $var){
            $email->$var = str_replace(array('{mailid}', '%7Bmailid%7D', '{emailsubject}'), array($email->mailid, $email->mailid, $email->subject), $email->$var);
        }
        $email->body = str_replace('{textversion}', nl2br($email->altbody), $email->body);



        $found = false;
        foreach($variables as $var){
            if(empty($email->$var)) continue;
            $found = preg_match_all($match, $email->$var, $results[$var]) || $found;
            if(empty($results[$var][0])) unset($results[$var]);
        }

        if ($found) {
            $parentId = PlLibHelperAffiliate::getParent($user->userid);

            $tags = array();
            foreach ($results as $var => $allresults) {
                foreach ($allresults[0] as $i => $oneTag) {

                    if (isset($tags[$oneTag])) continue;

                    $tags[$oneTag] = 'atid='.$parentId;

                }
            }

            foreach(array_keys($results) as $var){
                $email->$var = str_replace(array_keys($tags), $tags, $email->$var);
            }

        }


	}

    function getAccountTotalLeads($user)
    {
        $db = JFactory::getDbo();
        $sql = '
                SELECT a.account_type
                FROM #__affiliate_tracker_accounts a                
                WHERE a.user_id = '.$user->userid;

        $db->setQuery($sql);

        $result = $db->loadResult();

        switch($result) {
            case 'proba_free':
            case 'proba':
            case 'proba250':
                $totalLeads = 83;
                break;
            case 'start':
                $totalLeads = 100;
                break;
            case 'business':
                $totalLeads = 110;
                break;
            case 'investor':
                $totalLeads = 833;
                break;
            default:
                $totalLeads = 83;
                break;
        }

        return $totalLeads;


    }




	function getPartnerName($user)
    {
        $dateMin = date('Y-m-d').' 00:00:00';
        $dateMax = date('Y-m-d').' 23:59:59';
        $db = JFactory::getDbo();
        $sql = '
                SELECT COUNT(l.id) leads
                FROM #__affiliate_tracker_accounts a 
                INNER JOIN #__affiliate_tracker_logs l ON l.atid = a.id  
                INNER JOIN #__users u ON u.id = l.user_id
                INNER JOIN #__acymailing_subscriber s ON s.email = u.email
                WHERE a.user_id = '.$user->userid.' AND (l.datetime >= "'.$dateMin.'" AND l.datetime <= "'.$dateMax.'")
                ';

        $db->setQuery($sql);
        $result = $db->loadResult();

        return $result;


    }

    function getTotalPartners($user)
    {
        $db = JFactory::getDbo();
        $sql = '
                SELECT COUNT(l.id)
				FROM #__affiliate_tracker_accounts a
				LEFT JOIN #__affiliate_tracker_logs l ON (l.account_id = a.id AND l.user_id > 0 AND l.user_id != a.user_id)
				WHERE a.user_id = '.$user->userid.' 
				GROUP BY a.id
			';
				
				

        $db->setQuery($sql);

        return $db->loadResult();
    }
	
	function getAddedPartners($user)
    {
        $db = JFactory::getDbo();
        $sql = '
                SELECT COUNT(l.account_id) 
                FROM #__affiliate_tracker_logs l 
                INNER JOIN #__affiliate_tracker_accounts a ON a.id = l.account_id
                WHERE a.user_id = '.$user->userid.' AND l.sessionid = "" AND l.user_id > 0 AND l.user_id != a.user_id
                GROUP BY l.account_id                
                ';

        $db->setQuery($sql);

        return $db->loadResult();
    }

    function getLastLead($user)
    {
        $dateMin = date('Y-m-d').' 00:00:00';
        $dateMax = date('Y-m-d').' 23:59:59';

        $db = JFactory::getDbo();
        $sql = '
                SELECT u.name, c.name product_name
                FROM #__affiliate_tracker_conversions c 
                INNER JOIN #__affiliate_tracker_accounts a ON a.id = c.atid
                INNER JOIN #__users u ON u.id = c.user_id
                WHERE a.user_id = '.$user->userid.' AND (c.date_created >= "'.$dateMin.'" AND c.date_created <= "'.$dateMax.'") AND c.component IN ("com_virtuemart","com_hikashop")
                ORDER BY c.date_created DESC       
                LIMIT 1
                ';

        $db->setQuery($sql);

        return $db->loadObject();
    }

}//endclass
