<?php

$_SERVER['HTTP_HOST']  = 'prizolove.com';
$_SERVER['SCRIPT_NAME']  = '';
$_SERVER['SCRIPT_FILENAME'] = '';
$_SERVER['PATH_TRANSLATED'] = '';

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
 
if (file_exists(dirname(__FILE__) . '/defines.php')) {
 include_once dirname(__FILE__) . '/defines.php';
}
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', dirname(__FILE__));
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');


$row = 0;

$db = JFactory::getDBO();

if (($handle = fopen("test.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {		
		$row++;
		if ($row == 1) continue;		
		$categoryId = checkCategory($data[0], $db);
		$productId = checkProduct($data[6],$db);
		if (!$productId) {
			$newProduct = new StdClass();
			$newProduct->name = $data[2];
			$newProduct->price_usd = 1.55 * floatval(str_replace('$','',$data[7]));
			$newProduct->original_url = $data[6];
			$newProduct->status  = 1;
			$db->insertObject('#__prizolove_products', $newProduct);
			$productId = $db->insertID();
			$productCategory = new StdClass();
			$productCategory->product_id = $productId;
			$productCategory->category_id = $categoryId;
			$db->insertObject('#__prizolove_product_categories_products', $productCategory);			
		} else {
			mkdir('/var/www/preview/images/products/'.$productId, 0777);
			
			$path = parse_url($data[4]);
			
			$dateFile = explode('/',$path['path']);
			$filename = end($dateFile);
			
			file_put_contents('/var/www/preview/images/products/'.$productId.'/'.$filename, file_get_contents($data[4]));			
			$db->setQuery('UPDATE #__prizolove_products SET image = '.$db->Quote('images/products/'.$productId.'/'.$filename).' WHERE id = '.$productId);
			$db->execute();
//			$db->setQuery('UPDATE #__prizolove_products SET price_usd = '.$db->Quote((1.55 * floatval(str_replace('$','',$data[7])))).', status = 1, created = '.$db->Quote(date('Y-m-d H:i:s')).', modified= '.$db->Quote(date('Y-m-d H:i:s')).' WHERE id = '.$productId);
//			$db->execute();
		}
	}
}

function checkCategory($category, $db) {
	$db->setQuery('SELECT id FROM #__prizolove_product_categories WHERE `name` LIKE "%'.$category.'%"');
	$category = $db->loadResult();
	if (!$category) {
		$newCategory = new StdClass();
		$newCategory->name = $category;
		$newCategory->state = 1;
		$db->insertObject('#__prizolove_product_categories', $newCategory);
		$category = $db->insertID();		
	}
	return $category;
}

function checkProduct($url, $db) {
	$db->setQuery('SELECT id FROM #__prizolove_products WHERE original_url LIKE "%'.$url.'%"');
	return $db->loadResult();
}




?>
