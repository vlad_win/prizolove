<?php
$_SERVER['HTTP_HOST']  = 'prizolove.com`';

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
    include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__FILE__));
    require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');

$db = JFactory::getDBO();

JLoader::import('pl_lib.library');
//JPluginHelper::importPlugin( 'system' );


$user = JFactory::getUser();        // Get the user object
$app  = JFactory::getApplication(); // Get the application

if ($user->id == 0)
{
    die();
}




$steps = [
    "2" => [
        'leads' => 0,
        'sales' => 0,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 1,
        'salesplus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "3" => [
        'leads' => 1,
        'sales' => 0,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 2,
        'salesplus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316]
    ],
    "4" => [
        'leads' => 3,
        'sales' => 0,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 1,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316,311]
    ],
    "6" => [
        'leads' => 3,
        'sales' => 1,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 1,
        'salesplus' => 0,
        'datediff' => 24*60*60*2,
        'datediffsign' => '>',
        'messages' => [316,311,320]
    ],
    "7" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316,311,320,317]
    ],
    "8" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 1,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 3,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316,311,320,317,315]
    ],
    "9" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 4,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 1,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316,311,320,317,315,318]
    ],
    "10" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 5,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316,311,320,317,315,318,312,319]
    ],
    "11" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 6,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 3,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316,311,320,317,315,318,312,319,328]
    ],
    "12" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 9,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [316,311,320,317,315,318,312,319,328,330,313]
    ],
    "13" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 10,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 0,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],


];


$sql = '
		SELECT a.id, a.user_id userid, l.leads, ls.leads_system, s.sales, l.datetime lead_date, s.date_created sale_date, l2.leads2, s2.sales2, s2.date2_created, u.email, s.subid, u.registerDate, u.lastVisitDate, s.name, s.phone, pc.packets
		FROM #__affiliate_tracker_accounts a
		INNER JOIN #__users u ON a.user_id = u.id
		INNER JOIN #__acymailing_subscriber s ON s.email = u.email
		LEFT JOIN (
			SELECT COUNT(logs.id) leads, max(logs.datetime) datetime, ac0.id
			FROM #__affiliate_tracker_accounts ac0 
			LEFT JOIN #__affiliate_tracker_logs logs ON (logs.account_id = ac0.id AND logs.user_id > 0 AND logs.user_id != ac0.user_id) 						
			GROUP BY ac0.id
			ORDER BY logs.datetime DESC
		) l ON l.id = a.id
		LEFT JOIN (
			SELECT  GROUP_CONCAT(p.name SEPARATOR ",") packets, o.customer_id subid 
			FROM #__prizolove_orders o
			INNER JOIN #__prizolove_order_items oi ON (oi.order_id = o.order_id AND oi.product_id IN (254, 256,257,258)) 			
			INNER JOIN #__prizolove_products p ON (p.id = oi.product_id)
			WHERE o.sent IN (5,9)
			GROUP BY o.customer_id			
		) pc ON pc.subid = s.subid
		LEFT JOIN (
			SELECT COUNT(logs0.id) leads_system, acs.id
			FROM #__affiliate_tracker_accounts acs
			LEFT JOIN #__affiliate_tracker_logs logs0 ON (logs0.account_id = acs.id AND logs0.user_id > 0 AND logs0.user_id != acs.user_id AND logs0.sessionid = "") 						
			GROUP BY acs.id			
		) ls ON ls.id = a.id
		LEFT JOIN (
			SELECT COUNT(sales.id) sales, max(sales.date_created) date_created, ac1.id
			FROM #__affiliate_tracker_accounts ac1 
			LEFT JOIN #__affiliate_tracker_conversions sales ON (sales.atid = ac1.id AND sales.component = "com_virtuemart")
			GROUP BY ac1.id
			ORDER BY sales.date_created DESC
		) s ON s.id = a.id
		LEFT JOIN (			
			SELECT COUNT(logs1.id) leads2, logs1.datetime lead2_date, ac1.parent_id
			FROM #__affiliate_tracker_accounts ac1			
			LEFT JOIN #__affiliate_tracker_logs logs1 ON (logs1.account_id = ac1.id AND logs1.user_id > 0 AND logs1.user_id != ac1.user_id)			
			GROUP BY ac1.parent_id
			ORDER BY logs1.datetime DESC
		) l2 ON l2.parent_id = a.id
		LEFT JOIN (			
			SELECT COUNT(sales1.id) sales2, max(sales1.date_created) date2_created, ac2.parent_id
			FROM #__affiliate_tracker_accounts ac2
			LEFT JOIN #__affiliate_tracker_conversions sales1 ON (sales1.atid = ac2.id AND sales1.component = "com_virtuemart")
			GROUP BY ac2.parent_id
			ORDER BY sales1.date_created DESC
		) s2 ON s2.parent_id = a.id		
		
		 WHERE a.account_type IN  ("Proba","proba_free","start", "Busines")
		-- WHERE a.account_type !=  ("virtual")
		GROUP BY a.id
';

$db->setQuery($sql);
$partners = $db->loadObjectList();






foreach($partners as $partner) {

    $sql = 'SELECT mailid FROM #__acymailing_userstats WHERE subid='.$partner->subid.' AND mailid IN (316,311,320,317,315,318,312,319,328,330,313,329,314,305,307)';
    $db->setQuery($sql);
    $mails = $db->loadObjectList();
    $mailList = [];
    foreach($mails as $value) {
        $mailList[] = $value->mailid;
    }

    $userStep = 0;
    echo $partner->id.';'.$partner->email.';'.$partner->leads.';'.$partner->leads_system.';'.$partner->sales.';'.intval($partner->leads2).';'.intval($partner->sales2).';';


    foreach($steps as $key=>$step) {

        if ($userStep !== 0) continue;

        asort($step['messages']);

        if (!empty($step['leadsplus']))
            $dateCompare = $partner->lead_date;
        else
            $dateCompare = $partner->date_created;

        if (!empty($step['leads2plus']))
            $date2Compare = $partner->lead2_date;
        else
            $date2Compare = $partner->date2_created;

        if ($partner->leads == $step['leads']
            && $partner->sales == $step['sales']
            && empty($partner->leads2)
            && empty($partner->sales2)
        ) {

            $userStep = $key;
            echo ($key-1).';';
            echo ' "'.implode('-',$step['messages']).'";';

        } else
            if ($partner->leads == $step['leads']
                && $partner->sales == $step['sales']
                && $partner->leads2 == $step['leads2']
                && $partner->sales2 == $step['sales2']
            ) {

                $userStep = $key;

                echo ($key-1).',';
                echo '"'.implode('-',$step['messages']).'";';
            } else {

            }

    }
    if (!$userStep) echo ';;';
    $totalComission = PlLibHelperAffiliate::getTotalComission($partner);

    echo '"'.implode('-',$mailList).'";'.$partner->registerDate.';'.$partner->lastVisitDate.';'.$partner->name.';'.$partner->phone.';'.$partner->packets.';'.$totalComission.';<br />';
}







die();






if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
    echo 'This code can not work without the AcyMailing Component';
    return false;
}
$db = JFactory::getDBO();

$steps = [
    "2" => [
        'leads' => 0,
        'sales' => 0,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 1,
        'salesplus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "3" => [
        'leads' => 1,
        'sales' => 0,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 2,
        'salesplus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "4" => [
        'leads' => 3,
        'sales' => 0,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 1,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "6" => [
        'leads' => 3,
        'sales' => 1,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 1,
        'salesplus' => 0,
        'datediff' => 24*60*60*2,
        'datediffsign' => '>',
        'messages' => []
    ],
    "7" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 0,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "8" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 3,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 3,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "9" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 4,
        'sales2' => 0,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 1,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "10" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 5,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "11" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 6,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 3,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],
    "12" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 9,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 1,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => [313]
    ],
    "13" => [
        'leads' => 4,
        'sales' => 1,
        'leads2'=> 10,
        'sales2' => 1,
        'leadsplus' => 0,
        'salesplus' => 0,
        'leads2plus' => 0,
        'sales2plus' => 0,
        'datediff' => 24*60*60,
        'datediffsign' => '>',
        'messages' => []
    ],


];










$sql = '
		SELECT a.id, a.user_id, l.leads, s.sales, l.datetime lead_date, s.date_created sale_date, l2.leads2, s2.sales2, s2.date2_created, u.email
		FROM #__affiliate_tracker_accounts a
		INNER JOIN #__users u ON a.user_id = u.id
		LEFT JOIN (
			SELECT COUNT(logs.id) leads, max(logs.datetime) datetime, ac0.id
			FROM #__affiliate_tracker_accounts ac0 
			LEFT JOIN #__affiliate_tracker_logs logs ON (logs.account_id = ac0.id AND logs.user_id > 0 AND logs.user_id != ac0.user_id) 						
			GROUP BY ac0.id
			ORDER BY logs.datetime DESC
		) l ON l.id = a.id
		LEFT JOIN (
			SELECT COUNT(sales.id) sales, max(sales.date_created) date_created, ac1.id
			FROM #__affiliate_tracker_accounts ac1 
			LEFT JOIN #__affiliate_tracker_conversions sales ON (sales.atid = ac1.id AND sales.component = "com_virtuemart")
			GROUP BY ac1.id
			ORDER BY sales.date_created DESC
		) s ON s.id = a.id
		LEFT JOIN (			
			SELECT COUNT(logs1.id) leads2, logs1.datetime lead2_date, ac1.parent_id
			FROM #__affiliate_tracker_accounts ac1			
			LEFT JOIN #__affiliate_tracker_logs logs1 ON (logs1.account_id = ac1.id AND logs1.user_id > 0 AND logs1.user_id != ac1.user_id)			
			GROUP BY ac1.parent_id
			ORDER BY logs1.datetime DESC
		) l2 ON l2.parent_id = a.id
		LEFT JOIN (			
			SELECT COUNT(sales1.id) sales2, max(sales1.date_created) date2_created, ac2.parent_id
			FROM #__affiliate_tracker_accounts ac2
			LEFT JOIN #__affiliate_tracker_conversions sales1 ON (sales1.atid = ac2.id AND sales1.component = "com_virtuemart")
			GROUP BY ac2.parent_id
			ORDER BY sales1.date_created DESC
		) s2 ON s2.parent_id = a.id		
		
		WHERE a.account_type IN ("proba", "proba_free") AND a.id != 21	 	
		GROUP BY a.id
';

$db->setQuery($sql);
$partners = $db->loadObjectList();



foreach($partners as $partner) {
    $userStep = 0;
    //echo $partner->id.' '.$partner->email.' L:'.$partner->leads.' S: '.$partner->sales.' L2:'.$partner->leads2.' S2: '.$partner->sales2;
    foreach($steps as $key=>$step) {
        if ($userStep !== 0) continue;

        if (!empty($step['leadsplus']))
            $dateCompare = $partner->lead_date;
        else
            $dateCompare = $partner->date_created;

        if (!empty($step['leads2plus']))
            $date2Compare = $partner->lead2_date;
        else
            $date2Compare = $partner->date2_created;

        if ($partner->leads == $step['leads']
            && $partner->sales == $step['sales']
            && (time() - $step['dateDiff']) >= $dateCompare
            && empty($partner->leads2) && empty($partner->sales2)
        ) {

            echo 'Day'.($key-1).';';
            echo ' "'.implode(',',$step['messages']).'";';
            $userStep = $key;
            //echo ' step'.$key;

        } else
            if ($partner->leads == $step['leads']
                && $partner->sales == $step['sales']
                && $partner->leads2 == $step['leads2']
                && $partner->sales2 == $step['sales2']
                && (time() - $step['dateDiff']) >= $date2Compare
            ) {
                $userStep = $key;
                echo 'Day'.($key-1).';';
                echo ' "'.implode(',',$step['messages']).'";';
            } else {

            }
    }
    //echo '<br />';
}





