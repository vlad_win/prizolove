<?php
defined('_JEXEC') or die;

class PlLibHelperPersonal
{
    private $db;

    private static $instance;

    private $ipaySettings;

    private $params;

    public function __construct()
    {
        $this->db = JFactory::getDbo();

    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getLatestProducts($num = 6)
    {
        $sql = '
            SELECT * FROM #__prizolove_products WHERE status = 1 AND id NOT IN (254,256,257,258) ORDER BY id DESC
        ';

        $this->db->setQuery($sql, 0, $num);

        $events = $this->db->loadObjectList();

        return $events;
    }
}