<?php
defined('_JEXEC') or die;

class PlLibHelperUsers
{
    private $db;

    private static $instance;

    public function __construct()
    {
        $this->db = JFactory::getDbo();
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function checkUser($userData)
    {
        if (empty($_COOKIE["atid"])) return false;

        jimport('joomla.user.helper');
        $userId = $this->getUserId($userData->email);


        if (!$userId) {
            $pwd = bin2hex(openssl_random_pseudo_bytes(4));
            $udata = array(
                "name"=>$userData->name,
                "username"=>$userData->email,
                "password"=>$pwd,
                "password2"=>$pwd,
                "email"=>$userData->email,
                "block"=>0,
                "groups"=>array("1","2")
            );
            $user = new JUser;

            //Write to database
            if(!$user->bind($udata)) {
                throw new Exception("Could not bind data. Error: " . $user->getError());
            }
            if (!$user->save()) {
                throw new Exception("Could not save user. Error: " . $user->getError());
            }
            //$this->createPartnerProfile($userData, $user->id);
            $userId = $user->id;
        } else {
            return $userId;
        }


        $profilePsw = new StdClass();
        $profilePsw->user_id = $userData->subid;
        $profilePsw->psw = $pwd;
        $this->db->insertObject('#__plprofile_psws', $profilePsw);

        return $userId;
    }

    public function getAtidByUserId($userId)
    {
        $query = 'SELECT id FROM #__affiliate_tracker_accounts WHERE user_id = '.$userId;
        $this->db->setQuery($query);
        return $this->db->loadResult();
    }

    private function createPartnerProfile($userData, $userId)
    {
        $atid = 0;
        if (!empty($_COOKIE["atid"])) {
            // if the cookie is available and the user is logged in, we check if there is a log linked to that user
            $cookiecontent = unserialize(base64_decode($_COOKIE["atid"]));
            $atid = (int)$cookiecontent["atid"];
        }

        $affiliate = new StdClass();
        $affiliate->account_name = $userData->name;
        $affiliate->user_id = $userId;
        $affiliate->publish = 1;
        $affiliate->params = '';
        $affiliate->type = 'flat';
        $affiliate->comission = '0.00';
        $affiliate->payment_options = '';
        $affiliate->name = $userData->name;
        $affiliate->email = $userData->email;
        $affiliate->address = '';
        $affiliate->zipcode = '';
        $affiliate->city = '';
        $affiliate->state = '';
        $affiliate->country = '';
        $affiliate->phone = '';
        $affiliate->company = '';
        $affiliate->ref_word = '';
        $affiliate->variable_comissions = '';
        $affiliate->refer_url = '';
        $affiliate->parent_id = $atid;
        $affiliate->account_type = 'proba_free';

        $installedPlugins = AffiliateHelper::getInstalledPlugins(true);
        //print_r($installedPlugins);
        $variable_commissions = array();

        foreach	($installedPlugins as $plugin) {
            $pluginCommission = new stdClass();
            $pluginCommission->extension = $plugin->name;
            $pluginCommission->type = $plugin->type;
            $pluginCommission->commission = $plugin->commissions[0];

            $pluginCommission->levels = new stdClass();
            for ($j = 1; $j < sizeof($plugin->commissions); $j++) {
                $nextLevel = $j + 1;
                $pluginCommission->levels->$nextLevel = $plugin->commissions[$j];
            }
            array_push($variable_commissions, $pluginCommission);
        }

        $affiliate->variable_comissions = json_encode($variable_commissions);

        $this->db->insertObject('#__affiliate_tracker_accounts', $affiliate);


    }

    public function getUserId($email)
    {

        $query = $this->db->getQuery(true)
            ->select($this->db->quoteName('id'))
            ->from($this->db->quoteName('#__users'))
            ->where($this->db->quoteName('email') . ' = ' . $this->db->quote($email));
        $this->db->setQuery($query, 0, 1);

        return $this->db->loadResult();
    }

    public function confirmAcyMailingUser()
    {
        $helperFile = rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php';

        if(!file_exists($helperFile) || !include_once($helperFile)) return true;

        $userClass = acymailing_get('class.subscriber');
        $userClass->geolocRight = true;

        $user = $userClass->identify();

        if(empty($user)) return false;


        if(!$user->confirmed) $userClass->confirmSubscription($user->subid);

    }
    
    public function checkPartner($userData, $createPartner = false)
    {

        jimport('joomla.user.helper');
        $userId = $this->getUserId($userData->email);


        if (!$userId) {
            $pwd = bin2hex(openssl_random_pseudo_bytes(4));
            $udata = array(
                "name"=>$userData->name,
                "username"=>$userData->email,
                "password"=>$pwd,
                "password2"=>$pwd,
                "email"=>$userData->email,
                "block"=>0,
                "groups"=>array("1","2")
            );
            $user = new JUser;

            //Write to database
            if(!$user->bind($udata)) {
                return 0;
            }
            if (!$user->save()) {
                return 0;
            }

            $userId = $user->id;
	        $userGroup = new StdClass();
	        $userGroup->user_id = $userId;
	        $userGroup->group_id = 15;

            $this->db->insertObject('#__user_usergroup_map', $userGroup);
	
        
            $profilePsw = new StdClass();
            $profilePsw->user_id = $userData->subid;
            $profilePsw->psw = $pwd;
            $this->db->insertObject('#__plprofile_psws', $profilePsw);
        }

        $this->createPartnerFreeProfile($userData, $userId);

        return $userId;
    }

    public function createPartnerFreeProfile($userData, $userId)
    {

        $helperFile = rtrim(JPATH_BASE,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_affiliatetracker'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helpers.php';

        if(!file_exists($helperFile) || !include_once($helperFile)) return true;

        $atid = 0;

        $affiliate = new StdClass();
        $affiliate->account_name = $userData->name;
        $affiliate->user_id = $userId;
        $affiliate->publish = 1;
        $affiliate->params = '';
        $affiliate->type = 'flat';
        $affiliate->comission = '0.00';
        $affiliate->payment_options = '';
        $affiliate->name = $userData->name;
        $affiliate->email = $userData->email;
        $affiliate->address = '';
        $affiliate->zipcode = '';
        $affiliate->city = '';
        $affiliate->state = '';
        $affiliate->country = '';
        $affiliate->phone = '';
        $affiliate->company = '';
        $affiliate->ref_word = '';
        $affiliate->variable_comissions = '';
        $affiliate->refer_url = '';
        $affiliate->parent_id = $atid;
        $affiliate->account_type = 'proba_free';

        $installedPlugins = AffiliateHelper::getInstalledPlugins(true);

        $variable_commissions = array();

        foreach	($installedPlugins as $plugin) {
            $pluginCommission = new stdClass();
            $pluginCommission->extension = $plugin->name;
            $pluginCommission->type = $plugin->type;
            $pluginCommission->commission = $plugin->commissions[0];

            $pluginCommission->levels = new stdClass();
            for ($j = 1; $j < sizeof($plugin->commissions); $j++) {
                $nextLevel = $j + 1;
                $pluginCommission->levels->$nextLevel = $plugin->commissions[$j];
            }
            array_push($variable_commissions, $pluginCommission);
        }

        $affiliate->variable_comissions = json_encode($variable_commissions);



        $this->db->insertObject('#__affiliate_tracker_accounts', $affiliate);

    }

    public function checkJoomlaUser($userData, $partnerFlag = false)
    {
        $userId = $this->getUserId($userData->email);


        if (!$userId) {
            $pwd = bin2hex(openssl_random_pseudo_bytes(4));
            $udata = array(
                "name"=>$userData->name,
                "username"=>$userData->email,
                "password"=>$pwd,
                "password2"=>$pwd,
                "email"=>$userData->email,
                "block"=>0,
                "groups"=>array("1","2")
            );
            $user = new JUser;

            //Write to database
            if(!$user->bind($udata)) {
                return 0;
            }
            if (!$user->save()) {
                return 0;
            }
            //$this->createPartnerProfile($userData, $user->id);
            $userId = $user->id;

            if ($partnerFlag) $this->addPartnerGroup($userId);
        } else {
            if ($partnerFlag) $this->addPartnerGroup($userId);
            return $userId;
        }

        $profilePsw = new StdClass();
        $profilePsw->user_id = $userData->subid;
        $profilePsw->psw = $pwd;
        $this->db->insertObject('#__plprofile_psws', $profilePsw);

        return $userId;
    }

    public function createPaidAccount($userData)
    {
        $businessProductInfo = PlLibHelperOrders::getInstance()->getBusinessProductInfo($userData->order_id);

        $status = ($userData->confirmed)?1:2;

        $helperFile = rtrim(JPATH_ROOT,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_affiliatetracker'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helpers.php';

        if(!file_exists($helperFile) || !include_once($helperFile)) return true;


        $atid = intval($userData->parent_atid);

        $affiliate = new StdClass();
        $affiliate->account_name = $userData->name;
        $affiliate->user_id = $userData->userid;
        $affiliate->publish = 1;
        $affiliate->params = '';
        $affiliate->type = 'flat';
        $affiliate->comission = '0.00';
        $affiliate->payment_options = '';
        $affiliate->name = $userData->name;
        $affiliate->email = $userData->email;
        $affiliate->address = '';
        $affiliate->zipcode = '';
        $affiliate->city = '';
        $affiliate->state = '';
        $affiliate->country = '';
        $affiliate->phone = '';
        $affiliate->company = '';
        $affiliate->ref_word = '';
        $affiliate->variable_comissions = '';
        $affiliate->refer_url = '';
        $affiliate->parent_id = $atid;
        $affiliate->account_type = $businessProductInfo->alias;

        $installedPlugins = AffiliateHelper::getInstalledPlugins(true);

        $variable_commissions = array();

        foreach	($installedPlugins as $plugin) {
            $pluginCommission = new stdClass();
            $pluginCommission->extension = $plugin->name;
            $pluginCommission->type = $plugin->type;
            $pluginCommission->commission = $plugin->commissions[0];

            $pluginCommission->levels = new stdClass();
            for ($j = 1; $j < sizeof($plugin->commissions); $j++) {
                $nextLevel = $j + 1;
                $pluginCommission->levels->$nextLevel = $plugin->commissions[$j];
            }
            array_push($variable_commissions, $pluginCommission);
        }

        $affiliate->variable_comissions = json_encode($variable_commissions);

        $this->db->insertObject('#__affiliate_tracker_accounts', $affiliate);
        $atid = $this->db->insertID();
        $this->db->setQuery('INSERT IGNORE INTO #__user_usergroup_map (`user_id`,`group_id`) VALUES ('.$userData->userid.',15)');
        $this->db->query();
        $this->db->setQuery('INSERT IGNORE INTO #__user_usergroup_map (`user_id`,`group_id`) VALUES ('.$userData->userid.',16)');
        $this->db->query();
        $this->db->setQuery('INSERT IGNORE INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$this->db->Quote(48).','.$this->db->Quote($userData->subid).','.$this->db->Quote(time()).','.$status.')');
        $this->db->query();
        $this->db->setQuery('INSERT IGNORE INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$this->db->Quote($businessProductInfo->listid).','.$this->db->Quote($userData->subid).','.$this->db->Quote(time()).','.$status.')');
        $this->db->query();
        $params = json_decode($businessProductInfo->params, true);
        if (!empty($params['leads'])) {
            for($i=0; $i<$params['leads'];$i++) {
                PlLibHelperDemo::getInstance()->addLead($atid);
            }
        }
        if (!empty($params['partners'])) {
            $sql = 'SELECT a.id, a.user_id FROM #__affiliate_tracker_accounts a WHERE a.account_type = "proba_free" AND a.parent_id < 1 LIMIT '.intval($params['partners']);

            $this->db->setQuery($sql);

            $partners = $this->db->loadObjectList();

            foreach($partners as $partner) {
                $sql = 'UPDATE #__affiliate_tracker_accounts SET parent_id = '.$atid.' WHERE id = '.$partner->id;
                $this->db->setQuery($sql);
                $this->db->execute();
                $sql = 'INSERT INTO #__affiliate_tracker_logs (datetime, user_id, account_id, atid) VALUES ("'.date('Y-m-d H:i:s').'",'.$partner->user_id.','.$atid.', '.$atid.')';
                $this->db->setQuery($sql);
                $this->db->execute();
            }
        }
        if ($status == 1) {
            PlLibHelperDemo::getInstance()->sendEmailNotification($userData->userid,304, true);
            PlLibHelperDemo::getInstance()->sendEmailNotification($userData->userid,$businessProductInfo->mailid, true);
        }
    }


    public function createPrivateAccount($userData)
    {

        $status = ($userData->confirmed)?1:2;

        $helperFile = rtrim(JPATH_ROOT,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_affiliatetracker'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helpers.php';

        if(!file_exists($helperFile) || !include_once($helperFile)) return true;


        $affiliate = new StdClass();
        $affiliate->account_name = $userData->name;
        $affiliate->user_id = $userData->id;
        $affiliate->publish = 1;
        $affiliate->params = '';
        $affiliate->type = 'flat';
        $affiliate->comission = '0.00';
        $affiliate->payment_options = '';
        $affiliate->name = $userData->name;
        $affiliate->email = $userData->email;
        $affiliate->address = '';
        $affiliate->zipcode = '';
        $affiliate->city = '';
        $affiliate->state = '';
        $affiliate->country = '';
        $affiliate->phone = '';
        $affiliate->company = '';
        $affiliate->ref_word = '';
        $affiliate->variable_comissions = '';
        $affiliate->refer_url = '';
        $affiliate->parent_id = intval($userData->atid);
        $affiliate->account_type = 'private';

        $installedPlugins = AffiliateHelper::getInstalledPlugins(true);

        $variable_commissions = array();

        foreach	($installedPlugins as $plugin) {
            $pluginCommission = new stdClass();
            $pluginCommission->extension = $plugin->name;
            $pluginCommission->type = $plugin->type;
            $pluginCommission->commission = $plugin->commissions[0];

            $pluginCommission->levels = new stdClass();
            for ($j = 1; $j < sizeof($plugin->commissions); $j++) {
                $nextLevel = $j + 1;
                $pluginCommission->levels->$nextLevel = $plugin->commissions[$j];
            }
            array_push($variable_commissions, $pluginCommission);
        }

        $affiliate->variable_comissions = json_encode($variable_commissions);

        $this->db->insertObject('#__affiliate_tracker_accounts', $affiliate);
        $atid = $this->db->insertID();

        $this->db->setQuery('INSERT IGNORE INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$this->db->Quote(48).','.$this->db->Quote($userData->subid).','.$this->db->Quote(time()).','.$status.')');
        $this->db->query();
    }

    private function addPartnerGroup($userId)
    {
        $this->db->setQuery('INSERT IGNORE INTO #__user_usergroup_map (`user_id`,`group_id`) VALUES ('.$userId.',15)');
        $this->db->query();
    }

    public function activateBusinessProduct($userData)
    {
        $businessProductInfo = PlLibHelperOrders::getInstance()->getBusinessProductInfo($userData->order_id);
        $this->db->setQuery('INSERT IGNORE INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$this->db->Quote($businessProductInfo->listid).','.$this->db->Quote($userData->subid).','.$this->db->Quote(time()).',1)');
        $this->db->query();
        $this->db->setQuery('INSERT IGNORE INTO #__user_usergroup_map (`user_id`,`group_id`) VALUES ('.$userData->userid.',16)');
        $this->db->query();
        PlLibHelperDemo::getInstance()->sendEmailNotification($userData->userid,$businessProductInfo->mailid, true);
        $this->db->setQuery('UPDATE #__affiliate_tracker_accounts SET account_type = '.$this->db->Quote($businessProductInfo->alias).' WHERE id = '.$userData->atid);
        $this->db->query();

        $stat = PlLibHelperDemo::getInstance()->finishDemoAccount($userData->userid);
        if ($stat['balance'] && $stat['remove']) {
            PlLibHelperDemo::getInstance()->sendEmailNotification(
                $userData->userid,
                $businessProductInfo->mailid,
                true,
                [
                    'customvar1'=>'Из Вашего кабинета удалено '.$stat['remove'].' покупателей из пробного пакета.<br />Сумма '.$stat['balance'].'$ полученные на пробном пакете переведены на ваш бонусный счет.<br /> 
Вы сможете потратить их на покупки товаров и партнерских пакетов.'

                ]
            );
        }


        $params = json_decode($businessProductInfo->params, true);
        if (!empty($params['leads'])) {
            for($i=0; $i<$params['leads'];$i++) {
                PlLibHelperDemo::getInstance()->addLead($userData->atid);
            }
        }
        if (!empty($params['partners'])) {
            $sql = 'SELECT a.id, a.user_id FROM #__affiliate_tracker_accounts a WHERE a.account_type = "proba_free" AND a.parent_id < 1 LIMIT '.intval($params['partners']);

            $this->db->setQuery($sql);

            $partners = $this->db->loadObjectList();

            foreach($partners as $partner) {
                $sql = 'UPDATE #__affiliate_tracker_accounts SET parent_id = '.$userData->atid.' WHERE id = '.$partner->id;
                $this->db->setQuery($sql);
                $this->db->execute();
                $sql = 'INSERT INTO #__affiliate_tracker_logs (datetime, user_id, account_id, atid) VALUES ("'.date('Y-m-d H:i:s').'",'.$partner->user_id.','.$userData->atid.', '.$userData->atid.')';
                $this->db->setQuery($sql);
                $this->db->execute();
            }
        }
    }

    private function updateBonuses($userId, $newValue)
    {
        $this->db->setQuery('UPDATE #__pl_bonuses SET value = '.$newValue.' WHERE user_id = '.$userId);
        return $this->db->query();
    }

    public function verifySubscription($userInfo, $listId)
    {
        $helperFile = rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php';

        if(!file_exists($helperFile) || !include_once($helperFile)) return true;

        $listsubClass = acymailing_get('class.listsub');
        $userClass = acymailing_get('class.subscriber');

        $userSubscriptions = $listsubClass->getSubscription($userInfo->subid);

        if (!array_key_exists($listId, $userSubscriptions)) {
            $newList = array();
            $newList['status'] = ($userInfo->confirmed)?1:2;
            $newSubscription[$listId] = $newList;
            $userClass->saveSubscription($userInfo->subid, $newSubscription);
        }

    }

    private function createBonusAccount($userId)
    {
        $account = new StdClass();
        $account->value = 0;
        $account->user_id = $userId;
        $account->created = date('Y-m-d H:i:s');
        $account->modified = date('Y-m-d H:i:s');

        $this->db->insertObject('#__pl_bonuses',$account);

        $balanceLog = new StdClass();
        $balanceLog->value = 0;
        $balanceLog->old_value = 0;
        $balanceLog->type = 'deb';
        $balanceLog->created = date('Y-m-d H:i:s');
        $balanceLog->user_id = $userId;
        $balanceLog->action_user_id = 0;
        $balanceLog->comment = 'Automatically created user balance entity';

        $this->db->insertObject('#__pl_bonuses_log', $balanceLog);

    }

    public function getBonusAccount($userId)
    {
        $sql = 'SELECT * FROM #__pl_bonuses WHERE user_id = '.$userId;

        $this->db->setQuery($sql);
        $account = $this->db->loadObject();

        if (!$account) $account = $this->createBonusAccount($userId);

        return $account;
    }

    public function changeBonuses($userId, $value, $type, $comment)
    {
        $account = $this->getBonusAccount($userId);

        $oldValue = $account->value;

        switch($type) {
            case 'deb':
                $newValue = number_format(($oldValue + $value), 2);
                break;
            case 'cr':
                $newValue = number_format(($oldValue - $value), 2);
                break;
        }

        $this->updateBonuses($userId, $newValue);

        $balanceLog = new StdClass();
        $balanceLog->value = $newValue;
        $balanceLog->old_value = $oldValue;
        $balanceLog->type = $type;
        $balanceLog->created = date('Y-m-d H:i:s');
        $balanceLog->user_id = $userId;
        $balanceLog->action_user_id = $userId;
        $balanceLog->comment = $comment;

        $this->db->insertObject('#__pl_bonuses_log', $balanceLog);
        file_put_contents(
            JPATH_ROOT.'../../logs/bonuses'.date('Ymd').'.log',
            date('Y-m-d H:i:s').' UserId:'.$userId.' OldValue:'.$oldValue.' NewValue:'.$newValue."\r\n",
            FILE_APPEND
        );

    }

}
