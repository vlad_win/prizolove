<?php
defined('_JEXEC') or die;

class PlLibHelperBalances
{
    private $db;

    private static $instance;

    public function __construct()
    {
        $this->db = JFactory::getDbo();
    }

    public static function getInstance()
    {
        if ( is_null( self::$instance ) )
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function makeRedeemedConversions($userId)
    {
        $parentId = PlLibHelperAffiliate::getParent($userId);

        $accountInfo = PlLibHelperDemo::getInstance()->getAccountInfo($userId);

        if ($accountInfo && !empty($accountInfo->sales)) {
            foreach ($accountInfo->sales as $key => $sales) {
                foreach($sales as $keySale=>$sale) {
                    $accountInfo->sales[$key][$keySale]['redeemed'] = 1;
                }
            }
            PlLibHelperDemo::getInstance()->updateAccountInfo($accountInfo, $userId);
        }

        if ($parentId) {
            $sql = 'UPDATE #__affiliate_tracker_conversions SET redeemed = 1 WHERE atid = '.intval($parentId);
            $this->db->setQuery($sql);
            $this->db->execute();
        }

        return true;

    }

    private function createBalance($userId)
    {
        $userObj = new StdClass();
        $userObj->userid = $userId;

        $balance = new StdClass();
        $balance->value = PlLibHelperAffiliate::getTotalComission($userObj);
        $balance->user_id = $userId;
        $balance->created = date('Y-m-d H:i:s');
        $balance->modified = date('Y-m-d H:i:s');

        $this->db->insertObject('#__pl_balances',$balance);

        $balanceLog = new StdClass();
        $balanceLog->value = $balance->value;
        $balanceLog->old_value = 0;
        $balanceLog->type = 'deb';
        $balanceLog->created = date('Y-m-d H:i:s');
        $balanceLog->user_id = $userId;
        $balanceLog->action_user_id = 0;
        $balanceLog->comment = 'Automatically created user balance entity';

        $this->db->insertObject('#__pl_balances_log', $balanceLog);

        $this->makeRedeemedConversions($userId);

        return $balance->value;
    }

    private function updateBalance($userId, $newValue)
    {
        $this->db->setQuery('UPDATE #__pl_balances SET value = '.$newValue.' WHERE user_id = '.$userId);
        return $this->db->query();
    }

    public function getBalance($userId)
    {
        $sql = 'SELECT value FROM #__pl_balances WHERE user_id = '.intval($userId);

        $this->db->setQuery($sql);

        $balance = $this->db->loadResult();

        if (!is_numeric($balance)) $balance = $this->createBalance($userId);

        return number_format($balance ,2);
    }

    public function changeBalance($userId, $value, $type, $comment = '')
    {
        $user = JFactory::getUser();

        $oldValue = $this->getBalance($userId);

        switch($type) {
            case 'deb':
                $newValue = $oldValue + $value;
                break;
            case 'cr':
                $newValue = $oldValue - $value;
                break;
        }

        $this->updateBalance($userId, $newValue);

        $balanceLog = new StdClass();
        $balanceLog->value = $newValue;
        $balanceLog->old_value = $oldValue;
        $balanceLog->type = $type;
        $balanceLog->created = date('Y-m-d H:i:s');
        $balanceLog->user_id = $userId;
        $balanceLog->action_user_id = $user->id;
        $balanceLog->comment = $comment;

        $this->db->insertObject('#__pl_balances_log', $balanceLog);
        file_put_contents(
            JPATH_ROOT.'../../logs/balances'.date('Ymd').'.log',
            date('Y-m-d H:i:s').' UserId:'.$userId.' OldValue:'.$oldValue.' NewValue:'.$newValue."\r\n",
            FILE_APPEND
        );
    }
}