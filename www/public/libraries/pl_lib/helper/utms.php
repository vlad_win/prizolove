<?php
defined('_JEXEC') or die;

class PlLibHelperUtms
{
    private static $utmTags = array(
        'utm_source' => '',
        'utm_medium' => '',
        'utm_campaign' => '',
        'utm_term' => '',
        'utm_content' => '',
    );


    public static function utmToCookie()
    {

        if (strpos($_SERVER['QUERY_STRING'],'utm_') !== false) {

            self::getUtmTags($_SERVER['QUERY_STRING']);
            $currentSession = JFactory::getSession();

            $utmTags = json_decode($currentSession->get('utmTags', self::$utmTags));

            setcookie('orderUtms', json_encode($utmTags));
        }
    }

    public static function getUtmTags($url)
    {

        $queryStr = urldecode($url);

        $tags = explode('&', $queryStr);

        $utmTags = self::processUtms($tags);

        $currentSession = JFactory::getSession();
        $currentSession->set('utmTags', json_encode($utmTags));

        return $utmTags;
    }

    private static function processUtms($tags)
    {
        $utmTags = self::$utmTags;

        foreach($tags as $tag) {
            if (strpos($tag,'utm') !== false) {
                $utm = explode('=', $tag);
                if (count($utm) != 2) continue;
                $utmTags[$utm[0]] = $utm[1];
            }
        }
        return $utmTags;
    }

    public static function getUtmLink($ref, $cookieVar = 'orderUtms')
    {

        if (!empty($_COOKIE[$cookieVar])) {
            $utmTags = json_decode($_COOKIE[$cookieVar], true);
            if (strpos($ref, 'utm_') === false) {

                foreach($utmTags as $key=>$tag) {
                    if (empty($tag)) continue;

                    $utmTags[$key] = $tag;

                    if (strpos($ref, '?' )) {
                        $ref .= '&'.$key.'='.$tag;
                    } else {
                        $ref .= '?'.$key.'='.$tag;
                    }
                }
            }
        }

        return $ref;
    }

    public static function getCookieUtms($cookieVar = 'orderUtms')
    {
        $utmTags = self::$utmTags;
        if (!empty($_COOKIE[$cookieVar])) {
            $cookieUtms = json_decode($_COOKIE[$cookieVar], true);

            foreach ($cookieUtms as $key => $tag) {
                if (empty($tag)) continue;
                $utmTags[$key] = $tag;
            }
        }


        return $utmTags;
    }

}

