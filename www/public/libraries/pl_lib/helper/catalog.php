<?php
defined('_JEXEC') or die;

class PlLibHelperCatalog
{
    private $db;

    private static $instance;

    public function __construct()
    {
        $this->db = JFactory::getDbo();
    }

    public static function getInstance()
    {
        if ( is_null( self::$instance ) )
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getCategories()
    {
        $this->db->setQuery('SELECT * FROM #__prizolove_product_categories WHERE state =1');
        return $this->db->loadObjectList();
    }

    public function getProducts($limit = 24, $offset = 0)
    {
        $sql = '
                SELECT p.id, p.name, p.price, p.image, c.name category_name, p.price_usd
                FROM #__prizolove_products p 
                LEFT JOIN #__prizolove_product_categories_products cp ON p.id = cp.product_id
                LEFT JOIN #__prizolove_product_categories c ON cp.category_id = c.id
                WHERE c.state = 1 AND p.status = 1         
                ORDER BY p.id DESC       
        ';
        $this->db->setQuery($sql, $offset, $limit);

        return $this->db->loadObjectList();
    }

    public function getProductsByCategoryId($id)
    {
        $sql = '
                SELECT p.id, p.name, p.price, p.image, c.name category_name, p.price_usd, p.discount_percent
                FROM #__prizolove_products p 
                LEFT JOIN #__prizolove_product_categories_products cp ON p.id = cp.product_id
                LEFT JOIN #__prizolove_product_categories c ON cp.category_id = c.id
                WHERE cp.category_id = '.$id.' AND p.status = 1                 
        ';
        $this->db->setQuery($sql);

        return $this->db->loadObjectList();
    }

    public function getProductById($id)
    {
        $this->db->setQuery('SELECT * FROM #__prizolove_products WHERE id = '.$id);
        return $this->db->loadObject();
    }

}