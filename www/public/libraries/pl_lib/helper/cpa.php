<?php
defined('_JEXEC') or die;

class PlLibHelperCpa
{

    public static function processRegistration($subid, $cpaClickId, $cpaClickType)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $columns = array('user_id', 'cpa_click_id', 'cpa_click_type', 'status', 'date');

        $values = array(
            $subid,
            $db->quote($cpaClickId),
            $db->quote($cpaClickType),
            2,
            $db->quote(date('Y-m-d H:i:s'))
        );

        $query
            ->insert($db->quoteName('#__cpa_users'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();
    }

    public static function processConfirmation($subscriber)
    {
        $db = JFactory::getDbo();

        $query = "SELECT `cpa_click_id`, `cpa_click_type` FROM #__cpa_users WHERE `user_id` = ".$subscriber->subid." LIMIT 1";

        $db->setQuery($query);

        $cpaObject = $db->loadObject();

        if ($cpaObject) self::sendConfirmation($subscriber, $cpaObject);
    }

    public static function sendConfirmation($subscriber, $cpaObject)
    {
        $ch = curl_init();

        $apiUrl = '';

        switch($cpaObject->cpa_click_type) {
            case 'kadam':
                $apiUrl = 'http://postback.kadam.net/ru/postback/?data='.$cpaObject->cpa_click_id;
                break;
        }

        if (!$apiUrl) {
            return;
        }

        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($ch);

        $logLine = date('Y-m-d H:i:s').' "UserId:'.$subscriber->subid.'" "SendTo'.ucfirst($cpaObject->cpa_click_type).':'.$apiUrl.'" "'.ucfirst($cpaObject->cpa_click_type).'Response:'.$resp.'"';

        $fp = fopen(JPATH_ROOT.'/logs/log_affise.log', 'a+');

        fwrite($fp, $logLine."\n");

        $currentSession = JFactory::getSession();
        $currentSession->clear('сpaClickType');
        $currentSession->clear('cpaClickId');
    }
}