<?php
defined('_JEXEC') or die;

class PlLibHelperConversions
{
    private $db;

    private static $instance;

    public function __construct()
    {
        $this->db = JFactory::getDbo();
    }

    public static function getInstance()
    {
        if ( is_null( self::$instance ) )
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function processBusinessConversion($orderId, $userInfo)
    {
        $this->db->setQuery('SELECT id FROM #__affiliate_tracker_conversions WHERE component = "com_hikashop" AND reference_id = '.$orderId);

        if ($this->db->loadResult()) return;

        preg_match('/Цена: ([0-9,.]+)/', $userInfo->product_name, $match);

        if ($match && !empty($match[1])) {
            $price = str_replace(',','',$match[1]);
            $conversion_data = array(
                "name" => $userInfo->db_product_name,
                "component" => "com_hikashop",
                "extended_name" => $userInfo->name,
                "type" => 1,
                "value" => PlLibHelperRates::convert($price),
                "reference_id" => $orderId,
                "approved" => 1,
                "atid" => $userInfo->parent_atid
            );
        }

        require_once(JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');
        AffiliateHelper::create_conversion($conversion_data, $userInfo->userid);
    }

    public function processConversion($orderId, $userInfo)
    {
        $this->db->setQuery('SELECT id FROM #__affiliate_tracker_conversions WHERE component = "com_virtuemart" AND reference_id = '.$orderId);

        if ($this->db->loadResult()) return;

        preg_match('/Цена: ([0-9,.]+)/', $userInfo->product_name, $match);

        if ($match && !empty($match[1])) {
            $price = str_replace(',', '', $match[1]);
            $conversion_data = array(
                "name" => $userInfo->db_product_name,
                "component" => "com_virtuemart",
                "extended_name" => $userInfo->name,
                "type" => 1,
                "value" => PlLibHelperRates::convert($price),
                "reference_id" => $orderId,
                "approved" => 1,
                "atid" => $userInfo->parent_atid
            );


            require_once(JPATH_SITE . DS . 'components' . DS . 'com_affiliatetracker' . DS . 'helpers' . DS . 'helpers.php');
            AffiliateHelper::create_conversion($conversion_data, $userInfo->userid);
        }
    }

    public function processLevelOneConversion($orderId, $userInfo)
    {
        $this->db->setQuery('SELECT id FROM #__affiliate_tracker_conversions WHERE component = "com_j2store" AND reference_id = '.$orderId);

        if ($this->db->loadResult()) return;

        preg_match('/Цена: ([0-9,.]+)/', $userInfo->product_name, $match);

        if ($match && !empty($match[1])) {
            $price = str_replace(',','',$match[1]);
            $conversion_data = array(
                "name" => $userInfo->db_product_name,
                "component" => "com_j2store",
                "extended_name" => $userInfo->name,
                "type" => 1,
                "value" => 3,
                "reference_id" => $orderId,
                "approved" => 1,
                "atid" => $userInfo->parent_atid
            );
        }

        require_once(JPATH_SITE.DS.'components'.DS.'com_affiliatetracker'.DS.'helpers'.DS.'helpers.php');
        AffiliateHelper::create_conversion($conversion_data, $userInfo->userid);
    }

    public function processUserFirstConversion($orderId, $userInfo)
    {
        $this->db->setQuery('SELECT id FROM #__affiliate_tracker_conversions WHERE component = "com_j2store" AND reference_id = '.$orderId);
        if ($this->db->loadResult()) return;

        preg_match('/Цена: ([0-9,.]+)/', $userInfo->product_name, $match);

        if ($match && !empty($match[1])) {
            $sale = new StdClass();
            $sale->user_id = $userInfo->userid;
            $sale->reference_id = $orderId;
            $sale->extended_name = $userInfo->name;
            $sale->name = 'Конверсия за регистрицию пользователя';
            $sale->value = 0;
            $sale->comission = 1;
            $sale->date_created = date('Y-m-d H:i:s');
            $sale->approved = 1;
            $sale->type = 1;
            $sale->atid = $userInfo->parent_atid;
            $sale->component = 'com_virtuemart';
            $this->db->insertObject('#__affiliate_tracker_conversions', $sale);
            PlLibHelperBalances::getInstance()->changeBalance($userInfo->userid, 1 , 'deb','комиссия за регистрицию пользователя');
        }


    }

    public function checkFirstNetworkOrder($userInfo)
    {
        $sql = 'SELECT COUNT(o.order_id) FROM #__prizolove_orders o WHERE o.sent = 5 AND o.customer_id = '.$userInfo->subid;
        $this->db->setQuery($sql);
        if ($this->db->loadResult()) return false;
        return true;
    }


}
