<?php
defined('_JEXEC') or die;

class PlLibHelperPartners
{
    private $db;

    private static $instance;

    public function __construct()
    {
        $this->db = JFactory::getDbo();
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function createPartner($userData, $profileType = 'proba_free')
    {
        $userId = PlLibHelperUsers::getInstance()->checkJoomlaUser($userData, true);

        $this->checkPartnerProfile($userData, $userId, $profileType );
    }

    private function checkPartnerProfile($userData, $userId, $profile_type = 'proba_free')
    {

        $sql = 'SELECT id FROM #__affiliate_tracker_accounts WHERE user_id = '.intval($userId);

        $this->db->setQuery($sql);

        $partnerId = $this->db->loadResult();

        if (!empty($partnerId->id)) return $partnerId;

        return $this->createPartnerProfile($userData, $userId, $profile_type);

    }

    private function createPartnerProfile($userData, $userId, $profile_type)
    {
        $helperFile = rtrim(JPATH_SITE,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_affiliatetracker'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helpers.php';

        if(!file_exists($helperFile) || !include_once($helperFile)) return true;

        $atid = 0;

        $affiliate = new StdClass();
        $affiliate->account_name = $userData->name;
        $affiliate->user_id = $userId;
        $affiliate->publish = 1;
        $affiliate->params = '';
        $affiliate->type = 'flat';
        $affiliate->comission = '0.00';
        $affiliate->payment_options = '';
        $affiliate->name = $userData->name;
        $affiliate->email = $userData->email;
        $affiliate->address = '';
        $affiliate->zipcode = '';
        $affiliate->city = '';
        $affiliate->state = '';
        $affiliate->country = '';
        $affiliate->phone = '';
        $affiliate->company = '';
        $affiliate->ref_word = '';
        $affiliate->variable_comissions = '';
        $affiliate->refer_url = '';
        $affiliate->parent_id = $atid;
        $affiliate->account_type = $profile_type;

        $installedPlugins = AffiliateHelper::getInstalledPlugins(true);

        $variable_commissions = array();

        foreach	($installedPlugins as $plugin) {
            $pluginCommission = new stdClass();
            $pluginCommission->extension = $plugin->name;
            $pluginCommission->type = $plugin->type;
            $pluginCommission->commission = $plugin->commissions[0];

            $pluginCommission->levels = new stdClass();
            for ($j = 1; $j < sizeof($plugin->commissions); $j++) {
                $nextLevel = $j + 1;
                $pluginCommission->levels->$nextLevel = $plugin->commissions[$j];
            }
            array_push($variable_commissions, $pluginCommission);
        }


        $affiliate->variable_comissions = json_encode($variable_commissions);

        $this->db->insertObject('#__affiliate_tracker_accounts', $affiliate);

        $this->db->setQuery('INSERT INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$this->db->Quote(43).','.$this->db->Quote($userData->subid).','.$this->db->Quote(time()).',1)');
        $this->db->query();

        $mailer = acymailing_get('helper.mailer');
        $mailer->report = true;
        $mailer->trackEmail = true;
        $mailer->autoAddUser = false;
        $mailer->forceVersion = 1;
        $mailer->sendOne(304, $userData->email);

    }

    public function getLastPaidPacket($userId)
    {
        $sql = '
            SELECT p.name
            FROM #__prizolove_orders o
            INNER JOIN #__prizolove_order_items oi ON o.order_id = oi.order_id
            INNER JOIN #__prizolove_products p ON p.id = oi.product_id
            INNER JOIN #__acymailing_subscriber s ON s.subid = o.customer_id
            WHERE p.type = "business" AND o.sent = 5 AND s.userid = '.$userId.'
            ORDER BY o.order_id DESC 
            LIMIT 1
        ';

        $this->db->setQuery($sql);

        return $this->db->loadResult();
    }


}