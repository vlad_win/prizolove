<?php

defined('_JEXEC') or die;

class PlLibHelperCForms
{
	public static function processForm($data)
	{
		switch($data['form_id']) {
			case 327:
				self::addWebmaster($data);
				break;
			case 328:
				self::addShareholder($data);
				break;
			case 329:
				self::addInvestor($data);
				break;
		}
		return true;
	}
	
	private static function addWebmaster($data)
	{
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $columns = array('name', 'email', 'phone','skype','website','status','type');

        $values = array(
            $db->quote($data['name']),
            $db->quote($data['email']),
            $db->quote($data['phone']),
            $db->quote($data['skype']),
            $db->quote($data['website']),
            0,
            $db->quote('webmasters')            
        );

        $query
            ->insert($db->quoteName('#__prizolove_forms'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();
	}
	
	private static function addInvestor($data)
	{
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $columns = array('name', 'email', 'phone','status','type');

        $values = array(
            $db->quote($data['name']),
            $db->quote($data['email']),
            $db->quote($data['phone']),            
            0,
            $db->quote('investor')            
        );

        $query
            ->insert($db->quoteName('#__prizolove_forms'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();
	}
	
	private static function addShareholder($data)
	{
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $columns = array('name', 'email', 'phone','skype','website','status','type','fb', 'company');

        $values = array(
            $db->quote($data['name']),
            $db->quote($data['email']),
            $db->quote($data['phone']),
            $db->quote($data['skype']),
            $db->quote($data['website']),
            0,
            $db->quote('sharholder'),
            $db->quote($data['fb']),
            $db->quote($data['company'])
        );

        $query
            ->insert($db->quoteName('#__prizolove_forms'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();
	}
	
}
