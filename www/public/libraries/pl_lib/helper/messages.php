<?php
defined('_JEXEC') or die;

class PlLibHelperMessages
{
    private $db;

    private static $instance;

    public function __construct()
    {
        $this->db = JFactory::getDbo();
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function addMessage($userId, $text, $type)
    {
        $newMessage = new StdClass();
        $newMessage->id = $this->getMessageId(8);
        $newMessage->text = $text;
        $newMessage->showtime = date('Y-m-d H:i:s');
        $newMessage->type = $type;
        $newMessage->user_id = $userId;

        $this->db->insertObject('#__pl_message_queue',$newMessage);

    }

    public function removeMessage($id, $userId)
    {
        $this->db->setQuery('DELETE FROM #__pl_message_queue WHERE id = '.$this->db->Quote($id).' AND user_id = '.intval($userId));
        return $this->db->execute();
    }

    public function getMessages($userId)
    {
        $this->db->setQuery('
                              SELECT * 
                              FROM #__pl_message_queue 
                              WHERE user_id = '.$userId.' AND showtime <= '.$this->db->Quote(date('Y-m-d H:i:s'))
                   );

        return $this->db->loadObjectList();
    }

    private function getMessageId($length = 8)
    {
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }

}