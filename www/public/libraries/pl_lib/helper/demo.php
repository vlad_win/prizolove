<?php
defined('_JEXEC') or die;
JPluginHelper::importPlugin( 'system' );

class PlLibHelperDemo
{
    private $db;

    private static $instance;

    public function __construct()
    {
        $this->db = JFactory::getDbo();
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function addPartners($userId, $num =1)
    {
        for($i=0;$i<$num;$i++)
        {
            $this->addPartner($userId);
        }
    }

    private function addPartner($userId)
    {

    }

    public function addLevelOneLead($userId)
    {

    }

    public function addLevelOneSale($partnerId, $userId)
    {
        $sql = '
			SELECT u.id, l.account_id atid
			FROM #__affiliate_tracker_logs l 
			INNER JOIN #__users u ON u.id = l.user_id 
			WHERE l.account_id = '.$partnerId.'
			ORDER BY RAND()
			';
        $this->db->setQuery($sql);

        $lead = $this->db->loadObject();



        $sql = '
			SELECT o.order_id, p.name, p.price
			FROM #__prizolove_orders o
			INNER JOIN #__prizolove_order_items oi ON o.order_id = oi.order_id
			INNER JOIN #__prizolove_products p ON (oi.product_id = p.id AND p.id NOT IN (254,256,257,258,324,354)) 
			ORDER BY RAND()
			LIMIT 1
	';
        /* $sql = 'SELECT s.name, s.email, s.phone, s.subid
                FROM #__acymailing_listsub ls
                INNER JOIN #__acymailing_subscriber s ON ls.subid = s.subid
                LEFT JOIN #__users u ON u.email = s.email
                WHERE ls.listid IN (30, 31, 32) AND ls.status IN (1,2) AND u.id IS NULL
                ORDER BY RAND()
                LIMIT 1
                '; */

        $this->db->setQuery($sql);
        $order = $this->db->loadObject();

        $sale = new StdClass();
        $sale->user_id = $lead->id;
        $sale->reference_id = $order->order_id;
        $sale->atid = $lead->atid;
        $sale->name = $order->name;
        $sale->value = 0;
        $sale->comission = 3;
        $sale->date_created = date('Y-m-d H:i:s');
        $sale->approved = 1;
        $sale->type = 1;
        $sale->component = 'com_virtuemart';
        $this->db->insertObject('#__affiliate_tracker_conversions', $sale);

        PlLibHelperBalances::getInstance()->changeBalance($userId, $sale->comission , 'deb','комиссия за купленный товар');
    }

    public function addSale($lead)
    {
        $sql = '
			SELECT o.order_id, p.name, p.price
			FROM #__prizolove_orders o
			INNER JOIN #__prizolove_order_items oi ON o.order_id = oi.order_id
                        INNER JOIN #__prizolove_products p ON (oi.product_id = p.id AND p.type != "business")
			ORDER BY RAND()
			LIMIT 1
	';
        /* $sql = 'SELECT s.name, s.email, s.phone, s.subid
                FROM #__acymailing_listsub ls
                INNER JOIN #__acymailing_subscriber s ON ls.subid = s.subid
                LEFT JOIN #__users u ON u.email = s.email
                WHERE ls.listid IN (30, 31, 32) AND ls.status IN (1,2) AND u.id IS NULL
                ORDER BY RAND()
                LIMIT 1
                '; */

        $this->db->setQuery($sql);
        $order = $this->db->loadObject();


        $sale = new StdClass();
        $sale->user_id = $lead->user_id;
        $sale->reference_id = $order->order_id;
        $sale->atid = $lead->id;
        $sale->name = $order->name;
        $sale->value = 0;
        $sale->comission = PlLibHelperRates::convert($order->price) * 0.05;
        $sale->date_created = date('Y-m-d H:i:s');
        $sale->approved = 1;
        $sale->type = 1;
        $sale->component = 'com_virtuemart';
        $this->db->insertObject('#__affiliate_tracker_conversions', $sale);
        PlLibHelperBalances::getInstance()->changeBalance($lead->parent_user_id, $sale->comission , 'deb','комиссия за купленный товар');
    }

    public function addSaleByProductId($lead, $productId)
    {
        $sql = '
			SELECT o.order_id, p.name, p.price
			FROM #__prizolove_orders o
			INNER JOIN #__prizolove_order_items oi ON o.order_id = oi.order_id
			INNER JOIN #__prizolove_products p ON (oi.product_id = p.id AND p.id NOT IN (254,256,257,258) AND p.id = '.$productId.') 
			ORDER BY RAND()
			LIMIT 1
	';
        /* $sql = 'SELECT s.name, s.email, s.phone, s.subid
                FROM #__acymailing_listsub ls
                INNER JOIN #__acymailing_subscriber s ON ls.subid = s.subid
                LEFT JOIN #__users u ON u.email = s.email
                WHERE ls.listid IN (30, 31, 32) AND ls.status IN (1,2) AND u.id IS NULL
                ORDER BY RAND()
                LIMIT 1
                '; */

        $this->db->setQuery($sql);
        $order = $this->db->loadObject();

        $sale = new StdClass();
        $sale->user_id = $lead->user_id;
        $sale->reference_id = $order->order_id;
        $sale->atid = $lead->id;
        $sale->name = $order->name;
        $sale->value = 0;
        $sale->comission = PlLibHelperRates::convert($order->price) * 0.05;
        $sale->date_created = date('Y-m-d H:i:s');
        $sale->approved = 1;
        $sale->type = 1;
        $sale->component = 'com_virtuemart';
        $this->db->insertObject('#__affiliate_tracker_conversions', $sale);
    }




    public function addLevelTwoSale($userId, $mailId, $leads = 1)
    {

        $sql = '
          SELECT logs.user_id, a.parent_id id, a1.user_id parent_user_id 
		  FROM #__affiliate_tracker_accounts a
		  LEFT JOIN #__affiliate_tracker_accounts a1 ON a1.id = a.parent_id
		  LEFT JOIN #__affiliate_tracker_logs logs ON logs.account_id = a.id 
		  INNER JOIN #__users u ON a.user_id = u.id
		  WHERE a1.user_id = '.$userId.'
		  ORDER BY RAND()
		  LIMIT 1
        ';

        $this->db->setQuery($sql);

        $lead = $this->db->loadObject();

        if ($lead) {
            $this->addSale($lead);
            $this->sendEmailNotification($userId, $mailId);
        }
    }


    public function addLevelTwoLead($userId, $mailId, $leads = 1)
    {


        $sql = '
          SELECT a.user_id, u.name, u.email, a.id atid 
		  FROM #__affiliate_tracker_accounts a
		  LEFT JOIN #__affiliate_tracker_accounts a1 ON a1.id = a.parent_id
		  LEFT JOIN #__users u ON a.user_id = u.id
		  WHERE a1.user_id = '.$userId.'
		  ORDER BY RAND()
		  LIMIT 1
        ';

        $this->db->setQuery($sql);
        $lead = $this->db->loadObject();

        if (!$lead) {

            $sql = '
              SELECT l.user_id, u.name, u.email, a.id atid 
		      FROM  #__affiliate_tracker_logs l
		      INNER JOIN #__affiliate_tracker_accounts a ON a.id = l.atid
		      LEFT JOIN #__affiliate_tracker_accounts a1 ON a1.user_id = l.user_id
		      LEFT JOIN #__users u ON l.user_id = u.id
		      WHERE a.user_id = ' . $userId . ' AND l.user_id > 0 AND l.user_id != a.user_id AND a1.id IS NULL
            ';
            $this->db->setQuery($sql, 0, 1);
            $lead = $this->db->loadObject();
            $partnerId = $this->createVirtualPartner($lead);
        } else {
            $partnerId = $lead->atid;
        }



        for($i=0; $i<$leads; $i++) {
            $this->addLead($partnerId);
        }

        $this->sendEmailNotification($userId, $mailId);
    }

    public function sendEmailNotification($userId, $emailId, $sendNow = false, $params = ['customvar1'=>'','customvar2'=>''])
    {
        if ($sendNow) {
            $senddate = time();
        } else {
            $senddate = $this->getRandomDate();
        }
        $sql = '
                SELECT s.subid
                FROM #__acymailing_subscriber s 
                WHERE s.userid = '.$userId;

        $this->db->setQuery($sql);
        $user = $this->db->loadObject();



        $this->db->setQuery('INSERT IGNORE INTO #__acymailing_queue (`subid`,`mailid`,`senddate`,`priority`,`paramqueue`) VALUES ('.$this->db->Quote($user->subid).','.$this->db->Quote($emailId).','.$this->db->Quote($senddate).',1,'.$this->db->Quote(json_encode($params, JSON_UNESCAPED_UNICODE)).')');
        $this->db->query();

    }

    private function getRandomDate()
    {
        $startTime = time() + (60*60);
        $endTime = time() + (3*60*60);
        return rand($startTime, $endTime);
    }

    private function createVirtualPartner($lead)
    {
        $helperFile = rtrim(JPATH_BASE,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_affiliatetracker'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helpers.php';

        if(!file_exists($helperFile) || !include_once($helperFile)) return true;

        $affiliate = new StdClass();
        $affiliate->account_name = $lead->name;
        $affiliate->user_id = $lead->user_id;
        $affiliate->publish = 0;
        $affiliate->params = '';
        $affiliate->type = 'flat';
        $affiliate->comission = '0.00';
        $affiliate->payment_options = '';
        $affiliate->name = $lead->name;
        $affiliate->email = $lead->email;
        $affiliate->address = '';
        $affiliate->zipcode = '';
        $affiliate->city = '';
        $affiliate->state = '';
        $affiliate->country = '';
        $affiliate->phone = '';
        $affiliate->company = '';
        $affiliate->ref_word = '';
        $affiliate->variable_comissions = '';
        $affiliate->refer_url = '';
        $affiliate->parent_id = $lead->atid;
        $affiliate->account_type = 'virtual';

        $installedPlugins = AffiliateHelper::getInstalledPlugins(true);

        $variable_commissions = array();

        foreach	($installedPlugins as $plugin) {
            $pluginCommission = new stdClass();
            $pluginCommission->extension = $plugin->name;
            $pluginCommission->type = $plugin->type;
            $pluginCommission->commission = $plugin->commissions[0];

            $pluginCommission->levels = new stdClass();
            for ($j = 1; $j < sizeof($plugin->commissions); $j++) {
                $nextLevel = $j + 1;
                $pluginCommission->levels->$nextLevel = $plugin->commissions[$j];
            }
            array_push($variable_commissions, $pluginCommission);
        }

        $affiliate->variable_comissions = json_encode($variable_commissions);



        $this->db->insertObject('#__affiliate_tracker_accounts', $affiliate);

        return $this->db->insertID();
    }

    public function addLead($atid)
    {
        $sql = 'SELECT s.name, s.email, s.phone, s.subid, u.id
			FROM #__acymailing_listsub ls 
			INNER JOIN #__acymailing_subscriber s ON ls.subid = s.subid
			LEFT JOIN #__users u ON u.email = s.email 
			LEFT JOIN #__affiliate_tracker_logs l ON l.user_id = u.id
			WHERE ls.listid IN (30, 31, 32) AND ls.status = 1 AND l.id IS NULL
			ORDER BY RAND() 
			LIMIT 1
			';
        $this->db->setQuery($sql);
        $user = $this->db->loadObject();


        $userId = $user->id;

        $log = new StdClass();
        $log->user_id = $userId;
        $log->account_id = $atid;
        $log->atid = $atid;
        $log->datetime = date('Y-m-d H:i:s');

        $this->db->insertObject('#__affiliate_tracker_logs', $log);
    }

    public function setExpireDate($userId, $days)
    {
        $curDate = new \DateTime();
        $curDate->modify("+".$days.' day');

        $sql = 'UPDATE #__affiliate_tracker_accounts SET expire = "'.$curDate->format('Y-m-d 23:59:59').'" WHERE user_id='.$userId;

        $this->db->setQuery($sql);
        $this->db->query();
    }

    public function addLevelTwoSaleByProductId($userId, $mailId, $productId)
    {

        $sql = '
          SELECT logs.user_id, a.id 
		  FROM #__affiliate_tracker_accounts a
		  LEFT JOIN #__affiliate_tracker_accounts a1 ON a1.id = a.parent_id
		  LEFT JOIN #__affiliate_tracker_logs logs ON logs.account_id = a.id 
		  LEFT JOIN #__users u ON a.user_id = u.id
		  WHERE a1.user_id = '.$userId.'
		  ORDER BY RAND()
		  LIMIT 1
        ';

        $this->db->setQuery($sql);

        $lead = $this->db->loadObject();


        if ($lead) {
            $this->addSaleByProductId($lead, $productId);
            $this->sendEmailNotification($userId, $mailId);
        }
    }

    private function getUsersId($userId)
    {
        $sql = '
        SELECT GROUP_CONCAT(user_id SEPARATOR ",") 
                FROM (
                      SELECT l.id, u.name, l.atid, a.user_id parent_id, l.user_id
                      FROM fu3f5_affiliate_tracker_logs l
                      INNER JOIN fu3f5_affiliate_tracker_accounts a ON l.atid = a.id
					  INNER JOIN fu3f5_users u ON u.id = l.user_id AND u.id <> '.$userId.'                     
                      ORDER BY u.id ASC                
                ) affiliates_sorted,
                (SELECT @pv := "'.$userId.'") initialisation
                WHERE find_in_set(parent_id, @pv) 
                AND LENGTH(@pv := concat(@pv, \',\', user_id))
        ';

        $this->db->setQuery($sql);

        return $this->db->loadResult();
    }

    private function getNewLeads($num, $userId, $realLeads = false)
    {

        $inQuery = ' WHERE user_id != '.$userId;

        if ($realLeads) $inQuery = ' WHERE user_id NOT IN ('.$realLeads.','.$userId.') ';

        $sql = '
                SELECT * 
                FROM #__pl_clones_proba 
                '.$inQuery.' 
                ORDER BY RAND()
        ';

        $this->db->setQuery($sql, 0, $num);

        $leads = $this->db->loadObjectList();

        return  $leads;
    }

    private function addAccountInfo($userId)
    {
        $atid = PlLibHelperAffiliate::getParent($userId);

        if (!$atid) return false;

        $accountInfo = new StdClass();
        $accountInfo->atid = $atid;
        $accountInfo->user_id = $userId;
        $accountInfo->users = json_encode([1=>[]]);
        $accountInfo->sales = json_encode([]);
        $this->db->insertObject('#__pl_accounts_info', $accountInfo);

        return $accountInfo;
    }

    public function getAccountInfo($userId)
    {
        $sql = '
                SELECT *
                FROM #__pl_accounts_info
                WHERE user_id = '.$userId.'
        ';

        $this->db->setQuery($sql);

        $accountInfo = $this->db->loadObject();

        if (!$accountInfo) {
            $accountInfo = $this->addAccountInfo($userId);
        }

        $accountInfo->users = json_decode($accountInfo->users, true,512, JSON_UNESCAPED_UNICODE);
        $accountInfo->sales = json_decode($accountInfo->sales, true,512, JSON_UNESCAPED_UNICODE);


        return $accountInfo;
    }

    public function updateAccountInfo($accountInfo, $userId)
    {
        $sql = '
            UPDATE #__pl_accounts_info 
            SET users = '.$this->db->Quote(json_encode($accountInfo->users,JSON_UNESCAPED_UNICODE)).
            ', sales = '.$this->db->Quote(json_encode($accountInfo->sales,JSON_UNESCAPED_UNICODE)).'
            WHERE user_id = '.$userId.'
        ';

        $this->db->setQuery($sql);
        return $this->db->query();
    }

    private function getRandomSale()
    {
        $sql = '
			SELECT o.order_id, p.name, p.price, o.product_name
			FROM #__prizolove_orders o
			INNER JOIN #__prizolove_order_items oi ON o.order_id = oi.order_id
			INNER JOIN #__prizolove_products p ON (oi.product_id = p.id AND p.id NOT IN (254,256,257,258,324,350)) 
			ORDER BY RAND()
			LIMIT 1
	    ';

        $this->db->setQuery($sql);

        $order = $this->db->loadObject();

        preg_match('/Цена: ([0-9,.]+)/', $order->product_name, $match);

        if ($match && !empty($match[1])) {
            $order->price = str_replace(',','',$match[1]);
        }
        return $order;
    }

    public function addCloneLeads($userId, $num, $level = 1)
    {
        $realLeads = $this->getUsersId($userId);

        $accountInfo = $this->getAccountInfo($userId);


        if (!empty($accountInfo->users[1])) {
            $level1 = array_keys($accountInfo->users[1]);
            if ($realLeads)
                $realLeads .=  ','. implode(',',$level1);
            else
                $realLeads = implode(',',$level1);
        }
        if (!empty($accountInfo->users[2])) {
            $level2 = array_keys($accountInfo->users[2]);
            if ($realLeads)
                $realLeads .=  ','. implode(',',$level2);
            else
                $realLeads = implode(',',$level2);
        }



        $leads = $this->getNewLeads($num, $userId, $realLeads);

        foreach($leads as $lead) {
            $accountInfo->users[$level][$lead->user_id]['date'] = date('Y-m-d');
            $accountInfo->users[$level][$lead->user_id]['name'] = $lead->name;
            $accountInfo->users[$level][$lead->user_id]['user_id'] = $lead->user_id;
        }

        if ($this->updateAccountInfo($accountInfo,$userId)) {
            /* foreach($leads as $lead) {
                $this->db->setQuery('UPDATE #__pl_clones_proba SET partner_id = '.$userId.' WHERE user_id = '.$lead->user_id);
                $this->db->query();
            } */
        }
        return true;
    }

    public function addCloneSale($userId, $num, $level = 1)
    {
        $accountInfo = $this->getAccountInfo($userId);

        for($i=0; $i<$num; $i++) {
            $key = array_rand($accountInfo->users[$level]);
            $saleUserId = $accountInfo->users[$level][$key]['user_id'];
            $saleUserName = $accountInfo->users[$level][$key]['name'];
            $order = $this->getRandomSale();
            $newSale = new StdClass();
            $newSale->redeemed = 0;
            $newSale->name = $saleUserName;
            $newSale->level = $level;
            $newSale->product_name = $order->name;
            $newSale->date = date('Y-m-d');
            switch($level) {
                case 1:
                    $newSale->comission = 3;
                    break;
                case 2:
                case 3:
                    $newSale->comission =  PlLibHelperRates::convert($order->price) * 0.05;
                    break;
            }
            $accountInfo->sales[$saleUserId][] = $newSale;
            PlLibHelperBalances::getInstance()->changeBalance($userId, $newSale->comission , 'deb','комиссия за купленный товар');
        }

        $this->updateAccountInfo($accountInfo,$userId);
    }

    public function finishDemoAccount($userId)
    {
        $accountInfo = $this->getAccountInfo($userId);
        $balance = 0;

        foreach($accountInfo->sales as $userSales) {
            foreach($userSales as $sale) $balance += $sale['comission'];

        }
        $this->db->setQuery('SELECT SUM(old_value - value) FROM #__pl_balances_log WHERE user_id = '.$userId.' AND type = "cr"');
        $total = $this->db->loadResult();

        $balance = $balance - $total;

        $realBalance = PlLibHelperBalances::getInstance()->getBalance($userId);

        $stat = ['balance'=>0, 'remove'=>0];
        if ($realBalance >= $balance) {
            $balanceDiff = $realBalance - $balance;
            if ($balanceDiff > 0 && $balanceDiff < 1) $balance = $realBalance;
            $stat['balance'] = number_format($balance,2);
            PlLibHelperBalances::getInstance()->changeBalance($userId, $balance , 'cr','перевод в юонусы [закрытие демо кабинета]');
            PlLibHelperUsers::getInstance()->changeBonuses($userId, $balance, 'deb', 'перевод в юонусы [закрытие демо кабинета]');
        }
        if (!empty($accountInfo->users[1])) $stat['remove'] += count($accountInfo->users[1]);
        if (!empty($accountInfo->users[2])) $stat['remove'] += count($accountInfo->users[2]);

        $accountInfo->sales = [];
        $accountInfo->users = [1=>[],2=>[]];

        $this->updateAccountInfo($accountInfo, $userId);
        return $stat;

    }

}