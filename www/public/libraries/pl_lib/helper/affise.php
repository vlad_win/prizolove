<?php
defined('_JEXEC') or die;

class PlLibHelperAffise
{
	private static $apiKey = '1eb4187c071d340db53b2d004a6d307c48e64357';
	private static $conversionUrl = 'http://api.forest.digital/3.0/stats/conversions';
	private static $postbackUrl = 'http://offers.forest.digital/postback';
	private static $logLine;

	
	public static function sendRegistration($subscriber, $status, $lastRowId, $clickId, $things = false, $landId = false, $task = false, $goal = 1)
	{
        $apiUrl = self::$postbackUrl.'?clickid='.$clickId
            .'&action_id='.$lastRowId
            .'&status='.$status
            .'&custom_field1='.$subscriber->name
            ."&custom_field2=".$subscriber->email;

        if (!empty($subscriber->phone)) {
            $phone = $subscriber->phone;
            $apiUrl .= "&custom_field3=".$phone;
        }
        if ($things) $apiUrl .= "&custom_field4=".urlencode($things);
        if ($landId) {
            $apiUrl .= '&custom_field5='.$landId;
        }
        if ($task) $apiUrl .= "&custom_field6=".$task;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: multipart/form-data',
            'Api-Key: ' . self::$apiKey
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($ch);

        self::$logLine = date('Y-m-d H:i:s').' "UserId:'.$subscriber->subid.'" "SendToAffise:'.$apiUrl.'" "AffiseResponse:'.$resp.'"';
        self::logging();


        $currentSession = JFactory::getSession();
        $currentSession->clear('сpaClickType');
        $currentSession->clear('cpaClickId');
        $currentSession->clear('penguinClickId');
        $currentSession->clear('penguinPid');
        $currentSession->clear('penguinThings');
        $currentSession->clear('penguinTask');

        curl_close($ch);
    }

    public static function sendConfirmation($subscriber, $status, $goal = 2, $orderId = 0)
    {
		switch($goal) {
			case 3:				
			case 4:
				$actionId = substr(sha1($orderId),0,16);
				break;
			default:
				$actionId = ($subscriber->action_id)?$subscriber->action_id:'pl'.$subscriber->id;
		}
		
        $apiUrl = self::$postbackUrl.'?clickid='.$subscriber->click_id
            .'&action_id='.$actionId
            .'&status='.$status
            .'&custom_field1='.$subscriber->name
            ."&custom_field2=".$subscriber->email;

        $apiUrl .= "&goal=".$goal;

        if (!empty($subscriber->phone)) $apiUrl .= "&custom_field3=".$subscriber->phone;
        if ($subscriber->things) $apiUrl .= "&custom_field4=".$subscriber->things;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: multipart/form-data',
            'Api-Key: ' . self::$apiKey
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($ch);
        //$resp = 'Ok';

        self::$logLine = date('Y-m-d H:i:s').' "UserId:'.$subscriber->user_id.'" "SendToAffise:'.$apiUrl.'" "AffiseResponse:'.$resp.'"';

        self::logging();


        if ($subscriber->pid == 4256 && $goal != 4) {
            $refId = self::getRefId($subscriber->click_id);
            if ($refId) self::sendToMobytize($refId, $goal, $orderId);
        }

        $currentSession = JFactory::getSession();
        $currentSession->clear('penguinClickId');
        $currentSession->clear('penguinPid');
        $currentSession->clear('penguinThings');
        $currentSession->clear('penguinTask');

        curl_close($ch);
    }

    private static function getRefId($clickId)
    {
        $ch = curl_init();
        $apiUrl = self::$conversionUrl.'?clickid='.$clickId;

        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: multipart/form-data',
            'Api-Key: ' . self::$apiKey
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($ch);

        $answer = json_decode($resp);

        foreach($answer->conversions as $click) {
            if (!empty($click->sub3)) return $click->sub3;
        }

        return false;
    }

    private function sendToMobytize($refId, $goal, $orderId)
    {
        switch($goal) {
            case 2:
                $action = 'signup';
                break;
            case 3:
            case 4:
                $refId .= '&uid='.substr(sha1($orderId),0,16);
                $action = 'buy';
                break;
        }


        $apiUrl = 'https://px.mobytize.com/c/p85qbks4c30wxsdhso7vn18c7w3zo441?click_id='.$refId.'&action='.$action;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $apiUrl);


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt ($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);

        $resp = curl_exec($ch);

        $respCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

        self::$logLine = date('Y-m-d H:i:s').' "RefId:'.$refId.'" "SendToMobytize:'.$apiUrl.'" "MobytizeResponse:'.$respCode.'"';


        self::logging($resp);
    }

    public static function findUser($subid)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select($db->quoteName(array('a.user_id','a.things', 'a.click_id', 'a.pid', 'a.action_id', 'a.id', 'a.status','s.phone','s.name', 's.email')));
        $query->from($db->quoteName('#__affise','a'));
        $query->join('INNER', $db->quoteName('#__acymailing_subscriber', 's') . ' ON (' . $db->quoteName('a.user_id') . ' = ' . $db->quoteName('s.subid') . ')');

        $query->where($db->quoteName('a.user_id') . ' = '. $subid);

        $query->limit(1);

        $db->setQuery($query);

        $userInfo = $db->loadObject();

        return $userInfo;
    }

    private static function logging()
    {
        if (!self::$logLine) return;

        $fp = fopen(JPATH_ROOT.'/logs/log_affise.log', 'a+');

        fwrite($fp, self::$logLine."\n");
    }

    public static function addRecord($subid, $clickId, $pid, $things)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $columns = array('user_id', 'click_id', 'pid', 'category_id','offer_id','status', 'date', 'things');

        $values = array(
            $subid,
            $db->quote($clickId),
            $pid,
            0,
            0,
            5,
            $db->quote(date('Y-m-d H:i:s')),
            $db->quote($things)
        );

        $query
            ->insert($db->quoteName('#__affise'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();

        return 'pl'.$db->insertid();
    }
}
