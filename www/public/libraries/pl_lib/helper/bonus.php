<?php

defined('_JEXEC') or die;
date_default_timezone_set("Europe/Kiev");

class PlLibHelperBonus
{

    public static function getBonusArray()
    {
        $app = JFactory::getApplication();
        $bonuses = array();
        $mix = $app->input->cookie->getArray();
        foreach($mix as $key=>$el) {
            preg_match('/(plprmp_+)(\d)+/', $key, $match);
            if (!empty($match[2])) {
                $bonuses[] = self::getBonusText($el);
            }
        }

        $bns = $app->input->cookie->getBase64('_bns', false);

        if ($bns) {
            $bns = base64_decode($bns);
            if ($bns) {
                $bonuses[] = $bns;
            }
        }

        return $bonuses;
    }
	public static function checkBonusesById($id)
	{        	
        $bonuses = self::getBonuses($id);
	    
		foreach($bonuses as $bonus)
		{
			self::setCookieBonus($bonus);
		}        
	}

	public static function checkBonuses()
	{
        $jinput = JFactory::getApplication()->input;
        $option = $jinput->get('option', false);
        $id = $jinput->get('id', false);
        if ($option == 'com_sppagebuilder' && $id) {
            $bonuses = self::getBonuses($id);
	    file_put_contents('bonuses.log', print_r($bonuses, true), FILE_APPEND);	
            foreach($bonuses as $bonus)
            {
                self::setCookieBonus($bonus);
            }
        }
	}

	private static function getBonuses($id)
    {
        $db = JFactory::getDbo();
        $query = '
                    SELECT * FROM #__page_bonuses WHERE page = '.$id.' AND status = 1
        ';
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    private static function setCookieBonus($bonus)
    {
        $inputCookie  = JFactory::getApplication()->input->cookie;
        if ($bonus->life)
            $inputCookie->set($name = '_bns', $value = base64_encode($bonus->name), $expire = intval(time() + PlLibHelperCalculate::calculate($bonus->life)));
        else
            $inputCookie->set($name = '_bns', $value = base64_encode($bonus->name));
    }

    private static function getBonusText($id)
    {
        $db = JFactory::getDbo();

        $query = "SELECT name FROM #__prizolove_promo_items WHERE `id` = ".intval($id);

        $db->setQuery($query);

        $result =  $db->loadObject();

        if ($result && !empty($result->name)) {
            return $result->name;
        }

        return;
    }

}
