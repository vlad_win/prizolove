<?php
define('_JEXEC', 1);
define('JPATH_BASE', '/var/www/preview/');
define('XML_EXPORT_PATH', '/home/devops/Exchange/out/');
require_once JPATH_BASE . 'includes/defines.php';
require_once JPATH_BASE . 'includes/framework.php';

// Create the Application
$app = JFactory::getApplication('site');

$db = JFactory::getDBO();

if (!file_exists('sms.csv')) die();

$h = fopen('sms.csv', 'r');
$users = [];
while ( ($order = fgetcsv($h, 0, ';')) !== FALSE ) {
	if (!empty($order[0])) {
		$query = "
			SELECT o.`customer_id`
			FROM #__prizolove_orders o
			WHERE o.order_id =".$order[0];

		$db->setQuery($query);
	
		$customer = $db->loadObject();
		
		if ($customer && !in_array($customer->customer_id, $users)) {
			$users[] = $customer->customer_id;
		}
		
	}
}

foreach($users as $userId) {
	$finalQuery = 'INSERT IGNORE INTO `#__acysms_queue` (`queue_message_id`,`queue_receiver_id`,`queue_receiver_table`,`queue_senddate`,`queue_try`,`queue_priority`, `queue_paramqueue`) 
						VALUES ("6","'.$userId.'","acymailing",'.(time()+100).',"1","1","")';
	$db->setQuery($finalQuery);
	$db->query();
}
print_r($users);
die();



$prevDay = new DateTime();
$prevDay->modify('-1 day');
$prevDayFormatted = $prevDay->format('Y-m-d');

$result = [ 'customers' => 0, 'orders' => 0, 'xmlFile' => false ];

$prevDay = $app->input->get('day', $prevDayFormatted);



$query = "
			SELECT c.`customer_name`, c.`customer_email`, c.`customer_phone`, c.`customer_id`
			FROM #__prizolove_orders o 
			LEFT JOIN #__prizolove_customers c ON c.customer_id = o.customer_id
			WHERE o.date_modified >= '".$prevDay." 00:00:00' AND o.date_modified <= '".$prevDay." 23:59:59' AND  o.address_delivery <> ''
			GROUP BY c.`customer_id`
";

$db->setQuery($query);

$customers = $db->loadObjectList();

if (empty($customers)) {
	echo json_encode($result);
	die();	
}

$result['customers'] = count($customers);

$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><data/>');
$xmlCustomers = $xml->addChild("customers");
foreach($customers as $customer) {
	$xmlCustomer = $xmlCustomers->addChild("customer");
	$xmlCustomer->addChild("customer_id", $customer->customer_id);
	$xmlCustomer->addChild("customer_name", $customer->customer_name);
	$xmlCustomer->addChild("customer_email", $customer->customer_email);
	$xmlCustomer->addChild("customer_phone", '+38'.$customer->customer_phone);
}


$query = "
			SELECT o.`order_id`, o.`date`, o.`customer_id`, o.`product_id`, o.`product_name`, o.`address_delivery`
			FROM #__prizolove_orders o 			
			WHERE o.date_modified >= '".$prevDay." 00:00:00' AND o.date_modified <= '".$prevDay." 23:59:59' AND o.address_delivery <> ''
			ORDER BY o.order_id	
";

$db->setQuery($query);


$orders = $db->loadObjectList();

$result['orders'] = count($orders);

$xmlOrders = $xml->addChild("orders");

$regex = '/\*(\d*?)\*/i';


foreach($orders as $order) {
	$xmlOrder = $xmlOrders->addChild("order");
	$xmlOrder->addChild("order_id", $order->order_id);
	$xmlOrder->addChild("date", $order->{'date'});
	$xmlOrder->addChild("customer_id", $order->customer_id);
	$xmlProductsId = $xmlOrder->addChild("products_id");
	$xmlProductsName = $xmlOrder->addChild("products_name");
	
	$productIds = explode(',', $order->product_name);
	
	foreach($productIds as $productId) {
		preg_match($regex, $productId, $match);
		if (!empty($match[1])) $xmlProductsId->addChild("product_id", $match[1]);
		$xmlProductsName->addChild("product_name", $productId);
	}
	$xmlOrder->addChild("address_delivery", $order->address_delivery);
}

//$xml = html_entity_decode($xml, ENT_NOQUOTES, 'UTF-8');

$result['xmlFile'] = $xml->asXML(XML_EXPORT_PATH.'data'.$prevDay.'.xml');

echo json_encode($result);