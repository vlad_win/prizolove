<?php

define('_JEXEC', 1);
define('JPATH_BASE', '/var/www/preview/');
define('XML_EXPORT_PATH', '/home/devops/Exchange/out/');
require_once JPATH_BASE . 'includes/defines.php';
require_once JPATH_BASE . 'includes/framework.php';

// Create the Application
$app = JFactory::getApplication('site');

$db = JFactory::getDBO();

ini_set('soap.wsdl_cache_enabled', 0 );
ini_set('soap.wsdl_cache_ttl', 0);

//http://185.213.209.118/prizolov/ws/joomla.1cws?wsdl

if (!function_exists('is_soap_fault')){
    print 'Не настроен web сервер. Не найден модуль php-soap.';
    return false;
}

$client = new SoapClient('http://185.213.209.118/prizolov/ws/joomla.1cws?wsdl',
    array(
        'soap_version'   => SOAP_1_2,
        'cache_wsdl'     => WSDL_CACHE_NONE, //WSDL_CACHE_MEMORY, //, WSDL_CACHE_NONE, WSDL_CACHE_DISK or WSDL_CACHE_BOTH
        'exceptions'     => true,
        'trace'          => 1
    )
);


$result = $client->SendToJoomla();


if (empty($result->return)) exit;

$xml = simplexml_load_string($result->return, "SimpleXMLElement", LIBXML_NOCDATA);
$json = json_encode($xml);
$array = json_decode($json,TRUE);

if (
    empty($array['code']) ||
    empty($array['name']) ||
    empty($array['kvorest']) ||
    empty($array['price'])
) exit;

if (count($array['code']) == count($array['name']) &&
    count($array['name']) == count($array['kvorest']) &&
    count($array['kvorest']) == count($array['price'])
) {
    foreach($array['code'] as $key=>$value) {

        if (empty($array['name'][$key]) ||
            !isset($array['price'][$key]) ||
            !isset($array['kvorest'][$key])
        ) continue;

        if (is_array($array['price'][$key])) {
            $array['price'][$key] = 0;
        } else {
            $array['price'][$key] = floatval($array['price'][$key]);
        }

        $query = "
			SELECT p.`id`
			FROM #__prizolove_products p
			WHERE p.id_1c =".$value;

        $db->setQuery($query);

        $product = $db->loadObject();

        if ($product) {
            $query = '
                UPDATE `#__prizolove_products`
                SET `name`="'.$db->quote($array['name'][$key]).'", `quantity`='.intval($array['kvorest'][$key]).',  `price` = "'.$array['price'][$key].'", `modified` = "'.date('Y-m-d H:i:s').'"
                WHERE `id` = '.$product->id;
            $db->setQuery($query);
            $db->query();

        } else {

            $query = '
                        INSERT INTO `#__prizolove_products` 
                          (
                            `name`,
                            `price`,
                            `id_1c`,
                            `quantity`,
                            `status`,
                            `created`,
                            `modified`                            
                          ) 
						VALUES (
						  "'.$db->quote($array['name'][$key]).'",
						  "'.$array['price'][$key].'",
						  "'.$value.'",
						  '.intval($array['kvorest'][$key]).',
						  1,
						  "'.date('Y-m-d H:i:s').'",
						  "'.date('Y-m-d H:i:s').'"						  
						  )
            ';
            $db->setQuery($query);
            $db->query();
        }
    }
}


