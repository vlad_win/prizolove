<?php
file_put_contents('statuses.log', print_r($_REQUEST, true), FILE_APPEND);
if (empty(intval($_REQUEST['orderId']))) exit(json_encode(array('status'=>2)));

if (empty($_REQUEST['dealStatus'])) exit(json_encode(array('status'=>2)));

define('_JEXEC', 1);
define('JPATH_BASE', '/var/www/preview/');

require_once JPATH_BASE . 'includes/defines.php';
require_once JPATH_BASE . 'includes/framework.php';

// Create the Application
$app = JFactory::getApplication('site');

$jinput = JFactory::getApplication()->input;

$db = JFactory::getDBO();

switch($_REQUEST['dealStatus']) {
	case "К ОТПРАВКЕ":
		$status = 7;
		break;
	case "ЗАБРАЛ ТОВАР":
		$status = 9;
		break;
	case "ВОЗВРАТ ЗАКАЗА":
		$status = 8;
		break;
	
}

$db->getQuery(true);

$query = 'UPDATE #__prizolove_orders SET `sent` = '.$db->quote($status).' WHERE `order_id` ='.intval($_REQUEST['orderId']);

$db->setQuery($query);
$db->execute();

$dispatcher = JEventDispatcher::getInstance();
$dispatcher->trigger('onOrderStatusChange', array(intval($_REQUEST['orderId']), $status));

echo json_encode(array('status'=>1));
exit();


