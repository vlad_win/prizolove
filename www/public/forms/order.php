<?php

define('_JEXEC', 1);
define('JPATH_BASE', '/var/www/preview/');

require_once JPATH_BASE . 'includes/defines.php';
require_once JPATH_BASE . 'includes/framework.php';

// Create the Application
$app = JFactory::getApplication('site');

$jinput = JFactory::getApplication()->input;

$db = JFactory::getDBO();


$name = $jinput->post->get('name', false, 'STRING');
$email = $jinput->post->get('email', false, 'STRING');
$phone = $jinput->post->get('phone', false, 'STRING');
$productName = $jinput->post->get('product', false, 'STRING');
$url = $jinput->post->get('url', false, 'STRING');



if (!$name || !$email || !$phone || !$productName || !$url) {
    echo json_encode(
        array(
            'error' => 'Not all required fields'
        )
    );
    exit();
}


$response = array(
    'subid' => '',
    'customer_id' => '',
    'order_id' => '',
    'mail' => 0
);

if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
    echo 'This code can not work without the AcyMailing Component';
    return false;
}

$listClass = acymailing_get('class.list');
$userClass = acymailing_get('class.subscriber');
$mailer = acymailing_get('helper.mailer');
$mailer->report = true;
$mailer->trackEmail = true;
$mailer->autoAddUser = false;



$subid = $userClass->subid($email);

if (!$subid) {
    $myUser = new stdClass();

    $myUser->email = $email;
    $myUser->phone = $phone;

    $myUser->name = $name; //this information is optional

    $subscriberClass = acymailing_get('class.subscriber');

    $subid = $subscriberClass->save($myUser); //this function will return you the ID of the user inserted in the AcyMailing table

    if ($subid) $response['mail'] = intval($mailer->sendOne(8, $email));
}

$response['subid'] = $subid;

$newSubscription = array();
$newList = array();

$newList['status'] = 1;
$newSubscription[2] = $newList;
$newSubscription[16] = $newList;


$userClass->saveSubscription($subid,$newSubscription);



$db = JFactory::getDbo();
$query = $db->getQuery(true);

$columns = array('customer_id', 'customer_name', 'customer_phone', 'customer_email');

$values = array(
    $subid,
    $db->quote($name),
    $db->quote($phone),
    $db->quote($email)
);

$query
    ->insert($db->quoteName('#__prizolove_customers'))
    ->columns($db->quoteName($columns))
    ->values(implode(',', $values));

$db->setQuery($query);
$db->execute();

$response['customer_id'] = $db->insertid();

$query = $db->getQuery(true);


$columns = array(
    'date',
    'customer_id',
    'product_id',
    'product_name',
    'address_delivery',
    'click_id',
    'order_url',
    'customer_comment'
);

$values = array(
    $db->quote(date('Y-m-d H:i:s')),
    $subid,
    $db->quote(''),
    $db->quote($productName),
    $db->quote(''),
    $db->quote(''),
    $db->quote($url),
    $db->quote(''),
);

$query
    ->insert($db->quoteName('#__prizolove_orders'))
    ->columns($db->quoteName($columns))
    ->values(implode(',', $values));

$db->setQuery($query);
$db->execute();

$response['order_id'] = $db->insertid();

/*** Send Order to CRM **/

$url = urldecode($url);
$uriStr = explode('?', $url);

$utmTags = array(
	'utm_source' => '',
	'utm_medium' => '',
	'utm_campaign' => '',
	'utm_term' => '',
	'utm_content' => '',
);

if (!empty($uriStr[1])) {	
	$tags = explode('&', $uriStr[1]);   
	foreach($tags as $tag) {
		if (strpos($tag,'utm') !== false) {
			$utm = explode('=', $tag);
			if (count($utm) != 2) continue;
			$utmTags[$utm[0]] = $utm[1];
		}
	}
}

$order = new \StdClass();

$order->id = (string) $response['order_id'];
$order->name = $name;
$order->phone = (string) $phone;
$order->email = $email;
$order->url = (string) $url;
$order->orderId = (string) $response['order_id'];
$order->paymentType = 'Наложенный платеж';
$order->login = '';
$order->password = '';

$uri = parse_url($url, PHP_URL_PATH);
if ($uri[0] == '/') $uri = substr($uri, 1);

$order->landId = $uri;

$order->utm = new \StdClass();
$order->utm = $utmTags;

$regex = '/\*(\d*?)\*/i';

$productIds = array();
$productNames = array();

preg_match($regex, $productName, $match);

if (!empty($match[1])) {
	$productId = $match[1];
} else {
	return;
}

$query = $db->getQuery(true);

$query->select('* FROM #__prizolove_products p')
	->where('p.id_1c = '.$db->quote($productId));

$db->setQuery($query);

$product = $db->loadObject();

$items = array();

$newProduct = new \StdClass();
$newProduct->id = $productId;
$newProduct->name = $productName;
$newProduct->price = $product->price;
$newProduct->status = ($product->quantity) ? 1 : 0;
$newProduct->quantity = 1;
$newProduct->store = $product->quantity;
$items[] = $newProduct;

$order->items = new \StdClass();

$order->items = $items;

$dataString = json_encode($order);

$ch = curl_init('http://adm.insideiot.tech/web/widgets/bitrex/forest/handler.php');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($dataString))
);

$result = curl_exec($ch);

$log = date('Y-m-d H:i:s').' "'.json_encode($order, JSON_UNESCAPED_UNICODE).'" Result:"'.$result.'""';

$fp = fopen(JPATH_ROOT.'/logs/orders.log', 'a+');

fwrite($fp, $log."\n");


echo json_encode($response);
exit();