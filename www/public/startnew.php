<?php
$_SERVER['HTTP_HOST']  = 'prizolove.com`';

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
 
if (file_exists(dirname(__FILE__) . '/defines.php')) {
 include_once dirname(__FILE__) . '/defines.php';
}
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', dirname(__FILE__));
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');

JLoader::import('pl_lib.library');
//JPluginHelper::importPlugin( 'system' );

$steps = [	
	"2" => [
		'leads' => 0,
		'sales' => 0,		
		'leadsplus' => 1,
		'salesplus' => 0,
		'salesdif' => 0,		
		'datediff' => 24*60*60,		
		'messages' => []
	],
	"3" => [
		'leads' => 1,
		'sales' => 0,		
		'leadsplus' => 4,
		'salesplus' => 0,
		'salesdif' => 0,		
		'datediff' => 24*60*60,		
		'messages' => []
	],
	"4" => [
		'leads' => 5,
		'sales' => 0,		
		'leadsplus' => 5,
		'salesplus' => 0,
		'salesdif' => 0,		
		'datediff' => 24*60*60,		
		'messages' => []
	],
	"5" => [
		'leads' => 10,
		'sales' => 0,		
		'leadsplus' => 0,
		'salesplus' => 1,
		'salesdif' => 0,		
		'datediff' => 24*60*60,		
		'messages' => []
	],
	"6" => [
		'leads' => 10,
		'sales' => 1,		
		'leadsplus' => 1,
		'salesplus' => 0,
		'salesdif' => 1,		
		'datediff' => 24*60*60,		
		'messages' => []
	],
	"9" => [
		'leads' => 11,
		'sales' => 1,		
		'leadsplus' => 0,
		'salesplus' => 1,
		'salesdif' => 0,		
		'datediff' => 1*24*60*60,		
		'messages' => []
	],
	"11" => [
		'leads' => 11,
		'sales' => 2,		
		'leadsplus' => 0,
		'salesplus' => 1,
		'salesdif' => 0,		
		'datediff' => 1*24*60*60,		
		'messages' => []
	],
	
	
];


if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
 echo 'This code can not work without the AcyMailing Component';
 return false;
}
$mailer = acymailing_get('helper.mailer');
$mailer->report = true; 
$mailer->trackEmail = true;

$db = JFactory::getDBO();

$sql = '
		SELECT a.id, a.user_id, l.leads, s.sales, l.datetime lead_date, s.date_created,u.email, s.subid
		FROM #__affiliate_tracker_accounts a		
		INNER JOIN #__users u ON a.user_id = u.id
		INNER JOIN #__acymailing_subscriber s ON s.email = u.email
		LEFT JOIN (
			SELECT COUNT(logs.id) leads, max(logs.datetime) datetime, ac0.id
			FROM #__affiliate_tracker_accounts ac0 
			LEFT JOIN #__affiliate_tracker_logs logs ON (logs.account_id = ac0.id AND logs.user_id > 0 AND logs.user_id != ac0.user_id AND logs.sessionid = "") 						
			GROUP BY ac0.id
			ORDER BY logs.datetime DESC
		) l ON l.id = a.id
		LEFT JOIN (
			SELECT COUNT(sales.id) sales, max(sales.date_created) date_created, ac1.id
			FROM #__affiliate_tracker_accounts ac1 
			LEFT JOIN #__affiliate_tracker_conversions sales ON (sales.atid = ac1.id AND sales.component = "com_virtuemart")
			GROUP BY ac1.id
			ORDER BY sales.date_created DESC
		) s ON s.id = a.id
		WHERE a.account_type IN ("start")
		GROUP BY a.id
';
		
/* $sql = 'SELECT COUNT(l.id) leads
		FROM #__affiliate_tracker_accounts a
		LEFT JOIN #__affiliate_tracker_logs l ON l.account_id = a.id
		WHERE a.account_type = "proba_free"
		HAVING COUNT(l.id) = 0
		';		 */

$db->setQuery($sql);		
$partners = $db->loadObjectList();
//print_r($partners);


foreach($partners as $partner) {
	
	$userStep = 0;	
	
	foreach($steps as $key=>$step) {
		
		if ($userStep !== 0) continue;
		
		
		if (!empty($step['salesdif'])) 	
			$dateCompare = $partner->date_created;					
		else
			$dateCompare = $partner->lead_date;			
			
		/* echo date('Y-m-d H:i:s',(time() - $step['datediff'])).' '.$dateCompare.' '.$step['leads'].'-'.$step['sales'].'<br />';
		if ($partner->leads == $step['leads']
			&& $partner->sales == $step['sales']) {
			if (date('Y-m-d H:i:s',(time() - $step['datediff'])) >= $dateCompare) {				
				echo $key.' '.$dateCompare.' '.date('Y-m-d H:i:s',(time() - $step['datediff']));
				print_r($partner);	
				print_r($step);
			}
			//echo $key.$dateCompare;
			//echo date('Y-m-d H:i:s',(time() - $step['datediff'])) >= $dateCompare;
			//print_r($partner);	
			//echo $dateCompare;
			//
		} */
		
					
		
		if ($partner->leads == $step['leads'] 
			&& $partner->sales == $step['sales']
			&& date('Y-m-d H:i:s',(time() - $step['datediff'])) >= $dateCompare			
		) {
			
			switch($key) {
				case "2":				
					PlLibHelperDemo::getInstance()->addLead($partner->id);											
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,321);				
					$userStep = $key;
					file_put_contents('start34763.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "3":				
					for($i=0; $i<$step['leadsplus'];$i++) {
						PlLibHelperDemo::getInstance()->addLead($partner->id);											
					}
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,322);				
					$userStep = $key;
					file_put_contents('start34763.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "4":				
					for($i=0; $i<$step['leadsplus'];$i++) {
						PlLibHelperDemo::getInstance()->addLead($partner->id);				
					}					
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,323);				
					$userStep = $key;
					file_put_contents('start34763.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "5":									
					PlLibHelperDemo::getInstance()->addLevelOneSale($partner->id);
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,325);				
					$userStep = $key;
					file_put_contents('start34763.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "6":				
					for($i=0; $i<$step['leadsplus'];$i++) {
						PlLibHelperDemo::getInstance()->addLead($partner->id);				
					}																
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,324);				
					$userStep = $key;
					file_put_contents('start34763.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "9":				
					PlLibHelperDemo::getInstance()->addLevelOneSale($partner->id);											
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,326);				
					$userStep = $key;
					file_put_contents('start34763.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "11":				
					PlLibHelperDemo::getInstance()->addLevelOneSale($partner->id);											
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,316);				
					$userStep = $key;
					file_put_contents('start34763.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;	
				default:
					
					//print_r($partner);
					break;
			}
		}
	}
}
		



