<?php
$_SERVER['HTTP_HOST']  = 'prizolove.com`';

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
 
if (file_exists(dirname(__FILE__) . '/defines.php')) {
 include_once dirname(__FILE__) . '/defines.php';
}
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', dirname(__FILE__));
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');

JLoader::import('pl_lib.library');

$db = JFactory::getDBO();

		
$sql = '	
		SELECT ls.subid, st.step, st.modified, s.userid 
		FROM #__acymailing_listsub ls
		INNER JOIN #__acymailing_subscriber s ON ls.subid = s.subid
		LEFT JOIN #__pl_user_steps st ON s.userid = st.user_id
		WHERE listid = 46 AND status = 1
		';		
		
$db->setQuery($sql);

$users = $db->loadObjectList();

$prevDay = new DateTime();
$prevDay->modify("-1 day");
foreach($users as $user) {
	if (!$user->userid) continue;
	if ($user->modified && $user->modified > $prevDay->format('Y-m-d H:i:s')) continue;
	if (!$user->step) {
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 10, 1);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 356);
				$step = new StdClass();
				$step->user_id = $user->userid;
				$step->step = 1;
				$step->modified = date('Y-m-d H:i:s');
				$db->insertObject('#__pl_user_steps',$step);
	}
	switch($user->step) {
		case 1:				
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 20, 1);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 356);
				$db->setQuery('UPDATE #__pl_user_steps SET step = 2, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 2:
				PlLibHelperDemo::getInstance()->addCloneSale($user->userid, 4, 1);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 353);				
				$db->setQuery('UPDATE #__pl_user_steps SET step = 3, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 3:
				PlLibHelperDemo::getInstance()->addCloneSale($user->userid, 4, 1);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 353);				
				$db->setQuery('UPDATE #__pl_user_steps SET step = 4, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 4:
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 20, 1);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 356);
				$db->setQuery('UPDATE #__pl_user_steps SET step = 5, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 5:
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 20, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 355);
				$db->query();
				PlLibHelperDemo::getInstance()->addCloneSale($user->userid, 5, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 354);				
				$db->setQuery('UPDATE #__pl_user_steps SET step = 6, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 6:
				PlLibHelperDemo::getInstance()->addCloneSale($user->userid, 5, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 354);				
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 20, 1);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 356);
				$db->setQuery('UPDATE #__pl_user_steps SET step = 7, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 7:
				PlLibHelperDemo::getInstance()->addCloneSale($user->userid, 5, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 354);				
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 20, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 355);
				$db->setQuery('UPDATE #__pl_user_steps SET step = 8, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 8:
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 20, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 355);
				$db->setQuery('UPDATE #__pl_user_steps SET step = 9, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 9:
				PlLibHelperDemo::getInstance()->addCloneSale($user->userid, 5, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 354);				
				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 60, 2);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 355);
				$db->setQuery('UPDATE #__pl_user_steps SET step = 10, modified = '.$db->Quote(date('Y-m-d H:i:s')).' WHERE user_id = '.$user->userid);
				$db->query();
			break;
		case 10:
		case 11:
		case 15:
			break;
		default:
/*				PlLibHelperDemo::getInstance()->addCloneLeads($user->userid, 10, 1);
				PlLibHelperDemo::getInstance()->sendEmailNotification($user->userid, 356);
				$step = new StdClass();
				$step->user_id = $user->userid;
				$step->step = 1;
				$step->modified = date('Y-m-d H:i:s');
				$db->insertObject('#__pl_user_steps',$step); */
			break; 
	}

}

		