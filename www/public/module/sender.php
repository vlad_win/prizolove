<?php

if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['product']) || empty($_POST['phone'])) exit();

$post = [
    'name' => trim(urldecode(htmlspecialchars($_POST['name']))),
    'email' => trim(urldecode(htmlspecialchars($_POST['email']))),
    'phone'   => trim(urldecode(htmlspecialchars($_POST['phone']))),
    'product'   => trim(urldecode(htmlspecialchars($_POST['product']))),
    'url'   => $_SERVER['HTTP_REFERER']    
];

if (!empty($_POST['param'])) $post['product'] .= ' '.trim(urldecode(htmlspecialchars($_POST['param'])));

if (!empty($_COOKIE['orderUtms'])) {
	$ref = $post['url'];
	$utmTags = json_decode($_COOKIE['orderUtms']);
	if (strpos($ref, 'utm_') === false) {
			foreach($utmTags as $key=>$tag) {
			if (empty($tag)) continue;

			if (strpos($ref, '?' )) {
				$ref .= '&'.$key.'='.$tag;
			} else {
				$ref .= '?'.$key.'='.$tag;
			}
		}
	}
	$post['url'] = $ref;
}

if (!empty($_COOKIE['_bns'])) {
	$bns = base64_decode($_COOKIE['_bns']);

	if ($bns) {
		$post['product'] .= ' '.$bns;
	}

}
	
$ch = curl_init('https://prizolove.com/forms/order.php');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

$response = curl_exec($ch);

curl_close($ch);
echo $response;
exit();

//if (!empty($_SERVER['HTTP_REFERER'])) header('Location: '.$_SERVER['HTTP_REFERER']);
//exit();
?>