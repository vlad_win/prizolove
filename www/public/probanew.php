<?php
date_default_timezone_set ( 'Europe/Kiev' );
$_SERVER['HTTP_HOST']  = 'prizolove.com';

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
 
if (file_exists(dirname(__FILE__) . '/defines.php')) {
 include_once dirname(__FILE__) . '/defines.php';
}
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', dirname(__FILE__));
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');

JLoader::import('pl_lib.library');
//JPluginHelper::importPlugin( 'system' );

if(!include_once(rtrim(JPATH_ADMINISTRATOR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acymailing'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php')){
 echo 'This code can not work without the AcyMailing Component';
 return false;
}
$db = JFactory::getDBO();

$steps = [	
	"2" => [
		'leads' => 0,
		'sales' => 0,
		'leads2'=> 0,
		'sales2' => 0,
		'leadsplus' => 1,
		'salesplus' => 0,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"3" => [
		'leads' => 1,
		'sales' => 0,
		'leads2'=> 0,
		'sales2' => 0,
		'leadsplus' => 2,
		'salesplus' => 0,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"4" => [
		'leads' => 3,
		'sales' => 0,
		'leads2'=> 0,
		'sales2' => 0,
		'leadsplus' => 0,
		'salesplus' => 1,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"6" => [
		'leads' => 3,
		'sales' => 1,
		'leads2'=> 0,
		'sales2' => 0,
		'leadsplus' => 1,
		'salesplus' => 0,
		'salesdif' => 1,
		'sales2dif' => 0,
		'datediff' => 24*60*60*2,
		'datediffsign' => '>',
		'messages' => []
	],
	"7" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 0,
		'sales2' => 0,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 1,
		'sales2plus' => 0,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"8" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 1,
		'sales2' => 0,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 3,
		'sales2plus' => 0,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"9" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 4,
		'sales2' => 0,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 1,
		'sales2plus' => 1,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"10" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 5,
		'sales2' => 1,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 1,
		'sales2plus' => 0,
		'salesdif' => 0,
		'sales2dif' => 1,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"11" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 6,
		'sales2' => 1,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 3,
		'sales2plus' => 0,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	],
	"12" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 9,
		'sales2' => 1,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 1,
		'sales2plus' => 0,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => [313]
	],
	"13" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 10,
		'sales2' => 1,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 0,
		'sales2plus' => 1,
		'salesdif' => 0,
		'sales2dif' => 0,
		'datediff' => 24*60*60,
		'datediffsign' => '>',
		'messages' => []
	], 
/*	"14" => [
		'leads' => 4,
		'sales' => 1,
		'leads2'=> 10,
		'sales2' => 2,
		'leadsplus' => 0,
		'salesplus' => 0,
		'leads2plus' => 0,
		'sales2plus' => 0,
		'salesdif' => 0,
		'sales2dif' => 1,
		'datediff' => 12*60*60,
		'datediffsign' => '>',
		'messages' => []
	],  */
	
	
];

$sql = '
		SELECT a.id, a.user_id, l.leads, s.sales, l.datetime lead_date, s.date_created sale_date, l2.leads2, s2.sales2, s2.date2_created, l2.lead2_date, u.email
		FROM #__affiliate_tracker_accounts a
		INNER JOIN #__users u ON a.user_id = u.id
		LEFT JOIN (
			SELECT COUNT(logs.id) leads, max(logs.datetime) datetime, ac0.id
			FROM #__affiliate_tracker_accounts ac0 
			LEFT JOIN #__affiliate_tracker_logs logs ON (logs.account_id = ac0.id AND logs.user_id > 0 AND logs.user_id != ac0.user_id AND logs.sessionid = "") 						
			GROUP BY ac0.id
			ORDER BY logs.datetime DESC
		) l ON l.id = a.id		
		LEFT JOIN (
			SELECT COUNT(sales.id) sales, max(sales.date_created) date_created, ac1.id
			FROM #__affiliate_tracker_accounts ac1 
			LEFT JOIN #__affiliate_tracker_conversions sales ON (sales.atid = ac1.id AND sales.component = "com_virtuemart")
			GROUP BY ac1.id
			ORDER BY sales.date_created DESC
		) s ON s.id = a.id
		LEFT JOIN (			
			SELECT COUNT(logs1.id) leads2, max(logs1.datetime) lead2_date, ac1.parent_id
			FROM #__affiliate_tracker_accounts ac1			
			LEFT JOIN #__affiliate_tracker_logs logs1 ON (logs1.account_id = ac1.id AND logs1.user_id > 0 AND logs1.user_id != ac1.user_id)			
			GROUP BY ac1.parent_id
			ORDER BY logs1.datetime DESC
		) l2 ON l2.parent_id = a.id
		LEFT JOIN (			
			SELECT COUNT(sales1.id) sales2, max(sales1.date_created) date2_created, ac2.parent_id
			FROM #__affiliate_tracker_accounts ac2
			LEFT JOIN #__affiliate_tracker_conversions sales1 ON (sales1.atid = ac2.id AND sales1.component = "com_virtuemart")
			GROUP BY ac2.parent_id
			ORDER BY sales1.date_created DESC
		) s2 ON s2.parent_id = a.id		
				 
		WHERE a.account_type IN ("proba", "proba_free") 
		GROUP BY a.id
';

$db->setQuery($sql);		
$partners = $db->loadObjectList();
//print_r($partners);

foreach($partners as $partner) {
	$userStep = 0;	
	//echo $partner->id.' '.$partner->email.' L:'.$partner->leads.' S: '.$partner->sales.' L2:'.$partner->leads2.' S2: '.$partner->sales2;
	foreach($steps as $key=>$step) {
		
		if ($userStep !== 0) continue;
		
		
		if (!empty($step['salesdif'])) 
			$dateCompare = $partner->lead_date;
		else 
			$dateCompare = $partner->date_created;			
		
		if (empty($step['sales2dif'])) 
			$date2Compare = $partner->lead2_date;
		else 
			$date2Compare = $partner->date2_created;			
		
					
		
		if ($partner->leads == $step['leads'] 
			&& $partner->sales == $step['sales']
			&& date('Y-m-d H:i:s',(time() - $step['datediff'])) >= $dateCompare
			&& empty($partner->leads2) && empty($partner->sales2)			
		) {
			
			switch($key) {
				case "2":
						for($i=0; $i<$step['leadsplus'];$i++) {
							PlLibHelperDemo::getInstance()->addLead($partner->id);							
						}
						PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,316);				
						$userStep = $key;
						file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
						break;
				case "3":
						for($i=0; $i<$step['leadsplus'];$i++) {
							PlLibHelperDemo::getInstance()->addLead($partner->id);				
						}
						PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,311);			
						
						$userStep = $key;
						file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "4":			
						PlLibHelperDemo::getInstance()->addLevelOneSale($partner->id);
						PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,320);		
						$userStep = $key;						
						file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "6":
						for($i=0; $i<$step['leadsplus'];$i++) {
							PlLibHelperDemo::getInstance()->addLead($partner->id);							
						}
						PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,317);
						$userStep = $key;
						file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
			}
			
			
			//echo ' step'.$key;
			
		} else 
		if ($partner->leads >= 4 
			&& $partner->sales >= 1			
			&& empty($partner->leads2)
			&& empty($partner->sales2)				
		) {	
			$userStep = $key;		
			PlLibHelperDemo::getInstance()->addLevelTwoLead($partner->user_id,315,1);			
			file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step=7'."\r\n", FILE_APPEND);			
			$userStep = $key;					
		}
		else 
		if ($partner->leads >= $step['leads'] 
			&& $partner->sales >= $step['sales']			
			&& $partner->leads2 == $step['leads2']
			&& $partner->sales2 == $step['sales2']
			&& date('Y-m-d H:i:s',(time() - $step['datediff'])) >= $date2Compare
		) {	
			$userStep = $key;		
			switch($key) {
				
				case "8":	
					PlLibHelperDemo::getInstance()->addLevelTwoLead($partner->user_id,318,3);
					$userStep = $key;					
					file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "9": 					
					PlLibHelperDemo::getInstance()->addLevelTwoLead($partner->user_id,319,1);
					PlLibHelperDemo::getInstance()->addLevelTwoSale($partner->user_id,312,1);			
					$userStep = $key;					
					file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "10": 					
					PlLibHelperDemo::getInstance()->addLevelTwoLead($partner->user_id,328,1);
					$userStep = $key;					
					file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "11":
					PlLibHelperDemo::getInstance()->addLevelTwoLead($partner->user_id,329,3);
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,313);
					PlLibHelperDemo::getInstance()->setExpireDate($partner->user_id,2);
					$userStep = $key;					
					file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;
				case "12":
					PlLibHelperDemo::getInstance()->addLevelTwoLead($partner->user_id,330,1);
					PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,314);
					$userStep = $key;					
					file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step='.$key."\r\n", FILE_APPEND);
					break;					
			}
					
			
			//echo ' step'.$key;
		} else	if ($partner->leads >= $step['leads'] 
			&& $partner->sales >= $step['sales']			
			&& $partner->leads2 >= 10
			&& $partner->sales2 == 1
			&& date('Y-m-d H:i:s',(time() - $step['datediff'])) >= $date2Compare
		) {
			PlLibHelperDemo::getInstance()->addLevelTwoSale($partner->user_id,334,1);			
			$userStep = 13;					
			file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step=13'."\r\n", FILE_APPEND);
			break;		
		}		
/*		 else	if ($partner->leads >= $step['leads'] 
			&& $partner->sales >= $step['sales']			
			&& $partner->leads2 >= 10
			&& $partner->sales2 == 2
			&& date('Y-m-d H:i:s',(time() - $step['datediff'])) >= $date2Compare
		) {
			PlLibHelperDemo::getInstance()->sendEmailNotification($partner->user_id,332);
			$userStep = 14;					
			file_put_contents('test999.log', $partner->id.' '.$partner->email.' Step=14'."\r\n", FILE_APPEND);
			break;		
		}		 */
		
	}
	//echo '<br />';
}
