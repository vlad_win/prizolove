<?php

$_SERVER['HTTP_HOST']  = 'prizolove.com';
$_SERVER['SCRIPT_NAME']  = '';
$_SERVER['SCRIPT_FILENAME'] = '';
$_SERVER['PATH_TRANSLATED'] = '';

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
    include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__FILE__));
    require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';

$app = JFactory::getApplication('site');

JLoader::import('pl_lib.library');

$db = JFactory::getDBO();

$query = $db->getQuery(true);

$sql = '
		SELECT s.email, s.name, s.subid 
		FROM #__acymailing_subscriber s		
		WHERE s.userid < 1
';
$db->setQuery($sql);
$userList = $db->loadObjectList();
foreach($userList as $user) {	
	$user->email =  filter_var($user->email, FILTER_SANITIZE_EMAIL);
	if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) continue;
	if (empty(trim($user->name))) continue;
	if ( strpos($user->email, "'") == true ) continue;
	$userId = PlLibHelperUsers::getInstance()->checkJoomlaUser($user);
	if ($userId) {
		$sql = 'UPDATE #__acymailing_subscriber SET userid = '.$userId.' WHERE subid = '.$user->subid;
		$db->setQuery($sql);
		$db->query($sql);
	}
}

die();


$query = "
          SELECT pc.customer_id
          FROM #__prizolove_orders po
          INNER JOIN #__prizolove_customers pc ON po.customer_id = pc.customer_id         
          GROUP BY pc.customer_id          
          ";

$db->setQuery($query);

$userList = $db->loadObjectList();

foreach($userList as $user) {
    $query = "
          SELECT s.subid, s.confirmed_date, s.confirmed, l.listid
          FROM #__acymailing_subscriber s
          LEFT JOIN #__acymailing_listsub l ON (s.subid = l.subid AND l.listid = 36)         
          WHERE s.subid = ".$user->customer_id;

    $db->setQuery($query);

    $subs = $db->loadObject();


    if ($subs && empty($subs->listid) && !empty($subs->confirmed) && !empty($subs->confirmed_date)) {
        $db->setQuery('INSERT INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$db->Quote(36).','.$db->Quote($subs->subid).','.$db->Quote(time()).',1)');

        $db->query();
    }
}

die();
