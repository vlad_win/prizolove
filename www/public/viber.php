<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("messenger/vendor/autoload.php");

use Viber\Bot;
use Viber\Api\Sender;

$apiKey = '49700c2ad327d020-cd1969288364d672-7be95666380a177a';

// так будет выглядеть наш бот (имя и аватар - можно менять)
$botSender = new Sender([
    'name' => 'Whois bot',
    'avatar' => 'https://developers.viber.com/img/favicon.ico',
]);

$url = 'https://chatapi.viber.com/pa/send_message';
$data = array (
    'receiver' => '01234567890A=',
    'min_api_version' => 1,
    'sender' =>
        array (
            'name' => 'John McClane',
            'avatar' => 'http://avatar.example.com',
        ),
    'tracking_data' => 'tracking data',
    'type' => 'text',
    'text' => 'Hello world!',
);

try {
    $bot = new Bot(['token' => $apiKey]);
    $bot
        ->onConversation(function ($event) use ($bot, $botSender) {
            // это событие будет вызвано, как только пользователь перейдет в чат
            // вы можете отправить "привествие", но не можете посылать более сообщений
            return (new \Viber\Api\Message\Text())
                ->setSender($botSender)
                ->setText("Can i help you?");
        })
        ->onText('|whois .*|si', function ($event) use ($bot, $botSender) {
            // это событие будет вызвано если пользователь пошлет сообщение
            // которое совпадет с регулярным выражением
            $bot->getClient()->sendMessage(
                (new \Viber\Api\Message\Text())
                    ->setSender($botSender)
                    ->setReceiver($event->getSender()->getId())
                    ->setText("I do not know )")
            );
        })
        ->run();
} catch (Exception $e) {
    // todo - log exceptions
}
echo 1;