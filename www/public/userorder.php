<?php
date_default_timezone_set ( 'Europe/Kiev' );
$_SERVER['HTTP_HOST']  = 'prizolove.com';

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
 
if (file_exists(dirname(__FILE__) . '/defines.php')) {
 include_once dirname(__FILE__) . '/defines.php';
}
 
if (!defined('_JDEFINES')) {
 define('JPATH_BASE', dirname(__FILE__));
 require_once JPATH_BASE.'/includes/defines.php';
}
 
require_once JPATH_BASE.'/includes/framework.php';
$app = JFactory::getApplication('site');

JLoader::import('pl_lib.library');

$db = JFactory::getDBO();

$sql = '
			SELECT c.customer_email, c.customer_id, b.customer_id business_id, o.order_id, p.type, s.confirmed
			FROM #__prizolove_orders o
			LEFT JOIN #__prizolove_customers c ON c.customer_id = o.customer_id
			LEFT JOIN #__acymailing_subscriber s ON s.subid = c.customer_id
			LEFT JOIN #__prizolove_order_items oi ON o.order_id = oi.order_id
			LEFT JOIN #__prizolove_products p ON p.id = oi.product_id
			LEFT JOIN (
				SELECT o1.customer_id
				FROM #__prizolove_orders o1
				WHERE o1.sent = 5
			) b ON b.customer_id = o.customer_id 
			WHERE o.sent != 5 AND p.type = "business" AND b.customer_id IS NULL
			GROUP BY c.customer_id
';

$db->setQuery($sql);
$users = $db->loadObjectList();

foreach($users as $user) {
	$status = ($user->confirmed)?1:2;
	$db->setQuery('INSERT IGNORE INTO #__acymailing_listsub (`listid`, `subid`, `subdate`, `status`) VALUES ('.$db->Quote(65).','.$db->Quote($user->customer_id).','.$db->Quote(time()).','.$status.')');
	$db->query();
}
 
